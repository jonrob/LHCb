/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// local
#include "RichFutureDAQ/RichPDMDBDecodeMapping.h"

// RICH
#include "RichUtils/RichException.h"
#include "RichUtils/ToArray.h"
#include "RichUtils/ZipRange.h"

// Gaudi
#include "Gaudi/Algorithm.h"

// Messaging
#include "RichFutureUtils/RichMessaging.h"
#define debug( ... )                                                                                                   \
  if ( messenger() ) { ri_debug( __VA_ARGS__ ); }
#define verbo( ... )                                                                                                   \
  if ( messenger() ) { ri_verbo( __VA_ARGS__ ); }

// STL
#include <algorithm>
#include <array>
#include <cstdint>
#include <string>
#include <vector>

using namespace Rich::Future::DAQ;
using namespace Rich::DAQ;
using namespace Rich::Detector;

void PDMDBDecodeMapping::checkVersion( const DecodingConds& C ) {

  // check status is (still) OK before continuing
  if ( !isInitialised() ) { return; }

  auto getVersion = []( const auto& cond ) {
    // Access version, with default value 0 if missing
    return condition_param<int>( cond, "MappingVersion", 0 );
  };

  // extract version from all conditions
  const auto condVers = std::array{getVersion( C.rTypeConds[Rich::Rich1] ), //
                                   getVersion( C.rTypeConds[Rich::Rich2] ), //
                                   getVersion( C.hTypeCond )};

  // Should all be the same
  if ( !condVers.empty() && std::all_of( condVers.begin(), condVers.end(),
                                         [first = condVers.front()]( const auto& e ) { return e == first; } ) ) {
    m_mappingVer = condVers.front();
  } else {
    setIsInitialised( false );
    throw Rich::Exception( "Inconsistent mapping versions" );
  }
  debug( " -> Mapping Version ", m_mappingVer, endmsg );
}

void PDMDBDecodeMapping::fillRType( const DecodingConds& C ) {

  // check status is (still) OK before continuing
  if ( !isInitialised() ) { return; }

  // Loop over RICHes
  for ( const auto rich : Rich::detectors() ) {

    // load the condition
    const auto cond = C.rTypeConds[rich];

    // load the active PDBDBs
    const auto activePDMDBs = condition_param<std::vector<int>>( cond, "ActivePDMDBs" );
    debug( " -> Found ", activePDMDBs.size(), " active R-type PDMDBs for ", rich, endmsg );

    // Loop over active PDMDBs
    for ( const auto pdmdb : activePDMDBs ) {
      const auto pdmdbS = "PDMDB" + std::to_string( pdmdb ) + "R";

      // load the active Frames
      const auto activeFrames = condition_param<std::vector<int>>( cond, pdmdbS + "_ActiveFrames" );
      verbo( "  -> ", pdmdbS, " active frames ", activeFrames, endmsg );

      // loop over active frames
      for ( const auto frame : activeFrames ) {
        const auto frameS = "Frame" + std::to_string( frame );

        // load ECs, PMTs and Anodes data
        const std::string pA = pdmdbS + "_" + frameS;
        const auto        ECs =
            Rich::toArray<ElementaryCell::Type, BitsPerFrame>( condition_param<std::vector<int>>( cond, pA + "_ECs" ) );
        const auto PMTs =
            Rich::toArray<PMTInEC::Type, BitsPerFrame>( condition_param<std::vector<int>>( cond, pA + "_PMTs" ) );
        const auto Anodes =
            Rich::toArray<AnodeIndex::Type, BitsPerFrame>( condition_param<std::vector<int>>( cond, pA + "_Anodes" ) );

        verbo( "   -> ", frameS, endmsg );

        // fill the data cache
        auto& richD  = m_pdmDataR.at( rich );
        auto& pdmdbD = richD.at( pdmdb );
        auto& frameD = pdmdbD.at( frame );

        // loop over bit data and fill
        std::uint16_t bit{0};
        for ( const auto&& [ec, pmt, anode] : Ranges::ConstZip( ECs, PMTs, Anodes ) ) {
          auto& d = frameD.at( bit );
          assert( !d.isValid() ); // should not be initialised yet
          d = BitData( ElementaryCell( ec ), PMTInEC( pmt ), AnodeIndex( anode ) );
          verbo( "    -> Bit ", bit, d, endmsg );
          ++bit;
        } // bit loop

      } // frame loop
    }   // PDMDB loop

  } // RICH loop
}

void PDMDBDecodeMapping::fillHType( const DecodingConds& C ) {

  // check status is (still) OK before continuing
  if ( !isInitialised() ) { return; }

  // load condition
  const auto cond = C.hTypeCond;

  // load the active PDBDBs
  const auto activePDMDBs = condition_param<std::vector<int>>( cond, "ActivePDMDBs" );
  debug( " -> Found ", activePDMDBs.size(), " active H-type PDMDBs", endmsg );

  // Loop over active PDMDBs
  for ( const auto pdmdb : activePDMDBs ) {
    const auto pdmdbS = "PDMDB" + std::to_string( pdmdb ) + "H";

    // load the active Frames
    const auto activeFrames = condition_param<std::vector<int>>( cond, pdmdbS + "_ActiveFrames" );
    verbo( "  -> ", pdmdbS, " active frames ", activeFrames, endmsg );

    // loop over active frames
    for ( const auto frame : activeFrames ) {
      const auto frameS = "Frame" + std::to_string( frame );

      // load ECs, PMTs and Anodes data
      const std::string pA = pdmdbS + "_" + frameS;
      const auto        ECs =
          Rich::toArray<ElementaryCell::Type, BitsPerFrame>( condition_param<std::vector<int>>( cond, pA + "_ECs" ) );
      const auto PMTs =
          Rich::toArray<PMTInEC::Type, BitsPerFrame>( condition_param<std::vector<int>>( cond, pA + "_PMTs" ) );
      const auto Anodes =
          Rich::toArray<AnodeIndex::Type, BitsPerFrame>( condition_param<std::vector<int>>( cond, pA + "_Anodes" ) );

      verbo( "   -> ", frameS, endmsg );

      // fill the data cache
      auto& pdmdbD = m_pdmDataH.at( pdmdb );
      auto& frameD = pdmdbD.at( frame );

      // loop over bit data and fill
      std::uint16_t bit{0};
      for ( const auto&& [ec, pmt, anode] : Ranges::ConstZip( ECs, PMTs, Anodes ) ) {
        auto& d = frameD.at( bit );
        assert( !d.isValid() ); // should not be initialised yet
        d = BitData( ElementaryCell( ec ), PMTInEC( pmt ), AnodeIndex( anode ) );
        verbo( "    -> Bit ", bit, d, endmsg );
        ++bit;
      } // bit loop
    }
  }
}
