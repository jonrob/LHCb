/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

#ifdef USE_DD4HEP
#  include "Detector/Rich/DeRichMirror.h"
#else
#  include "RichDet/DeRichSphMirror.h"
#endif

#include "RichDet/Rich1DTabProperty.h"
#include "RichDetectors/Utilities.h"

#include "LHCbMath/SIMDTypes.h"

#include <GaudiKernel/Plane3DTypes.h>

#include <cassert>
#include <ostream>

namespace Rich::Detector {

  //-----------------------------------------------------------------------------
  /** @class Mirror RichMirror.h
   *
   *  Rich Spherical Mirror helper class
   *
   *  @author Chris Jones
   *  @date   2020-10-05
   */
  //-----------------------------------------------------------------------------

  class Mirror final : public LHCb::SIMD::AlignedBase<LHCb::SIMD::VectorAlignment> {

  public:
    // types

#ifdef USE_DD4HEP
    using DetElem = LHCb::Detector::DeRichBaseMirror;
#else
    using DetElem = DeRichSphMirror;
#endif

    /// Basic cache of mirror data needed by SIMD reconstruction as required type
    template <typename TYPE>
    struct MirrorData {
      /// radius of curvature
      alignas( LHCb::SIMD::VectorAlignment ) TYPE radius{0};
      /// Plane parameters
      alignas( LHCb::SIMD::VectorAlignment ) TYPE planeA{0}, planeB{0}, planeC{0}, planeD{0};
      /// Centre of curvature
      alignas( LHCb::SIMD::VectorAlignment ) TYPE cocX{0}, cocY{0}, cocZ{0};
    };

  public:
#ifdef USE_DD4HEP
    /// Constructor from DD4HEP mirror object.
    template <typename DEMIRROR>
    Mirror( const DEMIRROR& m ) : m_mirr( m.access() ) {
      m_mirrorNumber      = m.mirrorNumber();
      m_centreOfCurvature = m.GlobalCOC();
      m_radius            = m.ROC();
      m_side              = m.mirrorSide();
      m_reflectivity      = std::make_shared<const Rich::TabulatedFunction1D>( m.RetrieveReflectivity() );
      m_mirrorCentre      = m.mirrorCentre();
      m_centreNormal      = m.centreNormal();
      // Construct plane
      m_centreNormalPlane = Gaudi::Plane3D( m_centreNormal, m_mirrorCentre );
      // update SIMD mirror data
      updateMirrorData();
    }
#else
    /// Constructor from DetDesc
    Mirror( const DeRichSphMirror* m ) {
      assert( m );
      m_mirr              = m;
      m_mirrorNumber      = m->mirrorNumber();
      m_centreOfCurvature = m->centreOfCurvature();
      m_radius            = m->radius();
      m_mirrorCentre      = m->mirrorCentre();
      m_reflectivity      = m->reflectivity();
      m_centreNormal      = m->centreNormal();
      m_centreNormalPlane = m->centreNormalPlane();
      // to do. If needed for DetDesc set mirror side.
      // update SIMD mirror data
      updateMirrorData();
    }
#endif

    /// Default contructor
    Mirror() = default;

  public:
    // accessors

    /// Detector Side
    inline auto side() const noexcept {
      assert( m_side != Rich::InvalidSide );
      return m_side;
    }
    /// Mirror number
    inline auto mirrorNumber() const noexcept { return m_mirrorNumber; }
    /// CoC
    inline const auto& centreOfCurvature() const noexcept { return m_centreOfCurvature; }
    /// RoC
    inline auto radius() const noexcept { return m_radius; }
    /// Mirror centre point
    inline const auto& mirrorCentre() const noexcept { return m_mirrorCentre; }
    // reflectivity
    inline auto reflectivity() const noexcept { return m_reflectivity.get(); }
    // centre point normal
    inline const auto& centreNormal() const noexcept { return m_centreNormal; }
    // centre point normal plane
    inline const auto& centreNormalPlane() const noexcept { return m_centreNormalPlane; }
    /// Access the mirror data
    inline const auto& mirrorData() const noexcept { return m_mirrorData; }

  public:
    // Accesssors that forward on to underlying det object

    FORWARD_TO_DET_OBJ( intersects )

  private:
    /// Get access to the underlying object
    inline const DetElem* get() const noexcept {
      assert( m_mirr );
      return m_mirr;
    }

    void updateMirrorData() {
      // update SIMD float mirror data cache
      m_mirrorData.radius = radius();
      m_mirrorData.planeA = centreNormalPlane().A();
      m_mirrorData.planeB = centreNormalPlane().B();
      m_mirrorData.planeC = centreNormalPlane().C();
      m_mirrorData.planeD = centreNormalPlane().D();
      m_mirrorData.cocX   = centreOfCurvature().X();
      m_mirrorData.cocY   = centreOfCurvature().Y();
      m_mirrorData.cocZ   = centreOfCurvature().Z();
    }

  public:
    // messaging

    /// Overload ostream operator
    friend inline std::ostream& operator<<( std::ostream& s, const Mirror& m ) {
      return s << "[ Num=" << m.mirrorNumber()       //
               << " RoC=" << m.radius()              //
               << " CoC=" << m.centreOfCurvature()   //
               << " CentrePnt=" << m.mirrorCentre()  //
               << " CentreNorm=" << m.centreNormal() //
               << " Refl=" << *m.reflectivity() << " ]";
    }

  private:
    // data

    /// Mirror segment number
    int m_mirrorNumber{-1};

    /// Detector Side
    Rich::Side m_side = Rich::InvalidSide;

    /// floating point mirror data for SIMD support
    alignas( LHCb::SIMD::VectorAlignment ) MirrorData<LHCb::SIMD::DefaultScalarFP> m_mirrorData;

    /// Mirror CoC
    Gaudi::XYZPoint m_centreOfCurvature;
    /// Mirror RoC
    double m_radius{0};
    /// Mirror centre point
    Gaudi::XYZPoint m_mirrorCentre;

    /// The normal vector at the centre of the mirror
    Gaudi::XYZVector m_centreNormal;
    /// The plane normal to the normal vector at the centre of the mirror
    Gaudi::Plane3D m_centreNormalPlane;

    /// PD quantum efficiency
    std::shared_ptr<const Rich::TabulatedFunction1D> m_reflectivity;

    /// DetDesc Rich1 object
    const DetElem* m_mirr = nullptr;
  };

} // namespace Rich::Detector
