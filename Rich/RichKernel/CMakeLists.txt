###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#[=======================================================================[.rst:
Rich/RichKernel
---------------
#]=======================================================================]

gaudi_add_library(RichKernelLib
    SOURCES
        src/lib/RichAlgBase.cpp
        src/lib/RichCommonConstructors.cpp
        src/lib/RichConverter.cpp
        src/lib/RichConverter_Imp.cpp
        src/lib/RichHistoAlgBase.cpp
        src/lib/RichHistoToolBase.cpp
        src/lib/RichToolBase.cpp
        src/lib/RichTupleAlgBase.cpp
        src/lib/RichTupleToolBase.cpp
    LINK
        PUBLIC
            AIDA::aida
            Boost::headers
            Gaudi::GaudiAlgLib
            Gaudi::GaudiKernel
            LHCb::RichInterfaces
            LHCb::RichUtils
        PRIVATE
            Gaudi::GaudiUtilsLib
)

gaudi_add_module(RichKernel
    SOURCES
        src/component/RichToolRegistry.cpp
    LINK
        Gaudi::GaudiAlgLib
        LHCb::RichInterfaces
        LHCb::RichUtils
)

gaudi_install(PYTHON)
gaudi_generate_confuserdb()
lhcb_add_confuser_dependencies(
    Kernel/LHCbKernel
)
