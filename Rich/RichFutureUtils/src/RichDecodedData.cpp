/*****************************************************************************\
* (c) Copyright 2000-2023 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "RichFutureUtils/RichDecodedData.h"
#include <algorithm>
#include <cassert>

using namespace Rich::Future::DAQ;

void DecodedData::addSmartIDs( const LHCb::RichSmartID::Vector& ids ) {

  // cache the last info
  PDInfo*                   last_pdInfo = nullptr;
  ModuleData*               last_mInfo  = nullptr;
  LHCb::RichSmartID         last_pdID;
  Rich::DAQ::PDModuleNumber last_mID;

  // loop over the IDs to save
  for ( const auto id : ids ) {

    assert( id.isValid() );

    // The RICH and panel
    const auto rich = id.rich();
    const auto side = id.panel();

    // The module number
    const Rich::DAQ::PDModuleNumber mID( id.pdCol() );

    // The PD ID
    const auto pdID = id.pdID();

    // Get the Module Data vector
    auto& mDataV = ( ( *this )[rich] )[side];

    // The info objects to fill for this channel
    PDInfo*     pdInfo = nullptr;
    ModuleData* mInfo  = nullptr;

    // Has PD changed ?
    if ( pdID != last_pdID || !last_pdInfo || !last_mInfo ) {

      // has the module changed ?
      if ( mID != last_mID || !last_mInfo ) {
        // Find module data
        const auto mIt = std::find_if( mDataV.begin(), mDataV.end(), //
                                       [&mID]( const auto& i ) { return mID == i.moduleNumber(); } );
        // If get here, most likely a new module
        if ( mIt == mDataV.end() ) {
          // make a new entry
          mInfo = &mDataV.emplace_back( mID );
        } else {
          // use found entry
          mInfo = &( *mIt );
        }
        // update cached info
        last_mID   = mID;
        last_mInfo = mInfo;
      } else {
        // use cached info
        mInfo = last_mInfo;
      }

      // Find PD data object
      const auto pdIt = std::find_if( mInfo->begin(), mInfo->end(), //
                                      [&pdID]( const auto& i ) { return pdID == i.pdID(); } );
      // If get here most likely new PD
      if ( pdIt == mInfo->end() ) {
        // make a new entry
        pdInfo = &mInfo->emplace_back( pdID );
        // Add to active PD count for current rich
        addToActivePDs( rich );
      } else {
        // Use found entry
        pdInfo = &( *pdIt );
      }
      // update the PD cache
      last_pdID   = pdID;
      last_pdInfo = pdInfo;
    } else {
      // use last PD cache
      pdInfo = last_pdInfo;
    }

    // ID vector to fill
    auto& ids = pdInfo->smartIDs();

    // add the hit to the list
    ids.emplace_back( id );

    // count the hits
    addToTotalHits( rich );

  } // loop over IDs
}
