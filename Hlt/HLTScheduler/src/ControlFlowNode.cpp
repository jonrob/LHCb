/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifdef NDEBUG
#  pragma GCC diagnostic push
#  pragma GCC diagnostic ignored "-Wunused-parameter"
#  define GSL_UNENFORCED_ON_CONTRACT_VIOLATION
#endif
#include "ControlFlowNode.h"
#ifdef NDEBUG
#  pragma GCC diagnostic pop
#endif

bool CompositeNode::CompositeNode::updateState( nodeType type, int senderNodeID,
                                                LHCb::span<NodeState> nodeStates ) const {
  switch ( type ) {
  case nodeType::LAZY_AND:
    // implements the updateState for the LAZY_AND CompositeNode type: If a child
    // did not select anything (did not pass), the LAZY_AND node sets its own
    // executed and passed flag and is then considered inactive, not requesting any
    // more children. If an executed child returns TRUE, a counter is decremented,
    // to be sure to finish execution after every child is executed.
    if ( !nodeStates[senderNodeID].passed ) {
      nodeStates[id()].executionCtr = 0;
      nodeStates[id()].passed       = false;
      return true;
    }
    nodeStates[id()].executionCtr--;
    if ( nodeStates[id()].executionCtr == 0 ) {
      // nodeStates[id()].passed = true;  //its true by default, thats why we dont need it here
      return true;
    }
    return false;
  case nodeType::LAZY_OR:
    if ( nodeStates[senderNodeID].passed ) {
      nodeStates[id()].executionCtr = 0;
      nodeStates[id()].passed       = true;
      return true;
    }
    nodeStates[id()].executionCtr--;
    if ( nodeStates[id()].executionCtr == 0 ) {
      nodeStates[id()].passed = false;
      return true;
    }
    return false;
  case nodeType::NONLAZY_OR:
    // implements the updateState for the NONLAZY_OR CompositeNode type: requests
    // all children, sets executed state as soon as every child ran and if one child
    // passed, this passes as well
    nodeStates[id()].executionCtr--;
    if ( nodeStates[id()].executionCtr == 0 ) {
      nodeStates[id()].passed = std::any_of( begin( children() ), end( children() ),
                                             [&]( VNode const* n ) { return n->passed( nodeStates ); } );
      return true;
    }
    return false;
  case nodeType::NONLAZY_AND:
    nodeStates[id()].executionCtr--;
    if ( nodeStates[id()].executionCtr == 0 ) {
      nodeStates[id()].passed = std::all_of( begin( children() ), end( children() ),
                                             [&]( VNode const* n ) { return n->passed( nodeStates ); } );
      return true;
    }
    return false;
  case nodeType::NOT:
    nodeStates[id()].executionCtr--;
    nodeStates[id()].passed = !children().front()->passed( nodeStates );
    return true;
  }
  throw GaudiException( "Unknown composite node logic", __PRETTY_FUNCTION__, StatusCode::FAILURE );
  return false;
}

// ----------DEFINITION OF FUNCTIONS FOR SCHEDULING---------------------------------------

void childrenNamesToPointers( std::map<std::string, VNode>& allNodes ) {
  for ( auto& [_, vnode] : allNodes ) {
    vnode.visit(
        [&]( auto& node ) {
          assert( node.children().empty() );
          for ( auto const& name : node.childrenNames() ) { node.children().emplace_back( &allNodes.at( name ) ); }
        },
        []( BasicNode& ) {} );
  }
}

// function to get all basic grandchildren and children from a CompositeNode
std::set<gsl::not_null<VNode*>> reachableBasics( gsl::not_null<VNode*> vnode ) {
  std::set<gsl::not_null<VNode*>> nodes;
  vnode->walk_node_and_children( [&]( VNode& vn ) {
    vn.visit( [&]( BasicNode const& ) { nodes.emplace( &vn ); }, []( CompositeNode const& ) {} );
  } );
  return nodes;
}

// same for composite
std::set<gsl::not_null<VNode*>> reachableComposites( gsl::not_null<VNode*> vnode ) {
  std::set<gsl::not_null<VNode*>> nodes;
  vnode->walk_node_and_children( [&]( VNode& vn ) {
    vn.visit( [&]( CompositeNode const& ) { nodes.emplace( &vn ); }, []( BasicNode const& ) {} );
  } );
  return nodes;
}

std::map<gsl::not_null<VNode*>, std::set<gsl::not_null<VNode*>>>
findAllEdges( gsl::not_null<VNode const*> vnode, std::set<std::array<gsl::not_null<VNode*>, 2>> const& custom_edges ) {

  std::map<gsl::not_null<VNode*>, std::set<gsl::not_null<VNode*>>> nodePrerequisites;
  auto                                                             append = [&]( const auto& i, const auto& j ) {
    for ( auto const& n : reachableBasics( j ) ) {
      auto const prerequisites = reachableBasics( i );
      nodePrerequisites[n].insert( begin( prerequisites ), end( prerequisites ) );
    }
  };

  vnode->walk_node_and_children( [&]( VNode const& vn ) {
    vn.visit( []( BasicNode const& ) {},
              [&]( CompositeNode const& node ) {
                for ( auto const& edge : node.Edges() ) append( edge.first, edge.second );
              } );
  } );

  for ( auto const& edge : custom_edges ) append( edge[0], edge[1] );

  // check that nodes do not depend on themselves....
  auto it = std::find_if( nodePrerequisites.begin(), nodePrerequisites.end(),
                          []( const auto& kv ) { return kv.second.find( kv.first ) != kv.second.end(); } );
  if ( it != nodePrerequisites.end() ) {
    throw GaudiException( "Node " + it->first->name() + " depends on itself.", __PRETTY_FUNCTION__,
                          StatusCode::FAILURE );
  }

  return nodePrerequisites;
}

/// Check whether all prerequisites of `nodeToCheck` are in `alreadyOrdered`
bool CFDependenciesMet( gsl::not_null<VNode*>                                                   nodeToCheck,
                        std::map<gsl::not_null<VNode*>, std::set<gsl::not_null<VNode*>>> const& nodePrerequisites,
                        std::set<gsl::not_null<VNode*>> const&                                  alreadyOrdered ) {

  auto const it = nodePrerequisites.find( nodeToCheck );
  if ( it == end( nodePrerequisites ) ) { return true; }
  auto const& prerequisites = it->second; // list of all dependencies of `nodeToCheck`
  // Optimization that doesn't help: prerequisites.empty() || ( prerequisites.size() <= alreadyOrdered.size() && ...)
  return std::includes( begin( alreadyOrdered ), end( alreadyOrdered ), begin( prerequisites ), end( prerequisites ) );
}

// this should resolve the CF and DD dependencies and return a ordered vector which meets
// all dependencies. Pick from unordered, append to ordered and erase from unordered when dependencies met.
std::vector<gsl::not_null<BasicNode*>>
resolveDependencies( std::set<gsl::not_null<VNode*>>&                                        unordered,
                     std::map<gsl::not_null<VNode*>, std::set<gsl::not_null<VNode*>>> const& nodePrerequisites ) {

  // check that nodes do not depend on themselves....
  auto it = std::find_if( nodePrerequisites.begin(), nodePrerequisites.end(),
                          []( const auto& kv ) { return kv.second.find( kv.first ) != kv.second.end(); } );
  if ( it != nodePrerequisites.end() ) {
    throw GaudiException( "Node " + it->first->name() + " depends on itself.", __PRETTY_FUNCTION__,
                          StatusCode::FAILURE );
  }

  std::vector<gsl::not_null<VNode*>> ordered;
  std::set<gsl::not_null<VNode*>>    orderedSet;
  ordered.reserve( unordered.size() );
  // check for each loop over unordered, whether at least one node was put into ordered,
  // otherwise it is an infinite loop, which is bad

  for ( bool infiniteLoop = true; !unordered.empty(); ) {
    for ( auto it = unordered.begin(); it != unordered.end(); ) {
      if ( CFDependenciesMet( *it, nodePrerequisites, orderedSet ) ) {
        infiniteLoop = false;
        ordered.emplace_back( *it );
        orderedSet.emplace( *it );
        it = unordered.erase( it );
      } else {
        it++;
      }
    }

    if ( infiniteLoop ) {
      std::string message = "Dependency circle in control flow, review your configuration: \n ";
      message += "Unordered ones: \n";
      for ( auto i : unordered ) {
        message += i->name();
        message += " not yet satisfied dependencies:  ";
        auto const it = nodePrerequisites.find( i );
        if ( it != nodePrerequisites.end() ) {
          for ( auto const& pr : it->second ) {
            if ( orderedSet.find( pr ) != orderedSet.end() ) continue;
            message += " ";
            message += pr->name();
          }
        }
        message += "\n";
      }
      message += "Ordered ones: \n";
      for ( auto i : ordered ) {
        message += i->name();
        message += "\n";
      }

      throw GaudiException( message, __func__, StatusCode::FAILURE );
    } else {
      infiniteLoop = true; // reset for the next loop, to check again
    }
  }

  // unpack into basic nodes (we know these are the only executable ones)
  std::vector<gsl::not_null<BasicNode*>> ordered_unpacked{};
  ordered_unpacked.reserve( ordered.size() );

  for ( gsl::not_null<VNode*> vbasic : ordered ) {
    vbasic->visit(
        [ou = std::ref( ordered_unpacked )]( BasicNode& node ) { ou.get().emplace_back( &node ); },
        []( ... ) { throw GaudiException( "there should only be basic nodes here", __func__, StatusCode::FAILURE ); } );
  }

  return ordered_unpacked;
}

// fill the parents member of all nodes that are interconnected.
// you can give a list of composite nodes, and all their children's parents-member will
// be filled.
void addParentsToAllNodes( std::set<gsl::not_null<VNode*>> const& composites ) {
  using R           = std::vector<gsl::not_null<VNode*>> const&;
  auto get_children = [empty = std::vector<gsl::not_null<VNode*>>{}]( VNode const* n ) {
    return n->visit( []( CompositeNode const& node ) -> R { return node.children(); },
                     [&]( BasicNode const& ) -> R { return empty; } );
  };
  for ( gsl::not_null<VNode*> composite : composites ) {
    for ( gsl::not_null<VNode*> node : get_children( composite ) ) {
      node->visit( [&]( auto& toAppendTo ) { return toAppendTo.parents().emplace_back( composite ); } );
    }
  }
}
