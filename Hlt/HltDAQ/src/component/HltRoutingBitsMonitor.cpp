/*****************************************************************************\
* (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Gaudi/Accumulators/Histogram.h"

#include "Event/RawBank.h"
#include "LHCbAlgs/Consumer.h"

class HltRoutingBitsMonitor : public LHCb::Algorithm::Consumer<void( LHCb::RawBank::View const& )> {
public:
  HltRoutingBitsMonitor( const std::string& name, ISvcLocator* pSvcLocator );
  void operator()( LHCb::RawBank::View const& ) const override;

private:
  mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_unexpectedRawbanks{
      this, "#unexpected number of HltRoutingBits rawbanks"};
  mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_unexpectedRawbanksSize{
      this, "#unexpected HltRoutingBits rawbank size"};

  mutable Gaudi::Accumulators::WeightedHistogram<1> m_rbCount{
      this, "rb_count", "#events with RB set", {4 * 32, -0.5, 4 * 32 - 0.5}};
  mutable Gaudi::Accumulators::ProfileHistogram<1> m_rbAcceptFracion{
      this, "rb_frac", "#events with RB set / #events with RB present", {4 * 32, -0.5, 4 * 32 - 0.5}};
};

DECLARE_COMPONENT( HltRoutingBitsMonitor )

HltRoutingBitsMonitor::HltRoutingBitsMonitor( const std::string& name, ISvcLocator* pSvcLocator )
    : Consumer( name, pSvcLocator, KeyValue{"RawBanks", ""} ) {}

void HltRoutingBitsMonitor::operator()( LHCb::RawBank::View const& banks ) const {
  if ( banks.size() != 1 ) {
    ++m_unexpectedRawbanks;
    return;
  }
  auto size = banks[0]->size();
  if ( size % 4 != 0 || size > 4 * 4 ) {
    ++m_unexpectedRawbanksSize;
    return;
  }
  for ( auto const& [i, word] : LHCb::range::enumerate( banks[0]->range<uint32_t>() ) ) {
    for ( unsigned int j = 0; j < 32; ++j ) {
      const auto bit   = i * 32 + j;
      const auto value = ( word & ( 1 << j ) ) != 0;
      m_rbCount[bit] += value;
      m_rbAcceptFracion[bit] += value;
    }
  }
}
