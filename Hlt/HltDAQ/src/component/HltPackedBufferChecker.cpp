/*****************************************************************************\
* (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/PackedDataBuffer.h"
#include "LHCbAlgs/Consumer.h"

namespace Hlt::HltDAQ {

  struct Checker final : public LHCb::Algorithm::Consumer<void( LHCb::Hlt::PackedData::MappedInBuffers const& )> {
    Checker( const std::string& name, ISvcLocator* pSvcLocator )
        : Consumer{name, pSvcLocator, {"InputName", "/Event/DAQ/MappedDstData"}} {}
    void operator()( LHCb::Hlt::PackedData::MappedInBuffers const& buffers ) const override {
      always() << buffers << endmsg;
    }
  };

  DECLARE_COMPONENT_WITH_ID( Checker, "HltPackedBufferChecker" )
} // namespace Hlt::HltDAQ
