/*****************************************************************************\
* (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/HltDecReports.h"
#include "LHCbAlgs/FilterPredicate.h"

#include "Gaudi/Accumulators.h"
#include "GaudiKernel/GaudiException.h"

#include <algorithm>
#include <string>
#include <vector>

#include <numeric>
/** @class HltDecReportsFilter
 *
 *  Checks content of DecReports for list of requested line
 *  Filter fails if none of the lines have positive decision
 */
class HltDecReportsFilter final : public LHCb::Algorithm::FilterPredicate<bool( const LHCb::HltDecReports& )> {

  Gaudi::Property<std::vector<std::string>> m_lines{this, "Lines", {}};

public:
  /// Standard constructor
  HltDecReportsFilter( const std::string& name, ISvcLocator* pSvcLocator )
      : FilterPredicate{name, pSvcLocator, KeyValue{"DecReports", ""}} {}

  bool operator()( const LHCb::HltDecReports& decReports ) const override {

    auto any_fired = [&]( std::vector<std::string> const& lines ) {
      return std::any_of( lines.begin(), lines.end(), [&]( const auto& line ) -> bool {
        if ( auto i = decReports.find( line ); i != decReports.end() ) {
          m_cutEff += i->second.decision();
          return i->second.decision();
        }
        throw GaudiException( "requested line not present", __PRETTY_FUNCTION__, StatusCode::FAILURE );
      } );
    };

    return any_fired( m_lines );
  }

  // Counter for recording cut retention statistics
  mutable Gaudi::Accumulators::BinomialCounter<> m_cutEff{this, "Cut selection efficiency"};
};

//=============================================================================
// Declaration of the Algorithm Factory
DECLARE_COMPONENT( HltDecReportsFilter )
//=============================================================================
