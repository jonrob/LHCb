/*****************************************************************************\
* (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/HltDecReports.h"
#include "LHCbAlgs/Consumer.h"

#include "Gaudi/Accumulators/Histogram.h"
#include "GaudiKernel/EventContext.h"
#include "GaudiKernel/StatusCode.h"
#include "Kernel/EventContextExt.h"
#include "Kernel/IANNSvc.h"
#include <string>
#include <type_traits>
#include <vector>

class HltDecReportsMonitor final : public LHCb::Algorithm::Consumer<void( const LHCb::HltDecReports& )> {
public:
  HltDecReportsMonitor( const std::string& name, ISvcLocator* pSvcLocator )
      : Consumer{name, pSvcLocator, {"Input", ""}} {}
  void operator()( const LHCb::HltDecReports& ) const override;

private:
  mutable std::optional<Gaudi::Accumulators::WeightedHistogram<1, Gaudi::Accumulators::atomicity::full, unsigned int>>
      m_decCount;
  mutable std::optional<Gaudi::Accumulators::ProfileHistogram<1, Gaudi::Accumulators::atomicity::full, unsigned int>>
      m_decAcceptFraction;
  mutable std::optional<Gaudi::Accumulators::WeightedHistogram<1, Gaudi::Accumulators::atomicity::none>>
      m_decCountUnlabelled;
  mutable std::optional<Gaudi::Accumulators::ProfileHistogram<1, Gaudi::Accumulators::atomicity::none>>
      m_decAcceptFractionUnlabelled;

  mutable std::mutex               m_lock;
  mutable std::vector<std::string> m_decNames;
};

DECLARE_COMPONENT( HltDecReportsMonitor )

void HltDecReportsMonitor::operator()( const LHCb::HltDecReports& reports ) const {
  auto lock = std::scoped_lock{m_lock};

  if ( m_decCount.has_value() ) {
    bool identical_structure = true;
    if ( reports.size() != m_decNames.size() ) {
      identical_structure = false;
    } else {
      for ( const auto& [dr, name] : Gaudi::Functional::details::zip::range( reports, m_decNames ) ) {
        if ( dr.first != name ) {
          identical_structure = false;
          break;
        }
      }
    }
    if ( !identical_structure ) {
      warning() << "Got different HltDecReports structure, resetting histogram." << endmsg;
      m_decCount.reset();
      m_decAcceptFraction.reset();
      m_decCountUnlabelled.reset();
      m_decAcceptFractionUnlabelled.reset();
    }
  }
  if ( !m_decCount.has_value() ) {
    m_decNames = reports.decisionNames();
    auto size  = static_cast<unsigned int>( reports.size() );
    Gaudi::Accumulators::Axis<decltype( m_decCount )::value_type::AxisArithmeticType> axis{size, 0, size, "",
                                                                                           m_decNames};
    m_decCount.emplace( this, "pass_count", "Number of positive decisions", axis );
    m_decAcceptFraction.emplace( this, "pass_frac", "Accept fraction per decision", axis );
    Gaudi::Accumulators::Axis<decltype( m_decCountUnlabelled )::value_type::AxisArithmeticType> axisUL{size, -0.5,
                                                                                                       size - 0.5};
    m_decCountUnlabelled.emplace( this, "pass_count_ul", "Number of positive decisions", axisUL );
    m_decAcceptFractionUnlabelled.emplace( this, "pass_frac_ul", "Accept fraction per decision", axisUL );
  }

  for ( const auto& [i, dr] : LHCb::range::enumerate( reports.decReports() ) ) {
    m_decCount.value()[i] += dr.second.decision();
    m_decAcceptFraction.value()[i] += dr.second.decision();
    m_decCountUnlabelled.value()[i] += dr.second.decision();
    m_decAcceptFractionUnlabelled.value()[i] += dr.second.decision();
  }
}
