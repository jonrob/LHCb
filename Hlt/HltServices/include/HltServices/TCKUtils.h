/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include <git2.h>
#include <iostream>
#include <nlohmann/json.hpp>
#include <string>

namespace LHCb::TCK {

  struct Info {

    std::string digest   = {};
    std::string tck      = {};
    std::string release  = {};
    std::string type     = {};
    std::string label    = {};
    std::string metadata = {};
  };

  namespace Git {

    template <auto traverse = GIT_TREEWALK_POST, typename CB>
    int walk_tree( git_tree const* tree, CB* cb ) {
      return git_tree_walk(
          tree, traverse,
          []( char const* root, git_tree_entry const* te, void* ptr ) {
            return ( *static_cast<CB*>( ptr ) )( root, te );
          },
          cb );
    }

    void check( int error );

    void check_user( std::string const& repo_dir, git_repository* repo );

    std::tuple<git_repository*, git_signature*> create_git_repository( const std::string& repo_name );

    void add_json_manifest_entry( nlohmann::json& json, LHCb::TCK::Info const& info );

    LHCb::TCK::Info tck_info( git_repository* repo, std::string tck );

    std::tuple<std::string, nlohmann::json> blob_to_json( git_repository*, git_tree_entry const* te );

    nlohmann::json tree_to_json( git_repository* repo, git_tree const* tree );

    std::string extract_json( git_repository* repo, std::string const& tck );

    std::ostream& operator<<( std::ostream& s, git_oid const& oid );

  } // namespace Git
} // namespace LHCb::TCK
