/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "Event/CaloClusters_v2.h"
#include "Event/SIMDEventTypes.h"
#include "GaudiKernel/GaudiException.h"
#include "Kernel/STLExtensions.h"
#include "Kernel/meta_enum.h"

namespace LHCb::Event::Calo {
  inline namespace v2 {

    constexpr CLID CLID_HyposV2 = 2024;

    // Namespace for locations in TDS
    namespace HypothesesLocation {
      inline const std::string Default      = "Rec/Calo/Hypos";
      inline const std::string Photons      = "Rec/Calo/Photons";
      inline const std::string Electrons    = "Rec/Calo/Electrons";
      inline const std::string MergedPi0s   = "Rec/Calo/MergedPi0s";
      inline const std::string SplitPhotons = "Rec/Calo/SplitPhotons";
    } // namespace HypothesesLocation

    namespace Enum {
      /// CaloHypo type enumerations
      meta_enum_class( Hypothesis, unsigned, Unknown, Undefined, Mip, Photon, PhotonFromMergedPi0, BremmstrahlungPhoton,
                       Pi0Resolved, Pi0Overlapped, Pi0Merged, EmCharged, NeutralHadron, ChargedHadron, Jet )
    } // namespace Enum

    namespace HypoTag {
      struct Id : Event::int_field {}; // TODO: make CellID field
      struct Clusters : Event::vector_field<Event::index_field<LHCb::Event::Calo::v2::Clusters>> {};
      struct Type : Event::enum_field<Enum::Hypothesis> {};

      template <typename T>
      using Hypotheses_t = Event::SOACollection<T, Id, Clusters, Type>;
    } // namespace HypoTag

    struct Hypotheses : HypoTag::Hypotheses_t<Hypotheses> {
      using base_t = typename HypoTag::Hypotheses_t<Hypotheses>;
      using Type   = Enum::Hypothesis;

      using base_t::allocator_type;
      using base_t::base_t;

      const CLID&        clID() const { return Hypotheses::classID(); }
      static const CLID& classID() { return CLID_HyposV2; }

      Hypotheses( Zipping::ZipFamilyNumber zipIdentifier = Zipping::generateZipIdentifier(), allocator_type alloc = {} )
          : base_t{std::move( zipIdentifier ), std::move( alloc )}
          , m_clusters( std::move( zipIdentifier ), std::move( alloc ) ) {}
      // Note: hypos and clusters are not always zippable, but if their size is equal they are, which means the
      // zipIdentifier can be shared

      Hypotheses( Hypotheses&& old ) noexcept : base_t{std::move( old )}, m_clusters( std::move( old.m_clusters ) ) {}

      template <SIMDWrapper::InstructionSet simd, LHCb::Pr::ProxyBehaviour behaviour, typename ContainerType>
      struct HypoProxy : Event::Proxy<simd, behaviour, ContainerType> {
        using base_t = typename Event::Proxy<simd, behaviour, ContainerType>;
        using base_t::loop_mask;
        using base_t::Proxy;
        using base_t::width;

        [[nodiscard, gnu::always_inline]] auto cellID() const {
          if constexpr ( simd == SIMDWrapper::InstructionSet::Scalar ) { // hack to ease transition to soacollections
            return LHCb::Detector::Calo::CellID{static_cast<unsigned>( this->template get<HypoTag::Id>().cast() )};
          } else {
            return this->template get<HypoTag::Id>();
          }
        }
        [[nodiscard, gnu::always_inline]] auto hypothesis() const {
          if constexpr ( simd == SIMDWrapper::InstructionSet::Scalar ) {
            return this->template get<HypoTag::Type>().cast(); // hack to ease transition to soacollections
          } else {
            return this->template get<HypoTag::Type>();
          }
        }
        [[nodiscard, gnu::always_inline]] auto size() const {
          if constexpr ( simd == SIMDWrapper::InstructionSet::Scalar ) {
            return this->template field<HypoTag::Clusters>().size().cast(); // hack to ease transition to soacollections
          } else {
            return this->template field<HypoTag::Clusters>().size();
          }
        }
        [[nodiscard, gnu::always_inline]] auto clusters() const { return this->template field<HypoTag::Clusters>(); };
        [[nodiscard, gnu::always_inline]] auto position() const { return clusters()[0].position(); }
        [[nodiscard, gnu::always_inline]] auto energy() const { return clusters()[0].energy(); }
        [[nodiscard, gnu::always_inline]] auto e() const { return energy(); }
      };

      template <SIMDWrapper::InstructionSet simd, LHCb::Pr::ProxyBehaviour behaviour, typename ContainerType>
      using proxy_type = HypoProxy<simd, behaviour, ContainerType>;

      [[nodiscard, gnu::always_inline]] const auto& clusters() const { return m_clusters; }
      [[nodiscard, gnu::always_inline]] auto&       clusters() { return m_clusters; }

      template <typename Tag>
      [[nodiscard, gnu::always_inline]] auto container() const {
        return &m_clusters;
      }

      [[gnu::always_inline, gnu::flatten]] auto emplace_back(
          Type h, LHCb::Detector::Calo::CellID id,
          std::initializer_list<Clusters::const_reference<SIMDWrapper::Scalar, LHCb::Pr::ProxyBehaviour::Contiguous>>
              clusters ) {
        assert( clusters.size() != 0 );
        auto hypo = base_t::emplace_back<SIMDWrapper::Scalar>();
        hypo.template field<HypoTag::Id>().set( id.all() );
        hypo.template field<HypoTag::Type>().set( h );
        for ( auto c : clusters ) {
          auto index = m_clusters.size();
          m_clusters.copy_back<SIMDWrapper::Scalar>( c );
          hypo.clusters().emplace_back().set( index );
        }
        return hypo;
      }

      template <SIMDWrapper::InstructionSet simd, LHCb::Pr::ProxyBehaviour behaviour>
      using reference = LHCb::Event::ZipProxy<HypoProxy<simd, behaviour, Hypotheses>>;
      template <SIMDWrapper::InstructionSet simd, LHCb::Pr::ProxyBehaviour behaviour>
      using const_reference = const LHCb::Event::ZipProxy<HypoProxy<simd, behaviour, const Hypotheses>>;
      template <SIMDWrapper::InstructionSet simd, LHCb::Pr::ProxyBehaviour behaviour>
      using value_type    = LHCb::Event::ZipProxy<HypoProxy<simd, behaviour, Hypotheses>>;
      using pointer       = void;
      using const_pointer = void;

      using ScalarClusters = LHCb::Event::vector_field<LHCb::Event::index_field<Clusters>>::VectorProxy<
          SIMDWrapper::Scalar, LHCb::Pr::ProxyBehaviour::Contiguous, const Hypotheses,
          LHCb::Event::SOAPath<HypoTag::Clusters>>;

    private:
      Clusters m_clusters; // owned
    };
  } // namespace v2
} // namespace LHCb::Event::Calo
