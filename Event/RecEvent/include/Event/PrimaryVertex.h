/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

// Include files
#include "Event/VertexBase.h"
#include "GaudiKernel/SharedObjectsContainer.h"
#include "Kernel/EventLocalAllocator.h"

// Forward declarations

namespace LHCb {

  // Forward declarations

  // Class ID definition
  static const CLID CLID_PrimaryVertex = 812;

  /** @class PrimaryVertex PrimaryVertex.h
   *
   * Light PrimaryVertex class: basically just a wrapper around VertexBase. Needed to make sure we can identify an
   * object as PrimaryVertex in the packer
   *
   * @author Wouter Hulsbergen
   *
   */

  class PrimaryVertex : public LHCb::VertexBase {
  public:
    using Vector    = std::vector<PrimaryVertex, LHCb::Allocators::EventLocal<PrimaryVertex>>;
    using Container = KeyedContainer<PrimaryVertex, Containers::HashMap>;
    using Selection = SharedObjectsContainer<PrimaryVertex>;
    using Range     = Gaudi::NamedRange_<std::vector<const PrimaryVertex*>>;
    using VertexBase::VertexBase;

    /// Default Constructor
    PrimaryVertex() = default;
    /// Constructor with a selected key (only for testing)
    explicit PrimaryVertex( int key ) : VertexBase{key} {}
    /// Copy constructor
    PrimaryVertex( const PrimaryVertex& rhs ) = default;
    /// Move constructor
    PrimaryVertex( PrimaryVertex&& rhs ) = default;
    // Assignment operators
    PrimaryVertex& operator=( const PrimaryVertex& rhs ) = default;
    PrimaryVertex& operator=( PrimaryVertex&& ) = default;
    // Return number of (active) tracks in vertex
    auto nTracks() const { return ( nDoF() + 3 ) / 2; }
    // Interface compatibility with ThOr. This is already in the base class. Do we actually need it?
    [[nodiscard, gnu::always_inline]] friend LinAlg::Vec<SIMDWrapper::scalar::float_v, 3>
    endVertexPos( const PrimaryVertex& vtx ) {
      auto pos = vtx.position();
      return {pos.x(), pos.y(), pos.z()};
    }
    [[nodiscard, gnu::always_inline]] friend auto posCovMatrix( const PrimaryVertex& vtx ) {
      return LHCb::LinAlg::convert<SIMDWrapper::scalar::float_v>( vtx.covMatrix() );
    }
    const CLID&        clID() const override { return PrimaryVertex::classID(); }
    static const CLID& classID() { return CLID_PrimaryVertex; }
  };
} // namespace LHCb
