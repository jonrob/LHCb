/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "GaudiKernel/KeyedContainer.h"
#include "GaudiKernel/KeyedObject.h"
#include "GaudiKernel/SerializeSTL.h"
#include "Kernel/CondDBNameTagPair.h"
#include <ostream>
#include <string>
#include <vector>

namespace LHCb {

  // Class ID definition
  static const CLID CLID_ProcessFSR = 13507;

  // Namespace for locations in TDS
  namespace ProcessFSRLocation {
    inline const std::string Default = "/FileRecords/ProcessFSR";
  }

  /** @class ProcessFSR ProcessFSR.h
   *
   * FSR based upon common base class for all process headers: GenHeader,
   * MCHeader...
   *
   * @author R. Lambert
   *
   */

  class ProcessFSR : public KeyedObject<int> {
  public:
    /// typedef for KeyedContainer of ProcessFSR
    typedef KeyedContainer<ProcessFSR, Containers::HashMap> Container;

    /// Default Constructor
    ProcessFSR() = default;

    // Retrieve pointer to class definition structure
    const CLID&        clID() const override { return LHCb::ProcessFSR::classID(); }
    static const CLID& classID() { return CLID_ProcessFSR; }

    /// Fill the ASCII output stream
    std::ostream& fillStream( std::ostream& s ) const override {
      using GaudiUtils::operator<<;
      return s << "{ "
               << "applicationName :	" << m_applicationName
               << "\napplicationVersion :	" << m_applicationVersion << "\ncondDBTags :	" << m_condDBTags
               << "\nFID :	" << m_FID << "\n }";
    }

    /// Retrieve const  Application or Program Name (e.g. Gauss, Boole or Pythia)
    const std::string& applicationName() const { return m_applicationName; }

    /// Update  Application or Program Name (e.g. Gauss, Boole or Pythia)
    void setApplicationName( const std::string& value ) { m_applicationName = value; }

    /// Retrieve const  Application or Program version
    const std::string& applicationVersion() const { return m_applicationVersion; }

    /// Update  Application or Program version
    void setApplicationVersion( const std::string& value ) { m_applicationVersion = value; }

    /// Retrieve const  Conditions database tags
    const std::vector<LHCb::CondDBNameTagPair>& condDBTags() const { return m_condDBTags; }

    /// Update  Conditions database tags
    void setCondDBTags( const std::vector<LHCb::CondDBNameTagPair>& value ) { m_condDBTags = value; }

    /// Retrieve const  Only used after merging, should be filled with file GUID
    const std::string& fid() const { return m_FID; }

    /// Update  Only used after merging, should be filled with file GUID
    void setFID( const std::string& value ) { m_FID = value; }

    friend std::ostream& operator<<( std::ostream& str, const ProcessFSR& obj ) { return obj.fillStream( str ); }

  private:
    std::string m_applicationName    = "Unknown";      ///< Application or Program Name (e.g. Gauss, Boole or Pythia)
    std::string m_applicationVersion = "Unknown";      ///< Application or Program version
    std::vector<LHCb::CondDBNameTagPair> m_condDBTags; ///< Conditions database tags
    std::string                          m_FID;        ///< Only used after merging, should be filled with file GUID

  }; // class ProcessFSR

  /// Definition of Keyed Container for ProcessFSR
  typedef KeyedContainer<ProcessFSR, Containers::HashMap> ProcessFSRs;

} // namespace LHCb
