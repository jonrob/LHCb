/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once
#include "Detector/VP/VPChannelID.h"
#include "Event/PrProxyHelpers.h"
#include "Event/PrVeloHits.h"
#include "Event/SIMDEventTypes.h"
#include "Event/SOACollection.h"
#include "Event/Track_v3.h"
#include "Kernel/EventLocalAllocator.h"
#include "Kernel/LHCbID.h"
#include "Kernel/Traits.h"
#include "LHCbMath/MatVec.h"
#include "LHCbMath/SIMDWrapper.h"
#include "PrTracksTag.h"

/**
 * Track data for exchanges between VeloTracking and UT
 *
 * @author: Arthur Hennequin
 * 2020-08-25 updated to SOACollection structure by Peilian Li
 */

namespace LHCb::Pr::Velo {

  enum struct CovXVector { x_x, x_tx, tx_tx };
  enum struct CovYVector { y_y, y_ty, ty_ty };

  struct Tag {
    struct Index : Event::int_field {};
    struct LHCbID : Event::lhcbid_field {};
    struct Hits : Event::vector_field<Event::struct_field<Index, LHCbID>> {};

    static constexpr auto NumVeloStates = Event::v3::num_states<Event::Enum::Track::Type::Velo>();
    // Technically these are not states, as there is no q/p field :
    struct States : Event::pos_dirs_field<NumVeloStates> {};
    struct StateCovXs : Event::floats_field<NumVeloStates, 3> {};
    struct StateCovYs : Event::floats_field<NumVeloStates, 3> {};

    template <typename T>
    using velo_t = Event::SOACollection<T, Hits, States, StateCovXs, StateCovYs>;
  };

  namespace PD = LHCb::Event::PosDirParameters;

  struct Tracks : Tag::velo_t<Tracks> {
    using base_t = typename Tag::velo_t<Tracks>;
    using tag_t  = Tag; // Needed for tag_type_t helper defined in PrTracksTag.h
    using base_t::base_t;

    using base_t::allocator_type;

    constexpr static auto MaxVPHits  = TracksInfo::MaxVPHits;
    constexpr static auto MaxLHCbIDs = TracksInfo::MaxVPHits;

    // -- needs to be explicit, as otherwise the first argument clashes with the
    // -- default constructor
    explicit Tracks( bool backward, Zipping::ZipFamilyNumber zipIdentifier = Zipping::generateZipIdentifier(),
                     allocator_type alloc = {} )
        : base_t{std::move( zipIdentifier ), std::move( alloc )}, m_backward( backward ) {}

    // Constructor used by zipping machinery when making a copy of a zip
    Tracks( Zipping::ZipFamilyNumber zn, Tracks const& old )
        : base_t{std::move( zn ), old}, m_backward{old.m_backward} {}

    [[nodiscard]] auto backward() const { return m_backward; };

    template <SIMDWrapper::InstructionSet simd, ProxyBehaviour behaviour, typename ContainerType>
    struct VeloProxy : Event::Proxy<simd, behaviour, ContainerType> {
      using base_t = typename Event::Proxy<simd, behaviour, ContainerType>;
      using base_t::base_t;
      using base_t::width;
      using simd_t    = SIMDWrapper::type_map_t<simd>;
      using int_v     = typename simd_t::int_v;
      using float_v   = typename simd_t::float_v;
      using Location  = Event::Enum::State::Location;
      using TrackType = Event::Enum::Track::Type;

      [[nodiscard]] auto                     backward() const { return this->container()->backward(); };
      [[nodiscard, gnu::always_inline]] auto nHits() const { return this->template field<Tag::Hits>().size(); }
      [[nodiscard, gnu::always_inline]] auto nVPHits() const { return nHits(); }
      [[nodiscard, gnu::always_inline]] auto vp_index( std::size_t i ) const {
        return this->template field<Tag::Hits>()[i].template get<Tag::Index>();
      }
      [[nodiscard, gnu::always_inline]] auto vp_indices() const {
        std::array<int_v, TracksInfo::MaxVPHits> out = {0};
        for ( auto i = 0; i < nHits().hmax( this->loop_mask() ); i++ ) out[i] = vp_index( i );
        return out;
      }
      [[nodiscard, gnu::always_inline]] auto vp_lhcbID( std::size_t i ) const {
        return this->template field<Tag::Hits>()[i].template get<Tag::LHCbID>();
      }
      [[nodiscard, gnu::always_inline]] auto StateCovXElement( Location loc, CovXVector j ) const {
        return this->template get<Tag::StateCovXs>( stateIndex<TrackType::Velo>( loc ), static_cast<std::size_t>( j ) );
      }
      [[nodiscard, gnu::always_inline]] auto StateCovYElement( Location loc, CovYVector j ) const {
        return this->template get<Tag::StateCovYs>( stateIndex<TrackType::Velo>( loc ), static_cast<std::size_t>( j ) );
      }

      // Retrieve state info
      [[nodiscard, gnu::always_inline]] auto StatePosDir( Location loc ) const {
        return this->template get<Tag::States>( stateIndex<TrackType::Velo>( loc ) );
      }
      [[nodiscard, gnu::always_inline]] auto StatePos( Location loc ) const {
        auto state = StatePosDir( loc );
        return LHCb::LinAlg::Vec<float_v, 3>{state.x(), state.y(), state.z()};
      }
      [[nodiscard, gnu::always_inline]] auto StateDir( Location loc ) const {
        auto state = StatePosDir( loc );
        return LHCb::LinAlg::Vec<float_v, 3>{state.tx(), state.ty(), 1.f};
      }
      [[nodiscard, gnu::always_inline]] auto StateCovX( Location loc ) const {
        return LHCb::LinAlg::Vec<float_v, 3>{StateCovXElement( loc, CovXVector::x_x ),
                                             StateCovXElement( loc, CovXVector::x_tx ),
                                             StateCovXElement( loc, CovXVector::tx_tx )};
      }
      [[nodiscard, gnu::always_inline]] auto StateCovY( Location loc ) const {
        return LHCb::LinAlg::Vec<float_v, 3>{StateCovYElement( loc, CovYVector::y_y ),
                                             StateCovYElement( loc, CovYVector::y_ty ),
                                             StateCovYElement( loc, CovYVector::ty_ty )};
      }
      [[nodiscard, gnu::always_inline]] auto setStateCovXY( Location loc, LHCb::LinAlg::Vec<float_v, 3> const& covx,
                                                            LHCb::LinAlg::Vec<float_v, 3> const& covy ) const {

        this->template field<Tag::StateCovXs>( stateIndex<TrackType::Velo>( loc ), 0 ).set( covx.x() );
        this->template field<Tag::StateCovXs>( stateIndex<TrackType::Velo>( loc ), 1 ).set( covx.y() );
        this->template field<Tag::StateCovXs>( stateIndex<TrackType::Velo>( loc ), 2 ).set( covx.z() );
        this->template field<Tag::StateCovYs>( stateIndex<TrackType::Velo>( loc ), 0 ).set( covy.x() );
        this->template field<Tag::StateCovYs>( stateIndex<TrackType::Velo>( loc ), 1 ).set( covy.y() );
        this->template field<Tag::StateCovYs>( stateIndex<TrackType::Velo>( loc ), 2 ).set( covy.z() );
      }

      [[nodiscard, gnu::always_inline]] auto slopes() const { return StateDir( Location::ClosestToBeam ); }

      [[nodiscard, gnu::always_inline]] auto closestToBeamStateDir() const { return slopes(); }
      [[nodiscard, gnu::always_inline]] auto closestToBeamStatePos() const {
        return StatePos( Location::ClosestToBeam );
      }
      [[nodiscard, gnu::always_inline]] auto closestToBeamState() const {
        return detail::VeloState{StatePos( Location::ClosestToBeam ), slopes()};
      }

      //  Retrieve the (sorted) set of LHCbIDs
      [[nodiscard]] std::vector<LHCbID> lhcbIDs() const {
        std::vector<LHCbID> ids;
        ids.reserve( TracksInfo::MaxVPHits );
        for ( auto i = 0; i < nHits().cast(); i++ ) {
          static_assert( width() == 1, "lhcbIDs() method cannot be used on vector proxies" );
          ids.emplace_back( vp_lhcbID( i ).LHCbID() );
        }
        std::sort( ids.begin(), ids.end() );
        return ids;
      }

      // flag which indicates client code can go for 'threeMomCovMatrix', `momPosCovMatrix` and `posCovMatrix` and not
      // for a track-like stateCov -- possible values: yes (for track-like objects ) no (for neutrals, composites),
      // maybe (particle, check at runtime, calls may return invalid results)
      static constexpr auto canBeExtrapolatedDownstream = Event::CanBeExtrapolatedDownstream::yes;

      [[nodiscard, gnu::always_inline]] friend auto referencePoint( VeloProxy const& vp ) {
        return vp.closestToBeamStatePos();
      }
      [[nodiscard, gnu::always_inline]] friend auto trackState( VeloProxy const& vp ) {
        return vp.closestToBeamState();
      }
    };
    template <SIMDWrapper::InstructionSet simd, ProxyBehaviour behaviour, typename ContainerType>
    using proxy_type = VeloProxy<simd, behaviour, ContainerType>;

  private:
    bool m_backward{false};
  };
} // namespace LHCb::Pr::Velo