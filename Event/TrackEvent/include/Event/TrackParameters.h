/*****************************************************************************\
* (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

#include "GaudiKernel/SystemOfUnits.h"
#include "LHCbMath/LHCbMath.h"
namespace TrackParameters {
  using LHCb::Math::hiTolerance;
  using LHCb::Math::lowTolerance;
  constexpr auto propagationTolerance = 1e-6 * Gaudi::Units::mm;  ///< Tolerance for extrapolators
  constexpr auto moliereFactor        = 13.6 * Gaudi::Units::MeV; ///< Moliere factor
  // defines max physical track momentum according to max LHC beam energy
  constexpr auto maxPhysicalMomentum   = 7. * Gaudi::Units::TeV;
  constexpr auto nStateParameters      = 5;
  constexpr auto nStateParametersNoMag = nStateParameters - 1;
} // namespace TrackParameters
