/*****************************************************************************\
* (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

#include "Event/SOACollection.h"
#include "Event/TrackEnums.h"
#include "Kernel/EventLocalAllocator.h"
#include "LHCbMath/MatVec.h"
#include "LHCbMath/SIMDWrapper.h"

/** @class States States.h
 *  SoA implementation of states class
 */

namespace LHCb::Event {

  namespace StateParameters {

    enum struct StateVector { x, y, tx, ty, qOverP, z };

    enum struct CovMatrix {
      x_x,
      x_y,
      x_tx,
      x_ty,
      x_qOverP,
      y_y,
      y_tx,
      y_ty,
      y_qOverP,
      tx_tx,
      tx_ty,
      tx_qOverP,
      ty_ty,
      ty_qOverP,
      qOverP_qOverP
    };

    constexpr unsigned int operator+( unsigned int i, StateVector s ) { return i + static_cast<unsigned>( s ); }

    constexpr unsigned int operator+( unsigned int i, CovMatrix s ) { return i + static_cast<unsigned>( s ); }
  } // namespace StateParameters

  /**
   * Tag and Proxy baseclasses for representing a 6 parameters state vector (x, y, tx, ty, q/p, z)
   */
  struct state_field : floats_field<6> {
    template <SIMDWrapper::InstructionSet Simd, LHCb::Pr::ProxyBehaviour Behaviour, typename ContainerTypeBase,
              typename Path>
    struct NDStateProxy {
      using simd_t        = SIMDWrapper::type_map_t<Simd>;
      using type          = typename simd_t::float_v;
      using OffsetType    = LHCb::Pr::detail::offset_t<Behaviour, Simd>;
      using ContainerType = LHCb::Pr::detail::container_t<Behaviour, Simd, ContainerTypeBase>;
      using proxy_type =
          NumericProxy<Simd, Behaviour, ContainerTypeBase, typename Path::template extend_t<std::size_t>, float>;

      NDStateProxy( ContainerType container, OffsetType offset, Path path )
          : m_container( container ), m_offset( offset ), m_path( path ) {}

      template <StateParameters::StateVector i>
      [[nodiscard, gnu::always_inline]] constexpr auto proxy() const {
        return proxy_type{m_container, m_offset, m_path.append( static_cast<std::size_t>( i ) )};
      }

      [[nodiscard, gnu::always_inline]] constexpr auto x() const {
        return proxy<StateParameters::StateVector::x>().get();
      }
      [[nodiscard, gnu::always_inline]] constexpr auto y() const {
        return proxy<StateParameters::StateVector::y>().get();
      }
      [[nodiscard, gnu::always_inline]] constexpr auto tx() const {
        return proxy<StateParameters::StateVector::tx>().get();
      }
      [[nodiscard, gnu::always_inline]] constexpr auto ty() const {
        return proxy<StateParameters::StateVector::ty>().get();
      }
      [[nodiscard, gnu::always_inline]] constexpr auto qOverP() const {
        return proxy<StateParameters::StateVector::qOverP>().get();
      }
      [[nodiscard, gnu::always_inline]] constexpr auto z() const {
        return proxy<StateParameters::StateVector::z>().get();
      }

      [[gnu::always_inline]] constexpr decltype( auto ) setPosition( type const& x, type const& y, type const& z ) {
        proxy<StateParameters::StateVector::x>().set( x );
        proxy<StateParameters::StateVector::y>().set( y );
        proxy<StateParameters::StateVector::z>().set( z );
        return *this;
      }

      template <typename F>
      [[gnu::always_inline]] constexpr decltype( auto ) setPosition( LHCb::LinAlg::Vec<F, 3> const& pos ) {
        proxy<StateParameters::StateVector::x>().set( pos.x() );
        proxy<StateParameters::StateVector::y>().set( pos.y() );
        proxy<StateParameters::StateVector::z>().set( pos.z() );
        return *this;
      }

      [[gnu::always_inline]] constexpr decltype( auto ) setDirection( type const& tx, type const& ty ) {
        proxy<StateParameters::StateVector::tx>().set( tx );
        proxy<StateParameters::StateVector::ty>().set( ty );
        return *this;
      }

      template <typename F>
      [[gnu::always_inline]] constexpr decltype( auto ) setDirection( LHCb::LinAlg::Vec<F, 3> const& dir ) {
        proxy<StateParameters::StateVector::tx>().set( dir.x() );
        proxy<StateParameters::StateVector::ty>().set( dir.y() );
        return *this;
      }

      [[gnu::always_inline]] constexpr decltype( auto ) setQOverP( type const& qOverP ) {
        proxy<StateParameters::StateVector::qOverP>().set( qOverP );
        return *this;
      }

      [[nodiscard, gnu::always_inline]] constexpr auto get() const { return *this; }

    private:
      ContainerType m_container;
      OffsetType    m_offset;
      const Path    m_path;
    };

    template <SIMDWrapper::InstructionSet Simd, LHCb::Pr::ProxyBehaviour Behaviour, typename ContainerType,
              typename Path>
    using proxy_type = NDStateProxy<Simd, Behaviour, ContainerType, Path>;
  };

  template <std::size_t... N>
  using states_field = ndarray_field<state_field, N...>;

  /**
   * Tag and Proxy baseclasses for representing a 5 X 5 state covariance matrix:
   *          | x  |  y | tx | ty | q/p|
   *         x|____|____|____|____|____|
   *         y|____|____|____|____|____|
   *        tx|____|____|____|____|____|
   *        ty|____|____|____|____|____|
   *       q/p|____|____|____|____|____|
   * The matrices are stored as a flattened vector length 15 vector
   * (as the matrix is symmetric) in the following form:
   * (x_x, x_y, x_tx, x_ty, x_q/p, y_y, y_tx, y_ty, y_q/p, tx_tx, tx_ty, tx_q/p, ty_ty, ty_q/p, q/p_q/p)
   *
   */
  struct state_cov_field : floats_field<15> {
    template <SIMDWrapper::InstructionSet Simd, LHCb::Pr::ProxyBehaviour Behaviour, typename ContainerTypeBase,
              typename Path>
    struct StateCovProxy {
      using simd_t        = SIMDWrapper::type_map_t<Simd>;
      using type          = typename simd_t::float_v;
      using OffsetType    = LHCb::Pr::detail::offset_t<Behaviour, Simd>;
      using ContainerType = LHCb::Pr::detail::container_t<Behaviour, Simd, ContainerTypeBase>;
      using proxy_type =
          NumericProxy<Simd, Behaviour, ContainerTypeBase, typename Path::template extend_t<std::size_t>, float>;

      StateCovProxy( ContainerType container, OffsetType offset, Path path )
          : m_container( container ), m_offset( offset ), m_path( path ) {}

      template <StateParameters::CovMatrix i>
      [[nodiscard, gnu::always_inline]] constexpr auto proxy() const {
        return proxy_type{m_container, m_offset, m_path.append( static_cast<std::size_t>( i ) )};
      }

      [[nodiscard, gnu::always_inline]] constexpr auto x_x() const {
        return proxy<StateParameters::CovMatrix::x_x>().get();
      }
      [[nodiscard, gnu::always_inline]] constexpr auto x_y() const {
        return proxy<StateParameters::CovMatrix::x_y>().get();
      }
      [[nodiscard, gnu::always_inline]] constexpr auto x_tx() const {
        return proxy<StateParameters::CovMatrix::x_tx>().get();
      }
      [[nodiscard, gnu::always_inline]] constexpr auto x_ty() const {
        return proxy<StateParameters::CovMatrix::x_ty>().get();
      }
      [[nodiscard, gnu::always_inline]] constexpr auto x_QoverP() const {
        return proxy<StateParameters::CovMatrix::x_qOverP>().get();
      }

      [[nodiscard, gnu::always_inline]] constexpr auto y_y() const {
        return proxy<StateParameters::CovMatrix::y_y>().get();
      }
      [[nodiscard, gnu::always_inline]] constexpr auto y_tx() const {
        return proxy<StateParameters::CovMatrix::y_tx>().get();
      }
      [[nodiscard, gnu::always_inline]] constexpr auto y_ty() const {
        return proxy<StateParameters::CovMatrix::y_ty>().get();
      }
      [[nodiscard, gnu::always_inline]] constexpr auto y_QoverP() const {
        return proxy<StateParameters::CovMatrix::y_qOverP>().get();
      }

      [[nodiscard, gnu::always_inline]] constexpr auto tx_tx() const {
        return proxy<StateParameters::CovMatrix::tx_tx>().get();
      }
      [[nodiscard, gnu::always_inline]] constexpr auto tx_ty() const {
        return proxy<StateParameters::CovMatrix::tx_ty>().get();
      }
      [[nodiscard, gnu::always_inline]] constexpr auto tx_QoverP() const {
        return proxy<StateParameters::CovMatrix::tx_qOverP>().get();
      }

      [[nodiscard, gnu::always_inline]] constexpr auto ty_ty() const {
        return proxy<StateParameters::CovMatrix::ty_ty>().get();
      }
      [[nodiscard, gnu::always_inline]] constexpr auto ty_QoverP() const {
        return proxy<StateParameters::CovMatrix::ty_qOverP>().get();
      }

      [[nodiscard, gnu::always_inline]] constexpr auto QoverP_QoverP() const {
        return proxy<StateParameters::CovMatrix::qOverP_qOverP>().get();
      }

      [[gnu::always_inline]] constexpr void setXCovariance( type const& x_x, type const& x_y, type const& x_tx,
                                                            type const& x_ty, type const& x_QoverP ) {
        proxy<StateParameters::CovMatrix::x_x>().set( x_x );
        proxy<StateParameters::CovMatrix::x_y>().set( x_y );
        proxy<StateParameters::CovMatrix::x_tx>().set( x_tx );
        proxy<StateParameters::CovMatrix::x_ty>().set( x_ty );
        proxy<StateParameters::CovMatrix::x_qOverP>().set( x_QoverP );
      }

      [[gnu::always_inline]] constexpr void setYCovariance( type const& y_y, type const& y_tx, type const& y_ty,
                                                            type const& y_QoverP ) {
        proxy<StateParameters::CovMatrix::y_y>().set( y_y );
        proxy<StateParameters::CovMatrix::y_tx>().set( y_tx );
        proxy<StateParameters::CovMatrix::y_ty>().set( y_ty );
        proxy<StateParameters::CovMatrix::y_qOverP>().set( y_QoverP );
      }

      [[gnu::always_inline]] constexpr void setTXCovariance( type const& tx_tx, type const& tx_ty,
                                                             type const& tx_QoverP ) {
        proxy<StateParameters::CovMatrix::tx_tx>().set( tx_tx );
        proxy<StateParameters::CovMatrix::tx_ty>().set( tx_ty );
        proxy<StateParameters::CovMatrix::tx_qOverP>().set( tx_QoverP );
      }

      [[gnu::always_inline]] constexpr void setTYCovariance( type const& ty_ty, type const& ty_QoverP ) {
        proxy<StateParameters::CovMatrix::ty_ty>().set( ty_ty );
        proxy<StateParameters::CovMatrix::ty_qOverP>().set( ty_QoverP );
      }

      [[gnu::always_inline]] constexpr void setQoverPCovariance( type const& QoverP_QoverP ) {
        proxy<StateParameters::CovMatrix::qOverP_qOverP>().set( QoverP_QoverP );
      }
      [[gnu::always_inline]] constexpr void set( type const& x_x, type const& x_y, type const& x_tx, type const& x_ty,
                                                 type const& x_QoverP, type const& y_y, type const& y_tx,
                                                 type const& y_ty, type const& y_QoverP, type const& tx_tx,
                                                 type const& tx_ty, type const& tx_QoverP, type const& ty_ty,
                                                 type const& ty_QoverP, type const& QoverP_QoverP ) {
        setXCovariance( x_x, x_y, x_tx, x_ty, x_QoverP );
        setYCovariance( y_y, y_tx, y_ty, y_QoverP );
        setTXCovariance( tx_tx, tx_ty, tx_QoverP );
        setTYCovariance( ty_ty, ty_QoverP );
        setQoverPCovariance( QoverP_QoverP );
      }

      [[gnu::always_inline]] constexpr auto& get() const { return *this; }

    private:
      ContainerType m_container;
      OffsetType    m_offset;
      const Path    m_path;
    };

    template <SIMDWrapper::InstructionSet Simd, LHCb::Pr::ProxyBehaviour Behaviour, typename ContainerType,
              typename Path>
    using proxy_type = StateCovProxy<Simd, Behaviour, ContainerType, Path>;
  };

  template <std::size_t... N>
  using state_covs_field = ndarray_field<state_cov_field, N...>;

  namespace v3 {

    namespace detail {
      using StateLocation = Enum::State::Location;

      constexpr unsigned int operator+( unsigned int i, StateLocation s ) {
        ASSUME( s != StateLocation::LocationUnknown );
        return i + static_cast<std::underlying_type_t<StateLocation>>( s );
      }
      constexpr unsigned int operator+( StateLocation s, unsigned int i ) { return i + s; }
    } // namespace detail

    /// Collection of states, expressed as template arguments
    template <detail::StateLocation... T>
    struct state_collection {
      static constexpr int set = 0;
    };
    template <detail::StateLocation First, detail::StateLocation... T>
    struct state_collection<First, T...> {
      static constexpr int set = ( 1 << static_cast<int>( First ) ) | state_collection<T...>::set;
    };

    /// Track types
    using TrackType  = Enum::Track::Type;
    using FitHistory = Enum::Track::FitHistory;
    namespace {
      using SL = detail::StateLocation;
    }

    /// Define the states available for a given track type
    template <TrackType T, FitHistory H = FitHistory::Unknown>
    struct available_states {};

    template <>
    struct available_states<TrackType::Unknown> {
      using type = state_collection<>;
    };

    template <>
    struct available_states<TrackType::Velo> {
      using type = state_collection<SL::ClosestToBeam, SL::EndVelo>;
    };

    template <>
    struct available_states<TrackType::VeloBackward> {
      using type = state_collection<SL::ClosestToBeam>;
    };

    template <>
    struct available_states<TrackType::Velo, FitHistory::PrKalmanFilter> {
      using type = state_collection<SL::ClosestToBeam, SL::FirstMeasurement, SL::LastMeasurement>;
    };

    template <>
    struct available_states<TrackType::VeloBackward, FitHistory::PrKalmanFilter> {
      using type = state_collection<SL::ClosestToBeam, SL::FirstMeasurement, SL::LastMeasurement>;
    };

    template <>
    struct available_states<TrackType::Long> {
      using type = state_collection<SL::EndVelo, SL::EndT>;
    };

    template <>
    struct available_states<TrackType::Long, FitHistory::PrKalmanFilter> {
      using type = state_collection<SL::ClosestToBeam, SL::FirstMeasurement, SL::LastMeasurement, SL::BegRich1,
                                    SL::EndRich1, SL::BegRich2, SL::EndRich2>;
    };

    template <>
    struct available_states<TrackType::Long, FitHistory::VeloKalman> {
      using type = state_collection<SL::ClosestToBeam>;
    };

    template <>
    struct available_states<TrackType::Upstream> {
      using type = state_collection<SL::AtUT>;
    };

    template <>
    struct available_states<TrackType::Upstream, FitHistory::PrKalmanFilter> {
      using type =
          state_collection<SL::ClosestToBeam, SL::FirstMeasurement, SL::LastMeasurement, SL::BegRich1, SL::EndRich1>;
    };

    template <>
    struct available_states<TrackType::Downstream> {
      using type = state_collection<SL::AtUT>;
    };

    template <>
    struct available_states<TrackType::Downstream, FitHistory::PrKalmanFilter> {
      using type = state_collection<SL::FirstMeasurement, SL::LastMeasurement, SL::BegRich2, SL::EndRich2, SL::BegRich1,
                                    SL::EndRich1>;
    };

    template <>
    struct available_states<TrackType::Ttrack> {
      using type = state_collection<SL::FirstMeasurement, SL::LastMeasurement, SL::EndT>;
    };

    template <>
    struct available_states<TrackType::Ttrack, FitHistory::PrKalmanFilter> {
      using type = state_collection<SL::FirstMeasurement, SL::LastMeasurement, SL::BegRich2, SL::EndRich2>;
    };

    template <>
    struct available_states<TrackType::Muon> {
      using type = state_collection<SL::FirstMeasurement, SL::LastMeasurement>;
    };

    template <>
    struct available_states<TrackType::UT> {
      using type = state_collection<SL::FirstMeasurement, SL::LastMeasurement>;
    };

    template <>
    struct available_states<TrackType::SeedMuon> {
      using type = state_collection<SL::FirstMeasurement, SL::LastMeasurement>;
    };

    template <>
    struct available_states<TrackType::VeloMuon> {
      using type = state_collection<SL::ClosestToBeam, SL::FirstMeasurement, SL::LastMeasurement>;
    };

    template <>
    struct available_states<TrackType::MuonUT> {
      using type = state_collection<SL::FirstMeasurement, SL::LastMeasurement>;
    };

    template <>
    struct available_states<TrackType::LongMuon> {
      using type = state_collection<SL::ClosestToBeam, SL::FirstMeasurement, SL::LastMeasurement, SL::BegRich1,
                                    SL::EndRich1, SL::BegRich2, SL::EndRich2>;
    };

    template <TrackType T, FitHistory H = FitHistory::Unknown>
    using available_states_t = typename available_states<T, H>::type;

    template <TrackType T, FitHistory H = FitHistory::Unknown>
    [[nodiscard]] constexpr auto num_states() {
      // TODO: c++20 std::popcount
      return __builtin_popcount( static_cast<unsigned>( available_states_t<T, H>::set ) );
    }

    template <typename T>
    struct get_state_locations {};
    template <template <SL...> typename BasicType, SL... Args>
    struct get_state_locations<BasicType<Args...>> {

      constexpr auto operator()() const { return std::array{Args...}; }
    };

    namespace StatesTag {

      struct State : Event::state_field {};
      struct Covariance : Event::state_cov_field {};
      struct Success : Event::int_field {};

      template <typename T>
      using states_t = Event::SOACollection<T, State, Covariance, Success>;
    } // namespace StatesTag

    struct States : StatesTag::states_t<States> {
      using base_t = typename StatesTag::states_t<States>;
      using base_t::base_t;
    };
  } // namespace v3
} // namespace LHCb::Event
