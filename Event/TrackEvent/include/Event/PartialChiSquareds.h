/*****************************************************************************\
* (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

#include "Event/ChiSquare.h"
#include "Event/SOACollection.h"
#include "Kernel/EventLocalAllocator.h"

/** @class PartialChiSquareds PartialChiSquareds.h
 *  SoA implementation of PartialChiSquareds class - info needed for track ghost prob.
 */

namespace LHCb::Event::v3::Track {

  namespace PartialChiSquaredsTag {
    struct FitVeloChi2 : Event::float_field {};
    struct FitVeloNDoF : Event::int_field {};
    struct FitTChi2 : Event::float_field {};
    struct FitTNDoF : Event::int_field {};
    struct FitMatchChi2 : Event::float_field {};
    struct NUTOutliers : Event::int_field {};

    template <typename T>
    using PartialChiSquareds_t =
        Event::SOACollection<T, FitVeloChi2, FitVeloNDoF, FitTChi2, FitTNDoF, FitMatchChi2, NUTOutliers>;
  } // namespace PartialChiSquaredsTag

  struct PartialChiSquareds : PartialChiSquaredsTag::PartialChiSquareds_t<PartialChiSquareds> {
    using base_t = typename PartialChiSquaredsTag::PartialChiSquareds_t<PartialChiSquareds>;
    using base_t::base_t;

    // Define an optional custom proxy for this track
    template <SIMDWrapper::InstructionSet simd, LHCb::Pr::ProxyBehaviour behaviour, typename ContainerType>
    struct PartialChiSquaredsProxy : Event::Proxy<simd, behaviour, ContainerType> {
      using Event::Proxy<simd, behaviour, ContainerType>::Proxy;
      using simd_t = SIMDWrapper::type_map_t<simd>;
      using int_v  = typename simd_t::int_v;

      [[nodiscard]] auto FitVeloChi2() const { return this->template get<PartialChiSquaredsTag::FitVeloChi2>(); }
      [[nodiscard]] auto FitVeloNDoF() const { return this->template get<PartialChiSquaredsTag::FitVeloNDoF>(); }
      [[nodiscard]] auto FitTChi2() const { return this->template get<PartialChiSquaredsTag::FitTChi2>(); }
      [[nodiscard]] auto FitTNDoF() const { return this->template get<PartialChiSquaredsTag::FitTNDoF>(); }
      [[nodiscard]] auto FitMatchChi2() const { return this->template get<PartialChiSquaredsTag::FitMatchChi2>(); }
      [[nodiscard]] auto NUTOutliers() const { return this->template get<PartialChiSquaredsTag::NUTOutliers>(); }
    };

    template <SIMDWrapper::InstructionSet simd, LHCb::Pr::ProxyBehaviour behaviour, typename ContainerType>
    using proxy_type = PartialChiSquaredsProxy<simd, behaviour, ContainerType>;
  };

} // namespace LHCb::Event::v3::Track
