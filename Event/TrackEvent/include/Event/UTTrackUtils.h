/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "Event/PrTracksTag.h"
#include "Event/SOACollection.h"

namespace LHCb::UT::TrackUtils {

  constexpr static int maxNumColsBoundariesNominal    = 3;
  constexpr static int maxNumRowsBoundariesNominal    = 3;
  constexpr static int maxNumSectorsBoundariesNominal = maxNumColsBoundariesNominal * maxNumRowsBoundariesNominal;

  constexpr static int maxNumColsBoundariesLoose    = 6;
  constexpr static int maxNumRowsBoundariesLoose    = 3;
  constexpr static int maxNumSectorsBoundariesLoose = maxNumColsBoundariesLoose * maxNumRowsBoundariesLoose;

  // -- A "small" state for internal purposes
  namespace MiniStateTag {
    struct State : Event::state_field {};
    struct index : Event::int_field {};

    template <typename T>
    using ministate_t = Event::SOACollection<T, index, State>;
  } // namespace MiniStateTag

  struct MiniStates : MiniStateTag::ministate_t<MiniStates> {
    using base_t = typename MiniStateTag::ministate_t<MiniStates>;
    using base_t::base_t;
  };

  // -- Helper for storing the boundaries of sectors one needs to look at
  // -- This is for the nominal boundaries
  // -- Wider boundaries can be included by creating a new structure with
  // -- a larger 'maxNumSectors'
  namespace BoundariesNominalTag {
    struct sects : Event::ints_field<maxNumSectorsBoundariesNominal> {};
    struct xTol : Event::float_field {};
    struct nPos : Event::int_field {};

    // -- helper to pass the namespace as temp
    struct types {
      using sects = BoundariesNominalTag::sects;
      using xTol  = BoundariesNominalTag::xTol;
      using nPos  = BoundariesNominalTag::nPos;
    };

    template <typename T>
    using boundary_t = Event::SOACollection<T, sects, xTol, nPos>;
  } // namespace BoundariesNominalTag

  struct BoundariesNominal : BoundariesNominalTag::boundary_t<BoundariesNominal> {
    using base_t = typename BoundariesNominalTag::boundary_t<BoundariesNominal>;
    using base_t::base_t;
  };

  // -- This is for a loose, low-PT configuration of VeloUT, where timing is less relevant
  namespace BoundariesLooseTag {
    struct sects : Event::ints_field<maxNumSectorsBoundariesLoose> {};
    struct xTol : Event::float_field {};
    struct nPos : Event::int_field {};

    // -- helper to pass the namespace as temp
    struct types {
      using sects = BoundariesLooseTag::sects;
      using xTol  = BoundariesLooseTag::xTol;
      using nPos  = BoundariesLooseTag::nPos;
    };

    template <typename T>
    using boundary_t = Event::SOACollection<T, sects, xTol, nPos>;
  } // namespace BoundariesLooseTag

  struct BoundariesLoose : BoundariesLooseTag::boundary_t<BoundariesLoose> {
    using base_t = typename BoundariesLooseTag::boundary_t<BoundariesLoose>;
    using base_t::base_t;
  };

} // namespace LHCb::UT::TrackUtils
