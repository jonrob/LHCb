/***************************************************************************** \
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "Event/State.h"
#include "GaudiKernel/Point3DTypes.h"
#include "LHCbMath/MatVec.h"
#include "LHCbMath/StateVertexUtils.h"

namespace LHCb::TrackVertexUtils {
  using namespace StateVertexUtils;

  /////////////////////////////////////////////////////////////////////////
  /// Add a track to a vertex represented by a position and a cov
  /// matrix. Returns the chi2 increment. This routine calls the
  /// routine below that also has a vertex weight matrix, because
  /// the formalism requires the computation of both. If you add
  /// tracks to vertex one-by-one, then it makes sense to keep the
  /// weight matrix as this is more precise.
  /////////////////////////////////////////////////////////////////////////
  double addToVertex( const LHCb::State& state, Gaudi::XYZPoint& vertexpos, Gaudi::SymMatrix3x3& vertexcov );

  /////////////////////////////////////////////////////////////////////////
  /// Add a track to a vertex represented by a position and a weight
  /// matrix. Both the weight matrix and the covariance matrix are
  /// computed. Make sure to initialize vertexweight with zeros. Returns the chi2.
  /////////////////////////////////////////////////////////////////////////
  double addToVertex( const LHCb::State& state, Gaudi::XYZPoint& vertexpos, Gaudi::SymMatrix3x3& vertexweight,
                      Gaudi::SymMatrix3x3& vertexcov );

  /////////////////////////////////////////////////////////////////////////
  /// Add a range of tracks to a vertex represented by a position and a weight
  /// matrix. Both the weight matrix and the covariance matrix are
  /// computed. Make sure to initialize vertexweight with zeros. Returns the chi2.
  /////////////////////////////////////////////////////////////////////////
  template <typename StateContainer>
  double addToVertex( const StateContainer& states, Gaudi::XYZPoint& vertexpos, Gaudi::SymMatrix3x3& vertexweight,
                      Gaudi::SymMatrix3x3& vertexcov );

  /////////////////////////////////////////////////////////////////////////
  /// Computes a vertex from two track states.
  /////////////////////////////////////////////////////////////////////////
  double vertex( const LHCb::State& stateA, const LHCb::State& stateB, Gaudi::XYZPoint& vertexpos,
                 Gaudi::SymMatrix3x3& vertexweight, Gaudi::SymMatrix3x3& vertexcov );

  /////////////////////////////////////////////////////////////////////////
  /// Computes a vertex from two track states.
  /////////////////////////////////////////////////////////////////////////
  double vertex( const LHCb::State& stateA, const LHCb::State& stateB, Gaudi::XYZPoint& vertexpos,
                 Gaudi::SymMatrix3x3& vertexcov );

} // namespace LHCb::TrackVertexUtils

// Below are implementations.
namespace LHCb::TrackVertexUtils {

  /// Helper function to compute chi2 derivatives for vertex fit. See .cpp file.
  double addToDerivatives( const LHCb::State& state, const Gaudi::XYZPoint& vertexpos, Gaudi::Vector3& halfDChi2DX,
                           Gaudi::SymMatrix3x3& halfD2Chi2DX2 );

  /// Helper function to compute vertex position from derivatives. See .cpp file.
  double solve( const Gaudi::Vector3& halfDChi2DX, const Gaudi::SymMatrix3x3& halfD2Chi2DX2, Gaudi::XYZPoint& vertexpos,
                Gaudi::SymMatrix3x3& vertexcov );

  template <typename StateContainer>
  double addToVertex( const StateContainer& states, Gaudi::XYZPoint& vertexpos, Gaudi::SymMatrix3x3& vertexweight,
                      Gaudi::SymMatrix3x3& vertexcov ) {
    Gaudi::Vector3 halfDChi2DX{}; // does default initializer initialize to zero? not for libEIGEN!
    double         chi2{0};
    for ( const auto& state : states ) chi2 += addToDerivatives( state, vertexpos, halfDChi2DX, vertexweight );
    // compute the vertex
    chi2 += solve( halfDChi2DX, vertexweight, vertexpos, vertexcov );
    return chi2;
  }
} // namespace LHCb::TrackVertexUtils
