
/*****************************************************************************\
* (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#undef NDEBUG
#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE utestSOARelations
#include <iostream>

#include "Event/RelationTable.h"
#include "Event/SIMDEventTypes.h"
#include "Event/SOACollection.h"
#include "LHCbMath/SIMDWrapper.h"

#include <boost/test/unit_test.hpp>

using namespace LHCb::Event;

struct A : int_field {};
struct ContainerA : SOACollection<ContainerA, A> {};

struct B : float_field {};
struct ContainerB : SOACollection<ContainerB, B> {};

struct Weight : float_field {};

template <typename C1>
struct RelationTable1DWithWeight : RelationTable1D<C1, Weight> {
  using RelationTable1D<C1, Weight>::RelationTable1D;
};

template <typename C1, typename C2>
struct RelationTable2DWithWeight : RelationTable2D<C1, C2, Weight> {
  using RelationTable2D<C1, C2, Weight>::RelationTable2D;
};

BOOST_AUTO_TEST_CASE( Relations1D ) {
  std::cout << "Relation 1D:" << std::endl;
  using simd_t = SIMDWrapper::best::types;
  using int_v  = simd_t::int_v;

  ContainerA a{};
  for ( int i = 0; i < 20; i++ ) { a.emplace_back<SIMDWrapper::InstructionSet::Scalar>().field<A>().set( i ); }

  // Fill the table
  RelationTable1DWithWeight<ContainerA> relTable( &a );
  for ( const auto& aProxy : a.simd() ) {
    auto selection = ( aProxy.get<A>() & int_v{1} ) == 0;
    relTable.compress_back( selection && aProxy.loop_mask() ).set( aProxy.indices(), 3.f ); // from, weight
  }

  // Direct iteration (retreive only objects which have relations)
  for ( auto const& rel : relTable.simd() ) {
    std::cout << rel.fromIndex() << " " << rel.from().get<A>() << std::endl;
    std::cout << rel.get<Weight>() << std::endl;
  }

  // Iteration on zip (retreive all A, tests if relation exists)
  auto rel      = relTable.buildView();
  auto aWithRel = make_zip<SIMDWrapper::InstructionSet::Best>( a, rel );
  for ( auto const& proxy : aWithRel ) {
    std::cout << proxy.hasRelation() << std::endl;
    std::cout << proxy.get<A>() << std::endl;
    std::cout << proxy.relation().get<Weight>() << std::endl;
    std::cout << std::get<0>( proxy.weight() ) << std::endl;
  }
}

BOOST_AUTO_TEST_CASE( Relations1D_scalar ) {
  std::cout << "Relation 1D (scalar):" << std::endl;
  using simd_t = SIMDWrapper::scalar::types;
  using int_v  = simd_t::int_v;

  ContainerA a{};
  for ( int i = 0; i < 20; i++ ) { a.emplace_back<SIMDWrapper::InstructionSet::Scalar>().field<A>().set( i ); }

  // Fill the table
  RelationTable1DWithWeight<ContainerA> relTable( &a );
  for ( const auto& aProxy : a.scalar() ) {
    auto selection = ( aProxy.get<A>() & int_v{1} ) == 1;
    std::cout << aProxy.indices() << " " << selection << std::endl;
    relTable.compress_back<SIMDWrapper::InstructionSet::Scalar>( selection && aProxy.loop_mask() )
        .set( aProxy.indices(), 3.f ); // from, weight
  }

  // Direct iteration (retreive only objects which have relations)
  for ( auto const& rel : relTable.scalar() ) {
    std::cout << rel.fromIndex() << " " << rel.from().get<A>() << std::endl;
    std::cout << rel.get<Weight>() << std::endl;
  }

  // Iteration on zip (retreive all A, tests if relation exists)
  auto rel      = relTable.buildView();
  auto aWithRel = make_zip<SIMDWrapper::InstructionSet::Scalar>( a, rel );
  for ( auto const& proxy : aWithRel ) {
    std::cout << proxy.hasRelation() << std::endl;
    std::cout << proxy.get<A>() << std::endl;
    std::cout << proxy.relation().get<Weight>() << std::endl;
    std::cout << std::get<0>( proxy.weight() ) << std::endl;
  }
}

BOOST_AUTO_TEST_CASE( Relations2D ) {
  std::cout << "Relation 2D:" << std::endl;
  ContainerA a{};
  ContainerB b{};
  for ( int i = 0; i < 20; i++ ) {
    a.emplace_back<SIMDWrapper::InstructionSet::Scalar>().field<A>().set( i );
    b.emplace_back<SIMDWrapper::InstructionSet::Scalar>().field<B>().set( i + 100 );
  }

  RelationTable2DWithWeight<ContainerA, ContainerB> relTable( &a, &b );
  for ( int i = 19; i >= 0; i-- ) {
    auto rel = relTable.emplace_back<SIMDWrapper::InstructionSet::Scalar>();
    rel.set( i, ( 19 - i ) % 20, i % 2 ); // from, to, weight
    if ( i % 2 ) {
      rel = relTable.emplace_back<SIMDWrapper::InstructionSet::Scalar>();
      rel.set( i, i % 20, 2 ); // from, to, weight
    }
  }

  // Direct iteration on relations
  for ( auto const& rel : relTable.simd() ) {
    std::cout << rel.fromIndex() << " " << rel.from().get<A>() << std::endl;
    std::cout << rel.toIndex() << " " << rel.to().get<B>() << std::endl;
    std::cout << rel.get<Weight>() << std::endl;
  }

  {
    std::cout << "Iteration on A with arrays of relations to B" << std::endl;
    auto rel      = relTable.buildFromView();
    auto aWithRel = make_zip<SIMDWrapper::InstructionSet::Best>( a, rel );
    for ( auto const& proxy : aWithRel ) {
      std::cout << proxy.numRelations() << std::endl;
      std::cout << proxy.get<A>() << std::endl;
      auto relatedWeights = std::get<0>( proxy.weight() );
      for ( int i = 0; i < proxy.numRelations().hmax( proxy.loop_mask() ); i++ ) {
        std::cout << "  " << proxy.hasRelation( i ) << std::endl;
        std::cout << "  " << proxy.relation( i ).toIndex() << std::endl;
        std::cout << "  " << proxy.relation( i ).to().get<B>() << std::endl; // field of b
        std::cout << "  " << proxy.relation( i ).get<Weight>() << std::endl;
        std::cout << "  " << relatedWeights.at( i ) << std::endl;
      }
    }
  }

  {
    std::cout << "Iteration on B with arrays of relations to A" << std::endl;
    auto rel      = relTable.buildToView();
    auto bWithRel = make_zip<SIMDWrapper::InstructionSet::Best>( b, rel );
    for ( auto const& proxy : bWithRel ) {
      std::cout << proxy.numRelations() << std::endl;
      std::cout << proxy.get<B>() << std::endl;
      auto relatedWeights = std::get<0>( proxy.weight() );
      for ( int i = 0; i < proxy.numRelations().hmax( proxy.loop_mask() ); i++ ) {
        std::cout << "  " << proxy.hasRelation( i ) << std::endl;
        std::cout << "  " << proxy.relation( i ).toIndex() << std::endl;
        std::cout << "  " << proxy.relation( i ).from().get<A>() << std::endl; // field of a
        std::cout << "  " << proxy.relation( i ).get<Weight>() << std::endl;
        std::cout << "  " << relatedWeights.at( i ) << std::endl;
      }
    }
  }
}
