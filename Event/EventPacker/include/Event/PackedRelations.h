/*****************************************************************************\
* (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "Event/StandardPacker.h"

// Include files
namespace LHCb {

  /** @class PackedRelation PackedRelations.h Event/PackedRelations.h
   *
   *  Packed Relation
   *
   *  @author Olivier Callot
   *  @date   2012-01-24
   */
  class PackedRelation {
  public:
    std::int64_t container{0};
    std::int32_t start{0};
    std::int32_t end{0};

#ifndef __CLING__
    template <typename T>
    void save( T& buf ) const {
      Packer::io( buf, *this );
    }
    template <typename T>
    void load( T& buf, unsigned int /*version*/ ) {
      Packer::io( buf, *this ); // identical operation until version is incremented
    }
#endif
  };

  // =================== Unweighted Relations =========================

  constexpr CLID CLID_PackedRelations = 1560;

  /// Namespace for locations in TDS
  namespace PackedRelationsLocation {
    inline const std::string InStream = "/pPhys/Relations";
    inline const std::string P2MCP    = "/pPhys/P2MCPRelations";
    inline const std::string P2Int    = "/pPhys/P2IntRelations";
  } // namespace PackedRelationsLocation

  /** @class PackedRelations PackedRelations.h Event/PackedRelations.h
   *
   *  Packed Relations
   *
   *  @author Olivier Callot
   *  @date   2012-01-24
   */
  class PackedRelations : public DataObject {

  public:
    /// Class ID
    static const CLID& classID() { return CLID_PackedRelations; }

    /// Class ID
    const CLID& clID() const override { return PackedRelations::classID(); }
    /// packing version
    static char packingVersion() { return 2; }

  public:
    std::vector<PackedRelation>&       data() { return m_relations; }
    const std::vector<PackedRelation>& data() const { return m_relations; }

    std::vector<std::int64_t>&       sources() { return m_source; }
    const std::vector<std::int64_t>& sources() const { return m_source; }

    std::vector<std::int64_t>&       dests() { return m_dest; }
    const std::vector<std::int64_t>& dests() const { return m_dest; }

    /// Describe serialization of object
    template <typename T>
    void save( T& buf ) const {
      // the object does not define a packing version, so save 0
      buf.save( static_cast<uint8_t>( 0 ) );
      buf.save( static_cast<uint8_t>( version() ) );
      buf.save( m_relations );
      buf.save( m_source );
      buf.save( m_dest );
    }

    /// Describe de-serialization of object
    template <typename T>
    void load( T& buf ) {
      // the object does not define a packing version, but we save one
      auto packingVersion = buf.template load<uint8_t>();
      setVersion( buf.template load<uint8_t>() );
      if ( packingVersion != 0 ) {
        throw GaudiException( "PackedRelations packing version is not supported: " + std::to_string( packingVersion ),
                              "PackedRelations", StatusCode::FAILURE );
      }
      buf.load( m_relations, packingVersion );
      buf.load( m_source );
      buf.load( m_dest );
    }

  private:
    std::vector<PackedRelation> m_relations;
    std::vector<std::int64_t>   m_source;
    std::vector<std::int64_t>   m_dest;
  };

  // =================== Weighted Relations =========================

  constexpr CLID CLID_PackedWeightedRelations = 1562;

  /// Namespace for locations in TDS
  namespace PackedWeightedRelationsLocation {
    inline const std::string PP2MCP = "/pPhys/PP2MCPRelations";
  }

  /** @class PackedWeightedRelations PackedRelations.h Event/PackedRelations.h
   *
   *  Packed Weighted Relations
   *
   *  @author Chris Jones
   *  @date   2015-04-13
   */
  class PackedWeightedRelations : public DataObject {

  public:
    /// Class ID
    static const CLID& classID() { return CLID_PackedWeightedRelations; }

    /// Class ID
    const CLID& clID() const override { return PackedWeightedRelations::classID(); }

    /// Packing version
    static char packingVersion() { return 2; }

  public:
    std::vector<PackedRelation>&       data() { return m_relations; }
    const std::vector<PackedRelation>& data() const { return m_relations; }

    std::vector<std::int64_t>&       sources() { return m_source; }
    const std::vector<std::int64_t>& sources() const { return m_source; }

    std::vector<std::int64_t>&       dests() { return m_dest; }
    const std::vector<std::int64_t>& dests() const { return m_dest; }

    std::vector<float>&       weights() { return m_weights; }
    const std::vector<float>& weights() const { return m_weights; }

    /// Describe serialization of object
    template <typename T>
    void save( T& buf ) const {
      // the object does not define a packing version, so save 0
      buf.save( static_cast<uint8_t>( 0 ) );
      buf.save( static_cast<uint8_t>( version() ) );
      buf.save( m_relations );
      buf.save( m_source );
      buf.save( m_dest );
      buf.save( m_weights );
    }

    /// Describe de-serialization of object
    template <typename T>
    void load( T& buf ) {
      // the object does not define a packing version, but we save one
      auto packingVersion = buf.template load<uint8_t>();
      setVersion( buf.template load<uint8_t>() );
      if ( packingVersion != 0 ) {
        throw GaudiException( "PackedWeightedRelations packing version is not supported: " +
                                  std::to_string( packingVersion ),
                              "PackedWeightedRelations", StatusCode::FAILURE );
      }
      buf.load( m_relations, packingVersion );
      buf.load( m_source );
      buf.load( m_dest );
      buf.load( m_weights );
    }

  private:
    std::vector<PackedRelation> m_relations;
    std::vector<std::int64_t>   m_source;
    std::vector<std::int64_t>   m_dest;
    std::vector<float>          m_weights;
  };

} // namespace LHCb
