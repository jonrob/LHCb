/*****************************************************************************\
* (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "Event/CaloChargedInfo_v1.h"
#include "Event/PackerBase.h"
#include "Event/StandardPacker.h"

#include "GaudiKernel/DataObject.h"
#include "GaudiKernel/StatusCode.h"

#include "fmt/format.h"

#include <cstdint>

namespace LHCb {

  /**
   *  Packed CaloChargedPID
   *
   */
  struct PackedCaloChargedPID {
    std::int32_t InAcc{0}; // combined variable for storing bools InEcal and InHcal
    std::int32_t ClusterID{0};
    std::int32_t ClusterMatch{0};
    std::int32_t ElectronID{0};
    std::int32_t ElectronMatch{0};
    std::int32_t ElectronEnergy{0};
    std::int32_t ElectronShowerEoP{0};
    std::int32_t ElectronShowerDLL{0};
    std::int32_t EcalPIDe{0};
    std::int32_t EcalPIDmu{0};
    std::int32_t HcalEoP{0};
    std::int32_t HcalPIDe{0};
    std::int32_t HcalPIDmu{0};
    std::int64_t key{-1};

#ifndef __CLING__
    template <typename Buf>
    void save( Buf& buf ) const {
      Packer::io( buf, *this );
    }

    template <typename Buf>
    void load( Buf& buf, unsigned int /*version*/ ) {
      Packer::io( buf, *this );
    }
#endif
  };

  constexpr CLID CLID_PackedCaloChargedPIDs = 1572;

  /// Namespace for locations in TDS
  namespace PackedCaloChargedPIDLocation {
    inline const std::string Default = "";
  } // namespace PackedCaloChargedPIDLocation

  /**
   *  Packed CaloChargedPIDs
   *
   */
  class PackedCaloChargedPIDs : public DataObject {

  public:
    /// Vector of packed objects
    typedef std::vector<LHCb::PackedCaloChargedPID> Vector;

    /// Default Packing Version
    static char defaultPackingVersion() { return 0; }

    /// Class ID
    static const CLID& classID() { return CLID_PackedCaloChargedPIDs; }

    /// Class ID
    const CLID& clID() const override { return PackedCaloChargedPIDs::classID(); }

    /// Write access to the data vector
    Vector& data() { return m_vect; }

    /// Read access to the data vector
    const Vector& data() const { return m_vect; }

    /// Access the packing version
    [[nodiscard]] char packingVersion() const { return m_packingVersion; }

    /// Describe serialization of object
    template <typename T>
    void save( T& buf ) const {
      buf.save( static_cast<uint8_t>( m_packingVersion ) );
      buf.save( static_cast<uint8_t>( version() ) );
      buf.save( m_vect );
    }

    /// Describe de-serialization of object
    template <typename T>
    void load( T& buf ) {
      m_packingVersion = buf.template load<uint8_t>();
      setVersion( buf.template load<uint8_t>() );
      buf.load( m_vect, m_packingVersion );
    }

    // Perform unpacking
    friend StatusCode unpack( Gaudi::Algorithm const*, const PackedCaloChargedPIDs&,
                              Event::Calo::v1::CaloChargedPIDs& );

  private:
    /// Data packing version
    char m_packingVersion{defaultPackingVersion()};

    /// The packed data objects
    Vector m_vect;
  };

  /**
   *  Utility class to handle the packing and unpacking of the CaloChargedPIDs
   *
   */
  class CaloChargedPIDPacker : public PackerBase {
  public:
    // These are required by the templated algorithms
    typedef LHCb::Event::Calo::v1::CaloChargedPID  Data;
    typedef LHCb::PackedCaloChargedPID             PackedData;
    typedef LHCb::Event::Calo::v1::CaloChargedPIDs DataVector;
    typedef LHCb::PackedCaloChargedPIDs            PackedDataVector;
    static const std::string&    packedLocation() { return LHCb::PackedCaloChargedPIDLocation::Default; }
    static const std::string&    unpackedLocation() { return LHCb::Event::Calo::v1::CaloChargedPIDLocation::Default; }
    constexpr inline static auto propertyName() { return "CaloChargedPIDs"; }

    using PackerBase::PackerBase;

    /// Pack CaloChargedPID
    template <typename CaloChargedPIDsRange>
    void pack( const CaloChargedPIDsRange& pids, PackedDataVector& ppids ) const {
      const auto ver = ppids.packingVersion();
      if ( !isSupportedVer( ver ) ) return;
      ppids.data().reserve( pids.size() );
      for ( const auto* pid : pids ) pack( *pid, ppids.data().emplace_back(), ppids );
    }

    /// Unpack a single CaloChargedPID
    StatusCode unpack( const PackedData& ppid, Data& pid, const PackedDataVector& ppids ) const;

    /// Unpack CaloChargedPIDs
    StatusCode unpack( const PackedDataVector& ppids, DataVector& pids ) const;

    /// Compare two CaloChargedPIDs to check the packing -> unpacking performance
    StatusCode check( const Data* dataA, const Data* dataB ) const;

  private:
    /// Pack a CaloChargedPID
    void pack( const Data& pid, PackedData& ppid, PackedDataVector& ppids ) const;

    /// Check if the given packing version is supported
    [[nodiscard]] static bool isSupportedVer( const char ver ) {
      const bool OK = ( 0 == ver );
      if ( !OK ) {
        throw GaudiException( fmt::format( "Unknown packed data version {}", (int)ver ), "CaloChargedPIDPacker",
                              StatusCode::FAILURE );
      }
      return OK;
    }
  };

  /**
   *  Packed BremInfo
   *
   */
  struct PackedBremInfo {
    std::int32_t BremAcc; // combined variable to store bools InBrem and HasBrem
    std::int32_t BremHypoID{0};
    std::int32_t BremHypoMatch{0};
    std::int32_t BremHypoEnergy{0};
    std::int32_t BremHypoDeltaX{0};
    std::int32_t BremTrackBasedEnergy{0};
    std::int16_t BremBendingCorrection{0};
    std::int32_t BremEnergy{0};
    std::int32_t BremPIDe{0};
    std::int64_t key{-1};

#ifndef __CLING__
    template <typename Buf>
    void save( Buf& buf ) const {
      Packer::io( buf, *this );
    }

    template <typename Buf>
    void load( Buf& buf, unsigned int /*version*/ ) {
      Packer::io( buf, *this );
    }
#endif
  };

  constexpr CLID CLID_PackedBremInfos = 1573;

  /// Namespace for locations in TDS
  namespace PackedBremInfoLocation {
    inline const std::string Default = "";
  } // namespace PackedBremInfoLocation

  /**
   *  Packed BremInfos
   *
   */
  class PackedBremInfos : public DataObject {

  public:
    /// Vector of packed objects
    typedef std::vector<LHCb::PackedBremInfo> Vector;

    /// Default Packing Version
    static char defaultPackingVersion() { return 0; }

    /// Class ID
    static const CLID& classID() { return CLID_PackedBremInfos; }

    /// Class ID
    const CLID& clID() const override { return PackedBremInfos::classID(); }

    /// Write access to the data vector
    Vector& data() { return m_vect; }

    /// Read access to the data vector
    const Vector& data() const { return m_vect; }

    /// Access the packing version
    [[nodiscard]] char packingVersion() const { return m_packingVersion; }

    /// Describe serialization of object
    template <typename T>
    void save( T& buf ) const {
      buf.save( static_cast<uint8_t>( m_packingVersion ) );
      buf.save( static_cast<uint8_t>( version() ) );
      buf.save( m_vect );
    }

    /// Describe de-serialization of object
    template <typename T>
    void load( T& buf ) {
      m_packingVersion = buf.template load<uint8_t>();
      setVersion( buf.template load<uint8_t>() );
      buf.load( m_vect, m_packingVersion );
    }

    // Perform unpacking
    friend StatusCode unpack( Gaudi::Algorithm const*, const PackedBremInfos&, Event::Calo::v1::BremInfos& );

  private:
    /// Data packing version
    char m_packingVersion{defaultPackingVersion()};

    /// The packed data objects
    Vector m_vect;
  };

  /**
   *  Utility class to handle the packing and unpacking of the BremInfos
   *
   */
  class BremInfoPacker : public PackerBase {
  public:
    // These are required by the templated algorithms
    typedef LHCb::Event::Calo::v1::BremInfo  Data;
    typedef LHCb::PackedBremInfo             PackedData;
    typedef LHCb::Event::Calo::v1::BremInfos DataVector;
    typedef LHCb::PackedBremInfos            PackedDataVector;
    static const std::string&                packedLocation() { return LHCb::PackedBremInfoLocation::Default; }
    static const std::string&    unpackedLocation() { return LHCb::Event::Calo::v1::BremInfoLocation::Default; }
    constexpr inline static auto propertyName() { return "BremInfos"; }

    using PackerBase::PackerBase;

    /// Pack BremInfo
    template <typename BremInfosRange>
    void pack( const BremInfosRange& pids, PackedDataVector& ppids ) const {
      const auto ver = ppids.packingVersion();
      if ( !isSupportedVer( ver ) ) return;
      ppids.data().reserve( pids.size() );
      for ( const auto* pid : pids ) pack( *pid, ppids.data().emplace_back(), ppids );
    }

    /// Unpack a single BremInfo
    StatusCode unpack( const PackedData& ppid, Data& pid, const PackedDataVector& ppids ) const;

    /// Unpack BremInfos
    StatusCode unpack( const PackedDataVector& ppids, DataVector& pids ) const;

    /// Compare two BremInfos to check the packing -> unpacking performance
    StatusCode check( const Data* dataA, const Data* dataB ) const;

  private:
    /// Pack a BremInfo
    void pack( const Data& pid, PackedData& ppid, PackedDataVector& ppids ) const;

    /// Check if the given packing version is supported
    [[nodiscard]] static bool isSupportedVer( const char ver ) {
      const bool OK = ( 0 == ver );
      if ( !OK ) {
        throw GaudiException( fmt::format( "Unknown packed data version {}", (int)ver ), "BremInfoPacker",
                              StatusCode::FAILURE );
      }
      return OK;
    }
  };

} // namespace LHCb
