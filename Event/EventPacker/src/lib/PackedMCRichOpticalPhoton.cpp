/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/PackedMCRichOpticalPhoton.h"
#include "Event/PackedEventChecks.h"

using namespace LHCb;

void MCRichOpticalPhotonPacker::pack( const DataVector& phots, PackedDataVector& pphots ) const {
  const auto ver = pphots.packingVersion();
  if ( !isSupportedVer( ver ) ) return;
  if ( ver == 0 ) throw GaudiException( "Unsupported packing version", __PRETTY_FUNCTION__, StatusCode::FAILURE );
  pphots.data().reserve( phots.size() );
  for ( const auto* phot : phots ) {

    auto& pphot = pphots.data().emplace_back();

    pphot.key = phot->key();

    pphot.hpdx = StandardPacker::position( phot->pdIncidencePoint().x() );
    pphot.hpdy = StandardPacker::position( phot->pdIncidencePoint().y() );
    pphot.hpdz = StandardPacker::position( phot->pdIncidencePoint().z() );

    pphot.pmirx = StandardPacker::position( phot->sphericalMirrorReflectPoint().x() );
    pphot.pmiry = StandardPacker::position( phot->sphericalMirrorReflectPoint().y() );
    pphot.pmirz = StandardPacker::position( phot->sphericalMirrorReflectPoint().z() );

    pphot.smirx = StandardPacker::position( phot->flatMirrorReflectPoint().x() );
    pphot.smiry = StandardPacker::position( phot->flatMirrorReflectPoint().y() );
    pphot.smirz = StandardPacker::position( phot->flatMirrorReflectPoint().z() );

    pphot.aerox = StandardPacker::position( phot->aerogelExitPoint().x() );
    pphot.aeroy = StandardPacker::position( phot->aerogelExitPoint().y() );
    pphot.aeroz = StandardPacker::position( phot->aerogelExitPoint().z() );

    pphot.theta = StandardPacker::fltPacked( phot->cherenkovTheta() );
    pphot.phi   = StandardPacker::fltPacked( phot->cherenkovPhi() );

    pphot.emisx = StandardPacker::position( phot->emissionPoint().x() );
    pphot.emisy = StandardPacker::position( phot->emissionPoint().y() );
    pphot.emisz = StandardPacker::position( phot->emissionPoint().z() );

    pphot.energy = StandardPacker::energy( phot->energyAtProduction() * PhotEnScale );

    pphot.pmomx = StandardPacker::energy( phot->parentMomentum().x() );
    pphot.pmomy = StandardPacker::energy( phot->parentMomentum().y() );
    pphot.pmomz = StandardPacker::energy( phot->parentMomentum().z() );

    pphot.hpdqwx = StandardPacker::position( phot->hpdQWIncidencePoint().x() );
    pphot.hpdqwy = StandardPacker::position( phot->hpdQWIncidencePoint().y() );
    pphot.hpdqwz = StandardPacker::position( phot->hpdQWIncidencePoint().z() );

    if ( phot->mcRichHit() ) {
      pphot.mcrichhit = StandardPacker::reference64( &pphots, phot->mcRichHit()->parent(), phot->mcRichHit()->index() );
    }
  }
}

StatusCode MCRichOpticalPhotonPacker::unpack( const PackedDataVector& pphots, DataVector& phots ) const {

  const auto ver = pphots.packingVersion();
  if ( !isSupportedVer( ver ) ) return StatusCode::FAILURE; // TODO: define dedicated error category
  phots.reserve( pphots.data().size() );
  auto unpack_ref = StandardPacker::UnpackRef{&pphots, &phots, StandardPacker::UnpackRef::Use32{ver == 0}};
  for ( const auto& pphot : pphots.data() ) {
    auto* phot = new Data();
    phots.insert( phot, pphot.key );

    phot->setPdIncidencePoint( Gaudi::XYZPoint( StandardPacker::position( pphot.hpdx ),
                                                StandardPacker::position( pphot.hpdy ),
                                                StandardPacker::position( pphot.hpdz ) ) );

    phot->setSphericalMirrorReflectPoint( Gaudi::XYZPoint( StandardPacker::position( pphot.pmirx ),
                                                           StandardPacker::position( pphot.pmiry ),
                                                           StandardPacker::position( pphot.pmirz ) ) );

    phot->setFlatMirrorReflectPoint( Gaudi::XYZPoint( StandardPacker::position( pphot.smirx ),
                                                      StandardPacker::position( pphot.smiry ),
                                                      StandardPacker::position( pphot.smirz ) ) );

    phot->setAerogelExitPoint( Gaudi::XYZPoint( StandardPacker::position( pphot.aerox ),
                                                StandardPacker::position( pphot.aeroy ),
                                                StandardPacker::position( pphot.aeroz ) ) );

    phot->setCherenkovTheta( (float)StandardPacker::fltPacked( pphot.theta ) );
    phot->setCherenkovPhi( (float)StandardPacker::fltPacked( pphot.phi ) );

    phot->setEmissionPoint( Gaudi::XYZPoint( StandardPacker::position( pphot.emisx ),
                                             StandardPacker::position( pphot.emisy ),
                                             StandardPacker::position( pphot.emisz ) ) );

    phot->setEnergyAtProduction( (float)( (double)StandardPacker::energy( pphot.energy ) / PhotEnScale ) );

    phot->setParentMomentum( Gaudi::XYZVector( StandardPacker::energy( pphot.pmomx ),
                                               StandardPacker::energy( pphot.pmomy ),
                                               StandardPacker::energy( pphot.pmomz ) ) );

    phot->setHpdQWIncidencePoint( Gaudi::XYZPoint( StandardPacker::position( pphot.hpdqwx ),
                                                   StandardPacker::position( pphot.hpdqwy ),
                                                   StandardPacker::position( pphot.hpdqwz ) ) );

    if ( -1 != pphot.mcrichhit ) {
      if ( auto h = unpack_ref( pphot.mcrichhit ); h ) {
        phot->setMcRichHit( h );
      } else {
        parent().error() << "Corrupt MCRichOpticalPhoton MCRichHit SmartRef detected." << endmsg;
      }
    }
  }
  return StatusCode::SUCCESS;
}

StatusCode MCRichOpticalPhotonPacker::check( const DataVector& dataA, const DataVector& dataB ) const {
  StatusCode sc = StatusCode::SUCCESS;

  // checker
  const DataPacking::DataChecks ch( parent() );

  // Loop over data containers together and compare
  auto iA( dataA.begin() ), iB( dataB.begin() );
  for ( ; iA != dataA.end() && iB != dataB.end(); ++iA, ++iB ) {
    // assume OK frm the start
    bool ok = true;
    // Key
    ok &= ch.compareInts( "Key", ( *iA )->key(), ( *iB )->key() );
    // Hit position
    ok &= ch.comparePoints( "HPD In. Point", ( *iA )->pdIncidencePoint(), ( *iB )->pdIncidencePoint() );
    // primary mirror point
    ok &= ch.comparePoints( "Prim. Mirr.", ( *iA )->sphericalMirrorReflectPoint(),
                            ( *iB )->sphericalMirrorReflectPoint() );
    // secondary mirror point
    ok &= ch.comparePoints( "Sec. Mirr.", ( *iA )->flatMirrorReflectPoint(), ( *iB )->flatMirrorReflectPoint() );
    // aerogel exit point
    ok &= ch.comparePoints( "Aero. Exit", ( *iA )->aerogelExitPoint(), ( *iB )->aerogelExitPoint() );
    // CK theta and phi
    ok &= ch.compareDoubles( "Cherenkov Theta", ( *iA )->cherenkovTheta(), ( *iB )->cherenkovTheta() );
    ok &= ch.compareDoubles( "Cherenkov Phi", ( *iA )->cherenkovPhi(), ( *iB )->cherenkovPhi() );
    // emission point
    ok &= ch.comparePoints( "Emission Point", ( *iA )->emissionPoint(), ( *iB )->emissionPoint() );
    // energy
    ok &= ch.compareEnergies( "Energy", ( *iA )->energyAtProduction(), ( *iB )->energyAtProduction(), 1.0e-7 );
    // parent momentum
    ok &= ch.compareEnergies( "Parent Momentum", ( *iA )->parentMomentum(), ( *iB )->parentMomentum() );
    // HPD QW point
    ok &= ch.comparePoints( "HPD QW Point", ( *iA )->hpdQWIncidencePoint(), ( *iB )->hpdQWIncidencePoint() );
    // MCRichHit
    ok &= ch.comparePointers( "MCRichHit", ( *iA )->mcRichHit(), ( *iB )->mcRichHit() );

    // force printout for tests
    // ok = false;
    // If comparison not OK, print full information
    if ( !ok ) {
      parent().warning() << "Problem with MCRichOpticalPhoton data packing :-" << endmsg
                         << "  Original Photon : " << **iA << endmsg << "  Unpacked Photon : " << **iB << endmsg;
      sc = StatusCode::FAILURE;
    }
  }

  // return final status
  return sc;
}
