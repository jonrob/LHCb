/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "Event/PackedMCRichDigitSummary.h"
#include "Event/PackedEventChecks.h"

using namespace LHCb;

void MCRichDigitSummaryPacker::pack( const DataVector& sums, PackedDataVector& psums ) const {
  const auto ver = psums.packingVersion();
  if ( !isSupportedVer( ver ) ) return;
  if ( ver == 0 ) throw GaudiException( "Unsupported packing version", __PRETTY_FUNCTION__, StatusCode::FAILURE );
  psums.data().reserve( sums.size() );
  for ( const auto* sum : sums ) {
    auto& psum       = psums.data().emplace_back();
    psum.history     = sum->history().historyCode();
    psum.richSmartID = sum->richSmartID().key();
    if ( sum->mcParticle() ) psum.mcParticle = StandardPacker::reference64( &psums, sum->mcParticle() );
  }
}

StatusCode MCRichDigitSummaryPacker::unpack( const PackedDataVector& psums, DataVector& sums ) const {
  const auto ver = psums.packingVersion();
  if ( !isSupportedVer( ver ) ) return StatusCode::FAILURE; // TODO: define dedicated error code
  sums.reserve( psums.data().size() );
  auto unpack_ref = StandardPacker::UnpackRef{&psums, &sums, StandardPacker::UnpackRef::Use32{ver == 0}};
  for ( const auto& psum : psums.data() ) {
    // make and save new sum in container
    auto* sum = new Data();
    sums.add( sum );
    // Fill data from packed object
    sum->setHistory( LHCb::MCRichDigitHistoryCode( psum.history ) );
    sum->setRichSmartID( LHCb::RichSmartID( psum.richSmartID ) );
    if ( -1 != psum.mcParticle ) {
      if ( auto p = unpack_ref( psum.mcParticle ); p ) {
        sum->setMCParticle( p );
      } else {
        parent().error() << "Corrupt MCRichDigitSummary MCParticle SmartRef detected." << endmsg;
      }
    }
  }
  return StatusCode::SUCCESS;
}

StatusCode MCRichDigitSummaryPacker::check( const DataVector& dataA, const DataVector& dataB ) const {
  StatusCode sc = StatusCode::SUCCESS;

  // checker
  const DataPacking::DataChecks ch( parent() );

  // Loop over data containers together and compare
  auto iA( dataA.begin() ), iB( dataB.begin() );
  for ( ; iA != dataA.end() && iB != dataB.end(); ++iA, ++iB ) {
    // assume OK from the start
    bool ok = true;
    // Detector ID
    ok &= ch.compareInts( "RichSmartID", ( *iA )->richSmartID(), ( *iB )->richSmartID() );
    // History code
    ok &= ch.compareInts( "HistoryCode", ( *iA )->history().historyCode(), ( *iB )->history().historyCode() );
    // MCParticle reference
    ok &= ch.comparePointers( "MCParticle", ( *iA )->mcParticle(), ( *iB )->mcParticle() );

    // force printout for tests
    // ok = false;
    // If comparison not OK, print full information
    if ( !ok ) {
      parent().warning() << "Problem with MCRichDigitSummary data packing :-" << endmsg
                         << "  Original Summary : " << **iA << endmsg << "  Unpacked Summary : " << **iB << endmsg;
      sc = StatusCode::FAILURE;
    }
  }

  // Return final status
  return sc;
}
