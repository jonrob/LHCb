/*****************************************************************************\
* (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/PackedNeutralPID.h"
#include "Event/PackedEventChecks.h"
#include <memory>

using namespace LHCb;

void NeutralPIDPacker::pack( const Data& pid, PackedData& ppid, PackedDataVector& ppids ) const {
  const auto ver = ppids.packingVersion();
  if ( !isSupportedVer( ver ) ) { return; }
  // save the key
  ppid.key = pid.key();
  // save data
  ppid.CaloNeutralID        = StandardPacker::caloCellIDPacked( pid.CaloNeutralID() );
  ppid.ClusterMass          = StandardPacker::mass( pid.ClusterMass() );
  ppid.CaloNeutralEcal      = StandardPacker::energy( pid.CaloNeutralEcal() );
  ppid.CaloTrMatch          = StandardPacker::fltPacked( pid.CaloTrMatch() );
  ppid.ShowerShape          = StandardPacker::fltPacked( pid.ShowerShape() );
  ppid.CaloNeutralHcal2Ecal = StandardPacker::fltPacked( pid.CaloNeutralHcal2Ecal() );
  ppid.CaloNeutralE19       = StandardPacker::fltPacked( pid.CaloNeutralE19() );
  ppid.CaloNeutralE49       = StandardPacker::fltPacked( pid.CaloNeutralE49() );
  ppid.CaloClusterCode      = pid.CaloClusterCode();
  ppid.CaloClusterFrac      = StandardPacker::fltPacked( pid.CaloClusterFrac() );
  ppid.Saturation           = pid.Saturation();
  ppid.IsNotH               = StandardPacker::probability( pid.IsNotH() );
  ppid.IsPhoton             = StandardPacker::probability( pid.IsPhoton() );
}

StatusCode NeutralPIDPacker::unpack( const PackedData& ppid, Data& pid, const PackedDataVector& ppids ) const {
  const auto ver = ppids.packingVersion();
  if ( !isSupportedVer( ver ) ) { return StatusCode::FAILURE; }
  // unpack the data
  pid.setCaloNeutralID( StandardPacker::caloCellIDPacked( ppid.CaloNeutralID ) );
  pid.setClusterMass( (float)StandardPacker::mass( ppid.ClusterMass ) );
  pid.setCaloNeutralEcal( (float)StandardPacker::energy( ppid.CaloNeutralEcal ) );
  pid.setCaloTrMatch( (float)StandardPacker::fltPacked( ppid.CaloTrMatch ) );
  pid.setShowerShape( (float)StandardPacker::fltPacked( ppid.ShowerShape ) );
  pid.setCaloNeutralHcal2Ecal( (float)StandardPacker::fltPacked( ppid.CaloNeutralHcal2Ecal ) );
  pid.setCaloNeutralE19( (float)StandardPacker::fltPacked( ppid.CaloNeutralE19 ) );
  pid.setCaloNeutralE49( (float)StandardPacker::fltPacked( ppid.CaloNeutralE49 ) );
  pid.setCaloClusterCode( ppid.CaloClusterCode );
  pid.setCaloClusterFrac( (float)StandardPacker::fltPacked( ppid.CaloClusterFrac ) );
  pid.setSaturation( ppid.Saturation );
  pid.setIsNotH( (float)StandardPacker::probability( ppid.IsNotH ) );
  pid.setIsPhoton( (float)StandardPacker::probability( ppid.IsPhoton ) );

  return StatusCode::SUCCESS;
}

StatusCode NeutralPIDPacker::unpack( const PackedDataVector& ppids, DataVector& pids ) const {
  const auto ver = ppids.packingVersion();
  if ( !isSupportedVer( ver ) ) { return StatusCode::FAILURE; }
  pids.reserve( ppids.data().size() );
  StatusCode sc = StatusCode::SUCCESS;
  for ( const auto& ppid : ppids.data() ) {
    // make new pid in container
    auto pid = std::make_unique<Data>();
    // Fill data from packed object
    auto sc2 = unpack( ppid, *pid, ppids );
    if ( sc.isSuccess() ) sc = sc2;
    // give to container
    pids.insert( pid.release(), ppid.key );
  }
  return sc;
}

StatusCode NeutralPIDPacker::check( const Data* dataA, const Data* dataB ) const {
  // checker
  const DataPacking::DataChecks ch( parent() );

  // assume OK from the start
  bool ok = true;

  ok &= ch.compareInts( "Key", dataA->key(), dataB->key() );
  ok &= ch.compareInts( "CaloNeutralID", dataA->CaloNeutralID(), dataB->CaloNeutralID() );
  ok &= ch.compareMasses( "ClusterMass", dataA->ClusterMass(), dataB->ClusterMass() );
  ok &= ch.compareEnergies( "CaloNeutralEcal", dataA->CaloNeutralEcal(), dataB->CaloNeutralEcal() );
  ok &= ch.compareFloats( "CaloTrMatch", dataA->CaloTrMatch(), dataB->CaloTrMatch() );
  ok &= ch.compareFloats( "CaloNeutralHcal2Ecal", dataA->CaloNeutralHcal2Ecal(), dataB->CaloNeutralHcal2Ecal() );
  ok &= ch.compareFloats( "ShowerShape", dataA->ShowerShape(), dataB->ShowerShape() );
  ok &= ch.compareFloats( "CaloNeutralE19", dataA->CaloNeutralE19(), dataB->CaloNeutralE19() );
  ok &= ch.compareFloats( "CaloNeutralE49", dataA->CaloNeutralE49(), dataB->CaloNeutralE49() );
  ok &= ch.compareInts( "CaloClusterCode", dataA->CaloClusterCode(), dataB->CaloClusterCode() );
  ok &= ch.compareFloats( "CaloClusterFrac", dataA->CaloClusterFrac(), dataB->CaloClusterFrac() );
  ok &= ch.compareInts( "Saturation", dataA->Saturation(), dataB->Saturation() );
  ok &= ch.compareFloats( "IsNotH", dataA->IsNotH(), dataB->IsNotH() );
  ok &= ch.compareFloats( "IsPhoton", dataA->IsPhoton(), dataB->IsPhoton() );

  // force printout for tests
  // ok = false;
  // If comparison not OK, print full information
  if ( !ok ) {
    const std::string loc =
        ( dataA->parent() && dataA->parent()->registry() ? dataA->parent()->registry()->identifier() : "Not in TES" );
    parent().warning() << "Problem with NeutralPID data packing :-" << endmsg << "  Original PID key=" << dataA->key()
                       << " in '" << loc << "'" << endmsg << dataA << endmsg << "  Unpacked PID" << endmsg << dataB
                       << endmsg;
  }

  return ( ok ? StatusCode::SUCCESS : StatusCode::FAILURE );
}

namespace LHCb {
  StatusCode unpack( Gaudi::Algorithm const* parent, const NeutralPIDPacker::PackedDataVector& in,
                     NeutralPIDPacker::DataVector& out ) {
    return NeutralPIDPacker{parent}.unpack( in, out );
  }
} // namespace LHCb
