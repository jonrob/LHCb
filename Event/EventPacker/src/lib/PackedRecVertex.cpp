/*****************************************************************************\
* (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/PackedRecVertex.h"
#include "Event/PackedEventChecks.h"
#include "GaudiKernel/DataObject.h"
#include "GaudiKernel/IRegistry.h"
#include <string>

namespace {
  /** Returns the full location of the given object in the Data Store
   *
   *  @param pObj Data object
   *
   *  @return Location of given data object
   */
  std::string location( const DataObject* pObj ) {
    return ( !pObj ? "Null DataObject !" : ( pObj->registry() ? pObj->registry()->identifier() : "UnRegistered" ) );
  }

  template <typename TYPE>
  static auto sqrt_or_0( const TYPE x ) {
    return ( x > TYPE( 0 ) ? std::sqrt( x ) : TYPE( 0 ) );
  }
} // namespace

using namespace LHCb;

void RecVertexPacker::pack( const Data& vert, PackedData& pvert, PackedDataVector& pverts ) const {
  pverts.setVersion( 2 );

  const auto ver = pverts.version();
  if ( !isSupportedVer( ver ) ) return;

  // Key
  pvert.key = vert.key();

  pvert.technique = vert.technique();
  pvert.chi2      = StandardPacker::fltPacked( vert.chi2() );
  pvert.nDoF      = vert.nDoF();
  pvert.x         = StandardPacker::position( vert.position().x() );
  pvert.y         = StandardPacker::position( vert.position().y() );
  pvert.z         = StandardPacker::position( vert.position().z() );

  // convariance Matrix
  const auto err0 = sqrt_or_0( vert.covMatrix()( 0, 0 ) );
  const auto err1 = sqrt_or_0( vert.covMatrix()( 1, 1 ) );
  const auto err2 = sqrt_or_0( vert.covMatrix()( 2, 2 ) );
  pvert.cov00     = StandardPacker::position( err0 );
  pvert.cov11     = StandardPacker::position( err1 );
  pvert.cov22     = StandardPacker::position( err2 );
  pvert.cov10     = StandardPacker::fraction( vert.covMatrix()( 1, 0 ), err1 * err0 );
  pvert.cov20     = StandardPacker::fraction( vert.covMatrix()( 2, 0 ), err2 * err0 );
  pvert.cov21     = StandardPacker::fraction( vert.covMatrix()( 2, 1 ), err2 * err1 );

  //== Store the Tracks and weights
  pvert.firstTrack = pverts.refs().size();
  if ( m_packTrackRefs ) {
    auto iW = vert.weights().begin();
    for ( auto itT = vert.tracks().begin(); vert.tracks().end() != itT; ++itT, ++iW ) {
      if ( *itT ) {
        pverts.refs().push_back( StandardPacker::reference64( &pverts, *itT ) );
        pverts.weights().push_back( StandardPacker::fraction( *iW ) );
      } else {
        parent().warning() << "Null Track SmartRef in '" + location( vert.parent() ) + "'" << endmsg;
      }
    }
  }
  pvert.lastTrack = pverts.refs().size();

  //== Handles the ExtraInfo
  pvert.firstInfo = pverts.extras().size();
  for ( const auto& [k, v] : vert.extraInfo() ) { pverts.addExtra( k, StandardPacker::fltPacked( v ) ); }
  pvert.lastInfo = pverts.extras().size();
}

StatusCode RecVertexPacker::unpack( const PackedData& pvert, Data& vert, const PackedDataVector& pverts,
                                    DataVector& verts ) const {
  const auto ver = pverts.version();
  if ( !isSupportedVer( ver ) ) return StatusCode::SUCCESS; // TODO: defined dedicated error code

  vert.setTechnique( static_cast<LHCb::RecVertex::RecVertexType>( pvert.technique ) );
  vert.setChi2AndDoF( StandardPacker::fltPacked( pvert.chi2 ), pvert.nDoF );
  vert.setPosition( Gaudi::XYZPoint( StandardPacker::position( pvert.x ), StandardPacker::position( pvert.y ),
                                     StandardPacker::position( pvert.z ) ) );

  // convariance Matrix
  const auto err0 = StandardPacker::position( pvert.cov00 );
  const auto err1 = StandardPacker::position( pvert.cov11 );
  const auto err2 = StandardPacker::position( pvert.cov22 );
  auto&      cov  = *( const_cast<Gaudi::SymMatrix3x3*>( &vert.covMatrix() ) );
  cov( 0, 0 )     = err0 * err0;
  cov( 1, 0 )     = err1 * err0 * StandardPacker::fraction( pvert.cov10 );
  cov( 1, 1 )     = err1 * err1;
  cov( 2, 0 )     = err2 * err0 * StandardPacker::fraction( pvert.cov20 );
  cov( 2, 1 )     = err2 * err1 * StandardPacker::fraction( pvert.cov21 );
  cov( 2, 2 )     = err2 * err2;

  //== Store the Tracks and weights
  auto unpack_ref = StandardPacker::UnpackRef{&pverts, &verts, StandardPacker::UnpackRef::Use32{0 == ver}};
  for ( auto kk = pvert.firstTrack; kk < pvert.lastTrack; ++kk ) {
    // Get the track
    if ( auto ref = unpack_ref( pverts.refs()[kk] ); ref ) {
      // If available, get the weight
      const float weight = ( (int)pverts.version() > 1 ? StandardPacker::fraction( pverts.weights()[kk] ) : 1.f );
      // save with weight
      vert.addToTracks( ref, weight );
    } else {
      parent().error() << "Corrupt RecVertex Track SmartRef detected." << endmsg;
    }
  }

  //== Handles the ExtraInfo
  for ( const auto& [k, v] : LHCb::Packer::subrange( pverts.extras(), pvert.firstInfo, pvert.lastInfo ) ) {
    vert.addInfo( k, StandardPacker::fltPacked( v ) );
  }

  return StatusCode::SUCCESS;
}

StatusCode RecVertexPacker::unpack( const PackedDataVector& pverts, DataVector& verts ) const {
  verts.reserve( pverts.data().size() );

  StatusCode sc = StatusCode::SUCCESS;
  for ( const auto& pvert : pverts.data() ) {
    // make and save new pid in container
    auto* vert = new Data();
    verts.insert( vert, pvert.key );

    // Fill data from packed object
    auto sc2 = unpack( pvert, *vert, pverts, verts );
    if ( sc.isSuccess() ) sc = sc2;
  }
  return sc;
}

StatusCode RecVertexPacker::check( const Data* dataA, const Data* dataB ) const {
  // checker
  const DataPacking::DataChecks ch( parent() );

  bool isOK = true;

  // key
  isOK &= ch.compareInts( "Key", dataA->key(), dataB->key() );
  isOK &= ch.compareFloats( "Chi2", dataA->chi2(), dataB->chi2() );
  isOK &= ch.compareInts( "nDOF", dataA->nDoF(), dataB->nDoF() );

  isOK &= ch.compareInts( "#Tracks", dataA->tracks().size(), dataB->tracks().size() );
  if ( isOK ) {
    for ( auto iC = std::make_pair( dataA->tracks().begin(), dataB->tracks().begin() );
          iC.first != dataA->tracks().end() && iC.second != dataB->tracks().end(); ++iC.first, ++iC.second ) {
      isOK &= ch.comparePointers( "Tracks", iC.first->target(), iC.second->target() );
    }
  }
  isOK &= ch.compareInts( "#Weights", dataA->weights().size(), dataB->weights().size() );

  // extra info
  isOK &= ch.compareInts( "#ExtraInfo", dataA->extraInfo().size(), dataB->extraInfo().size() );
  if ( isOK ) {
    for ( auto iE = std::make_pair( dataA->extraInfo().begin(), dataB->extraInfo().begin() );
          iE.first != dataA->extraInfo().end() && iE.second != dataB->extraInfo().end(); ++iE.first, ++iE.second ) {
      isOK &= ch.compareInts( "ExtraInfoKey", iE.first->first, iE.second->first );
      if ( isOK ) {
        if ( ( abs( iE.second->second ) < 1.e-7 && abs( iE.first->second ) > 1.e-7 ) || // both 0 ?
             ( abs( iE.second->second ) > 1.e-7 &&
               1.e-7 < std::abs( ( iE.second->second - iE.first->second ) / iE.second->second ) ) )
          isOK = false;
      }
    }
  }

  return ( isOK ? StatusCode::SUCCESS : StatusCode::FAILURE );
}

namespace LHCb {
  StatusCode unpack( Gaudi::Algorithm const* parent, const PackedRecVertices& in, RecVertices& out ) {
    return RecVertexPacker{parent}.unpack( in, out );
  }
} // namespace LHCb
