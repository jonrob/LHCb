/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "DetDesc/DetectorElement.h"
#include "Event/PackedProtoParticle.h"
#include "Event/ProtoParticle.h"
#include "GaudiAlg/FixTESPath.h"
#include "GaudiKernel/DataObjectHandle.h"
#include "Interfaces/IProtoParticleTool.h"
#include "LHCbAlgs/Consumer.h"

namespace LHCb::Obsolete {

  /**
   *  Unpack a protoparticle container
   *
   *  Note that the inheritance from Consumer is misleading. The algorithm is
   *  writing to TES, just via a Handle so that it can do it at the begining of
   *  the operator(), as unpacking references requires the target container to be
   *  in the TES.
   *
   *  @author Olivier Callot
   *  @date   2008-11-14
   */
  struct UnpackProtoParticle
      : Algorithm::Consumer<void( PackedProtoParticles const&, DetectorElement const& ),
                            Algorithm::Traits::usesBaseAndConditions<FixTESPath<Gaudi::Algorithm>, DetectorElement>> {

    UnpackProtoParticle( const std::string& name, ISvcLocator* pSvcLocator )
        : Consumer{name,
                   pSvcLocator,
                   {KeyValue{"InputName", PackedProtoParticleLocation::Charged},
                    KeyValue{"StandardGeometryTop", LHCb::standard_geometry_top}}} {}

    void operator()( LHCb::PackedProtoParticles const& dst, DetectorElement const& lhcb ) const override {
      if ( msgLevel( MSG::DEBUG ) )
        debug() << "Found " << dst.data().size() << " PackedProtoParticles at " << inputLocation<PackedProtoParticles>()
                << endmsg;

      // NOTE: the output container _must be on the TES_ prior to passing it to the
      //      unpacker, as otherwise filling the references to other objects does
      //      not work
      auto* newProtoParticles = m_protos.put( std::make_unique<ProtoParticles>() );

      // unpack
      ProtoParticlePacker{this}.unpack( dst, *newProtoParticles ).ignore();

      if ( msgLevel( MSG::DEBUG ) )
        debug() << "Created " << newProtoParticles->size() << " ProtoParticles at " << m_protos.fullKey() << endmsg;

      for ( auto& addInfo : m_addInfo ) ( *addInfo )( *newProtoParticles, *lhcb.geometry() ).ignore();
    }

    DataObjectWriteHandle<ProtoParticles> m_protos{this, "OutputName", ProtoParticleLocation::Charged};

    ToolHandleArray<Rec::Interfaces::IProtoParticles> m_addInfo{this, "AddInfo", {}};
  };

  DECLARE_COMPONENT_WITH_ID( UnpackProtoParticle, "UnpackProtoParticle" )

} // namespace LHCb::Obsolete
