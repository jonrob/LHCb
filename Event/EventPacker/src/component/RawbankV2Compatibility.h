/*****************************************************************************\
* (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include <algorithm>
#include <iterator>
#include <string_view>

namespace DataPacking::Buffer::v2_compatibility {
  inline auto match_first_with_missing_p_after_slash( std::string_view k ) {
    return [=]( const auto& i ) {
      // for 'v2' banks, the id stored is the '/p' version of the name
      // but here we want the actual (target) name...
      if ( i.first.size() != k.size() + 1 ) return false;
      auto [m, n] = std::mismatch( k.begin(), k.end(), i.first.begin() );
      if ( n == i.first.end() || *n != 'p' ) return false;
      if ( n == i.first.begin() || *std::prev( n ) != '/' ) return false;
      return std::mismatch( m, k.end(), std::next( n ) ).first == k.end();
    };
  }
  inline auto match_first_with_extra_p_after_slash( std::string_view k ) {
    return [=]( const auto& i ) {
      // k has the name with the '/p' in it, but we want the id of the location without the '/p'
      if ( i.first.size() + 1 != k.size() ) return false;
      auto [m, n] = std::mismatch( k.begin(), k.end(), i.first.begin() );
      if ( m == k.end() || *m != 'p' ) return false;
      if ( m == k.begin() || *std::prev( m ) != '/' ) return false;
      return std::mismatch( std::next( m ), k.end(), n ).second == i.first.end();
    };
  }

} // namespace DataPacking::Buffer::v2_compatibility
