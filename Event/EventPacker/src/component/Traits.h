/*****************************************************************************\
* (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/CaloAdc.h"
#include "Event/CaloCluster.h"
#include "Event/CaloClusters_v2.h"
#include "Event/CaloHypos_v2.h"
#include "Event/FlavourTag.h"
#include "Event/MCParticle.h"
#include "Event/PackedCaloAdc.h"
#include "Event/PackedCaloChargedInfo_v1.h"
#include "Event/PackedCaloCluster.h"
#include "Event/PackedCaloDigit.h"
#include "Event/PackedCaloHypo.h"
#include "Event/PackedFlavourTag.h"
#include "Event/PackedGlobalChargedPID.h"
#include "Event/PackedMuonPID.h"
#include "Event/PackedNeutralPID.h"
#include "Event/PackedPartToRelatedInfoRelation.h"
#include "Event/PackedParticle.h"
#include "Event/PackedPrimaryVertex.h"
#include "Event/PackedProtoParticle.h"
#include "Event/PackedRecSummary.h"
#include "Event/PackedRecVertex.h"
#include "Event/PackedRelations.h"
#include "Event/PackedRichPID.h"
#include "Event/PackedSharedObjectsContainer.h"
#include "Event/PackedTrack.h"
#include "Event/PackedTwoProngVertex.h"
#include "Event/PackedVertex.h"
#include "Event/PackedWeightsVector.h"
#include "Event/Particle.h"
#include "Event/PrimaryVertices.h"
#include "Event/RecSummary.h"
#include "Event/RelatedInfoMap.h"
#include "Event/Track_v3.h"
#include "Event/TwoProngVertex.h"
#include "Event/WeightsVector.h"
#include "GaudiKernel/SharedObjectsContainer.h"
#include "GaudiKernel/detected.h"
#include "RelationPackers.h"
#include "Relations/Relation1D.h"
#include "Relations/RelationWeighted1D.h"
#include "type_name.h"
#include <type_traits>

namespace LHCb::Packers::Traits {
  namespace details {

    template <typename T>
    struct is_selection : std::false_type {};
    template <typename T>
    struct is_selection<::SharedObjectsContainer<T>> : std::true_type {};

    template <typename...>
    struct concat;

    template <template <typename...> typename Tuple, typename... Ts>
    struct concat<Tuple<Ts...>> {
      using type = Tuple<Ts...>;
    };

    template <template <typename...> typename Tuple, typename... Us, typename... Vs, typename... Tuples>
    struct concat<Tuple<Us...>, Tuple<Vs...>, Tuples...> : concat<Tuple<Us..., Vs...>, Tuples...> {};

    template <typename... Ts>
    using concat_t = typename concat<Ts...>::type;

    template <typename, template <typename> typename>
    struct select;

    template <template <typename...> typename Tuple, template <typename> typename Predicate, typename... Ts>
    struct select<Tuple<Ts...>, Predicate> {
      using type = concat_t<std::conditional_t<Predicate<Ts>{}, Tuple<Ts>, Tuple<>>...>;
    };

    template <typename Tuple, template <typename> typename Predicate>
    using select_t = typename select<Tuple, Predicate>::type;

    /// Implementation of the detection idiom (negative, i.e. default, case).
    template <template <typename...> typename Tuple, typename, template <typename> class Fun, typename T>
    struct transform_helper {
      using type = Tuple<>;
    };

    /// Implementation of the detection idiom (positive case).
    template <template <typename...> typename Tuple, template <typename> class Fun, typename T>
    struct transform_helper<Tuple, std::void_t<Fun<T>>, Fun, T> {
      using type = Tuple<Fun<T>>;
    };

    template <typename, template <typename> typename>
    struct transform;

    template <template <typename...> typename Tuple, template <typename> typename Fun, typename... Ts>
    struct transform<Tuple<Ts...>, Fun> {
      using type = concat_t<typename transform_helper<Tuple, void, Fun, Ts>::type...>;
    };

    template <typename T>
    using is_not_void = std::negation<std::is_void<T>>;

    template <typename T>
    using remove_void = select_t<T, is_not_void>;

    template <template <typename...> typename T, template <typename> typename F, typename... Ts>
    using apply_helper = T<F<Ts>...>; // TODO: check whether F<Ts> is valid -- if not, skip

    template <typename T>
    using Container_t = typename T::Container;
    template <typename T>
    using Selection_t = typename T::Selection;
    template <typename T>
    using Contained_t = typename T::contained_type;
    template <typename T>
    using value_type_t = typename T::value_type;
    template <typename T>
    using Entry_t = typename T::Entry; // KeyedContainer has 'contained_type', relations have 'Entry'...

    template <typename D, typename P>
    struct packer_mapping {
      using Data            = D;
      using Packer          = P;
      using Container       = Gaudi::cpp17::detected_or_t<typename Packer::DataVector, Container_t, D>;
      using PackedContainer = typename Packer::PackedDataVector;
      using Selection       = Gaudi::cpp17::detected_or_t<void, Selection_t, D>;
      using PackedSelection = std::conditional_t<std::is_void_v<Selection>, void, PackedSharedObjectsContainer<D>>;
      constexpr inline static auto Name = Packer::propertyName();
      static_assert( std::is_same_v<Container, typename Packer::DataVector> );
    };

    template <typename D>
    struct soapacker_mapping {
      using Data            = D;
      using Container       = D;
      using PackedContainer = D;
      using Selection       = void;
      using PackedSelection = void;
      // using Packer          = P ; // we only need the packer for propertyName. there must be a better solution.
    };

    template <typename>
    struct Info_for_;
    template <>
    struct Info_for_<RecVertex> : packer_mapping<RecVertex, RecVertexPacker> {};
    template <>
    struct Info_for_<LHCb::Event::PV::PrimaryVertexContainer>
        : soapacker_mapping<LHCb::Event::PV::PrimaryVertexContainer> {
      constexpr inline static auto Name = "ExtendedPVs";
    };
    template <>
    struct Info_for_<Vertex> : packer_mapping<Vertex, VertexPacker> {};
    template <>
    struct Info_for_<PrimaryVertex> : packer_mapping<PrimaryVertex, PrimaryVertexPacker> {};
    template <>
    struct Info_for_<TwoProngVertex> : packer_mapping<TwoProngVertex, TwoProngVertexPacker> {};
    template <>
    struct Info_for_<RichPID> : packer_mapping<RichPID, RichPIDPacker> {};
    template <>
    struct Info_for_<MuonPID> : packer_mapping<MuonPID, MuonPIDPacker> {};
    template <>
    struct Info_for_<GlobalChargedPID> : packer_mapping<GlobalChargedPID, GlobalChargedPIDPacker> {};
    template <>
    struct Info_for_<NeutralPID> : packer_mapping<NeutralPID, NeutralPIDPacker> {};
    template <>
    struct Info_for_<Event::Calo::v1::CaloChargedPID>
        : packer_mapping<Event::Calo::v1::CaloChargedPID, CaloChargedPIDPacker> {};
    template <>
    struct Info_for_<Event::Calo::v1::BremInfo> : packer_mapping<Event::Calo::v1::BremInfo, BremInfoPacker> {};
    template <>
    struct Info_for_<ProtoParticle> : packer_mapping<ProtoParticle, ProtoParticlePacker> {};
    template <>
    struct Info_for_<Particle> : packer_mapping<Particle, ParticlePacker> {};
    template <>
    struct Info_for_<Track> : packer_mapping<Track, TrackPacker> {};
    template <>
    struct Info_for_<FlavourTag> : packer_mapping<FlavourTag, FlavourTagPacker> {};
    template <>
    struct Info_for_<CaloHypo> : packer_mapping<CaloHypo, CaloHypoPacker> {};
    template <>
    struct Info_for_<CaloCluster> : packer_mapping<CaloCluster, CaloClusterPacker> {};
    template <>
    struct Info_for_<CaloDigit> : packer_mapping<CaloDigit, CaloDigitPacker> {};
    template <>
    struct Info_for_<CaloAdc> : packer_mapping<CaloAdc, CaloAdcPacker> {};
    template <>
    struct Info_for_<WeightsVector> : packer_mapping<WeightsVector, WeightsVectorPacker> {};
    template <>
    struct Info_for_<RecSummary> : packer_mapping<RecSummary, RecSummaryPacker> {};
    template <>
    struct Info_for_<typename Relation1D<Particle, VertexBase>::Entry>
        : packer_mapping<typename Relation1D<Particle, VertexBase>::Entry, Packers::ParticleRelation<VertexBase>> {};
    template <>
    struct Info_for_<typename Relation1D<Particle, RecVertex>::Entry>
        : packer_mapping<typename Relation1D<Particle, RecVertex>::Entry, Packers::ParticleRelation<RecVertex>> {};
    template <>
    struct Info_for_<typename Relation1D<Particle, MCParticle>::Entry>
        : packer_mapping<typename Relation1D<Particle, MCParticle>::Entry, Packers::ParticleRelation<MCParticle>> {};
    template <>
    struct Info_for_<typename Relation1D<Particle, int>::Entry>
        : packer_mapping<typename Relation1D<Particle, int>::Entry, Packers::P2IntRelation> {};
    template <>
    struct Info_for_<typename Relation1D<Particle, RelatedInfoMap>::Entry>
        : packer_mapping<typename Relation1D<Particle, RelatedInfoMap>::Entry, Packers::P2InfoRelation> {};
    template <>
    struct Info_for_<typename LHCb::RelationWeighted1D<ProtoParticle, MCParticle, double>::Entry>
        : packer_mapping<typename LHCb::RelationWeighted1D<ProtoParticle, MCParticle, double>::Entry,
                         LHCb::Packers::PP2MCPRelation> {};

    template <typename T>
    struct contained_type_helper {
      using type = T;
    };
    template <typename T, typename... Ts>
    struct contained_type_helper<KeyedContainer<T, Ts...>> {
      using type = T;
    };
    template <typename T>
    struct contained_type_helper<::SharedObjectsContainer<T>> {
      using type = T;
    };
    template <typename... Ts>
    struct contained_type_helper<Relation1D<Ts...>> {
      using type = typename Relation1D<Ts...>::Entry;
    };
    template <typename... Ts>
    struct contained_type_helper<RelationWeighted1D<Ts...>> {
      using type = typename RelationWeighted1D<Ts...>::Entry;
    };

  } // namespace details

  template <typename T>
  using contained_type = typename details::contained_type_helper<T>::type;

  template <typename T>
  using container = typename details::Info_for_<contained_type<T>>::Container;

  template <typename T>
  using selection = typename details::Info_for_<contained_type<T>>::Selection;

  template <typename T>
  using packed_representation = std::conditional_t<details::is_selection<T>::value,
                                                   typename details::Info_for_<contained_type<T>>::PackedSelection,
                                                   typename details::Info_for_<contained_type<T>>::PackedContainer>;

  template <typename T>
  using packer = typename details::Info_for_<contained_type<T>>::Packer;

  template <typename T>
  using id_ = T;

  template <typename T>
  const char* containerName() {
    if constexpr ( details::is_selection<T>::value ) {
      static const std::string s = std::string{details::Info_for_<contained_type<T>>::Name}.append( "Selection" );
      return s.c_str();
    } else {
      return details::Info_for_<contained_type<T>>::Name;
    }
  }

  // relations are last, as we may want to prune entries where the 'from' side is not packed
  // so anything appearing as 'from' must be known at that point
  template <template <typename...> typename T, template <typename> typename Fun = id_>
  using Expand = details::apply_helper<
      T, Fun, RecVertex, PrimaryVertex, LHCb::Event::PV::PrimaryVertexContainer, Vertex, TwoProngVertex, RichPID,
      MuonPID, GlobalChargedPID, NeutralPID, Event::Calo::v1::CaloChargedPID, Event::Calo::v1::BremInfo, ProtoParticle,
      Particle, Track, FlavourTag, CaloHypo, CaloCluster, CaloDigit, CaloAdc, WeightsVector, RecSummary,
      Relation1D<Particle, VertexBase>::Entry, Relation1D<Particle, RecVertex>::Entry,
      Relation1D<Particle, MCParticle>::Entry, RelationWeighted1D<ProtoParticle, MCParticle, double>::Entry,
      Relation1D<Particle, int>::Entry, Relation1D<Particle, RelatedInfoMap>::Entry>;

  //  T<F<Ts>...>, but with the
  //  is_void_v<F<Ts>> cases removed
  // TODO: remove the 'void' bit in F<Ts>, and just pick the ones for which F<Ts> is valid...
  template <template <typename...> typename T, template <typename> typename Fun = id_>
  using Gather = details::remove_void<Expand<T, Fun>>;

  template <typename... Ts>
  struct TypeList;

  template <template <typename...> typename F, typename T>
  struct distribute_helper;

  template <template <typename...> typename F, typename... Ts>
  struct distribute_helper<F, TypeList<Ts...>> {
    using type = F<Ts...>;
  };

  template <template <typename...> typename F, typename TL>
  using distribute_t = typename distribute_helper<F, TL>::type;

  using SelectionTypes = Gather<TypeList, Traits::selection>;
  using ContainerTypes = Gather<TypeList, Traits::container>;

  template <template <typename...> typename T>
  using expand_t = distribute_t<T, Traits::details::concat_t<SelectionTypes, ContainerTypes>>;

#if 1
  static_assert(
      std::is_same_v<
          expand_t<std::tuple>,
          std::tuple<RecVertex::Selection, PrimaryVertex::Selection, Vertex::Selection, TwoProngVertex::Selection,
                     RichPID::Selection, MuonPID::Selection, GlobalChargedPID::Selection, NeutralPID::Selection,
                     Event::Calo::v1::CaloChargedPID::Selection, Event::Calo::v1::BremInfo::Selection,
                     ProtoParticle::Selection, Particle::Selection, Track::Selection, FlavourTag::Selection,
                     CaloHypo::Selection, CaloCluster::Selection, RecVertex::Container, PrimaryVertex::Container,
                     LHCb::Event::PV::PrimaryVertexContainer, Vertex::Container, TwoProngVertex::Container,
                     RichPID::Container, MuonPID::Container, GlobalChargedPID::Container, NeutralPID::Container,
                     Event::Calo::v1::CaloChargedPID::Container, Event::Calo::v1::BremInfo::Container,
                     ProtoParticle::Container, Particle::Container, Track::Container, FlavourTag::Container,
                     CaloHypo::Container, CaloCluster::Container, CaloDigit::Container, CaloAdc::Container,
                     WeightsVector::Container, RecSummary, Relation1D<Particle, VertexBase>,
                     Relation1D<Particle, RecVertex>, Relation1D<Particle, MCParticle>,
                     RelationWeighted1D<ProtoParticle, MCParticle, double>, Relation1D<Particle, int>,
                     Relation1D<Particle, RelatedInfoMap>>> );

#else

  template <typename T>
  int fun() {
    std::cout << "\n\n\n\\n*********************************************************\n";
    std::cout << "                      " << type_name<T>();
    std::cout << "\n\n\n\\n*********************************************************\n";
    return 0;
  }

  static int iiii = fun<input_t>();

  inline int oops() {
    std::cout << "\n\n\n\\n*********************************************************\n";
    std::cout << "RelationWeighted1D<Particle,RecVertex,double>::guid = "
              << RelationWeighted1D<Particle, RecVertex, double>::GUID() << std::endl;
    std::cout << "Relation1D<Particle,RecVertex>::GUID = " << Relation1D<Particle, RecVertex>::GUID() << std::endl;
    std::cout << "\n\n\n\\n*********************************************************\n";
    return 0;
  }

  static int jjjjj = oops();

#endif

} // namespace LHCb::Packers::Traits
