/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "Event/Track.h"

#include "Gaudi/Accumulators.h"
#include "LHCbAlgs/Consumer.h"

#include <string>

namespace LHCb {

  /**
   *  Dump all tracks in an event.
   *  Amount printed depends on OutputLevel:
   *    INFO: prints size of container
   *   DEBUG: prints also first "NumberOfObjectsToPrint" Tracks and States
   *          (default is 5)
   * VERBOSE: prints all Tracks and States
   *
   *  @author Marco Cattaneo
   *  @date   2004-07-14
   */
  struct DumpTracks : Algorithm::Consumer<void( Tracks const& )> {

    DumpTracks( const std::string& name, ISvcLocator* pSvcLocator )
        : Consumer{name, pSvcLocator, {KeyValue{"TracksLocation", TrackLocation::Default}}} {}

    void operator()( Tracks const& ) const override;

    Gaudi::Property<unsigned int> m_numObjects{this, "NumberOfObjectsToPrint", 5, "Number of objects to print"};
    mutable Gaudi::Accumulators::StatCounter<unsigned long> m_tracks{this, "#Tracks"};
  };

} // namespace LHCb

DECLARE_COMPONENT_WITH_ID( LHCb::DumpTracks, "DumpTracks" )

void LHCb::DumpTracks::operator()( LHCb::Tracks const& tracksCont ) const {
  info() << "There are " << tracksCont.size() << " tracks in " << inputLocation<Tracks>() << endmsg;
  m_tracks += tracksCont.size();

  if ( msgLevel( MSG::DEBUG ) ) {
    unsigned int numPrinted = 0;
    for ( const Track* track : tracksCont ) {
      if ( !msgLevel( MSG::VERBOSE ) && ++numPrinted > m_numObjects ) break;
      debug() << *track << endmsg;
    }
  }
}
