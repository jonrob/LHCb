###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#[=======================================================================[.rst:
Event/EventSys
--------------
#]=======================================================================]

find_package(RELAX REQUIRED COMPONENTS CLHEP HepMC STL Math)

# Add RELAX library dirs to LD_LIBRARY_PATH
foreach(libdir IN LISTS RELAX_LIBRARY_DIRS)
    set_property(
        TARGET target_runtime_paths
        APPEND PROPERTY runtime_ld_library_path $<SHELL_PATH:${libdir}>)
endforeach()

# Merge RELAX rootmap files into the global project rootmap using a
# function from GaudiToolbox.cmake
add_custom_target(RelaxRootmap) # ALL DEPENDS ${RELAX_ROOTMAPS})

set(merge_target ${PROJECT_NAME}_MergeRootmaps)
# compatibility with Gaudi without gaudi/Gaudi!1308
if(TARGET MergeRootmaps)
    set(merge_target MergeRootmaps)
endif()

foreach(rootmap IN LISTS RELAX_ROOTMAPS)
    _merge_files(
        ${merge_target} "${CMAKE_BINARY_DIR}/${PROJECT_NAME}Dict.rootmap"
        RelaxRootmap "${rootmap}"
        "Merging .rootmap files"
    )
endforeach()
