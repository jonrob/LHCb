/*****************************************************************************\
* (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "Detector/FT/FTChannelID.h"
#include <cstdint>
#include <string>
#include <vector>

namespace LHCb {

  namespace FTErrorLocation {
    inline const std::string Default = "Raw/FT/FTErrors";
  }

  /** @struct FTErrorCounter FTErrorCounter.h
   *
   * This class represents the errors counted per sourceID
   *
   */
  struct FTError {
    using FTErrors = std::vector<FTError>;

    std::uint32_t runNumber;
    std::uint16_t sourceID;

    std::uint16_t station;
    std::uint16_t layer;
    std::uint16_t quarter;

    std::uint16_t board;
    std::uint16_t port;
  };

} // namespace LHCb
