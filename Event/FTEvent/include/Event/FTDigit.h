/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

// Include files
#include "Detector/FT/FTChannelID.h"
#include "Kernel/MultiIndexedContainer.h"
#include "Kernel/SiDataFunctor.h"

// Forward declarations

namespace LHCb {

  // Forward declarations
  // Namespace for locations in TDS
  namespace FTDigitLocation {
    inline const std::string Default = "Raw/FT/FTDigits";
  }

  /** @class FTDigit FTDigit.h
   *
   * This class represents the ADC charge collected per FTChannelID
   *
   * @author Lex Greeven, Sevda Esen
   *
   */

  class FTDigit final {
  public:
    /// channelID type
    typedef Detector::FTChannelID chan_type;
    /// finding policy
    typedef SiDataFunctor::CompareByChannel<LHCb::FTDigit> findPolicy;
    /// fast container of FTDigits
    typedef Container::MultiIndexedContainer<LHCb::FTDigit, 48> FTDigits;

    /// Constructor
    FTDigit( Detector::FTChannelID chan, int adc ) : m_adcCount( adc ), m_channelID( chan ) {}

    /// Default Constructor
    FTDigit() = default;

    /// Returns the FTChannelID
    Detector::FTChannelID channelID() const { return m_channelID; }

    /// Retrieve constADC Counts (2bit ADC PE threshold check) for ChannelID
    int adcCount() const { return m_adcCount; }

  private:
    int                   m_adcCount{0}; ///< ADC Counts (2bit ADC PE threshold check) for ChannelID
    Detector::FTChannelID m_channelID;

  }; // class FTDigit

} // namespace LHCb

// -----------------------------------------------------------------------------
// end of class
// -----------------------------------------------------------------------------
