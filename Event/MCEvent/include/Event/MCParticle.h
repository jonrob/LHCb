/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

// Include files
#include "GaudiKernel/KeyedContainer.h"
#include "GaudiKernel/KeyedObject.h"
#include "GaudiKernel/NamedRange.h"
#include "GaudiKernel/SharedObjectsContainer.h"
#include "GaudiKernel/SmartRef.h"
#include "GaudiKernel/SmartRefVector.h"
#include "GaudiKernel/SystemOfUnits.h"
#include "GaudiKernel/Vector4DTypes.h"
#include "Kernel/ParticleID.h"
#include "Kernel/RelationObjectTypeTraits.h"
#include "LHCbMath/MatVec.h"
#include <optional>
#include <ostream>
#include <vector>

// Forward declarations

namespace LHCb {

  // Forward declarations
  class MCVertex;

  // Class ID definition
  static const CLID CLID_MCParticle = 210;

  // Namespace for locations in TDS
  namespace MCParticleLocation {
    inline const std::string Default = "MC/Particles";
  }

  /** @class MCParticle MCParticle.h
   *
   * The Monte Carlo particle kinematics information
   *
   * @author Gloria Corti, revised by P. Koppenburg
   *
   */

  class MCParticle final : public KeyedObject<int> {
    /// Bitmasks for bitfield flags
    enum struct FlagMask : unsigned int { fromSignal = 0x1, _reserved = 0xe, fromReDecay = 0x10 };

    template <FlagMask mask>
    [[nodiscard]] static constexpr unsigned int extract( unsigned int i ) {
      constexpr auto b = __builtin_ctz( static_cast<unsigned int>( mask ) ); // FIXME: C++20 replace __builtin_ctz with
                                                                             // std::countr_zero
      return ( i & static_cast<unsigned int>( mask ) ) >> b;
    }

    template <FlagMask mask, typename T>
    constexpr LHCb::MCParticle& set( T val ) {
      constexpr auto b = __builtin_ctz( static_cast<unsigned int>( mask ) ); // FIXME: C++20 replace __builtin_ctz with
                                                                             // std::countr_zero
      auto v = ( static_cast<unsigned int>( val ) << static_cast<unsigned int>( b ) );
      assert( ( v & static_cast<unsigned int>( mask ) ) == v ); // make sure 'val' is not large...
      m_flags &= ~static_cast<unsigned int>( mask );
      m_flags |= v;
      return *this;
    }

  public:
    /// typedef for std::vector of MCParticle
    using Vector      = std::vector<MCParticle*>;
    using ConstVector = std::vector<const MCParticle*>;

    /// typedef for KeyedContainer of MCParticle
    using Container = KeyedContainer<MCParticle, Containers::HashMap>;
    /// The container type for shared MCParticles (without ownership)
    using Selection = SharedObjectsContainer<MCParticle>;
    /// For uniform access to containers in TES (KeyedContainer,SharedContainer)
    using Range = Gaudi::NamedRange_<ConstVector>;

    /// const MCParticle keyed container
    using ConstContainer = KeyedContainer<const MCParticle, Containers::HashMap>;

    /// Copy Constructor[Attention: the key is NOT copied!]
    MCParticle( const LHCb::MCParticle& right )
        : KeyedObject()
        , m_momentum( right.m_momentum )
        , m_particleID( right.m_particleID )
        , m_flags( right.m_flags )
        , m_originVertex( right.m_originVertex )
        , m_endVertices( right.m_endVertices ) {}

    /// Default Constructor
    MCParticle() = default;

    // Retrieve pointer to class definition structure
    const CLID&        clID() const override { return LHCb::MCParticle::classID(); }
    static const CLID& classID() { return CLID_MCParticle; }

    /// Clone this MCParticle. Returns a pointer to a new MCParticle (user must take ownership)
    [[nodiscard]] MCParticle* clone() const { return new LHCb::MCParticle( *this ); }

    /// Clone this MCParticle including key. Returns a pointer to a new MCParticle (user must take ownership)
    [[nodiscard]] MCParticle* cloneWithKey() const {
      auto* clone = new LHCb::MCParticle( *this );
      clone->setKey( key() );
      return clone;
    }

    /// Describe if a particle has oscillated
    [[nodiscard]] bool hasOscillated() const;

    /// Retrieve virtual mass, i.e. sqrt(E^2-P^2)
    [[nodiscard]] double virtualMass() const { return m_momentum.M(); }

    /// Short-cut to pt value
    [[nodiscard]] double pt() const { return m_momentum.Pt(); }

    /// short cut for |P|
    [[nodiscard]] double p() const { return m_momentum.R(); }

    /// Pointer to parent particle
    [[nodiscard]] const LHCb::MCParticle* mother() const;

    /// Beta of the particle
    [[nodiscard]] double beta() const {
      const auto e = m_momentum.E();
      return e > 0. ? m_momentum.R() / e : 1.;
    }

    /// Gamma of the particle
    [[nodiscard]] double gamma() const {
      const auto m = m_momentum.M();
      return m > 0. ? m_momentum.E() / m : -999.;
    }

    /// Beta x Gamma of the particle
    [[nodiscard]] double betaGamma() const {
      const auto m2 = m_momentum.M2();
      return m2 > 0. ? std::sqrt( m_momentum.P2() / m2 ) : -999.;
    }

    /// Pseudorapidity of the particle
    [[nodiscard]] double pseudoRapidity() const { return m_momentum.eta(); }

    /// Get primary vertex up the decay tree
    [[nodiscard]] const LHCb::MCVertex* primaryVertex() const;

    /// Print this MCParticle in a human readable way
    std::ostream& fillStream( std::ostream& s ) const override;

    /// Retrieve const  4-momentum-vector
    [[nodiscard]] const Gaudi::LorentzVector& momentum() const { return m_momentum; }

    /// Update  4-momentum-vector
    MCParticle& setMomentum( Gaudi::LorentzVector value ) {
      m_momentum = std::move( value );
      return *this;
    }

    /// Retrieve const  Particle ID
    [[nodiscard]] const LHCb::ParticleID& particleID() const { return m_particleID; }

    /// Update  Particle ID
    MCParticle& setParticleID( LHCb::ParticleID value ) {
      m_particleID = std::move( value );
      return *this;
    }

    /// Retrieve const  Bit-packed information on how this MCParticle was produced
    [[nodiscard]] unsigned int flags() const { return m_flags; }

    /// Update  Bit-packed information on how this MCParticle was produced
    MCParticle& setFlags( unsigned int value ) {
      m_flags = value;
      return *this;
    }

    /** Retrieve Flag to indicate if this particle was produced as part of the signal decay tree
        WARNING: This method only works for signal decays produced with EvtGen.
        This for example excludes decays like Z -> e e.
    */
    [[nodiscard]] bool fromSignal() const { return extract<FlagMask::fromSignal>( m_flags ) != 0; }

    /// Update Flag to indicate if this particle was produced as part of the signal decay tree
    MCParticle& setFromSignal( bool value ) { return set<FlagMask::fromSignal>( value ); }

    /// Retrieve Reserved flags to extend fromSignal in the future (currently not used!)
    [[nodiscard]] unsigned int _reservedFlags() const { return extract<FlagMask::_reserved>( m_flags ); }

    /// Update Reserved flags to extend fromSignal in the future (currently not used!)
    MCParticle& set_reservedFlags( unsigned int value ) { return set<FlagMask::_reserved>( value ); }

    /// Retrieve Flag to indicate if this particle is in the ReDecay part of the event
    [[nodiscard]] bool fromReDecay() const { return extract<FlagMask::fromReDecay>( m_flags ) != 0; }

    /// Update Flag to indicate if this particle is in the ReDecay part of the event
    MCParticle& setFromReDecay( bool value ) { return set<FlagMask::fromReDecay>( value ); }

    /// Retrieve (const)  Pointer to origin vertex
    [[nodiscard]] const LHCb::MCVertex* originVertex() const { return m_originVertex; }

    /// Update  Pointer to origin vertex
    MCParticle& setOriginVertex( SmartRef<LHCb::MCVertex> value ) {
      m_originVertex = std::move( value );
      return *this;
    }

    /// Retrieve (const)  Vector of pointers to decay vertices
    [[nodiscard]] const SmartRefVector<LHCb::MCVertex>& endVertices() const { return m_endVertices; }

    [[nodiscard]] const MCVertex* goodEndVertex() const;

    /// Update  Vector of pointers to decay vertices
    MCParticle& setEndVertices( SmartRefVector<LHCb::MCVertex> value ) {
      m_endVertices = std::move( value );
      return *this;
    }

    /// Add to  Vector of pointers to decay vertices
    MCParticle& addToEndVertices( SmartRef<LHCb::MCVertex> value ) {
      m_endVertices.push_back( std::move( value ) );
      return *this;
    }

    /// Remove from  Vector of pointers to decay vertices
    MCParticle& removeFromEndVertices( const SmartRef<LHCb::MCVertex>& value ) {
      auto i = std::remove( m_endVertices.begin(), m_endVertices.end(), value );
      m_endVertices.erase( i, m_endVertices.end() );
      return *this;
    }

    /// Clear  Vector of pointers to decay vertices
    MCParticle& clearEndVertices() {
      m_endVertices.clear();
      return *this;
    }

    friend std::ostream& operator<<( std::ostream& str, const MCParticle& obj ) { return obj.fillStream( str ); }

    // opt-in for ThOr functors

    friend auto threeMomentum( const MCParticle& p ) {
      auto const& mom = p.momentum();
      return LinAlg::Vec<SIMDWrapper::scalar::float_v, 3>{mom.px(), mom.py(), mom.pz()};
    }

    friend auto slopes( const MCParticle& p ) {
      auto const& mom = p.momentum();
      return LinAlg::Vec<SIMDWrapper::scalar::float_v, 3>{mom.px() / mom.pz(), mom.py() / mom.pz(),
                                                          1.0f}; // note: backwards tracks have negated slopes
    }

    friend auto fourMomentum( const MCParticle& p ) {
      auto const& mom = p.momentum();
      return LinAlg::Vec<SIMDWrapper::scalar::float_v, 4>{mom.px(), mom.py(), mom.pz(), mom.e()};
    }

    friend LinAlg::Vec<SIMDWrapper::scalar::float_v, 3> referencePoint( const MCParticle& p );

    friend LinAlg::Vec<SIMDWrapper::scalar::float_v, 3> endVertexPos( const MCParticle& p );

    friend auto mass2( const MCParticle& p ) { return p.m_momentum.mass2(); }

    friend auto pid( const MCParticle& p ) { return p.particleID().pid(); }

  private:
    Gaudi::LorentzVector           m_momentum{0.0, 0.0, 0.0, -1 * Gaudi::Units::GeV}; ///< 4-momentum-vector
    LHCb::ParticleID               m_particleID{0};                                   ///< Particle ID
    unsigned int                   m_flags{0};     ///< Bit-packed information on how this MCParticle was produced
    SmartRef<LHCb::MCVertex>       m_originVertex; ///< Pointer to origin vertex
    SmartRefVector<LHCb::MCVertex> m_endVertices;  ///< Vector of pointers to decay vertices

  }; // class MCParticle

  /// Definition of Keyed Container for MCParticle
  using MCParticles = MCParticle::Container;

} // namespace LHCb

// -----------------------------------------------------------------------------
// end of class
// -----------------------------------------------------------------------------

// Including forward declarations
#include "MCVertex.h"

inline const LHCb::MCParticle* LHCb::MCParticle::mother() const {
  return originVertex() ? originVertex()->mother() : nullptr;
}

inline const LHCb::MCVertex* LHCb::MCParticle::primaryVertex() const {
  return originVertex() ? originVertex()->primaryVertex() : nullptr;
}

namespace Relations {
  template <typename>
  struct ObjectTypeTraits;

  template <>
  struct ObjectTypeTraits<LHCb::MCParticle> : KeyedObjectTypeTraits<LHCb::MCParticle> {};
} // namespace Relations
