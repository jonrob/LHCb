/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/CaloChargedInfo_v1.h"
#include "Event/ProtoParticle.h"
#include "GaudiAlg/GaudiTool.h"
#include "Interfaces/IProtoParticleTool.h"
#include "Relations/Relation2D.h"
#include <map>
#include <memory>
#include <string>
#include <vector>

/** @class ChargedProtoParticleAddCaloInfo ChargedProtoParticleAddCaloInfo.h
 *
 *  Adds calo info to protoparticles used for v3 to v1 conversion
 *
 */

namespace LHCb::Rec::ProtoParticle::Charged {

  namespace v2 {

    template <typename CaloPID>
    using TrackToCaloPID   = typename std::map<const Track*, CaloPID const*>;
    using ID2CaloHypoTable = LHCb::Relation2D<LHCb::Detector::Calo::CellID, LHCb::CaloHypo>;

    // base class
    template <typename CaloPID>
    class AddCaloInfoBase : public extends<GaudiTool, Interfaces::IProtoParticles> {

    public:
      using extends::extends;
      using CaloPIDs = typename CaloPID::Container;

      // initialize inputs
      StatusCode initialize() override {
        auto sc = GaudiTool::initialize();
        if ( !sc ) return sc;
        // declare inputs
        int i = 0;
        for ( auto input : m_inputs ) {
          m_inputhandles.push_back( std::make_unique<DataObjectReadHandle<CaloPIDs>>( input, this ) );
          declareProperty( "input_" + std::to_string( i ), *( m_inputhandles.back() ) );
          i++;
        }
        return sc;
      }

    protected:
      /// Load the pid/track relations in a map
      TrackToCaloPID<CaloPID> getPIDsMap() const {
        TrackToCaloPID<CaloPID> map{};
        for ( auto const& input : m_inputhandles ) {
          CaloPIDs const* pids = input->get();
          for ( auto const& pid : *pids ) {
            if ( pid->idTrack() ) { map[pid->idTrack()] = pid; }
          }
        }
        return map;
      };

      // to persist CaloHypo object for (saved) CellID of photon/electron hypo
      void addCaloHypo( ID2CaloHypoTable const& table, LHCb::Detector::Calo::CellID cellid, LHCb::ProtoParticle* proto,
                        bool report_missing = true ) const {
        if ( !cellid ) return;
        auto rels = table.relations( cellid );
        if ( rels.size() == 1 ) {
          proto->addToCalo( static_cast<LHCb::CaloHypo const*>( rels.front() ) );
        } else {
          if ( report_missing ) ++m_errnocalohypos;
        }
      };

    private:
      Gaudi::Property<std::vector<std::string>>                    m_inputs{this, "InputPIDs", {}, ""};
      std::vector<std::unique_ptr<DataObjectReadHandle<CaloPIDs>>> m_inputhandles;

      mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_errnocalohypos{
          this, "not the right number of associated CaloHypo objects found for registered CellID"};
    };

    //=============================================================================
    // specific implementations
    //=============================================================================
    using namespace LHCb::Event::Calo::v1;

    // Ecal info
    class AddCaloInfo final : public AddCaloInfoBase<CaloChargedPID> {
    public:
      // constructor
      AddCaloInfo( std::string const& type, std::string const& name, IInterface const* parent )
          : AddCaloInfoBase<CaloChargedPID>::AddCaloInfoBase( type, name, parent ) {}

      // main execution
      StatusCode operator()( ProtoParticles& protos, IGeometryInfo const& ) const override {
        // get electron calohypos
        auto const& etable = *m_calohypotable.get();
        // make map
        auto map = getPIDsMap();
        // update all relevant protoparticles
        for ( auto proto : protos ) {
          // remove current info
          proto->removeCaloEcalInfo();
          proto->removeCaloHcalInfo();
          // find pp with track
          if ( !proto->track() ) continue;

          if ( m_useTrackAncestor && !( m_trackAncestorIndex < proto->track()->ancestors().size() ) ) {
            throw GaudiException( "Requesting to relate a track ancestor that is not there", "LinkedTo",
                                  StatusCode::FAILURE );
          }
          auto trackToRelate =
              m_useTrackAncestor ? proto->track()->ancestors()[m_trackAncestorIndex].data() : proto->track();

          auto relation = map.find( trackToRelate );
          if ( map.end() == relation ) {
            if ( msgLevel( MSG::VERBOSE ) ) verbose() << " -> NO associated CaloPID object found" << endmsg;
            continue;
          }

          auto const* pid = relation->second;
          // skip if not in acceptance
          if ( !( pid->InEcal() || pid->InHcal() ) ) continue;
          // add relevant CaloHypo
          auto cellid = pid->ElectronID();
          addCaloHypo( etable, cellid, proto );
          // reference to CaloChargedPID object
          proto->setCaloChargedPID( pid );
        }
        return StatusCode::SUCCESS;
      }

    private:
      DataObjectReadHandle<ID2CaloHypoTable> m_calohypotable{this, "ID2CaloHypoTable", "",
                                                             "Table relating cellids to CaloHypos"};
      Gaudi::Property<bool>                  m_useTrackAncestor{this, "UseTrackAncestor", false, ""};
      Gaudi::Property<unsigned int>          m_trackAncestorIndex{this, "TrackAncestorIndex", 0, ""};
    };

    // Brem info
    class AddBremInfo final : public AddCaloInfoBase<BremInfo> {
    public:
      // constructor
      AddBremInfo( std::string const& type, std::string const& name, IInterface const* parent )
          : AddCaloInfoBase<BremInfo>::AddCaloInfoBase( type, name, parent ) {}

      // main execution
      StatusCode operator()( ProtoParticles& protos, IGeometryInfo const& ) const override {
        // get photon calohypos
        auto const& phtable = *m_calohypotable.get();
        // make map
        auto map = getPIDsMap();
        // update all relevant protoparticles
        for ( auto proto : protos ) {
          // remove current info
          proto->removeCaloBremInfo();
          // find pp with track
          if ( !proto->track() ) continue;
          auto relation = map.find( proto->track() );
          if ( map.end() == relation ) {
            if ( msgLevel( MSG::VERBOSE ) ) verbose() << " -> NO associated CaloPID object found" << endmsg;
            continue;
          }

          auto const* pid = relation->second;
          // skip if not in acceptance
          if ( !pid->InBrem() ) continue;
          // add relevant CaloHypo
          auto cellid = pid->BremHypoID();
          addCaloHypo( phtable, cellid, proto );
          // reference to BremInfo object
          proto->setBremInfo( pid );
        }
        return StatusCode::SUCCESS;
      }

    private:
      DataObjectReadHandle<ID2CaloHypoTable> m_calohypotable{this, "ID2CaloHypoTable", "",
                                                             "Table relating cellids to CaloHypos"};
    };

    // (re)add CaloHypo info
    class AddHypos final : public AddCaloInfoBase<CaloChargedPID> {
    public:
      // constructor
      AddHypos( std::string const& type, std::string const& name, IInterface const* parent )
          : AddCaloInfoBase<CaloChargedPID>::AddCaloInfoBase( type, name, parent ) {}

      // main execution
      StatusCode operator()( ProtoParticles& protos, IGeometryInfo const& ) const override {
        // get electron and photon calohypos
        LHCb::CaloHypos const& electrons = *m_electrons.get();
        LHCb::CaloHypos const& photons   = *m_photons.get();
        auto const             etable    = getHypoTable( electrons );
        auto const             ptable    = getHypoTable( photons );
        // update all relevant protoparticles
        for ( auto proto : protos ) {
          // find pp with track
          if ( !proto->track() ) continue;
          // remove current info
          proto->clearCalo( LHCb::CaloHypo::Hypothesis::Electron );
          proto->clearCalo( LHCb::CaloHypo::Hypothesis::Photon );
          // add relevant CaloHypo
          auto cellid_ecal =
              proto->caloChargedPID() ? proto->caloChargedPID()->ElectronID() : LHCb::Detector::Calo::CellID{};
          auto cellid_brem = proto->bremInfo() ? proto->bremInfo()->BremHypoID() : LHCb::Detector::Calo::CellID{};
          if ( cellid_ecal ) addCaloHypo( etable, cellid_ecal, proto, m_report_missing );
          if ( cellid_brem ) addCaloHypo( ptable, cellid_brem, proto, m_report_missing );
        }
        return StatusCode::SUCCESS;
      }

    private:
      Gaudi::Property<bool> m_report_missing{
          this, "ReportMissing", true,
          "Report missing/empty relation from registered cellid to (stored/packed) calohypo"};

      DataObjectReadHandle<LHCb::CaloHypos> m_electrons{this, "ElectronHypos", ""};
      DataObjectReadHandle<LHCb::CaloHypos> m_photons{this, "PhotonHypos", ""};

      ID2CaloHypoTable getHypoTable( LHCb::CaloHypos const& hypos ) const {
        ID2CaloHypoTable table;
        table.reserve( hypos.size() );
        for ( auto const hypo : hypos ) {
          auto clusters = hypo->clusters();
          if ( clusters.empty() ) continue;
          if ( !clusters.front() ) continue;
          table.i_push( clusters.front()->seed(), hypo );
        }
        table.i_sort();
        return table;
      }
    };

  } // namespace v2

  DECLARE_COMPONENT_WITH_ID( v2::AddCaloInfo, "ChargedProtoParticleAddCaloInfo" )
  DECLARE_COMPONENT_WITH_ID( v2::AddBremInfo, "ChargedProtoParticleAddBremInfo" )
  DECLARE_COMPONENT_WITH_ID( v2::AddHypos, "ChargedProtoParticleAddCaloHypos" )

} // namespace LHCb::Rec::ProtoParticle::Charged
