/*****************************************************************************\
* (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

// Include files
#include "Detector/VP/VPChannelID.h"
#include "Kernel/VPConstants.h"

#include <ostream>
#include <vector>

// This compile guard is present for gcc12 + lcg 104/105
// support for ROOT.
// See https://gitlab.cern.ch/lhcb/LHCb/-/issues/342
#ifndef __CLING__
#  include "Kernel/MultiIndexedContainer.h"
#endif

/**
 * VPMicroClusters are the bare minimal representation of the information
 * needed to build a 'VPLightCluster', the type of cluster needed to fit
 * tracks. These clusters priarily exists for reasons of persistency.
 *
 * @author Laurent Dufour <laruent.dufour@cern.ch>
 **/
namespace LHCb {

  // Namespace for locations in TDS
  namespace VPClusterLocation {
    inline const std::string Micro = "Raw/VP/MicroClusters";
  }

  class VPMicroCluster final {
  public:
    /// Default Constructor, used by packing/unpacking
    VPMicroCluster() = default;

    /// Constructor
    VPMicroCluster( const unsigned char xfraction, const unsigned char yfraction, const unsigned vpID )
        : m_fx( xfraction ), m_fy( yfraction ), m_vpID( vpID ) {}

    /// Return the cluster channelID
    [[nodiscard]] Detector::VPChannelID channelID() const noexcept { return m_vpID; }

    /// Print the cluster information
    std::ostream& fillStream( std::ostream& s ) const {
      s << "{VPCluster channelID: " << m_vpID << ", fx: " << m_fx << ", fy: " << m_fy << "}";
      return s;
    }

    /// Retrieve const  inter-pixel fraction
    [[nodiscard]] unsigned char xfraction() const noexcept { return m_fx; }
    [[nodiscard]] unsigned char yfraction() const noexcept { return m_fy; }

    // ! conversion from float to unsigned char
    // (to be compatible with the output type of the VPLightCluster)
    void setXfraction( float fx ) { m_fx = fx; }
    void setYfraction( float fy ) { m_fy = fy; }
    void setChannelID( Detector::VPChannelID channelID ) { m_vpID = channelID; }

    bool operator==( const VPMicroCluster& other ) const {
      return ( m_fx == other.m_fx && m_fy == other.m_fy && m_vpID == other.m_vpID );
    }

  private:
    unsigned char         m_fx;   ///< inter-pixel fraction in x coordinate
    unsigned char         m_fy;   ///< inter-pixel fraction in y coordinate
    Detector::VPChannelID m_vpID; ///< channelID of cluster

  }; // class VPMicroCluster

  using VPMicroClusterVector = std::vector<VPMicroCluster>;

#ifndef __CLING__
  /// indexed container of clusters
  using VPMicroClusters = Container::MultiIndexedContainer<VPMicroCluster, VP::NModules>;

  inline std::optional<VPMicroCluster> findCluster( Detector::VPChannelID channelID, const VPMicroClusters& clusters ) {
    unsigned int moduleNumber = channelID.module();

    for ( const auto& cluster : clusters.range( moduleNumber ) ) {
      if ( cluster.channelID() == channelID ) { return cluster; }
    }

    return std::nullopt;
  }
#endif

  inline std::ostream& operator<<( std::ostream& str, const VPMicroCluster& obj ) { return obj.fillStream( str ); }
} // namespace LHCb
