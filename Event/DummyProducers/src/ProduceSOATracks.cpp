/*****************************************************************************\
* (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/GenerateSOATracks.h"

#include "LHCbAlgs/Transformer.h"

using namespace LHCb::Event;

namespace LHCb {

  struct ProduceSOATracks final
      : public Algorithm::Transformer<v3::Tracks( EventContext const&, UniqueIDGenerator const& )> {

    ProduceSOATracks( std::string const& name, ISvcLocator* svcLoc )
        : Transformer( name, svcLoc, {KeyValue{"InputUniqueIDGenerator", UniqueIDGeneratorLocation::Default}},
                       KeyValue{"Output", ""} ) {}

    v3::Tracks operator()( EventContext const& evtCtx, UniqueIDGenerator const& unique_id_gen ) const override {
      auto res = v3::generate_tracks( m_nTracks, unique_id_gen, m_eventCount.value(), Zipping::generateZipIdentifier(),
                                      getMemResource( evtCtx ) );
      ++m_eventCount;
      return res;
    }

  private:
    mutable Gaudi::Accumulators::Counter<> m_eventCount{this, "Event"};
    Gaudi::Property<std::size_t>           m_nTracks{this, "NumberToGenerate", 100, "Number of objects to generate"};
  };

} // namespace LHCb

DECLARE_COMPONENT_WITH_ID( LHCb::ProduceSOATracks, "ProduceSOATracks" )
