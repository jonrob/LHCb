/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "Event/Particle.h"
#include "GaudiKernel/SmartRefVector.h"
#include "GaudiKernel/VectorMap.h"
#include <algorithm>
#include <ostream>
#include <string>
#include <vector>

namespace LHCb {

  /** @class Tagger Tagger.h
   *
   * Individual tagger decision on B flavour
   *
   * @author Marco Musy, Julian Wishahi
   *
   */

  class Tagger final {
  public:
    /// The type of tagger which has produced this decision
    enum TaggerType {
      none                = 0,
      unknown             = 1,
      Run2_SSPion         = 2,
      Run2_SSKaon         = 3,
      Run2_SSProton       = 4,
      Run2_OSKaon         = 5,
      Run2_OSMuon         = 6,
      Run2_OSElectron     = 7,
      Run2_OSVertexCharge = 8
    };
    friend std::ostream& operator<<( std::ostream& s, TaggerType e );
    enum struct TagResult : short int { b = -1, bbar = 1, none = 0 };
    friend std::ostream&                                   operator<<( std::ostream& s, TagResult e );
    [[nodiscard]] friend std::underlying_type_t<TagResult> to_underlying( TagResult tr ) {
      return static_cast<std::underlying_type_t<TagResult>>( tr );
    } // FIXME: C++23: use std::to_underlying...
    [[nodiscard]] friend TagResult reversed( TagResult tr ) { return static_cast<TagResult>( -to_underlying( tr ) ); }

    /// Default Constructor
    Tagger() = default;

    /// Fill the ASCII output stream
    std::ostream& fillStream( std::ostream& s ) const {
      return s << "{ "
               << "type :	" << m_type << "\ndecision :	" << m_decision << "\nomega :	" << m_omega
               << "\nmvaValue :	" << m_mvaValue << "\ncharge :	" << m_charge << "\n }";
    }

    /// conversion of string to enum for type TaggerType
    static TaggerType TaggerTypeToType( const std::string& aName );

    /// conversion to string for enum type TaggerType
    static const std::string& TaggerTypeToString( int aEnum );

    /// Retrieve const  The type of tagger
    [[nodiscard]] unsigned short int type() const { return m_type; }

    /// Update  The type of tagger
    Tagger& setType( unsigned short int value ) {
      m_type = value;
      return *this;
    }

    /// Retrieve const  decision of tagger
    [[nodiscard]] TagResult decision() const { return TagResult{m_decision}; }

    /// Update  decision of tagger
    Tagger& setDecision( TagResult value ) {
      m_decision = to_underlying( value );
      return *this;
    }

    /// Retrieve const  wrong tag fraction of tagger from MC tuning
    [[nodiscard]] float omega() const { return m_omega; }

    /// Update  wrong tag fraction of tagger from MC tuning
    Tagger& setOmega( float value ) {
      m_omega = value;
      return *this;
    }

    /// Retrieve const  raw output of the mva
    [[nodiscard]] float mvaValue() const { return m_mvaValue; }

    /// Update  raw output of the mva
    Tagger& setMvaValue( float value ) {
      m_mvaValue = value;
      return *this;
    }

    /// Retrieve const  (weighted) charge of the tagging particle
    [[nodiscard]] float charge() const { return m_charge; }

    /// Update  (weighted) charge of the tagging particle
    Tagger& setCharge( float value ) {
      m_charge = value;
      return *this;
    }

    /// Retrieve (const)  Vector of Particle(s) used to build the Tagger
    [[nodiscard]] const SmartRefVector<LHCb::Particle>& taggerParts() const { return m_taggerParts; }

    /// Update  Vector of Particle(s) used to build the Tagger
    Tagger& setTaggerParts( const SmartRefVector<LHCb::Particle>& value ) {
      m_taggerParts = value;
      return *this;
    }

    /// Add to  Vector of Particle(s) used to build the Tagger
    Tagger& addToTaggerParts( SmartRef<LHCb::Particle> value ) {
      m_taggerParts.push_back( std::move( value ) );
      return *this;
    }

    /// Remove from  Vector of Particle(s) used to build the Tagger
    Tagger& removeFromTaggerParts( const SmartRef<LHCb::Particle>& value ) {
      auto i = std::remove( m_taggerParts.begin(), m_taggerParts.end(), value );
      m_taggerParts.erase( i, m_taggerParts.end() );
      return *this;
    }

    /// Clear  Vector of Particle(s) used to build the Tagger
    Tagger& clearTaggerParts() {
      m_taggerParts.clear();
      return *this;
    }

    friend std::ostream& operator<<( std::ostream& str, const Tagger& obj ) { return obj.fillStream( str ); }

  private:
    unsigned short int             m_type     = 0;   ///< The type of tagger
    short int                      m_decision = 0;   ///< decision of tagger
    float                          m_omega    = 0.5; ///< wrong tag fraction of tagger from MC tuning
    float                          m_mvaValue = 0;   ///< raw output of the mva
    float                          m_charge   = 0;   ///< (weighted) charge of the tagging particle
    SmartRefVector<LHCb::Particle> m_taggerParts;    ///< Vector of Particle(s) used to build the Tagger

  }; // class Tagger

} // namespace LHCb
