/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "Event/Particle.h"
#include "Event/Tagger.h"
#include "GaudiKernel/KeyedContainer.h"
#include "GaudiKernel/KeyedObject.h"
#include "GaudiKernel/SerializeSTL.h"
#include "GaudiKernel/SmartRef.h"
#include <ostream>
#include <vector>

// Forward declarations

namespace LHCb {

  // Forward declarations
  using GaudiUtils::operator<<;

  // Class ID definition
  static const CLID CLID_FlavourTag = 805;

  // Namespace for locations in TDS
  namespace FlavourTagLocation {
    inline const std::string Default = "Phys/Tagging";
  }

  /** @class FlavourTag FlavourTag.h
   *
   * The result of the tagging algorithm
   *
   * @author Marco Musy
   *
   */

  class FlavourTag final : public KeyedObject<int> {
  public:
    /// typedef for std::vector of FlavourTag
    typedef std::vector<FlavourTag*>       Vector;
    typedef std::vector<const FlavourTag*> ConstVector;

    /// typedef for KeyedContainer of FlavourTag
    typedef KeyedContainer<FlavourTag, Containers::HashMap> Container;
    /// The container type for shared particles (without ownership)
    typedef SharedObjectsContainer<LHCb::FlavourTag> Selection;
    /// For uniform access to containers in TES (KeyedContainer,SharedContainer)
    typedef Gaudi::NamedRange_<ConstVector> Range;

    /// Possible result of the tagging algorithm
    using TagResult = Tagger::TagResult;

    /// Copy Constructor
    FlavourTag( const FlavourTag& c )
        : KeyedObject<int>()
        , m_decision( c.m_decision )
        , m_omega( c.m_omega )
        , m_taggers( c.m_taggers )
        , m_decisionOS( c.m_decisionOS )
        , m_omegaOS( c.m_omegaOS )
        , m_taggedB( c.m_taggedB ) {}

    /// Default Constructor
    FlavourTag() = default;

    // Retrieve pointer to class definition structure
    const CLID&        clID() const override { return LHCb::FlavourTag::classID(); }
    static const CLID& classID() { return CLID_FlavourTag; }

    /// Fill the ASCII output stream
    std::ostream& fillStream( std::ostream& s ) const override {
      return s << "{ "
               << "decision :	" << decision() << "\nomega :	" << m_omega << "\ntaggers :	" << m_taggers
               << "\ndecisionOS :	" << decisionOS() << "\nomegaOS :	" << m_omegaOS << "\n }";
    }

    LHCb::FlavourTag* clone() const { return new LHCb::FlavourTag( *this ); }

    /// Assignment operator
    FlavourTag& operator=( const FlavourTag& rhs ) {
      if ( this != &rhs ) {
        m_decision   = rhs.m_decision;
        m_omega      = rhs.m_omega;
        m_omegaOS    = rhs.m_omegaOS;
        m_taggers    = rhs.m_taggers;
        m_decisionOS = rhs.m_decisionOS;
        m_taggedB    = rhs.m_taggedB;
      }
      return *this;
    }

    /// Add one Tagger
    FlavourTag& addTagger( const LHCb::Tagger& HAS ) {
      m_taggers.push_back( HAS );
      return *this;
    }

    /// Retrieve const  The result of the tagging algorithm
    [[nodiscard]] TagResult decision() const { return TagResult{m_decision}; }

    /// Update  The result of the tagging algorithm
    FlavourTag& setDecision( TagResult value ) {
      m_decision = to_underlying( value );
      return *this;
    }

    /// Retrieve const  Wrong tag fraction (predicted)
    [[nodiscard]] float omega() const { return m_omega; }

    /// Update  Wrong tag fraction (predicted)
    FlavourTag& setOmega( float value ) {
      m_omega = value;
      return *this;
    }

    /// Retrieve const  Vector of Taggers
    [[nodiscard]] const std::vector<LHCb::Tagger>& taggers() const { return m_taggers; }

    /// Retrieve Vector of Taggers
    [[nodiscard]] std::vector<LHCb::Tagger>& taggers() { return m_taggers; }

    /// Update  Vector of Taggers
    FlavourTag& setTaggers( const std::vector<LHCb::Tagger>& value ) {
      m_taggers = value;
      return *this;
    }

    /// Retrieve const  decision of opposite side taggers only
    [[nodiscard]] TagResult decisionOS() const { return TagResult{m_decisionOS}; }

    /// Update  decision of opposite side taggers only
    FlavourTag& setDecisionOS( TagResult value ) {
      m_decisionOS = to_underlying( value );
      return *this;
    }

    /// Retrieve const  Wrong tag fraction (predicted) using opposite side only
    [[nodiscard]] float omegaOS() const { return m_omegaOS; }

    /// Update  Wrong tag fraction (predicted) using opposite side only
    FlavourTag& setOmegaOS( float value ) {
      m_omegaOS = value;
      return *this;
    }

    /// Retrieve (const)  The B meson for which this tag has been made
    [[nodiscard]] const LHCb::Particle* taggedB() const { return m_taggedB; }

    /// Update  The B meson for which this tag has been made
    FlavourTag& setTaggedB( SmartRef<LHCb::Particle> value ) {
      m_taggedB = std::move( value );
      return *this;
    }

    friend std::ostream& operator<<( std::ostream& str, const FlavourTag& obj ) { return obj.fillStream( str ); }

  private:
    short int                 m_decision{0};   ///< The result of the tagging algorithm
    float                     m_omega{0.5};    ///< Wrong tag fraction (predicted)
    std::vector<LHCb::Tagger> m_taggers{};     ///< Vector of Taggers
    short int                 m_decisionOS{0}; ///< decision of opposite side taggers only
    float                     m_omegaOS{0.5};  ///< Wrong tag fraction (predicted) using opposite side only
    SmartRef<LHCb::Particle>  m_taggedB;       ///< The B meson for which this tag has been made

  }; // class FlavourTag

  /// Definition of Keyed Container for FlavourTag
  using FlavourTags = FlavourTag::Container;

} // namespace LHCb

// -----------------------------------------------------------------------------
// end of class
// -----------------------------------------------------------------------------
