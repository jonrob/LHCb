/*****************************************************************************\
* (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/Tagger.h"

namespace {
  using TaggerType                                                        = LHCb::Tagger::TaggerType;
  const std::string                                    s_unknown          = "unknown";
  const GaudiUtils::VectorMap<std::string, TaggerType> s_TaggerTypeTypMap = {
      {"none", TaggerType::none},
      {"unknown", TaggerType::unknown},
      {"Run2_SSPion", TaggerType::Run2_SSPion},
      {"Run2_SSKaon", TaggerType::Run2_SSKaon},
      {"Run2_SSProton", TaggerType::Run2_SSProton},
      {"Run2_OSKaon", TaggerType::Run2_OSKaon},
      {"Run2_OSMuon", TaggerType::Run2_OSMuon},
      {"Run2_OSElectron", TaggerType::Run2_OSElectron},
      {"Run2_OSVertexCharge", TaggerType::Run2_OSVertexCharge},
  };
} // namespace

namespace LHCb {
  std::ostream& operator<<( std::ostream& s, LHCb::Tagger::TagResult e ) {
    switch ( e ) {
    case Tagger::TagResult::b:
      return s << "b";
    case Tagger::TagResult::bbar:
      return s << "bbar";
    case Tagger::TagResult::none:
      return s << "none";
    }
    return s << "ERROR wrong value " << int( e ) << " for enum LHCb::FlavourTag::TagResult";
  }
  std::ostream& operator<<( std::ostream& s, LHCb::Tagger::TaggerType e ) {
    switch ( e ) {
    case Tagger::none:
      return s << "none";
    case Tagger::unknown:
      return s << "unknown";
    case Tagger::Run2_SSPion:
      return s << "Run2_SSPion";
    case Tagger::Run2_SSKaon:
      return s << "Run2_SSKaon";
    case Tagger::Run2_SSProton:
      return s << "Run2_SSProton";
    case Tagger::Run2_OSKaon:
      return s << "Run2_OSKaon";
    case Tagger::Run2_OSMuon:
      return s << "Run2_OSMuon";
    case Tagger::Run2_OSElectron:
      return s << "Run2_OSElectron";
    case Tagger::Run2_OSVertexCharge:
      return s << "Run2_OSVertexCharge";
    }
    return s << "ERROR wrong value " << int( e ) << " for enum LHCb::Tagger::TaggerType";
  }

  Tagger::TaggerType Tagger::TaggerTypeToType( const std::string& aName ) {
    auto iter = s_TaggerTypeTypMap.find( aName );
    return iter != s_TaggerTypeTypMap.end() ? iter->second : unknown;
  }

  const std::string& Tagger::TaggerTypeToString( int aEnum ) {
    auto iter = std::find_if( s_TaggerTypeTypMap.begin(), s_TaggerTypeTypMap.end(),
                              [&]( const std::pair<const std::string, TaggerType>& i ) { return i.second == aEnum; } );
    return iter != s_TaggerTypeTypMap.end() ? iter->first : s_unknown;
  }
} // namespace LHCb
