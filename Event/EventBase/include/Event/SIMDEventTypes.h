/*****************************************************************************\
* (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "Event/SOACollection.h"
#include "Kernel/LHCbID.h"
#include "LHCbMath/MatVec.h"
#include "LHCbMath/SIMDWrapper.h"

namespace LHCb::Event {
  // SIMDWrapper compatible type for representing an enum
  template <typename simd, typename enumType>
  struct enum_v {
    using int_v  = typename simd::int_v;
    using mask_v = typename simd::mask_v;

    constexpr enum_v( enumType id ) : _data{bit_cast<int>( id )} {};
    constexpr enum_v( int_v id ) : _data{id} {};

    constexpr int_v data() const { return _data; }

    template <typename U = simd, std::enable_if_t<std::is_same_v<U, SIMDWrapper::scalar::types>>* = nullptr>
    constexpr enumType cast() const {
      return enumType( bit_cast<unsigned int>( _data.cast() ) );
    }

    friend mask_v operator==( enum_v lhs, enum_v rhs ) { return lhs._data == rhs._data; }
    friend mask_v operator!=( enum_v lhs, enum_v rhs ) { return !( lhs._data == rhs._data ); }

    friend std::ostream& operator<<( std::ostream& os, const enum_v id ) {
      std::array<int, simd::size> tmp;
      id._data.store( tmp );
      os << "enum_v"
         << "{";
      for ( std::size_t i = 0; i < simd::size - 1; ++i ) { os << enumType( bit_cast<unsigned int>( tmp[i] ) ) << ", "; }
      return os << enumType( bit_cast<unsigned int>( tmp[simd::size - 1] ) ) << "}";
    }

  private:
    int_v _data{};
  };

  // * Tag and Proxy baseclasses for representing a field of enum
  template <typename enumType, bool Unique = false>
  struct enum_field : int_field_<Unique> {
    template <SIMDWrapper::InstructionSet Simd, LHCb::Pr::ProxyBehaviour Behaviour, typename ContainerTypeBase,
              typename Path>
    struct EnumProxy {
      using simd_t        = SIMDWrapper::type_map_t<Simd>;
      using OffsetType    = LHCb::Pr::detail::offset_t<Behaviour, Simd>;
      using ContainerType = LHCb::Pr::detail::container_t<Behaviour, Simd, ContainerTypeBase>;
      using proxy_type    = NumericProxy<Simd, Behaviour, ContainerTypeBase, Path, int>;

      EnumProxy( ContainerType container, OffsetType offset, Path path )
          : m_container( container ), m_offset( offset ), m_path( path ) {}

      [[nodiscard, gnu::always_inline]] inline constexpr auto proxy() const {
        return proxy_type{m_container, m_offset, m_path};
      }

      [[gnu::always_inline]] inline constexpr void set( enum_v<simd_t, enumType> const& val ) {
        proxy().set( val.data() );
      }

      [[gnu::always_inline]] inline constexpr auto get() const { return enum_v<simd_t, enumType>{proxy().get()}; }

    private:
      ContainerType m_container;
      OffsetType    m_offset;
      const Path    m_path;
    };

    template <SIMDWrapper::InstructionSet Simd, LHCb::Pr::ProxyBehaviour Behaviour, typename ContainerType,
              typename Path>
    using proxy_type = EnumProxy<Simd, Behaviour, ContainerType, Path>;
  };
  template <typename enumType, std::size_t... N>
  using enums_field = ndarray_field<enum_field<enumType>, N...>;

  // SIMDWrapper compatible type for representing flags, i.e. booleans put in an int using a mask given as enum
  template <typename simd, typename enumType>
  struct flags_v {
    using int_v  = typename simd::int_v;
    using mask_v = typename simd::mask_v;

    constexpr flags_v( int_v flag ) : _data{flag} {};

    constexpr flags_v() : _data{0} {};

    constexpr int_v data() const { return _data; }

    template <typename U = simd, std::enable_if_t<std::is_same_v<U, SIMDWrapper::scalar::types>>* = nullptr>
    constexpr int cast() const {
      return _data.cast();
    }

    [[gnu::always_inline]] inline constexpr flags_v getFlags() const { return _data; }

    friend mask_v operator==( flags_v lhs, flags_v rhs ) { return lhs._data == rhs._data; }
    friend mask_v operator!=( flags_v lhs, flags_v rhs ) { return !( lhs._data == rhs._data ); }

    template <enumType mask>
    [[gnu::always_inline]] inline constexpr mask_v test() const {
      static_assert( static_cast<int>( mask ) < 33, "cannot have more flags than bits in an int" );
      int_v bitmask = ( 1 << static_cast<int>( mask ) );
      return !( ( _data & bitmask ) == int_v{0} );
    }

    [[gnu::always_inline]] inline constexpr mask_v has( enumType mask ) const {
      assert( static_cast<int>( mask ) < 33 && "cannot have more flags than bits in an int" );
      int_v bitmask = ( 1 << static_cast<int>( mask ) );
      return !( ( _data & bitmask ) == int_v{0} );
    }

    friend std::ostream& operator<<( std::ostream& os, const flags_v id ) {
      std::array<int, simd::size> tmp;
      id._data.store( tmp );
      os << "flags_v"
         << "{";
      for ( std::size_t i = 0; i < simd::size - 1; ++i ) { os << std::bitset<32>( tmp[i] ) << ", "; }
      return os << std::bitset<32>( tmp[simd::size - 1] ) << "}";
    }

    template <enumType mask>
    [[gnu::always_inline]] inline constexpr void set( const mask_v flagVal ) {
      static_assert( static_cast<int>( mask ) < 33, "cannot have more flags than bits in an int" );
      int_v bitmask = ( 1 << static_cast<int>( mask ) );
      _data         = select( flagVal, _data | bitmask, _data & ~bitmask );
    }

  private:
    int_v _data{};
  };

  // * Tag and Proxy baseclasses for representing a field of flags
  template <typename enumType, bool Unique = false>
  struct flag_field : int_field_<Unique> {
    template <SIMDWrapper::InstructionSet Simd, LHCb::Pr::ProxyBehaviour Behaviour, typename ContainerTypeBase,
              typename Path>
    struct FlagProxy {
      using simd_t        = SIMDWrapper::type_map_t<Simd>;
      using OffsetType    = LHCb::Pr::detail::offset_t<Behaviour, Simd>;
      using ContainerType = LHCb::Pr::detail::container_t<Behaviour, Simd, ContainerTypeBase>;
      using proxy_type    = NumericProxy<Simd, Behaviour, ContainerTypeBase, Path, int>;

      FlagProxy( ContainerType container, OffsetType offset, Path path )
          : m_container( container ), m_offset( offset ), m_path( path ) {}

      [[nodiscard, gnu::always_inline]] inline constexpr auto proxy() const {
        return proxy_type{m_container, m_offset, m_path};
      }

      [[gnu::always_inline]] inline constexpr void set( flags_v<simd_t, enumType> const& val ) {
        proxy().set( val.data() );
      }

      [[gnu::always_inline]] inline constexpr auto get() const { return flags_v<simd_t, enumType>{proxy().get()}; }

    private:
      ContainerType m_container;
      OffsetType    m_offset;
      const Path    m_path;
    };

    template <SIMDWrapper::InstructionSet Simd, LHCb::Pr::ProxyBehaviour Behaviour, typename ContainerType,
              typename Path>
    using proxy_type = FlagProxy<Simd, Behaviour, ContainerType, Path>;
  };
  template <typename enumType, std::size_t... N>
  using flags_field = ndarray_field<flag_field<enumType>, N...>;

  // SIMDWrapper compatible type for representing an LHCbID
  template <typename dType>
  struct lhcbid_v {

    using I = typename dType::int_v;
    using M = typename dType::mask_v;

    constexpr lhcbid_v( LHCb::LHCbID id ) : _data{static_cast<int>( id.lhcbID() )} {};
    constexpr lhcbid_v( I id ) : _data{id} {};

    template <typename U = dType, std::enable_if_t<std::is_same_v<U, SIMDWrapper::scalar::types>>* = nullptr>
    constexpr LHCb::LHCbID LHCbID() const {
      return LHCb::LHCbID( bit_cast<unsigned int>( _data.cast() ) );
    }

    constexpr I data() const { return _data; }

    [[gnu::always_inline]] inline constexpr lhcbid_v const& store( int* ptr ) const {
      _data.store( ptr );
      return *this;
    }

    [[gnu::always_inline]] inline constexpr const lhcbid_v& compressstore( M mask, int* ptr ) const {
      _data.compressstore( mask, ptr );
      return *this;
    }

    friend lhcbid_v select( M mask, lhcbid_v id1, lhcbid_v id2 ) {
      I val = select( mask, id1.data(), id2.data() );
      return lhcbid_v( val );
    }

    constexpr lhcbid_v& operator=( I f ) {
      _data = f;
      return *this;
    }

    friend std::ostream& operator<<( std::ostream& os, const lhcbid_v id ) {
      std::array<int, dType::size> tmp;
      id.data().store( tmp );
      os << "lhcbid_v"
         << "{";
      for ( std::size_t i = 0; i < dType::size - 1; ++i ) {
        os << LHCb::LHCbID( static_cast<unsigned int>( tmp[i] ) ) << ", ";
      }
      return os << LHCb::LHCbID( static_cast<unsigned int>( tmp[dType::size - 1] ) ) << "}";
    }

  private:
    I _data{};
  };

  // * Tag and Proxy baseclasses for representing a field of LHCbIDs
  struct lhcbid_field : int_field {
    template <SIMDWrapper::InstructionSet Simd, LHCb::Pr::ProxyBehaviour Behaviour, typename ContainerTypeBase,
              typename Path>
    struct LHCbIDsProxy {
      using simd_t        = SIMDWrapper::type_map_t<Simd>;
      using OffsetType    = LHCb::Pr::detail::offset_t<Behaviour, Simd>;
      using ContainerType = LHCb::Pr::detail::container_t<Behaviour, Simd, ContainerTypeBase>;
      using proxy_type    = NumericProxy<Simd, Behaviour, ContainerTypeBase, Path, int>;

      LHCbIDsProxy( ContainerType container, OffsetType offset, Path path )
          : m_container( container ), m_offset( offset ), m_path( path ) {}

      [[nodiscard, gnu::always_inline]] inline constexpr auto proxy() const {
        return proxy_type{m_container, m_offset, m_path};
      }

      [[gnu::always_inline]] inline constexpr void set( lhcbid_v<simd_t> const& lhcbID ) {
        proxy().set( lhcbID.data() );
      }

      [[nodiscard, gnu::always_inline]] constexpr lhcbid_v<simd_t> get() const {
        return lhcbid_v<simd_t>{proxy().get()};
      }

    private:
      ContainerType m_container;
      OffsetType    m_offset;
      const Path    m_path;
    };

    template <SIMDWrapper::InstructionSet Simd, LHCb::Pr::ProxyBehaviour Behaviour, typename ContainerType,
              typename Path>
    using proxy_type = LHCbIDsProxy<Simd, Behaviour, ContainerType, Path>;
  };
  template <std::size_t... N>
  using lhcbids_field = ndarray_field<lhcbid_field, N...>;

  // VecMat storage fields:

  template <int N, int M>
  struct Mat_field : floats_field<N * M> {
    template <SIMDWrapper::InstructionSet Simd, LHCb::Pr::ProxyBehaviour Behaviour, typename ContainerTypeBase,
              typename Path>
    struct MatProxy {
      using simd_t        = SIMDWrapper::type_map_t<Simd>;
      using type          = typename simd_t::float_v;
      using OffsetType    = LHCb::Pr::detail::offset_t<Behaviour, Simd>;
      using ContainerType = LHCb::Pr::detail::container_t<Behaviour, Simd, ContainerTypeBase>;
      using proxy_type =
          NumericProxy<Simd, Behaviour, ContainerTypeBase, typename Path::template extend_t<std::size_t>, float>;

      MatProxy( ContainerType container, OffsetType offset, Path path )
          : m_container( container ), m_offset( offset ), m_path( path ) {}

      [[nodiscard, gnu::always_inline]] constexpr auto proxy( std::size_t i ) const {
        return proxy_type{m_container, m_offset, m_path.append( i )};
      }

      [[gnu::always_inline]] constexpr void set( LHCb::LinAlg::Mat<type, N, M> const& mat ) {
        LHCb::Utils::unwind<0, N>(
            [&]( auto i ) { LHCb::Utils::unwind<0, M>( [&]( auto j ) { proxy( i * M + j ).set( mat( i, j ) ); } ); } );
      }

      [[gnu::always_inline]] constexpr auto get() const {
        LHCb::LinAlg::Mat<type, N, M> mat;
        LHCb::Utils::unwind<0, N>(
            [&]( auto i ) { LHCb::Utils::unwind<0, M>( [&]( auto j ) { mat( i, j ) = proxy( i * M + j ).get(); } ); } );
        return mat;
      }

    private:
      ContainerType m_container;
      OffsetType    m_offset;
      const Path    m_path;
    };

    template <SIMDWrapper::InstructionSet Simd, LHCb::Pr::ProxyBehaviour Behaviour, typename ContainerType,
              typename Path>
    using proxy_type = MatProxy<Simd, Behaviour, ContainerType, Path>;
  };

  template <int N>
  struct MatSym_field : floats_field<N*( N + 1 ) / 2> {
    template <SIMDWrapper::InstructionSet Simd, LHCb::Pr::ProxyBehaviour Behaviour, typename ContainerTypeBase,
              typename Path>
    struct MatSymProxy {
      using simd_t        = SIMDWrapper::type_map_t<Simd>;
      using type          = typename simd_t::float_v;
      using OffsetType    = LHCb::Pr::detail::offset_t<Behaviour, Simd>;
      using ContainerType = LHCb::Pr::detail::container_t<Behaviour, Simd, ContainerTypeBase>;
      using proxy_type =
          NumericProxy<Simd, Behaviour, ContainerTypeBase, typename Path::template extend_t<std::size_t>, float>;

      MatSymProxy( ContainerType container, OffsetType offset, Path path )
          : m_container( container ), m_offset( offset ), m_path( path ) {}

      [[nodiscard, gnu::always_inline]] constexpr auto proxy( std::size_t i ) const {
        return proxy_type{m_container, m_offset, m_path.append( i )};
      }

      [[gnu::always_inline]] constexpr void set( LHCb::LinAlg::MatSym<type, N> const& mat ) {
        LHCb::Utils::unwind<0, N>( [&]( auto i ) {
          LHCb::Utils::unwind<0, i + 1>( [&]( auto j ) { proxy( i * ( i + 1 ) / 2 + j ).set( mat( i, j ) ); } );
        } );
      }

      [[gnu::always_inline]] constexpr auto get() const {
        LHCb::LinAlg::MatSym<type, N> mat;
        LHCb::Utils::unwind<0, N>( [&]( auto i ) {
          LHCb::Utils::unwind<0, i + 1>( [&]( auto j ) { mat( i, j ) = proxy( i * ( i + 1 ) / 2 + j ).get(); } );
        } );
        return mat;
      }

    private:
      ContainerType m_container;
      OffsetType    m_offset;
      const Path    m_path;
    };

    template <SIMDWrapper::InstructionSet Simd, LHCb::Pr::ProxyBehaviour Behaviour, typename ContainerType,
              typename Path>
    using proxy_type = MatSymProxy<Simd, Behaviour, ContainerType, Path>;
  };

  template <int N>
  struct Vec_field : floats_field<N> {
    template <SIMDWrapper::InstructionSet Simd, LHCb::Pr::ProxyBehaviour Behaviour, typename ContainerTypeBase,
              typename Path>
    struct VecProxy {
      using simd_t        = SIMDWrapper::type_map_t<Simd>;
      using type          = typename simd_t::float_v;
      using OffsetType    = LHCb::Pr::detail::offset_t<Behaviour, Simd>;
      using ContainerType = LHCb::Pr::detail::container_t<Behaviour, Simd, ContainerTypeBase>;
      using proxy_type =
          NumericProxy<Simd, Behaviour, ContainerTypeBase, typename Path::template extend_t<std::size_t>, float>;

      VecProxy( ContainerType container, OffsetType offset, Path path )
          : m_container( container ), m_offset( offset ), m_path( path ) {}

      template <int i>
      [[nodiscard, gnu::always_inline]] constexpr auto proxy() const {
        return proxy_type{m_container, m_offset, m_path.append( static_cast<std::size_t>( i ) )};
      }

      [[gnu::always_inline, gnu::flatten]] constexpr void set( LHCb::LinAlg::Vec<type, N> const& vec ) {
        // LHCb::Utils::unwind<0, N>( [&]( auto i ) { proxy( i ).set( vec( i ) ); } );
        proxy<0>().set( vec.x() );
        proxy<1>().set( vec.y() );
        if constexpr ( N >= 3 ) { proxy<2>().set( vec.z() ); }
        if constexpr ( N >= 4 ) { proxy<3>().set( vec( 3 ) ); }
        if constexpr ( N >= 5 ) { proxy<4>().set( vec( 4 ) ); }
        static_assert( N <= 5 );
      }

      [[gnu::always_inline]] constexpr void setX( type const& x ) { proxy<0>().set( x ); }

      [[gnu::always_inline]] constexpr void setY( type const& y ) { proxy<1>().set( y ); }

      [[gnu::always_inline]] constexpr void setZ( type const& z ) { proxy<2>().set( z ); }

      [[gnu::always_inline, gnu::flatten]] constexpr auto get() const {
        /*LHCb::LinAlg::Vec<type, N> v;
        LHCb::Utils::unwind<0, N>( [&]( auto i ) { v( i ) = proxy( i ).get(); } );
        return v;*/
        LHCb::LinAlg::Vec<type, N> v;
        v( 0 ) = proxy<0>().get();
        v( 1 ) = proxy<1>().get();
        if constexpr ( N >= 3 ) { v( 2 ) = proxy<2>().get(); }
        if constexpr ( N >= 4 ) { v( 3 ) = proxy<3>().get(); }
        if constexpr ( N >= 5 ) { v( 4 ) = proxy<4>().get(); }
        static_assert( N <= 5 );
        return v;
      }

    private:
      ContainerType m_container;
      OffsetType    m_offset;
      const Path    m_path;
    };

    template <SIMDWrapper::InstructionSet Simd, LHCb::Pr::ProxyBehaviour Behaviour, typename ContainerType,
              typename Path>
    using proxy_type = VecProxy<Simd, Behaviour, ContainerType, Path>;
  };

} // namespace LHCb::Event
