/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "GaudiKernel/DataObject.h"
#include "GaudiKernel/SerializeSTL.h"
#include "Kernel/CondDBNameTagPair.h"
#include "Kernel/PlatformInfo.h"
#include <ostream>
#include <string>
#include <vector>

namespace LHCb {

  // Class ID definition
  static const CLID CLID_ProcessHeader = 103;

  // Namespace for locations in TDS
  namespace ProcessHeaderLocation {
    inline const std::string Digi = "MC/DigiHeader";
    inline const std::string MC   = "MC/Header";
    inline const std::string Rec  = "Rec/Header";
  } // namespace ProcessHeaderLocation

  /** @class ProcessHeader ProcessHeader.h
   *
   * Common base class for all process headers: GenHeader, MCHeader...
   *
   * @author P. Koppenburg
   *
   */

  class ProcessHeader : public DataObject {
  public:
    /// Default Constructor
    ProcessHeader() = default;

    /// Copy constructor. Creates a new ProcessHeader with the same information.
    ProcessHeader( const LHCb::ProcessHeader& head )
        : DataObject()
        , m_applicationName( head.applicationName() )
        , m_applicationVersion( head.applicationVersion() )
        , m_runNumber( head.runNumber() )
        , m_condDBTags( head.condDBTags() )
        , m_platformInfo( head.platformInfo() ) {}

    // Retrieve pointer to class definition structure
    const CLID&        clID() const override { return LHCb::ProcessHeader::classID(); }
    static const CLID& classID() { return CLID_ProcessHeader; }

    /// Fill the ASCII output stream
    std::ostream& fillStream( std::ostream& s ) const override {
      using GaudiUtils::operator<<;
      return s << "{ "
               << "applicationName :	" << m_applicationName
               << "\napplicationVersion :	" << m_applicationVersion << "\nrunNumber :	" << m_runNumber
               << "\ncondDBTags :	" << m_condDBTags << "\nplatformInfo :	" << m_platformInfo << "\n }";
    }

    /// Retrieve const  Application or Program Name (e.g. Gauss, Boole or Pythia)
    [[nodiscard]] const std::string& applicationName() const { return m_applicationName; }

    /// Update  Application or Program Name (e.g. Gauss, Boole or Pythia)
    ProcessHeader& setApplicationName( const std::string& value ) {
      m_applicationName = value;
      return *this;
    }

    /// Retrieve const  Application or Program version
    [[nodiscard]] const std::string& applicationVersion() const { return m_applicationVersion; }

    /// Update  Application or Program version
    ProcessHeader& setApplicationVersion( const std::string& value ) {
      m_applicationVersion = value;
      return *this;
    }

    /// Retrieve const  Run number
    [[nodiscard]] unsigned int runNumber() const { return m_runNumber; }

    /// Update  Run number
    ProcessHeader& setRunNumber( unsigned int value ) {
      m_runNumber = value;
      return *this;
    }

    /// Retrieve const  Conditions database tags
    [[nodiscard]] const std::vector<LHCb::CondDBNameTagPair>& condDBTags() const { return m_condDBTags; }

    /// Update  Conditions database tags
    ProcessHeader& setCondDBTags( const std::vector<LHCb::CondDBNameTagPair>& value ) {
      m_condDBTags = value;
      return *this;
    }

    /// Retrieve const  Details of binary and host during processing
    [[nodiscard]] const LHCb::PlatformInfo& platformInfo() const { return m_platformInfo; }

    /// Update  Details of binary and host during processing
    ProcessHeader& setPlatformInfo( const LHCb::PlatformInfo& value ) {
      m_platformInfo = value;
      return *this;
    }

    friend std::ostream& operator<<( std::ostream& str, const ProcessHeader& obj ) { return obj.fillStream( str ); }

  private:
    std::string  m_applicationName    = "Unknown";       ///< Application or Program Name (e.g. Gauss, Boole or Pythia)
    std::string  m_applicationVersion = "Unknown";       ///< Application or Program version
    unsigned int m_runNumber          = 0;               ///< Run number
    std::vector<LHCb::CondDBNameTagPair> m_condDBTags;   ///< Conditions database tags
    LHCb::PlatformInfo                   m_platformInfo; ///< Details of binary and host during processing

  }; // class ProcessHeader

} // namespace LHCb
