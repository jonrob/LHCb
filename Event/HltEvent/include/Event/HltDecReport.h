/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

// Include files
#include <cassert>
#include <cstdint>
#include <ostream>

namespace LHCb {

  /** @class HltDecReport HltDecReport.h
   *
   * Hlt Trigger Decision
   *
   * @author Tomasz Skwarnicki
   *
   */

  class HltDecReport final {

    unsigned int m_decReport = 0; ///< encoded decision, execution stage, errors, number of candidates and decision ID

    /// Bitmasks for bitfield decReport
    enum struct Mask : uint32_t {
      decision           = 0x1L,
      errorBits          = 0xeL,
      numberOfCandidates = 0xf0L,
      executionStage     = 0xff00L,
      decisionID         = 0xffff0000L
    };

    template <Mask m>
    [[nodiscard]] static constexpr unsigned int extract( unsigned int i ) {
      constexpr auto b =
          __builtin_ctz( static_cast<uint32_t>( m ) ); // FIXME: C++20 replace __builtin_ctz with std::countr_zero
      return ( i & static_cast<uint32_t>( m ) ) >> b;
    }

    template <Mask m>
    [[nodiscard]] constexpr HltDecReport& set( unsigned int i ) {
      constexpr auto b =
          __builtin_ctz( static_cast<uint32_t>( m ) ); // FIXME: C++20 replace __builtin_ctz with std::countr_zero
      m_decReport &= ~static_cast<uint32_t>( m );
      m_decReport |= ( i << static_cast<uint32_t>( b ) );
      assert( extract<m>( m_decReport ) == i );
      return *this;
    }

  public:
    /// Default Constructor
    constexpr HltDecReport() = default;

    /// create from integer
    explicit constexpr HltDecReport( unsigned int decRep ) : m_decReport( decRep ) {}

    /// create from subfield inputs with allowed range checking
    constexpr HltDecReport( bool dec, int stage, unsigned int eb, int noc, int decID ) {
      setDecision( dec );
      setExecutionStage( stage );
      setErrorBits( eb );
      setNumberOfCandidates( noc );
      setIntDecisionID( decID );
    }

    /// create from subset of subfield inputs with allowed range checking
    constexpr HltDecReport( bool dec, unsigned int eb, int noc, int decID ) : HltDecReport{dec, 0, eb, noc, decID} {}

    /// set decision bit; keep event= (dec==true)
    constexpr HltDecReport& setDecision( bool dec ) { return set<Mask::decision>( dec ? 1u : 0u ); }

    /// Update code specifying highest step reached during the trigger line execution (0-255)
    constexpr HltDecReport& setExecutionStage( unsigned int stage ) {
      return set<Mask::executionStage>( std::min( stage, extract<Mask::executionStage>( ~0u ) ) );
    }

    /// if numberOfCandidates in HltDecReport equals this then actual number might have been larger
    static constexpr unsigned int saturatedNumberOfCandidates() { return extract<Mask::numberOfCandidates>( ~0u ); }

    /// set numberOfCandidates; set to saturation value if larger than max allowed; set to 0 if negative
    constexpr HltDecReport& setNumberOfCandidates( unsigned int noc ) {
      return set<Mask::numberOfCandidates>( std::min( noc, saturatedNumberOfCandidates() ) );
    }

    /// if intDecisionID in HltDecReport equals this then passed decision ID was out of allowed range
    constexpr static unsigned int illegalIntDecisionID() { return extract<Mask::decisionID>( ~0u ); }

    /// set intDecisionID; set to illegal value if out of allowed range
    constexpr HltDecReport& setIntDecisionID( unsigned int decID ) {
      return set<Mask::decisionID>( std::min( decID, illegalIntDecisionID() ) );
    }

    /// checked if intDecisionID was invalid when set
    constexpr bool invalidIntDecisionID() const {
      return ( intDecisionID() == illegalIntDecisionID() ) || ( intDecisionID() == 0 );
    }

    /// intelligent printout
    std::ostream& fillStream( std::ostream& s ) const;

    /// Retrieve const  encoded decision, execution stage, errors, number of candidates and decision ID
    [[nodiscard]] constexpr unsigned int decReport() const { return m_decReport; }

    /// Retrieve decision if true this trigger line asked to keep the event;
    ///                   if false this trigger did not request to keep the event
    [[nodiscard]] constexpr bool decision() const { return extract<Mask::decision>( m_decReport ) != 0; }

    /// Retrieve error code 0=none
    [[nodiscard]] constexpr unsigned int errorBits() const { return extract<Mask::errorBits>( m_decReport ); }

    /// Update error code 0=none
    constexpr HltDecReport& setErrorBits( unsigned int value ) { return set<Mask::errorBits>( value ); }

    /// Retrieve number of selected candidates passing trigger decision criteria
    [[nodiscard]] constexpr unsigned int numberOfCandidates() const {
      return extract<Mask::numberOfCandidates>( m_decReport );
    }

    /// Retrieve code specifying highest step reached during the trigger line execution (0-255)
    [[nodiscard]] constexpr unsigned int executionStage() const { return extract<Mask::executionStage>( m_decReport ); }

    /// Retrieve integer decision ID
    [[nodiscard]] constexpr unsigned int intDecisionID() const { return extract<Mask::decisionID>( m_decReport ); }

    friend std::ostream& operator<<( std::ostream& str, const HltDecReport& obj ) { return obj.fillStream( str ); }

    [[nodiscard]] constexpr friend bool operator==( const HltDecReport& lhs, const HltDecReport& rhs ) {
      return lhs.m_decReport == rhs.m_decReport;
    }
    [[nodiscard]] constexpr friend bool operator!=( const HltDecReport& lhs, const HltDecReport& rhs ) {
      return !( lhs == rhs );
    }

  }; // class HltDecReport

} // namespace LHCb
