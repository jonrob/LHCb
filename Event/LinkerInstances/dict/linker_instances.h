/*****************************************************************************\
* (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Linker/LinkedFrom.h"
#include "Linker/LinkedTo.h"

#include "Event/CaloDigit.h"
#include "Event/FTCluster.h"
#include "Event/MCHit.h"
#include "Event/MCParticle.h"
#include "Event/MuonCoord.h"
#include "Event/MuonDigit.h"
#include "Event/Particle.h"
#include "Event/Track.h"
#include "Event/VPCluster.h"

namespace {
  struct LinkerInstances_Instantiations {
    LinkedTo<LHCb::MCParticle> i9;

    LinkedFrom<LHCb::VPCluster>  i301;
    LinkedFrom<LHCb::FTCluster>  i401;
    LinkedFrom<ContainedObject>  i103;
    LinkedFrom<LHCb::MuonCoord>  i104;
    LinkedFrom<LHCb::MuonDigit>  i107;
    LinkedFrom<LHCb::CaloDigit>  i108;
    LinkedFrom<LHCb::Track>      i105;
    LinkedFrom<LHCb::MCHit>      i106;
    LinkedFrom<LHCb::Particle>   i110;
    LinkedFrom<LHCb::MCParticle> i109;
  };
} // namespace
