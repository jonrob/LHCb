###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import sys, os, glob
from PyConf.application import (configure_input, configure, ApplicationOptions,
                                default_raw_banks)
from PyConf.control_flow import CompositeNode, NodeLogic
from PyConf.Algorithms import UTRawBankToUTDigitsAlg
from DDDB.CheckDD4Hep import UseDD4Hep
from Configurables import LHCb__Det__LbDD4hep__IOVProducer as IOVProducer
from Gaudi.Configuration import INFO, DEBUG, WARNING
from Moore import options

input_path = '/hlt2/objects/UT/0000289906/'

# Setup input
data = []
if os.path.exists(input_path):
    data = glob.glob(input_path + '*.mdf')
else:
    print("Input directory doesn't exist!")
    sys.exit()
if data == []:
    print("Input data doesn't exist!")
    sys.exit()

options.input_files = data
options.input_type = 'MDF'
options.geometry_version = 'run3/trunk'
options.conditions_version = 'master'
options.simulation = False
options.evt_max = -1
options.histo_file = "ut_data.root"

config = configure_input(options)

if UseDD4Hep:
    from Configurables import LHCb__Det__LbDD4hep__DD4hepSvc as DD4hepSvc
    dd4hepsvc = DD4hepSvc()
    dd4hepsvc.VerboseLevel = 1
    dd4hepsvc.GeometryLocation = "${DETECTOR_PROJECT_ROOT}/compact"
    dd4hepsvc.GeometryVersion = "run3/trunk"
    dd4hepsvc.GeometryMain = "LHCb.xml"
    dd4hepsvc.DetectorList = ["/world", "UT"]
    iovProd = IOVProducer("ReserveIOVDD4hep", ODIN='DAQ/ODIN')
    config.add(dd4hepsvc)
else:
    from Configurables import LHCb__DetDesc__ReserveDetDescForEvent as reserveIOV
    reserveIOV().PreloadGeometry = False

from PyConf.application import make_odin

decoder_zs = UTRawBankToUTDigitsAlg(
    name="UTRawToDigits_UT", UTBank=default_raw_banks("UT"), OutputLevel=INFO)
decoder_error = UTRawBankToUTDigitsAlg(
    name="UTRawToDigits_UTError",
    UTBank=default_raw_banks("UTError"),
    OutputLevel=INFO)

cf_node = CompositeNode(
    "UTRawToDigits", [decoder_zs, decoder_error],
    combine_logic=NodeLogic.LAZY_AND,
    force_order=True)

config.update(configure(options, cf_node))
