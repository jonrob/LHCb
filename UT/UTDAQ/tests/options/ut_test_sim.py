###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import logging
from Gaudi.Configuration import VERBOSE, DEBUG, INFO
from PRConfig.TestFileDB import test_file_db
from DDDB.CheckDD4Hep import UseDD4Hep
from PyConf.Algorithms import UTRawBankToUTDigitsAlg

from PyConf.application import (configure_input, configure, CompositeNode,
                                make_odin, default_raw_banks)

# DD4HEP configuration
if UseDD4Hep:
    from Configurables import LHCb__Det__LbDD4hep__DD4hepSvc as DD4hepSvc
    dd4hepsvc = DD4hepSvc()
    dd4hepsvc.GeometryVersion = "run3/trunk"
    dd4hepsvc.GeometryMain = "LHCb.xml"
    dd4hepsvc.DetectorList = ["/world", "UT"]

# I/O configuration
options = test_file_db["upgrade-baseline-ut-new-geo-digi"].make_lbexec_options(
    simulation=True,
    python_logging_level=logging.INFO,
    evt_max=-1,
    histo_file='ut_histo.root',
    ntuple_file='ut.root',
    data_type='Upgrade',
    geometry_version="run3/trunk",
    conditions_version='master',
    input_type="ROOT")

# Setting up algorithms pipeline
configure_input(options)
odin = make_odin()
decoder_zs = UTRawBankToUTDigitsAlg(
    name="UTRawToDigits_UT", UTBank=default_raw_banks("UT"), OutputLevel=INFO)
decoder_error = UTRawBankToUTDigitsAlg(
    name="UTRawToDigits_UTError",
    UTBank=default_raw_banks("UTError"),
    OutputLevel=INFO)
top_node = CompositeNode("UT_Decoding", [decoder_zs, decoder_error])
configure(options, top_node)
