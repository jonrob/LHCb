/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

/** @class UTHisto2DProperties UTHisto2DProperties
 *
 *  class to hold 2-D histogram properties
 *
 *  @author Andy Beiter based on code by:
 *  @author Johan Luisier
 *  @author M Needham
 *  @date   2018-09-04
 */

#include "Detector/UT/ChannelID.h"
#include <string>

namespace UT {
  class Histo2DProperties {

  public:
    Histo2DProperties( const std::string& name, const std::string& title, const unsigned int nbinx,
                       const unsigned int nbiny, const double xMin, const double xMax, const double yMin,
                       const double yMax );

    virtual ~Histo2DProperties() = default;

    const double& minBinX() const { return m_xMin; }

    const double& maxBinX() const { return m_xMax; }

    const double& minBinY() const { return m_yMin; }

    const double& maxBinY() const { return m_yMax; }

    const unsigned int& nBinX() const { return m_nBinX; }

    const unsigned int& nBinY() const { return m_nBinY; }

    const std::string& title() const { return m_title; }

    const std::string& name() const { return m_name; }

  private:
    std::string  m_name;
    std::string  m_title;
    unsigned int m_nBinX, m_nBinY;
    double       m_xMin, m_xMax, m_yMin, m_yMax;
  };

  inline Histo2DProperties::Histo2DProperties( const std::string& name, const std::string& title,
                                               const unsigned int nbinx, const unsigned int nbiny, const double xMin,
                                               const double xMax, const double yMin, const double yMax )
      : m_name( name )
      , m_title( title )
      , m_nBinX( nbinx )
      , m_nBinY( nbiny )
      , m_xMin( xMin )
      , m_xMax( xMax )
      , m_yMin( yMin )
      , m_yMax( yMax ) {
    // constructor
  }

} // namespace UT
