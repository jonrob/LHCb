/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

/** @namespace UTIDMapping UTIDMapping.h Kernel/UTIDMapping.h
 *
 *  Namespace for new/old UTID to old/new UTID
 *
 *  @author Xuhao Yuan
 *  @date   2021-03-02
 */

#include <map>
#include <sstream>
#include <string>

namespace LHCb::UTIDMapping {

  using Map = std::map<unsigned int, unsigned int>;

  const Map&   UTOldToNewMap();
  unsigned int ReconvertID( unsigned int oldchan );

#if 1 // only for coding test, should be removed finally XUHAO
  const Map&   UTNewToOldMap();
  unsigned int convertID( unsigned int newID );
#endif

} // namespace LHCb::UTIDMapping

// -----------------------------------------------------------------------------
// end of namespace
