/*****************************************************************************\
 * (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration      *
 *                                                                             *
 * This software is distributed under the terms of the GNU General Public      *
 * Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
 *                                                                             *
 * In applying this licence, CERN does not waive the privileges and immunities *
 * granted to it by virtue of its status as an Intergovernmental Organization  *
 * or submit itself to any jurisdiction.                                       *
 \*****************************************************************************/

#pragma once

#include "Detector/UT/ChannelID.h"
#include "Kernel/UTDAQDefinitions.h"
#include "Kernel/UTDAQID.h"
#include "Kernel/UTStripRepresentation.h"
#include "boost/container/static_vector.hpp"
#include <cstdint>
#include <iostream>
#include <vector>

/** @class UTDAQBoard UTDAQBoard.h "UTDAQ/UTDAQBoard.h"
 *
 *  Class that helps the UTReadoutTool organize the TELL40 DAQ boards and their associated offline sectors
 *
 *  @author M. Rudolph
 *  @date   2020-03-10
 */

namespace UTDAQ {

  class Board final {
    /// max # of lane in Board/RawBank
    constexpr static auto maxNumLanes = 6;

  public:
    // constructor from a single DAQ ID with the board number
    Board( const UTDAQID::BoardID daqid );

    // add this sector to the lane list, returning its full DAQID
    UTDAQID addSector( LHCb::Detector::UT::ChannelID sector, unsigned int orientation );

    // get the UTDAQID
    UTDAQID boardID() const;

    /// vector of sectors on the board
    const boost::container::static_vector<LHCb::Detector::UT::ChannelID, maxNumLanes>& sectorIDs() const;

    /// vector of hybrid orientations
    const boost::container::static_vector<unsigned int, maxNumLanes>& orientation() const;

  private:
    UTDAQID                                                                     m_boardID;
    boost::container::static_vector<LHCb::Detector::UT::ChannelID, maxNumLanes> m_sectorsVector;
    boost::container::static_vector<unsigned int, maxNumLanes>                  m_orientationVector;
  };
} // namespace UTDAQ

inline UTDAQID UTDAQ::Board::boardID() const { return m_boardID; }

inline const boost::container::static_vector<LHCb::Detector::UT::ChannelID, UTDAQ::Board::maxNumLanes>&
UTDAQ::Board::sectorIDs() const {
  return m_sectorsVector;
}

inline const boost::container::static_vector<unsigned int, UTDAQ::Board::maxNumLanes>&
UTDAQ::Board::orientation() const {
  return m_orientationVector;
}
