###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Simple script which takes an output location and a year
Checks for length of '/Event/Rec/Vertex/Primary' location
and returns the length if generated. Else, returns a problem.

usage: python CheckPGPrimaryVertexPropagation.py <outputfile> <year>

where outputfile can be the output of Gauss, Boole or Brunel, and
year is the datatype to be passed to LHCbApp
"""
import logging
log = logging.getLogger("CheckPGPrimaryVertexPropagation")
import GaudiPython as GP
import sys
from GaudiConf import IOHelper


def main():

    IOHelper('ROOT').inputFiles([sys.argv[1]])
    theyear = sys.argv[2]
    from Configurables import LHCbApp
    LHCbApp().DataType = theyear
    LHCbApp().Simulation = True

    appMgr = GP.AppMgr()
    evt = appMgr.evtsvc()

    appMgr.run(1)
    if evt['/Event/Rec/Vertex/Primary'].size() > 0:
        log.info('Got', evt['/Event/Rec/Vertex/Primary'].size(), 'PVs')

    else:
        log.error('Probem! No data at location at Rec/Vertex/Primary')


if __name__ == "__main__":
    main()
