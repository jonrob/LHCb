/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "DumpHepMCDecay.h"
#include "Event/HepMCEvent.h"
#include "HepMC/GenParticle.h"
#include "HepMC/GenVertex.h"
#include "Kernel/STLExtensions.h"

/**
 *  simple class to dump HepMC::GenEvent obejcts
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 *  @date 2006-10-25
 */
struct DumpHepMCTree : DumpHepMCDecay {
  using DumpHepMCDecay::DumpHepMCDecay;
  void operator()( Gaudi::Functional::vector_of_const_<LHCb::HepMCEvents> const& ) const override;

  mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_signalN1Warn{this, "Signal_process_vertex is NULL, use -1"};
  mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_signalSkipWarn{this, "Signal_process_vertex is NULL, skip!"};
  mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR>   m_nullGenPart{this,
                                                                    "printDecay(): HepMC::GenParticle* points to NULL"};
};

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( DumpHepMCTree )

void DumpHepMCTree::operator()( Gaudi::Functional::vector_of_const_<LHCb::HepMCEvents> const& containers ) const {
  // get the stream
  MsgStream& log = info();
  log << " Tree dump [cut-off at " << m_levels << " levels] " << endmsg;
  for ( const auto& [n, events] : LHCb::range::enumerate( containers ) ) {
    log << " Container '" << inputLocation( n ) << "' " << endmsg;
    for ( const auto& event : events ) {
      if ( !event ) { continue; } // CONTINUE
      const HepMC::GenEvent* evt = event->pGenEvt();
      if ( !evt ) { continue; } // CONTINUE
      log << " #particles/vertices : " << evt->particles_size() << "/" << evt->vertices_size() << endmsg;
      HepMC::GenVertex* signal = evt->signal_process_vertex();
      if ( !signal ) {
        signal = evt->barcode_to_vertex( -1 );
        if ( signal ) {
          ++m_signalN1Warn;
        } else {
          ++m_signalSkipWarn;
          continue;
        }
      }
      if ( log.isActive() ) {
        std::for_each( signal->particles_begin( HepMC::children ), signal->particles_end( HepMC::children ),
                       [&]( const HepMC::GenParticle* p ) {
                         if ( !p ) {
                           ++m_nullGenPart;
                         } else {
                           printDecay( *p, log.stream(), 0 );
                         }
                       } );
      }
    }
  }
  log << endmsg;
}
