2024-05-08 LHCb v55r8
===

This version uses
Detector [v1r32](../../../../Detector/-/tags/v1r32),
Gaudi [v38r1](../../../../Gaudi/-/tags/v38r1) and
LCG [105a](http://lcginfo.cern.ch/release/105a/) with ROOT 6.30.04.

This version is released on the `2024-patches` branch.
Built relative to LHCb [v55r7](/../../tags/v55r7), with the following changes:

### New features ~"new feature"

- ~UT | New sourceID encoding and new UT ReadoutMap, !4389 (@hawu)


### Fixes ~"bug fix" ~workaround

- Fix SelReport decoder/encoder mismatch, !4545 (@thboettc) [Allen#534]
- Consistently use LHCb::span instead of gsl::span, !4458 (@graven) [#351]
- ~VP | Fix confused VP A and C side assignment, !4523 (@tmombach) [PUB-2019]


### Enhancements ~enhancement

- ~PID | Add 2024 ProbNN type enum, !4552 (@mveghel)
- ~Persistency | Add get_flavourtags function and missing flavour tag selection unpacker, !4471 (@sesen)


### Code cleanups and changes to tests ~modernisation ~cleanup ~testing

- ~FT | Diminish verbosity of FT decoding, !4536 (@lohenry)
- Disable GaudiAlg tuple.ex1 test with clang, !4535 (@jonrob)
