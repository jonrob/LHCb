2024-03-18 LHCb v55r3
===

This version uses
Gaudi [v38r1](../../../../Gaudi/-/tags/v38r1),
Detector [v1r28](../../../../Detector/-/tags/v1r28) and
LCG [105a](http://lcginfo.cern.ch/release/105a/) with ROOT 6.30.04.

This version is released on the `master` branch.
Built relative to LHCb [v55r2](/../../tags/v55r2), with the following changes:

### New features ~"new feature"



### Fixes ~"bug fix" ~workaround



### Enhancements ~enhancement

- ~Configuration ~Decoding ~Persistency | Add option to ANNSvc to publish decoding keys to monitoring hub, and read decoding keys from FSR, !4404 (@graven)


### Code cleanups and changes to tests ~modernisation ~cleanup ~testing

- ~Tracking ~PID | SIMD ML inference backend, !4305 (@mveghel)


### Documentation ~Documentation


### Other

