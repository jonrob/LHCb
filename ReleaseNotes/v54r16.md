2023-09-01 LHCb v54r16
===

This version uses
Gaudi [v36r16](../../../../Gaudi/-/tags/v36r16),
Detector [v1r19](../../../../Detector/-/tags/v1r19) and
LCG [103](http://lcginfo.cern.ch/release/103/) with ROOT 6.28.00.

This version is released on the `master` branch.
Built relative to LHCb [v54r15](/../../tags/v54r15), with the following changes:

### New features ~"new feature"



### Fixes ~"bug fix" ~workaround



### Enhancements ~enhancement

- ~Tracking | Introduce BegT and EndT state locations, drop AtT, !4172 (@ausachov)


### Code cleanups and changes to tests ~modernisation ~cleanup ~testing

- ~Core | Suppress some INFO messages from HLTScheduler.write test, !4263 (@jonrob)
- Modernise CountIterator and apply it, !4256 (@gunther) [Rec#480]


### Documentation ~Documentation


### Other

- ~Decoding ~Muon | MuonRawInUpgradeToHits: Fix clang warning, !4261 (@jonrob)
- ~Decoding ~RICH | RichDecodedData: remove duplicate ID assert, !4267 (@jonrob)
- ~Decoding ~RICH | RichDecodedData: make some setters for # hits and PDs private, !4265 (@jonrob)
- ~RICH | DecodedDataFromMCRichHits: Also create MCRichDigitSummarys when decoding from MCRichHits, !4255 (@jonrob)
- Service to write luminosity information to TTree when reading from FSRs, !4174 (@egraveri)
