2023-05-09 LHCb v54r8
===

This version uses
Gaudi [v36r12](../../../../Gaudi/-/tags/v36r12),
Detector [v1r12](../../../../Detector/-/tags/v1r12) and
LCG [103](http://lcginfo.cern.ch/release/103/) with ROOT 6.28.00.

This version is released on the `master` branch.
Built relative to LHCb [v54r7](/../../tags/v54r7), with the following changes:

### New features ~"new feature"



### Fixes ~"bug fix" ~workaround

- Fix comparison operator for entries in relation tables, !4104 (@graven) [Moore#559]


### Enhancements ~enhancement

- ~Persistency | Packing: Allow dependencies to be anonymized, !4055 (@graven)


### Code cleanups and changes to tests ~modernisation ~cleanup ~testing



### Documentation ~Documentation


### Other

- ~Configuration | Add input_process option to Moore, !4017 (@kmattiol)
- ~Decoding ~FT | Fix FT odd bank detection, !4077 (@lohenry)
- ~UT | Update UT Channel ID and geometry, !2995 (@xuyuan)
- ~Persistency | Combining RawBank::Views for RawBanks SP, !3942 (@msaur)
- Update References for: LHCb!4106, Alignment!376, Analysis!981, DaVinci!899 based on lhcb-master-mr/7800, !4111 (@lhcbsoft)
- Reduce the number of `bind` statements needed: improved configuration (derivation) of SourceIDs, !4110 (@graven)
- Update References for: LHCb!4108 based on lhcb-master-mr/7773, !4109 (@lhcbsoft)
- Apply lumi counter shifts and scales when decoding/writing/monitoring, !4108 (@dcraik)
- Generate meaningfull unique deterministic names for unpackers, decoders, fetchers, ..., !4106 (@graven)
- Add hash to name of Online output alg, !4080 (@mstahl)
- Fixed bug in counter diffs, leading to empty missing/extra counter statements, !4107 (@sponce)
- Change list of algorithms in FSR periodic flush test, !4103 (@clemenci)
- Fixed detdesc ref for velo_motion_from_yaml_pyconf, !4095 (@sponce)
- Moved cluster max width to a constant rather than an option, !4084 (@lohenry)
- Add suggestion to use qmtexec if .qmt file is given to lbexec, !4081 (@ldufour)
- Fix recsummary necalcluster, !4066 (@sbelin)
- Make the FileSummaryRecord.periodic_flush test more reliable, !4061 (@clemenci)
- Dropped usage of GaudiAlgorithm where it is not needed, !4056 (@sponce)
- Follows https://gitlab.cern.ch/lhcb/Rec/-/merge_requests/3138 (Add opt-in for mass2 and pid), !4012 (@tfulghes)
