2022-10-07 LHCb v54r0
===

This version uses
Detector [v1r4](../../../../Detector/-/tags/v1r4),
Gaudi [v36r7](../../../../Gaudi/-/tags/v36r7) and
LCG [101](http://lcginfo.cern.ch/release/101/) with ROOT 6.24.06.

This version is released on `master` branch.
Built relative to LHCb [v53r10](/../../tags/v53r10), with the following changes:

### New features ~"new feature"

- ~Decoding ~FT | Add the FT v8 decoding, !3770 (@lohenry)
- ~Decoding ~Luminosity | Add header files used for encoding and decoding Allen LumiSummary banks, !3786 (@dcraik)
- ~Composites | Add function to unbiase PVs + bestPV function, !3716 (@cprouve)
- ~"Event model" | Add templated mergers, !3665 (@decianm)
- ~Persistency | Enforce use of an encoding/decoding key (TCK) in all Hlt encoders/decoders, do not use online-reserved bits in SourceID, remove explicit mention of 'packed' locations, enable 'stable' persist reco locations, !3528 (@graven) [#194,#202,#220,#221,#225]


### Fixes ~"bug fix" ~workaround

- ~Muon | MuonChamberLayout create tile id vector only locally to make move operator safe, !3749 (@gunther)
- ~Persistency | Fix bug while writing keys to manifest, !3782 (@graven)
- ~Persistency | Make manifest creation independent of the state of the local filecontent-metadata repository, !3777 (@graven)
- ~Persistency | Fix key collision detection when merging json files with decoding keys, !3773 (@graven)
- ~Utilities | Fix memory leak in SOADataVector, !3732 (@rmatev) [MooreOnline#8]


### Enhancements ~enhancement

- ~Configuration | Add Options.apply_binds to allow applications to bind before calling user code, !3797 (@cburr)
- ~Configuration | PyConf: Deterministic identity of components and data, !3754 (@rmatev)
- ~Decoding ~FT | Check SciFi banks and sort them by hand, !3735 (@lohenry)
- ~Calo | Add option to calibrate calo digits, !3758 (@jmarchan)
- ~Calo | Add Epos in CalculateEXY return, !3763 (@nuvallsc)
- ~Persistency | Loopy unpacking and multi packing, !3750 (@sesen)
- ~Simulation | Convert Condition to YAML on demand, !3765 (@clemenci) [#241,Moore#473]
- Feat(HltControlFlowMgr) sort timing table and introduce a row that shows the sum of all algorithms, !3696 (@chasse)


### Code cleanups and changes to tests ~modernisation ~cleanup ~testing

- ~Decoding | Change default ODIN encoding to v7, !3762 (@rmatev) [#202]
- ~Decoding ~Muon | Migrate muon decoding test to use an upgrade sample, !3794 (@jonrob)
- ~Decoding ~Muon | Re-enable muon daq decoding test with dd4hep, !3792 (@jonrob)
- ~Decoding ~Muon | Use proper SourceID in Muon encoding, !3764 (@rmatev) [#202]
- ~Decoding ~Muon | Skip MuonDAQ decoding test in dd4hep builds, !3730 (@jonrob)
- ~Decoding ~Calo | Update data tag used for calo decoding test, !3793 (@jonrob)
- ~VP | Fix for VPRetinaSPmixer, !3775 (@gbassi)
- ~FT | Add the stationIdx method, !3759 (@lohenry)
- ~Calo | Fix calo raw bank type, !3791 (@jmarchan)
- ~Calo | Remove CaloFutureMergeTAE, !3751 (@gwan)
- ~RICH | Rich DAQ - Remove unused operators, !3772 (@jonrob)
- ~RICH | RichDAQDefinitions - Clean up type specifications, !3771 (@jonrob)
- ~RICH ~Persistency | RICH SourceID - Add constructor from individual values, !3766 (@jonrob)
- ~"Event model" | Rename legacy detectors in MCTrackInfo, !3767 (@rmatev) [Boole#10]
- ~Persistency | Move reco objects from DaVinci to PyConf, !3802 (@sesen)
- ~Persistency | Add checksum tests to packing, !3799 (@sesen)
- ~Persistency | Fix CaloHypo packing accuracy check, !3795 (@rmatev)
- ~Core | Switch GaudiConf python test from CALO to RICH test, as it works with dd4hep, !3729 (@jonrob)
- ~Simulation ~Accelerators | Clean up LHCbID, !3226 (@decianm)
- Packer::Carry namespace and QMTest fix, !3801 (@rmatev)
- Simplifies DD4hep code now that /world is the default path, !3785 (@sponce)
- Fix warnings from super project build, !3778 (@rmatev)
- Add missing boost::hash include, !3761 (@clemenci) [gaudi/Gaudi#238]
- Reduce verbosity when starting application, !3756 (@sstahl)
- Add qmtexec command for running QMT tests directly, !3697 (@cburr) [#240]


### Other

- ~Decoding ~Muon | Improve DD4hep compatibility of MuonRawInUpgradeToHits, !3726 (@raaij)
- ~Decoding ~Calo | Check calo bank size, !3715 (@cmarinbe)
- ~Tracking ~"Event model" | Sort LHCbIDs before returning std::vector<LHCb::LHCbID>, !3746 (@decianm)
- ~FT | Changed calculateChannelAndFrac to stop passing by reference the fraction, !3745 (@lohenry)
- ~Calo | Add Hcal and Ecal to default dd4hep detector lists, !3742 (@chenjia)
- ~Calo | Remove getDet in CaloFutureElectron, !3741 (@gwan)
- ~Calo ~Monitoring | Use error counter instead of error message in CaloFuture2Dview, !3755 (@cmarinbe)
- ~Calo ~Monitoring | Remove getDet in CaloFuture2DView, !3717 (@cmarinbe)
- ~RICH | DeRichPMTPanel: protect against null test PD pointer, !3743 (@jonrob)
- ~RICH | Protect DeRichPMT::readPMTPropertiesFromDB from NULL condition, !3737 (@jonrob)
- ~Conditions | Add support for Velo Motion System override from YAML in PyConf, !3711 (@clemenci)
- Delay XRootD import to only require it if an eos file is requested, !3783 (@dcampora)
- Add test for decoding of CALO raw event, !3752 (@raaij)
- Make some lbexec tests independent of the value of GAUDIAPPNAME, !3760 (@clemenci) [gaudi/Gaudi#233]
- Lbexec improvements, !3725 (@cburr)
- Change default raw_event_format to 0.5, !3687 (@nskidmor)
- Adding motionSystemTransform to IDetectorElementPlus, !3731 (@rmohamme)
- Test for override of Velo motion system from YAML files, !3723 (@raaij) [#244]
- Add helper for input/output location names for LHCb::Algorithms when they are templated, !3724 (@decianm)
- Support for muon dumping in Allen, !3707 (@samarian)
