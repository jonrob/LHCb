2020-08-03 LHCb v51r1
===

This version uses
Gaudi [v33r2](../../../../Gaudi/-/tags/v33r2) and
LCG [97a](http://lcginfo.cern.ch/release/97a/) with ROOT 6.20.06.

This version is released on `master` branch.
Built relative to LHCb [v51r0](../-/tags/v51r0), with the following changes:

### New features ~"new feature"

- ~Tracking ~"Event model" | Add PrSeedTracks SOA container, !2559 (@decianm)
- ~Calo | Add a `find(CaloCellID)` function to CaloClusters, !2569 (@graven)
- ~Calo ~"Event model" | Add MC2Cluster relations table type, !2605 (@cmarinbe)
- ~Calo ~"Event model" | Add CaloDigits v2 and CaloClusters v2, !2166 (@graven) :star:
- ~RICH ~Simulation | Add SIN code type to RICH digit history., !2581 (@bmalecki)
- ~"Event model" ~Simulation | Add times to MCVPDigit and HV and bad pixels to DeVPSensor to update VP simulation, !2473 (@hcroft)
- ~Core | Generic algorithms + example/test for writing a barrier with ZipSelections, !2518 (@pseyfert)
- ~Core | Add machinery for tracking dynamic allocations and enable its use in the scheduler, !2472 (@olupton) :star:
- ~Conditions ~Build | Support for DD4hep in the framework and for the VP subdetector, !2455 (@sponce) :star:
- ~Utilities | Extend meta_enum: add a members_of<Enum>() function to get a range over all declared values, !2566 (@graven)
- ~Utilities | Migrate TrackKernel/Chunker to LHCb::range::chunk, !2560 (@graven)
- ~Utilities | Add helpers for making unique_ptrs with custom allocators, !2475 (@olupton)
- ~Simulation | Add pgprimaryvertex check script (@adavis), !2530 (@cattanem)
- ~Build | Apply sensitivity to counter numbers when explicitly specified, !2630 (@sponce)


### Fixes ~"bug fix" ~workaround

- ~Tracking | Use signed int for indices in field map bounds check to avoid FPE, !2657 (@jonrob)
- ~Tracking | TrajPoca: Fail and warn when point POCA fails on downgrade, !2499 (@jonrob)
- ~Calo | Fix CaloClusters v2 unit test, !2609 (@graven)
- ~Functors ~Build | Workaround for gaudi/Gaudi#117, !2438 (@rmatev) [gaudi/Gaudi#117]
- ~Core | Remove caching of the event-local memory resource pointer, !2613 (@olupton)
- ~Core | Properly fail when you define multiple control flow nodes with the same name, !2446 (@nnolte) [#81]
- ~Conditions | Avoid mixed use of inline const string and static string as with clang10 results in null value, !2658 (@jonrob)
- ~Conditions | Make sure FTMat cached geo data is updated when conditions change, !2640 (@wouter)
- ~Conditions | Fix asan stack-use-after-scope error (Alignment), !2578 (@rmatev) [#0,#1,#2]
- ~Utilities | Fix AVX256/512 loop_mask to handle general loops, !2458 (@gunther)
- ~Build | Fixed usage of small_vector in conjunction with cling, !2454 (@sponce) [#75]
- ~Build | Ignore unchecked status codes, !2419 (@clemenci)
- Fix typo in maskgather for variadic templates in SIMDWrapper, !2675 (@decianm)


### Enhancements ~enhancement

- ~Configuration | Demote warning on use of latest DB tags, !2665 (@rmatev)
- ~Decoding | New RawBank types for upgrade, !2577 (@gvouters)
- ~Decoding | Reserve DAQ error type range as per EDMS2100937, !2576 (@pdurante)
- ~Tracking | Put numbers needed when dealing with UT sectors and layers to 'UTInfo', !2590 (@decianm)
- ~Tracking | Updated UTDAQHelper for PrVeloUT / Some new numbers in UTInfo, !2564 (@decianm)
- ~Tracking | Store hits indices in Pr::Tracks, !2561 (@peilian)
- ~Calo | Use meta_enum for Calo::Enum::Datatype, !2568 (@sponce)
- ~Calo | Next evolution of CaloDigit container, !2546 (@graven)
- ~Calo | Extend CaloClusters v2 functionality, !2542 (@graven)
- ~Calo | Optimize CaloFutureRawToDigits, !2541 (@graven)
- ~Calo | Truncate calo digit energy to multiple of 0.2 MeV when computing position, !2465 (@graven)
- ~Calo | Improve position stability in calculateClusterEXY, !2423 (@graven) [Rec#118]
- ~Calo | Add HypoTable RelationWeighted1D table, !2380 (@ozenaiev)
- ~RICH | RichSmartID - Better support for PMTs, more specific printout and accessors, !2643 (@jonrob)
- ~RICH | RichFutureUtils - Various enhancements to derived condition objects, !2521 (@jonrob)
- ~RICH | LHCbKernel - rich types use boost small vector, !2513 (@jonrob)
- ~RICH | RichSystem - Add public RichDet accessors, !2487 (@jonrob)
- ~RICH ~"MC checking" | Add RICH MC helper class(es) for associations to extended MC Info, !2514 (@jonrob)
- ~RICH ~Conditions | Add DDDB locations for RICH PMT readout time info, !2481 (@bmalecki)
- ~RICH ~Utilities | SIMDTypes : Cleaner bit casting implentation and more Vc:: isolation, !2453 (@jonrob)
- ~Composites ~Functors | Functor/combiner proceedings, !2448 (@nnolte)
- ~Composites ~"Event model" ~Core | Move v2::Particle types from Phys to LHCb, add new zip proxy types, !2626 (@olupton)
- ~Functors ~Build | Faster decoration for context-dependent functors, !2627 (@rmatev) [Moore#187]
- ~"Event model" ~Core ~Utilities | Assorted fixes (StacktraceAllocationTracker, ZipFamilyNumber, ProducePrFittedForwardTracks), !2635 (@olupton)
- ~"Event model" ~Utilities | Minor improvements for SIMDWrapper / zipping / event model, !2661 (@olupton)
- ~"Event model" ~Utilities | Make loop_mask() return mask_true() for Broadcast/Gather proxies, !2650 (@olupton)
- ~Persistency | Use event-local memory pool when reading RawEvent, !2338 (@olupton) [ROOT-10526]
- ~Core | No vtable dispatch during node sequence traversal in scheduler, !2632 (@nnolte)
- ~Core | EventContextExt: use speculative get to avoid extraneous type check, !2614 (@graven)
- ~Core | Add time printout when scheduler starts/ends timing events, !2522 (@chasse)
- ~Core | Respect ZIPPING_SEMANTIC_CHECKS in areSemanticallyCompatible, !2412 (@pseyfert)
- ~Core ~Utilities | Tweak SIMDWrapper conversions / operations, !2621 (@olupton)
- ~Conditions | Extended DetectorElement interface to avoid direct use of IGeometryInfo in clients, !2656 (@sponce)
- ~Conditions | Add IOV to DD4hep conditions test and move to yml, !2623 (@bcouturi)
- ~Conditions ~Simulation | Update DDDB default global tags to latest ones from https://gitlab.cern.ch/lhcb-conddb/DDDB/-/issues/3, !2573 (@cattanem)
- ~Utilities | Extend Absolute precision truncation with support for arbitrary precision and rounding, !2502 (@jonrob)
- ~Accelerators | Updated Retina Clustering, !2436 (@flazzari)
- ~Build | Fix dependency of functor cache and GaudiConfig2 (follow up !2537), !2670 (@rmatev)
- ~Build | Dump gaudi job options for all tests, !2663 (@rmatev) [LHCBPS-1874]
- ~Build | Add new clang10 platform string to PlatformInfo.cpp, !2646 (@chasse)
- ~Build | Fix dependency problem with functor cache and GaudiConfig2, !2537 (@clemenci)
- ~Build | Ignore DataFault incidents count in ref validation, !2501 (@rmatev)


### Code cleanups and changes to tests ~modernisation ~cleanup ~testing

- ~Configuration | Prepare for the rename of DataObjectHandleBase to DataHandle, !2653 (@clemenci)
- ~Decoding | Prefer explicit `put(unique_ptr<T>)` over of `put(T*)`, !2628 (@graven)
- ~Decoding | Prefer explicit `put(unique_ptr<T>)` over of `put(T*)`, !2625 (@graven)
- ~Decoding | Modernize VeloDAQ, !2299 (@graven)
- ~Decoding ~Muon | MuonRawToHits: avoid repeating code, !2421 (@graven)
- ~Decoding ~Muon ~Persistency | Suppress warnings in LCG DEV(3/4) slots, !2610 (@jonrob)
- ~Decoding ~Monitoring ~Conditions | Modernize UTDAQ, !2290 (@graven)
- ~Tracking | Modernize MagneticField code, !2519 (@graven)
- ~Tracking ~"MC checking" | Make IIdealStateCreator Interface independent of TrackClass, !2562 (@chasse)
- ~Tracking ~Utilities | Dropped usage of VCL in LHCb, !2634 (@sponce)
- ~"PV finding" ~Composites | Modernize TrajPoca, !2504 (@graven)
- ~Calo | Introduce dedicated type for CaloDigitStatus::Status, !2666 (@graven)
- ~Calo | Move SpreadEstimator, Digit2ClustersConnector and SharedCells to CaloFutureReco in Rec, !2606 (@graven)
- ~Calo | Fixed includes in CaloFuture2Track.h so that it's self contained, !2567 (@sponce)
- ~Calo | Introduce dedicated type for FE cards, !2557 (@graven)
- ~Calo | Cleanup CaloFutureMergeTAE, !2547 (@graven)
- ~Calo | Move CovarianceEstimator to Rec, !2533 (@graven)
- ~Calo | Cleanup & Extend CaloCellID, add 'dense' index, !2532 (@graven)
- ~Calo ~Persistency | Backward compatible changes to prepare for Gaudi new JobOptionsSvc, !2659 (@clemenci)
- ~Calo ~Core ~Conditions ~Utilities | Fixes for clang10 build, !2642 (@chasse)
- ~Calo ~Core ~Utilities | Adapt to MS-GSL v3, !2550 (@graven)
- ~RICH | Remove HPD specific RICH code, !2636 (@jonrob)
- ~RICH | Remove unambiguous photon flag from core RICH Photon classes., !2558 (@jonrob)
- ~PID ~Composites | Modernize proto particle algorithms, !2422 (@sstahl)
- ~Functors | LoKi/Power.h - Suppress ignored StatusCode warning, !2629 (@jonrob)
- ~Functors | Remove slow Loki import, !2534 (@chasse)
- ~Functors ~Simulation | Modernize LoKi, !2293 (@graven)
- ~"Event model" | SOAExtensions/ZipUtils.h - Use explicit 32bit type, for clarity, and default initialise, !2654 (@jonrob)
- ~"Event model" | Fixed and cleaned up/modernized Relations, !2572 (@sponce)
  - ~"Event model" | Fix memory leaks in Relations{1,2}D (follow up !2572), !2617 (@graven) [Rec#145]
  - ~"Event model" | Add back public IRelation2D inheritance to Relation2 (follow up !2572), !2612 (@jonrob) [Analysis#3]
- ~"Event model" ~Utilities | Update difference logic of PrZip iterators, !2411 (@pseyfert) [#77]
- ~"Event model" ~Simulation | Modernize GenEvent, !2301 (@graven)
- ~Persistency | Modernize DAQUtils, !2303 (@graven)
- ~Persistency | Modernize WritePackedDst, !2298 (@graven)
- ~Core | Using tbb::task_arena as threadpool instead of global task_scheduler_init, !2381 (@nnolte)
- ~Core | Modernize LHCbAlgs, !2295 (@graven)
- ~Conditions | Fix Detector XML path and use send ROOT and DD4hep to the Gaudi message service, !2579 (@bcouturi) [#86]
- ~Conditions | Workaround for incompatibility between vectorclass 1 and 2, !2574 (@clemenci) [SPI-1619]
- ~Utilities | Move Online/ZeroMQ from Online to Tools/ZeroMQ, !2649 (@raaij)
- ~Utilities | Minor fixes for sanitizer builds, !2616 (@jonrob)
- ~Utilities | Follow up on closing of https://github.com/VcDevel/Vc/issues/177, !2543 (@pseyfert)
- ~Utilities | Add length-checked constructors to SIMDWrapper, !2439 (@pseyfert) [#78]
- ~Simulation | Modernize SimComponents, !2289 (@graven)
- ~Build | Remove LAPACK lookup in LHCbMath, !2669 (@graven)
- ~Build | Fix signed / unsigned comparison warning from meta_enum.h, !2622 (@jonrob)
- ~Build | Define CPP macro to suppress vectorclass AVX2 no FMA warning, !2586 (@jonrob)
- ~Build | Avoid operators in the global namespace, !2535 (@graven)
- ~Build | Ignore one more unchecked StatusCode, seen with clang, !2461 (@cattanem)
- ~Build | Unify cmake style for cppgsl detection, !2456 (@pseyfert)
- ~Build | Remove WIN32 dead code, !2441 (@cattanem)
- Minimize refs produced by validateWithRef, !2505 (@rmatev)
- Support Python 3, !2214 (@apearce)


### Documentation ~Documentation

- Update CONTRIBUTING.md, !2644 (@cattanem)
- Use c++ syntax highlighting for icpp files, !2509 (@chasse)
