###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Configurables import ApplicationMgr, ZeroMQSvc

ApplicationMgr().ExtSvc += ["ZeroMQSvc"]
ApplicationMgr().EvtMax = 1
ApplicationMgr().EvtSel = "NONE"
ApplicationMgr().HistogramPersistency = "NONE"
ZeroMQSvc().Encoding = "text"
ApplicationMgr().TopAlg = ["TestSerializationAlgo/TestAlgo"]
