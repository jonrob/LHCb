/*****************************************************************************\
* (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "GaudiAlg/Consumer.h"

#include "Event/RawEvent.h"

struct CountBanks : Gaudi::Functional::Consumer<void( const LHCb::RawEvent& )> {
  CountBanks( const std::string& name, ISvcLocator* pSvcLocator )
      : Consumer( name, pSvcLocator, {KeyValue{"RawEventLocation", ""}} ){};

  void operator()( const LHCb::RawEvent& evt ) const override {
    info() << "evt got " << evt.size() << " banks" << endmsg;
  }
};

DECLARE_COMPONENT( CountBanks )
