###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#[=======================================================================[.rst:
Ex/IOExample
------------
#]=======================================================================]

gaudi_add_module(IOExample
    SOURCES
        src/CountBanks.cpp
        src/MDFIOMixerExample.cpp
        src/RootIOMixerExample.cpp
    LINK
        Gaudi::GaudiAlgLib
        LHCb::DAQEventLib
        LHCb::MDFLib
        LHCb::IOAlgorithmsLib
)

gaudi_install(PYTHON)
gaudi_generate_confuserdb()
lhcb_add_confuser_dependencies(
    GaudiConf
    Kernel/LHCbAlgs:LHCbAlgs
)

gaudi_add_tests(QMTest)
