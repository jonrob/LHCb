###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Gaudi.Configuration import *

from PRConfig.TestFileDB import test_file_db

import Configurables
from Configurables import HLTControlFlowMgr, ApplicationMgr, HiveWhiteBoard, HiveDataBrokerSvc, CountBanks
from subprocess import check_call

nbFiles = 6


def fileName(n, extension):
    return f'rootIOMixerInput_{n}{extension}.dst'


def test(mixerAlgo, extension, evtMax=10):
    # replicate input file so that we have several at hand
    fileDbEntry = test_file_db["rootreadingexample"]
    check_call([
        "xrdcp", "-s", "-f", fileDbEntry.filenames[0],
        fileName(0, extension)
    ])
    for n in range(1, nbFiles):
        check_call(["cp", fileName(0, extension), fileName(n, extension)])

    raw_event_location = "/Event/DAQ/RawEvent"
    ioalgType = getattr(Configurables, mixerAlgo)
    ioalg = ioalgType(
        "RootIOMixer",
        Input=[[fileName(n, extension)] for n in range(nbFiles)],
        EventBufferLocation="/Event/DAQ/RawEventBuffer",
        EventBranches=[raw_event_location],
        BufferNbEvents=20,
        RandomSeed=12345)
    countalg = CountBanks(
        name="CountBanks", RawEventLocation=raw_event_location)

    # make sure Gaudi does not close files in our back !
    IODataManager().AgeLimit = nbFiles + 1

    hiveDataBroker = HiveDataBrokerSvc('HiveDataBrokerSvc')
    hiveDataBroker.DataProducers.append(ioalg)
    hiveDataBroker.DataProducers.append(countalg)

    scheduler = HLTControlFlowMgr(
        'HLTControlFlowMgr',
        CompositeCFNodes=[('testIOAlg', 'LAZY_AND',
                           ["RootIOMixer", "CountBanks"], True)],
        MemoryPoolSize=10 * 1024 * 1024,
        ThreadPoolSize=1,
        EnableLegacyMode=False,
        BarrierAlgNames=[])

    app = ApplicationMgr(
        EvtSel="NONE",
        EvtMax=evtMax,
        EventLoop=scheduler,
        TopAlg=[ioalg, countalg])
    whiteboard = HiveWhiteBoard('EventDataSvc', EventSlots=1, ForceLeaves=True)
    app.ExtSvc.insert(0, whiteboard)


def test_std():
    test("RootIOMixerExample", "")


def test_ext():
    test("RootIOMixerExtExample", "ext")


def test_infinite():
    test("RootIOMixerExample", "inf", -1)


def test_toomuch():
    test("RootIOMixerExample", "inf", 18)
