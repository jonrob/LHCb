###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# Test to copy a POOL dst file to a ROOT dst file
from Configurables import IOTest
from GaudiConf.IOHelper import IOHelper
from PRConfig import TestFileDB
from DDDB.CheckDD4Hep import UseDD4Hep

IOTest(
    EvtMax=5, LoadAll=True, OdinLocation="DAQ/ODIN" if not UseDD4Hep else '')

ioh = IOHelper()
ioh.setupServices()
ioh.outStream('PFN:ROOT-Reco08.dst', "InputCopyStream")

TestFileDB.test_file_db["Reco08-bhadron.dst"].run()
