###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from GaudiConf.LbExec import Options
from PRConfig.TestFileDB import test_file_db
import os
import tempfile
from subprocess import check_call

from PyConf.application import configure_input, configure
from PyConf.control_flow import CompositeNode

from PyConf.application import make_odin
from PyConf.Algorithms import PrintHeader


def main(options: Options):
    configure_input(options)
    cf = CompositeNode(
        "Top", [PrintHeader(name="PrintHeader", ODINLocation=make_odin())])
    return configure(options, cf)


fileDbEntry = test_file_db["mdfreadingexample"]
qualifiers = fileDbEntry.qualifiers

# Download a file with no compression
path1 = "./file1.mdf"
if not os.path.isfile(path1):
    check_call(
        ["xrdcp", "-s", fileDbEntry.filenames[0].replace("mdf:", ""), path1])

# Download a file with compressed MDF payloads
path2 = "./file2.mdf"
if not os.path.isfile(path2):
    check_call(
        ["xrdcp", "-s", fileDbEntry.filenames[1].replace("mdf:", ""), path2])

# create a compressed versions of path1 and path2
path1z = "./file1z.mdf.zst"
if not os.path.isfile(path1z):
    check_call(["zstd", "--quiet", "-f", path1, "-o", path1z])
path2z = "./file2z.mdf.zst"
if not os.path.isfile(path2z):
    check_call(["zstd", "--quiet", "-f", path2, "-o", path2z])

# Define the options for the application
options = {
    "data_type": "Upgrade",
    "simulation": True,
    "dddb_tag": qualifiers["DDDB"],
    "conddb_tag": qualifiers["CondDB"],
    "input_files": [path1, path1z, path2, path2z],
    "input_type": "RAW",
    "print_freq": 500,
}
