###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Gaudi.Configuration import DEBUG
from PyConf.application import ApplicationOptions, configure_input, configure, CompositeNode
from PyConf.reading import get_mc_header, get_mc_particles, get_mc_vertices
from PyConf.Algorithms import DumpMCEventAlg

options = ApplicationOptions(_enabled=False)
options.set_input_and_conds_from_testfiledb(
    "upgrade_Sept2022_minbias_0fb_md_xdigi")
options.evt_max = 10

configure_input(options)

dump = DumpMCEventAlg(
    name="DumpMCEventAlg",
    OutputLevel=DEBUG,
    MCHeader=get_mc_header(),
    MCParticles=get_mc_particles("/Event/MC/Particles"),
    MCVertices=get_mc_vertices("/Event/MC/Vertices"))

configure(options, CompositeNode("top", [dump]))
