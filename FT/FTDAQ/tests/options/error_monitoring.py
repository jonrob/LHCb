###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from os.path import basename, join
from Gaudi.Configuration import ApplicationMgr, MessageSvc, INFO, DEBUG, ERROR, VERBOSE
from Configurables import Gaudi__Monitoring__JSONSink as JSONSink
from Configurables import Gaudi__Histograming__Sink__Root as RootHistoSink
from Configurables import Gaudi__Monitoring__MessageSvcSink as MessageSvcSink
from Configurables import LHCb__UnpackRawEvent, createODIN
from Configurables import LHCbApp, GaudiSequencer
from Configurables import FTErrorBankDecoder

from GaudiConf import IOHelper
from DDDB.CheckDD4Hep import UseDD4Hep
from Configurables import EventSelector
from ast import literal_eval
from XRootD import client
import tempfile
from glob import glob

EventSelector().PrintFreq = 5000
MessageSvc().OutputLevel = ERROR

app = LHCbApp(DataType="Upgrade")
#app.EvtMax =-1 # 500000

if UseDD4Hep:
    app.Simulation = False
    app.CondDBtag = "master"
else:
    app.DDDBtag = "upgrade/master"
    app.Simulation = True
    app.CondDBtag = "upgrade/master"

#files = glob("/hlt2/objects/SF/0000263962/*.mdf")
files = glob("/calib/online/tmpHlt1Dumps/LHCb/0000266610/*.mdf")

files = [
    "mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/SciFi/Monitoring/test_files/errordecoding/Run_0000266442_20230610-020315-669_SAEB22_0955.mdf",
]

output_name = basename(files[0]).replace(".mdf", "_errordecoding.root")

IOHelper("MDF").inputFiles(files)  ##from input.py

monSeq = GaudiSequencer('FTErrorDecoding', IgnoreFilterPassed=False)

#  errors from RawBank.h
#      FTError,                   // 88
#      DaqErrorFragmentThrottled, // 89
#      DaqErrorBXIDCorrupted,     // 90
#      DaqErrorSyncBXIDCorrupted, // 91
#      DaqErrorFragmentMissing,   // 92
#      DaqErrorFragmentTruncated, // 93
#      DaqErrorIdleBXIDCorrupted, // 94
#      DaqErrorFragmentMalformed, // 95
#      DaqErrorEVIDJumped,        // 96
#      DaqErrorAlignFifoFull,     // 100
#      DaqErrorFEfragSizeWrong,   // 101

# errors are almost always BXID corrupted as this is the first error caught when something is wrong in the TELL40.

bankTypes = [
    'FTError', 'DaqErrorFragmentThrottled', 'DaqErrorBXIDCorrupted',
    'DaqErrorSyncBXIDCorrupted', 'DaqErrorFragmentMissing',
    'DaqErrorFragmentTruncated', 'DaqErrorIdleBXIDCorrupted',
    'DaqErrorFragmentMalformed', 'DaqErrorEVIDJumped', 'DaqErrorAlignFifoFull',
    'DaqErrorFEfragSizeWrong', 'ODIN'
]
unpacker = LHCb__UnpackRawEvent(
    'UnpackRawEvent',
    OutputLevel=INFO,
    BankTypes=bankTypes,
    RawEventLocation='/Event/DAQ/RawEvent',
    RawBankLocations=['/Event/DAQ/RawBanks/{}'.format(i) for i in bankTypes])

monSeq.Members += [unpacker]

decodeODIN = createODIN(RawBanks="/Event/DAQ/RawBanks/ODIN")
monSeq.Members += [decodeODIN]

if UseDD4Hep:
    from Configurables import LHCb__Det__LbDD4hep__IOVProducer as IOVProducer
    monSeq.Members += [IOVProducer("ReserveIOVDD4hep", ODIN=decodeODIN.ODIN)]
else:
    monSeq.Members += [decodeODIN]

monSeq.Members += [
    FTErrorBankDecoder(
        ErrorRawBanks="/Event/DAQ/RawBanks/DaqErrorBXIDCorrupted",
        OutputLevel=INFO)
]

appMgr = ApplicationMgr(
    EvtMax=100,
    TopAlg=[monSeq],
    HistogramPersistency="ROOT",
    ExtSvc=[
        MessageSvcSink(),
        JSONSink(
            FileName="error_monitoring.json",
            NamesToSave=[
                ".*errorsPerBankLocation",
                ".*ErrorsPerErrorBankType",
            ],
        ),
    ],
)

if UseDD4Hep:
    from Configurables import LHCb__Det__LbDD4hep__DD4hepSvc as DD4hepSvc
    appMgr.ExtSvc += [DD4hepSvc(DetectorList=["/world", "FT"])]
