/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include <Event/FTCluster.h>
#include <Event/RawBank.h>
#include <Event/RawEvent.h>

#include <FTDAQ/FTReadoutMap.h>
#include <LHCbAlgs/Consumer.h>

#include <bitset>
#include <cstdint>
#include <numeric>
#include <optional>

namespace LHCb {

  namespace FTRawBank = Detector::FT::RawBank;

  /** @class FTRawBankEncoder FTRawBankEncoder.cpp
   *  Encode the FTCLusters into raw banks
   *
   *  Note that this is not really a Consumer, it also creates a new RawBank,
   *  but has to do it outside the scope of the functional framework as it
   *  potentially attaches it to an existing RawEvent. To be reviewed
   *
   *  @author Olivier Callot, Lex Greeven, Louis Henry
   *  @date   2012-05-11
   */
  struct FTRawBankEncoder : Algorithm::Consumer<void( const FTClusters&, const FTReadoutMap& ),
                                                Algorithm::Traits::usesConditions<FTReadoutMap>> {

    FTRawBankEncoder( const std::string& name, ISvcLocator* pSvcLocator );
    StatusCode initialize() override;
    void       operator()( const FTClusters& clusters, const FTReadoutMap& ) const override;

  private:
    DataObjectReadHandle<RawEvent>  m_raw{this, "rawLocation", RawEventLocation::Default};
    ServiceHandle<IDataProviderSvc> m_dataSvc{this, "DataService", "DetectorDataSvc", "The detector data service"};
  };

  constexpr static int s_nbBanks        = FTRawBank::NbBanksMax;
  constexpr static int s_nbLinksPerBank = FTRawBank::NbLinksPerBank;

  // Declaration of the Algorithm Factory
  DECLARE_COMPONENT_WITH_ID( FTRawBankEncoder, "FTRawBankEncoder" )

} // namespace LHCb

LHCb::FTRawBankEncoder::FTRawBankEncoder( const std::string& name, ISvcLocator* pSvcLocator )
    : Consumer( name, pSvcLocator,
                {{"InputLocation", FTClusterLocation::Default},
                 {"ReadoutMapStorage", "AlgorithmSpecific-" + name + "-ReadoutMap"}} ) {}

StatusCode LHCb::FTRawBankEncoder::initialize() {
  return Consumer::initialize().andThen(
      [&] { FTReadoutMap::addConditionDerivation( this, &*m_dataSvc, inputLocation<FTReadoutMap>() ); } );
}

void LHCb::FTRawBankEncoder::operator()( const LHCb::FTClusters& clusters, const FTReadoutMap& readoutMap ) const {
  // retrieve the raw event
  RawEvent* event = m_raw.getOrCreate();

  // Incremented to deal with new numbering scheme
  // Check if encoding version corresponds with readout version.
  int codingVersion = 8;
  readoutMap.compatible( codingVersion );

  //== create the array of arrays of vectors with the proper size...
  std::array<std::vector<uint16_t>, s_nbBanks> sipmData;
  std::array<uint32_t, s_nbBanks>              headerData{};
  std::array<int, s_nbBanks* s_nbLinksPerBank> nClustersPerSipm = {0};
  for ( const auto& cluster : clusters ) {
    if ( cluster->isLarge() > 1 ) continue;

    Detector::FTChannelID id               = cluster->channelID();
    Detector::FTChannelID globalSiPMID     = Detector::FTChannelID( id.globalSiPMID() << 7 ); // FIXME:hardcoded
    auto                  bankAndLinkIndex = readoutMap.findBankNumberAndIndex( globalSiPMID );
    unsigned int          bankNumber       = bankAndLinkIndex.bankNumber;
    unsigned int          localLinkIndex   = bankAndLinkIndex.linkIndex;
    // Check if the globalSiPMID is available in the CONDDB. This should in principle never happen.
    if ( ( bankNumber == Detector::FTChannelID::kInvalidChannelID() ) ||
         ( localLinkIndex == Detector::FTChannelID::kInvalidChannelID() ) ) {
      error() << "Could not find SiPM " << globalSiPMID << " (" << globalSiPMID.channelID() << ") in CONDDB" << endmsg;
    }
    Detector::FTChannelID localLinkID     = Detector::FTChannelID( localLinkIndex, 0 );
    auto&                 data            = sipmData[bankNumber];
    unsigned int          globalLinkIndex = bankNumber * s_nbLinksPerBank + localLinkIndex;
    nClustersPerSipm[globalLinkIndex]++;
    // Truncate clusters when maximum per SiPM is reached
    if ( ( id.module() > Detector::FTChannelID::ModuleID{0} &&
           nClustersPerSipm[globalLinkIndex] > FTRawBank::nbClusFFMaximum ) ||
         ( id.module() == Detector::FTChannelID::ModuleID{0} &&
           nClustersPerSipm[globalLinkIndex] > FTRawBank::nbClusMaximum ) ) {
      if ( headerData[bankNumber] >> ( ( localLinkID >> 7 ) & 1 ) == 0 ) {
        headerData[bankNumber] += ( 1u << ( localLinkID >> 7 ) ); // set the truncation bit
      }
      continue;
    }

    data.push_back( ( localLinkIndex << FTRawBank::linkShift ) | ( id.channel() << FTRawBank::cellShift ) |
                    ( cluster->fractionBit() << FTRawBank::fractionShift ) |
                    ( ( cluster->isLarge() > 0 ) << FTRawBank::sizeShift ) );
    if ( msgLevel( MSG::VERBOSE ) ) {
      verbose() << format( "Bank%3d sipm%4d channel %4d frac %3.1f isLarge %2d code %4.4x", bankNumber, localLinkID,
                           id.channel(), cluster->fraction(), cluster->isLarge(), data.back() )
                << endmsg;
    }
  }

  //== Now build the banks: We need to put the 16 bits content into 32 bits words.
  for ( unsigned int iBank = 0; iBank < sipmData.size(); ++iBank ) {
    Detector::FTSourceID sourceID = readoutMap.sourceID( iBank );
    if ( msgLevel( MSG::VERBOSE ) ) verbose() << "*** Bank " << iBank << " with SourceID: " << sourceID << endmsg;
    auto                      words = sipmData[iBank].size();
    std::vector<unsigned int> bank;
    bank.reserve( ( words + 1 ) / 2 + 1 );
    bank.emplace_back( headerData[iBank] ); // insert the header
    std::optional<unsigned int> buf;

    // Sort the data
    std::sort( sipmData[iBank].begin(), sipmData[iBank].end(), []( uint16_t data1, uint16_t data2 ) {
      auto decompose = []( uint16_t data ) {
        return std::tuple{( data >> FTRawBank::linkShift ) & FTRawBank::linkMask,
                          ( data >> FTRawBank::cellShift ) & FTRawBank::cellMask};
      };
      return decompose( data1 ) < decompose( data2 );
    } ); // close sort

    for ( const auto& cluster : sipmData[iBank] ) {
      if ( !buf ) {
        buf = cluster;
      } else {
        bank.emplace_back( *buf | ( static_cast<unsigned int>( cluster ) << 16 ) ); // FIXME: hardcoded constant
        buf = {};
      }
    }
    if ( buf ) bank.emplace_back( *buf );
    if ( msgLevel( MSG::VERBOSE ) ) {
      for ( const auto& [offset, d] : range::enumerate( bank ) ) {
        verbose() << format( "    at %5d data %8.8x", offset, d ) << endmsg;
      }
    }
    event->addBank( sourceID, RawBank::FTCluster, codingVersion, bank );
  }
}
