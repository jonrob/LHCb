/*****************************************************************************\
* (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Detector/FT/FTConstants.h"
#include "Detector/FT/FTSourceID.h"
#include "Event/FTCounter.h"
#include "Event/ODIN.h"
#include "Event/RawBank.h"
#include "LHCbAlgs/Transformer.h"
#include <cstdint>

using FTCounters    = LHCb::FTCounter::FTCounters;
namespace FTRawBank = LHCb::Detector::FT::RawBank;

/** @class FTCalibRawBankDecoder FTCalibRawBankDecoder.cpp
 *
 * Decode FTCalibration raw banks into FTCounters.
 * The bank format is documented in https://edms.cern.ch/document/1904563/5
 *
 */
class FTCalibRawBankDecoder
    : public LHCb::Algorithm::Transformer<FTCounters( const LHCb::RawBank::View&, const LHCb::ODIN& )> {
public:
  FTCalibRawBankDecoder( const std::string& name, ISvcLocator* pSvcLocator );

  FTCounters operator()( const LHCb::RawBank::View& banks, const LHCb::ODIN& odin ) const override;

private:
  static constexpr int m_wordsPerHeader  = 16;
  static constexpr int m_wordsPerChannel = 8;
  static constexpr int m_wordsPerLink    = 1024;
  static constexpr int m_numCounters     = 8;
};

DECLARE_COMPONENT( FTCalibRawBankDecoder )

FTCalibRawBankDecoder::FTCalibRawBankDecoder( const std::string& name, ISvcLocator* pSvcLocator )
    : Transformer( name, pSvcLocator,
                   {KeyValue{"RawBanks", "DAQ/RawBanks/FTCalibration"}, KeyValue{"Odin", LHCb::ODINLocation::Default}},
                   KeyValue{"OutputLocation", LHCb::FTCounterLocation::Default} ) {}

FTCounters FTCalibRawBankDecoder::operator()( const LHCb::RawBank::View& banks, const LHCb::ODIN& odin ) const {
  if ( banks.empty() ) return {};

  auto runNumber  = odin.runNumber();
  auto stepNumber = odin.calibrationStep();

  // FTCalibration banks have a size of 32 * (1 + 64 * N_active_links) byte organized in words of 16 bit. The first 32
  // bytes (16 words) are reserved for the header, the remaining 2048 * N_active_links bytes (1024 * N_active_links
  // words) are used for the counters.
  auto counters = FTCounters();
  counters.reserve( m_wordsPerLink * FTRawBank::NbLinksPerBank );

  for ( const auto* bank : banks ) {
    auto words    = bank->range<std::uint16_t>();
    auto sourceID = static_cast<std::uint16_t>( bank->sourceID() );

    // Number of counter implemented in the firmware, either 6 or 8. We can only handle 8 counters at the moment
    auto numCounters = words[8];
    if ( numCounters != m_numCounters ) {
      error() << "Failed to decode bank from SourceID " << sourceID << ". Number of counters not equal to "
              << m_numCounters << endmsg;
      continue;
    }

    // Masks of active and aligned links
    auto active  = ( static_cast<std::uint32_t>( words[10] ) << 16 ) + static_cast<std::uint32_t>( words[9] );
    auto aligned = ( static_cast<std::uint32_t>( words[12] ) << 16 ) + static_cast<std::uint32_t>( words[11] );

    // Skip to the counter data
    words = words.subspan<m_wordsPerHeader>();

    for ( std::uint8_t link = 0; link < FTRawBank::NbLinksPerBank; link++ ) {
      // Skip inactive links
      if ( !( active & ( 1 << link ) ) ) continue;

      // Skip not aligned links
      if ( !( aligned & ( 1 << link ) ) ) {
        words = words.subspan<m_wordsPerLink>();
        continue;
      }

      for ( std::uint8_t channel = 0; channel < LHCb::Detector::FT::nChannels; channel++ ) {
        // Integrator even (0)
        counters.push_back(
            {runNumber, stepNumber, sourceID, link, channel, 0, words[0], words[1], words[2], words[3]} );
        // Integrator odd (1)
        counters.push_back(
            {runNumber, stepNumber, sourceID, link, channel, 1, words[4], words[5], words[6], words[7]} );
        // Skip forward to the next channel
        words = words.subspan<m_wordsPerChannel>();
      }
    }

    // We should have read through the full bank here
    error() << "Failed to decode bank from SourceID " << sourceID << ". Decoding incomplete. " << words.size()
            << " words left" << endmsg;
  }

  return counters;
}
