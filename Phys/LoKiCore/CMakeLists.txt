###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#[=======================================================================[.rst:
Phys/LoKiCore
-------------
#]=======================================================================]

gaudi_add_library(LoKiCoreLib
    SOURCES
        src/AlgFunctors.cpp
        src/AlgUtils.cpp
        src/Assert.cpp
        src/AuxFunBase.cpp
        src/Base.cpp
        src/CacheFactory.cpp
        src/Colors.cpp
        src/Context.cpp
        src/CoreEngine.cpp
        src/CoreEngineActor.cpp
        src/CoreLock.cpp
        src/DecayBase.cpp
        src/DecayChainBase.cpp
        src/DecayDescriptor.cpp
        src/Dump.cpp
        src/Dumper.cpp
        src/ErrorReport.cpp
        src/Exception.cpp
        src/FilterAlg.cpp
        src/FilterTool.cpp
        src/Filters.cpp
        src/FirstN.cpp
        src/Geometry.cpp
        src/HistoBook.cpp
        src/HybridBase.cpp
        src/ICoreAntiFactory.cpp
        src/ICoreFactory.cpp
        src/IDecayNode.cpp
        src/ILoKiSvc.cpp
        src/IReporter.cpp
        src/Kinematics.cpp
        src/Listener.cpp
        src/LoKiCore.cpp
        src/LoKiCore_dct.cpp
        src/LoKiNumbers.cpp
        src/LoKiNumbers_dct.cpp
        src/MissingParticle.cpp
        src/Monitor.cpp
        src/Monitoring.cpp
        src/NodeParser.cpp
        src/Param.cpp
        src/ParamFunctors.cpp
        src/ParticleProperties.cpp
        src/PidFunctions.cpp
        src/Polarization.cpp
        src/Print.cpp
        src/Random.cpp
        src/RecStat.cpp
        src/Record.cpp
        src/Report.cpp
        src/Scalers.cpp
        src/Services.cpp
        src/Status.cpp
        src/TES.cpp
        src/Tensors.cpp
        src/Timers.cpp
        src/ToCpp.cpp
        src/TreeHelpers.cpp
        src/TreeParser.cpp
        src/Trees.cpp
        src/Welcome.cpp
        src/WrongMass.cpp
        src/iTree.cpp
    LINK
        PUBLIC
            Boost::headers
            Gaudi::GaudiAlgLib
            Gaudi::GaudiKernel
            Gaudi::GaudiPluginService
            LHCb::LHCbKernel
            LHCb::LHCbMathLib
            LHCb::PartPropLib
            LHCb::TrackEvent
            ROOT::Core
        PRIVATE
            LHCb::PhysInterfacesLib
            LHCb::RecEvent
            Python::Python
)

gaudi_add_module(LoKiCore
    SOURCES
        src/Components/CoreFactory.cpp
        src/Components/CounterAlg.cpp
        src/Components/DecayNode.cpp
        src/Components/LoKiPIDTest.cpp
        src/Components/LoKiSvc.cpp
        src/Components/Reporter.cpp
        src/Components/VoidFilter.cpp
    LINK
        LHCb::LoKiCoreLib
)

gaudi_add_dictionary(LoKiCoreDict
    HEADERFILES dict/LoKiCoreDict.h
    SELECTION dict/LoKiCore.xml
    LINK LHCb::LoKiCoreLib
    OPTIONS ${LHCB_DICT_GEN_DEFAULT_OPTS}
)

gaudi_install(PYTHON)

gaudi_install(CMAKE
    cmake/check_functor_sources.sh
    cmake/disable_expandvars.py
    cmake/LoKiFunctorsCache.cmake
    cmake/LoKiFunctorsCachePostActionOpts.py
)

if(BUILD_TESTING)
    gaudi_add_executable(NodeGrammarTest
        SOURCES src/tests/NodeGrammarTest.cpp
        LINK LHCb::LoKiCoreLib
    )

    gaudi_add_executable(TreeGrammarTest
        SOURCES src/tests/TreeGrammarTest.cpp
        LINK LHCb::LoKiCoreLib
    )

    gaudi_add_executable(CCGrammarTest
        SOURCES src/tests/CCGrammarTest.cpp
        LINK LHCb::LoKiCoreLib
    )

    gaudi_add_tests(QMTest)
endif()
