/*****************************************************************************\
* (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "MDF/IOAlgMemoryMapBase.h"
#include "MDF/IOHandlerMemoryMap.h"

namespace LHCb::MDF {

  template <typename IOHANDLER>
  struct IOAlgMemoryMap : IOAlgMemoryMapBase<IOHANDLER> {
    using IOAlgMemoryMapBase<IOHANDLER>::IOAlgMemoryMapBase;
    StatusCode initialize() override {
      return LHCb::IO::IOAlgBase<IOHANDLER>::initialize().andThen( [&]() { this->initIOHandler(); } );
    }
  };

} // namespace LHCb::MDF

DECLARE_COMPONENT_WITH_ID( LHCb::MDF::IOAlgMemoryMap<LHCb::MDF::IOHandlerMemoryMap>, "IOAlgMemoryMap" )
