/*****************************************************************************\
* (c) Copyright 2000-2024 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include <MDF/IOAlgMemoryMapBase.h>
#include <MDF/IOHandlerMemoryMap.h>
#include <MDF/IOHandlerMixer.h>

namespace LHCb::MDF {

  struct IOAlgMemoryMapMixer : LHCb::MDF::IOAlgMemoryMapBase<LHCb::MDF::IOHandlerMixer<LHCb::MDF::IOHandlerMemoryMap>,
                                                             std::vector<std::vector<std::string>>> {
    using IOAlgMemoryMapBase::IOAlgMemoryMapBase;

    /**
     * Should return a number between 0 (included) and given number (excluded) specifying which
     * input file should be used for next event.
     * Any result greater or equal to given number may yield to undefined behavior
     */
    virtual unsigned int chooseInput( unsigned int ) const = 0;

    StatusCode initialize() override {
      return IOAlgMemoryMapBase::initialize().andThen(
          [&]() { this->initIOHandler( [this]( unsigned int max ) { return chooseInput( max ); } ); } );
    }
  };

} // namespace LHCb::MDF
