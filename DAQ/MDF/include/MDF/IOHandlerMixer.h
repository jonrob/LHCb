/*****************************************************************************\
* (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "GaudiKernel/EventContext.h"

#include <functional>
#include <memory>
#include <string>
#include <vector>

class IIncidentSvc;

namespace LHCb::MDF {

  /**
   * IOHandler allowing to mix content from different input file
   *
   * It will open all input files concurrently and choose the one to read from
   * at each iteration.
   * Note that the numbe rof events in buffer and the number of skipped events
   * are applied to each input file independently
   */
  template <typename IOHANDLER>
  class IOHandlerMixer {

  public:
    using EventType           = typename IOHANDLER::EventType;
    using TESEventType        = typename IOHANDLER::TESEventType;
    using KeyValue            = typename IOHANDLER::KeyValue;
    using ReturnKeyValuesType = typename IOHANDLER::ReturnKeyValuesType;
    using ChooseInputFunc     = std::function<unsigned int( unsigned int )>;

    template <typename Owner>
    IOHandlerMixer( Owner& owner, std::vector<std::vector<std::string>> const& input ) {
      // create one IOHandler per file to open
      m_ioHandlers.reserve( input.size() );
      for ( auto const& files : input ) { m_ioHandlers.emplace_back( std::make_unique<IOHANDLER>( owner, files ) ); }
    }

    template <typename Owner, typename... InputHandlerArgs>
    void initialize( Owner& owner, IIncidentSvc* incidentSvc, unsigned int bufferNbEvents, unsigned int nbSkippedEvents,
                     ChooseInputFunc chooseInput, InputHandlerArgs&&... args ) {
      m_chooseInput = chooseInput;
      for ( auto& h : m_ioHandlers ) { h->initialize( owner, incidentSvc, bufferNbEvents, nbSkippedEvents, args... ); }
    }

    void finalize() {
      for ( auto& h : m_ioHandlers ) { h->finalize(); }
    }

    template <typename Owner>
    EventType next( Owner& owner, EventContext const& ctx ) {
      // choose which input file to use and call corresponding handler
      return m_ioHandlers[m_chooseInput( m_ioHandlers.size() ) % m_ioHandlers.size()]->next( owner, ctx );
    }

  private:
    /// One regular IOHandler per file to open
    std::vector<std::unique_ptr<IOHANDLER>> m_ioHandlers;
    /// chooseInput method to use when selecting input file
    ChooseInputFunc m_chooseInput{nullptr};
  };

} // namespace LHCb::MDF
