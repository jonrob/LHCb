/*****************************************************************************\
* (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "Event/RawEvent.h"
#include "MDF/IOAlgBase.h"
#include "MDF/MMappedFile.h"

#include <exception>

namespace LHCb::MDF {

  template <typename IOHandler, typename InputType = std::vector<std::string>>
  class IOAlgMemoryMapBase : public LHCb::IO::IOAlgBase<IOHandler, InputType> {
  public:
    IOAlgMemoryMapBase( const std::string& name, ISvcLocator* pSvcLocator )
        : LHCb::IO::IOAlgBase<IOHandler, InputType>( name, pSvcLocator,
                                                     {{"RawEventLocation", ""}, {"EventBufferLocation", ""}} ){};

    /// overide operator() to count banks
    std::tuple<LHCb::RawEvent, std::shared_ptr<MMapBuffer>> operator()( EventContext const& evtCtx ) const override {
      // get returns std::tuple<std::vector<LHCb::RawEvent>, std::shared_ptr<ByteBuffer>>
      auto [raw_events, buf] = this->m_ioHandler->next( *this, evtCtx );
      m_numBanks += raw_events[0].size();
      return {std::move( raw_events[0] ), std::move( buf )};
    }

  private:
    Gaudi::Property<unsigned int> m_nbBanksReserve{
        this,
        "NBanksReserve",
        1200,
        [this]( auto& ) { LHCb::MDF::Buffer::setReservedNumberOfBanks( this->m_nbBanksReserve ); },
        Gaudi::Details::Property::ImmediatelyInvokeHandler{true},
        "Number of RawBanks to reserve space for in each Event"};
    mutable Gaudi::Accumulators::StatCounter<std::size_t> m_numBanks{this, "#banks in raw event"};
  };

} // namespace LHCb::MDF
