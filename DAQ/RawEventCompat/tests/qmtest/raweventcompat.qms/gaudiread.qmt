<?xml version="1.0" ?><!DOCTYPE extension  PUBLIC '-//QM/2.3/Extension//EN'  'http://www.codesourcery.com/qm/dtds/2.3/-//qm/2.3/extension//en.dtd'>
<!--
    (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration

    This software is distributed under the terms of the GNU General Public
    Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".

    In applying this licence, CERN does not waive the privileges and immunities
    granted to it by virtue of its status as an Intergovernmental Organization
    or submit itself to any jurisdiction.
-->
<!--
#######################################################
# SUMMARY OF THIS TEST
# ...................
# Author: rlambert
# Purpose: Dump the contents of a juggled file to verify it has been juggled
# Prerequisites: the gaudirun test must have created split-raw-event.dst beforehand
# Common failure modes, severities and cures:
#               . SEVERE: Segfault or raised exception, stderr, nonzero return code
#               . MAJOR: additional FATAL/ERROR messages always a major problem. no ERROR messages should ever be printed when running this test.
#               . MAJOR: Missing reference block indicates the raw event was not correctly juggled.
#               . MINOR: additional WARNING messages, it is unclear how severe these may be, you will need to hunt down the cause and either fix the problem or suppress the warning.
#######################################################
-->
<extension class="GaudiTest.GaudiExeTest" kind="test">
  <argument name="program"><text>gaudirun.py</text></argument>
  <argument name="use_temp_dir"><enumeral>per-test</enumeral></argument>
  <argument name="prerequisites"><set>
    <tuple><text>raweventcompat.gaudirun</text><enumeral>PASS</enumeral></tuple>
  </set></argument>
  <argument name="options"><text>
import os
from Gaudi.Configuration import *
from GaudiConf import IOHelper
from PRConfig import TestFileDB
from Configurables import StoreExplorerAlg

TestFileDB.test_file_db['2012_raw_default'].run()
IOHelper().inputFiles([os.path.join(os.getenv("PREREQUISITE_0", ""), 'split-raw-event.dst')], clear=True)

ApplicationMgr().TopAlg=[StoreExplorerAlg()]
StoreExplorerAlg().Load=True

@appendPostConfigAction
def drop_mag_field_svc():
    appMgr = ApplicationMgr()
    appMgr.ExtSvc = [
        svc for svc in appMgr.ExtSvc if "MagneticFieldSvc" not in str(svc)
    ]
</text></argument>
<argument name="validator"><text>
from DDDB.CheckDD4Hep import UseDD4Hep
if UseDD4Hep:
    ref = """StoreExplorerAlg     INFO | +--> /Calo [Address: CLID=0x1 Type=0x2]  DataObject
StoreExplorerAlg     INFO | | +--> /RawEvent [Address: CLID=0x3ea Type=0x2]  LHCb::RawEvent
StoreExplorerAlg     INFO | +--> /Muon [Address: CLID=0x1 Type=0x2]  DataObject
StoreExplorerAlg     INFO | | +--> /RawEvent [Address: CLID=0x3ea Type=0x2]  LHCb::RawEvent
StoreExplorerAlg     INFO | +--> /Other [Address: CLID=0x1 Type=0x2]  DataObject
StoreExplorerAlg     INFO | | +--> /RawEvent [Address: CLID=0x3ea Type=0x2]  LHCb::RawEvent
StoreExplorerAlg     INFO | +--> /Rich [Address: CLID=0x1 Type=0x2]  DataObject
StoreExplorerAlg     INFO | | +--> /RawEvent [Address: CLID=0x3ea Type=0x2]  LHCb::RawEvent
StoreExplorerAlg     INFO   +--> /Trigger [Address: CLID=0x1 Type=0x2]  DataObject
StoreExplorerAlg     INFO     +--> /RawEvent [Address: CLID=0x3ea Type=0x2]  LHCb::RawEvent"""
else:
    ref = """StoreExplorerAlg     INFO | +--> /Calo [Address: CLID=0x1 Type=0x2]  DataObject
StoreExplorerAlg     INFO | | +--> /RawEvent [Address: CLID=0x3ea Type=0x2]  LHCb::RawEvent
StoreExplorerAlg     INFO | +--> /Muon [Address: CLID=0x1 Type=0x2]  DataObject
StoreExplorerAlg     INFO | | +--> /RawEvent [Address: CLID=0x3ea Type=0x2]  LHCb::RawEvent
StoreExplorerAlg     INFO | +--> /Other [Address: CLID=0x1 Type=0x2]  DataObject
StoreExplorerAlg     INFO | | +--> /RawEvent [Address: CLID=0x3ea Type=0x2]  LHCb::RawEvent
StoreExplorerAlg     INFO | +--> /Rich [Address: CLID=0x1 Type=0x2]  DataObject
StoreExplorerAlg     INFO | | +--> /RawEvent [Address: CLID=0x3ea Type=0x2]  LHCb::RawEvent
StoreExplorerAlg     INFO | +--> /Trigger [Address: CLID=0x1 Type=0x2]  DataObject
StoreExplorerAlg     INFO | | +--> /RawEvent [Address: CLID=0x3ea Type=0x2]  LHCb::RawEvent
StoreExplorerAlg     INFO   +--> /DAQ [No Address]  DataObject
StoreExplorerAlg     INFO     +--> /ODIN [No Address]  LHCb::ODINImplementation::v7::OD"""
findReferenceBlock(ref, id="4100")

countErrorLines({"FATAL":0, "ERROR":0, "WARNING" :0})

</text></argument>
</extension>
