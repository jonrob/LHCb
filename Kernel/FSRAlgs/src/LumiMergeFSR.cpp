/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "FSRAlgs/IFSRNavigator.h"

#include "Event/EventCountFSR.h"
#include "Event/LumiFSR.h"
#include "Event/TimeSpanFSR.h"

#include "Gaudi/Algorithm.h"
#include "GaudiAlg/FixTESPath.h"

/**
 *  Implementation file for class : LumiMergeFSR
 *
 *  @author Jaap Panman
 *  @date   2010-10-05
 */
class LumiMergeFSR : public FixTESPath<Gaudi::Algorithm> {
public:
  using FixTESPath::FixTESPath;

  StatusCode initialize() override;
  StatusCode execute( const EventContext& ) const override { return StatusCode::SUCCESS; };
  StatusCode finalize() override;

private:
  std::vector<std::string> m_BXTypes; ///< list of bunch crossing types

  Gaudi::Property<std::string> m_FileRecordName{this, "FileRecordLocation", "/FileRecords", "location of FileRecords"};
  Gaudi::Property<std::string> m_FSRName{this, "FSRName", "/LumiFSR", "specific tag of summary data in FSR"};
  Gaudi::Property<std::string> m_TimeSpanFSRName{this, "TimeSpanFSRName", "/TimeSpanFSR",
                                                 "specific tag of time summary data in FSR"};
  Gaudi::Property<std::string> m_EventCountFSRName{this, "EventCountFSRName", "/EventCountFSR",
                                                   "specific tag of event summary data in FSR"};
  Gaudi::Property<std::string> m_PrimaryBXType{this, "PrimaryBXType", "BeamCrossing", "BXType to normalize"};
  Gaudi::Property<std::vector<std::string>> m_subtractBXTypes{
      this, "SubtractBXTypes", {"Beam1", "Beam2", "NoBeam"}, "list of bunch crossing types to be subtracted"};

  std::vector<LHCb::LumiFSRs*> m_lumiFSRsVec;           ///< TDS containers
  std::vector<std::string>     m_FSRNameVec;            ///< TDS container names
  LHCb::TimeSpanFSRs*          m_timeSpanFSRs{nullptr}; ///< TDS container

private:
  ToolHandle<IFSRNavigator>       m_navigatorTool{this, "NavigatorToolName", "FSRNavigator", "tool to navigate FSR"};
  ServiceHandle<IDataProviderSvc> m_fileRecordSvc{this, "FileRecordDataSvc", "FileRecordDataSvc",
                                                  "Reference to run records data service"};

  // helper functions mimicing old GaudiAlgorithm interface
  // DataHandle cannot be used as they do not work with the FileRecordDataSvc
  template <typename ObjType>
  ObjType* getIfExists( IDataProviderSvc* svc, std::string const& address ) {
    DataObject* obj = nullptr;
    return svc->retrieveObject( fullTESLocation( address, true ), obj ).isSuccess() ? dynamic_cast<ObjType*>( obj )
                                                                                    : nullptr;
  }
  template <typename ObjType>
  bool exist( IDataProviderSvc* svc, std::string const& address ) {
    return getIfExists<ObjType>( svc, address ) != nullptr;
  }
  template <typename ObjType>
  ObjType* get( IDataProviderSvc* svc, std::string const& address ) {
    auto* obj = getIfExists<ObjType>( svc, address );
    if ( !obj ) {
      throw GaudiException( this->name() + "::get():: No valid data at '" + address + "'", this->name(),
                            StatusCode::FAILURE );
    }
    return obj;
  }
  template <typename ObjType>
  void put( IDataProviderSvc* svc, ObjType* obj, std::string const& address ) {
    svc->registerObject( fullTESLocation( address, true ), obj ).ignore();
  }
};

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( LumiMergeFSR )

StatusCode LumiMergeFSR::initialize() {
  return FixTESPath::initialize().andThen( [&]() {
    // ensure consistency
    m_BXTypes.push_back( m_PrimaryBXType );
    info() << "Primary  BXType " << m_PrimaryBXType.value() << endmsg;
    for ( const auto& bx : m_subtractBXTypes ) {
      info() << "Subtract BXType " << bx << endmsg;
      if ( bx != "None" ) m_BXTypes.push_back( bx );
    }
    return StatusCode::SUCCESS;
  } );
}

StatusCode LumiMergeFSR::finalize() {
  info() << "========== Merging FSRs ==========" << endmsg;
  // a file can contain multiple sets of LumiFSRs - typically after reprocessing multiple input files
  // merge the FSRs of all input files at the same time

  // make an inventory of the FileRecord store
  auto addresses = m_navigatorTool->navigate( m_FileRecordName, m_FSRName );
  // print
  if ( msgLevel( MSG::DEBUG ) )
    for ( const auto& iAddr : addresses ) { debug() << "address: " << iAddr << endmsg; }
  // get timespans for later removal - and store earliest and latest times
  unsigned long long all_earliest = 0; // earliest time seen
  unsigned long long all_latest   = 0; // latest time seen
  auto               tsAddresses  = m_navigatorTool->navigate( m_FileRecordName, m_TimeSpanFSRName );
  for ( const auto& iAddr : tsAddresses ) {
    for ( const auto& tsfsr : *get<LHCb::TimeSpanFSRs>( &*m_fileRecordSvc, iAddr ) ) {
      // take earliest and latest
      unsigned long long t0 = tsfsr->earliest();
      unsigned long long t1 = tsfsr->latest();
      if ( all_earliest == 0 ) all_earliest = t0;
      all_latest = std::max( t1, all_latest );
    }
    if ( msgLevel( MSG::DEBUG ) ) { debug() << "address: " << iAddr << endmsg; }
  }

  // prepare TDS for FSR
  for ( unsigned int ibx = 0; ibx < m_BXTypes.size(); ++ibx ) {
    std::string bx = m_BXTypes[ibx];
    if ( msgLevel( MSG::DEBUG ) ) debug() << "BXType " << bx << endmsg;
    LHCb::LumiFSRs* fsrs = new LHCb::LumiFSRs(); // keyed container for FSRs
    fsrs->reserve( 100 );
    m_lumiFSRsVec.push_back( fsrs );                         // vector of keyed containers
    std::string name = m_FSRName + bx;                       //
    m_FSRNameVec.push_back( name );                          // vector of names
    put( &*m_fileRecordSvc, fsrs, m_FileRecordName + name ); // TS address of keyed container
  }
  // prepare TDS for TimeSpanFSR
  m_timeSpanFSRs = new LHCb::TimeSpanFSRs();
  m_timeSpanFSRs->reserve( 100 );
  put( &*m_fileRecordSvc, m_timeSpanFSRs, m_FileRecordName.value() + m_TimeSpanFSRName.value() );

  // look first for a primary BX in the list and then look for the corresponding background types
  std::string primaryFileRecordAddress( "undefined" );
  for ( std::vector<std::string>::iterator a = addresses.begin(); a != addresses.end(); ++a ) {
    if ( a->find( m_FSRName.value() + m_PrimaryBXType.value() ) != std::string::npos ) {
      primaryFileRecordAddress = ( *a ); // a primary BX is found
      bool needNewLumiFSR      = false;
      // get primary LumiFSR number
      LHCb::LumiFSRs* primaryFSRs   = getIfExists<LHCb::LumiFSRs>( &*m_fileRecordSvc, primaryFileRecordAddress );
      int             n_primaryFSRs = primaryFSRs->size();
      if ( msgLevel( MSG::DEBUG ) )
        debug() << primaryFileRecordAddress << ": " << n_primaryFSRs << " primary FSRs in container " << endmsg;

      // search for the TimeSpanFSR
      std::string timeSpanRecordAddress( primaryFileRecordAddress );
      timeSpanRecordAddress.replace( timeSpanRecordAddress.find( m_PrimaryBXType ), m_PrimaryBXType.size(), "" );
      timeSpanRecordAddress.replace( timeSpanRecordAddress.find( m_FSRName ), m_FSRName.size(), m_TimeSpanFSRName );
      if ( msgLevel( MSG::VERBOSE ) ) verbose() << "constructed time span address" << timeSpanRecordAddress << endmsg;
      // read TimeSpanFSR
      unsigned long       n_tsFSR      = 0;
      LHCb::TimeSpanFSRs* timeSpanFSRs = getIfExists<LHCb::TimeSpanFSRs>( &*m_fileRecordSvc, timeSpanRecordAddress );
      if ( NULL == timeSpanFSRs ) {
        if ( msgLevel( MSG::ERROR ) ) error() << timeSpanRecordAddress << " not found" << endmsg;
      } else {
        // if ( msgLevel(MSG::VERBOSE) )
        if ( msgLevel( MSG::VERBOSE ) ) verbose() << timeSpanRecordAddress << " found" << endmsg;
        int n_timeSpanFSRs = timeSpanFSRs->size();
        if ( msgLevel( MSG::DEBUG ) )
          debug() << timeSpanRecordAddress << ": " << n_timeSpanFSRs << " time span FSRs in container " << endmsg;

        // decide if fatal error
        bool needCorrection = false;
        bool needNewTsFSR   = false;
        if ( n_primaryFSRs > n_timeSpanFSRs && n_timeSpanFSRs > 0 ) {
          fatal() << " number of primary FSRs " << n_primaryFSRs << " larger than number of timeSpanFSRs "
                  << n_timeSpanFSRs << " - this cannot be right!" << endmsg;
          return StatusCode::FAILURE;
        }
        if ( n_primaryFSRs < n_timeSpanFSRs && n_primaryFSRs != 1 && n_primaryFSRs != 0 ) {
          fatal() << " number of primary FSRs " << n_primaryFSRs << " smaller than number of timeSpanFSRs "
                  << n_timeSpanFSRs << " and not equal to one - this cannot be right!" << endmsg;
          for ( unsigned int ibx = 0; ibx < m_BXTypes.size(); ++ibx ) {
            std::string bx = m_BXTypes[ibx];
            // construct the right name of the containers
            std::string fileRecordAddress( primaryFileRecordAddress );
            fileRecordAddress.replace( fileRecordAddress.find( m_PrimaryBXType ), m_PrimaryBXType.size(), bx );
            if ( msgLevel( MSG::VERBOSE ) ) verbose() << "constructed address: " << fileRecordAddress << endmsg;
            // read LumiFSR
            LHCb::LumiFSRs* lumiFSRs = getIfExists<LHCb::LumiFSRs>( &*m_fileRecordSvc, fileRecordAddress );
            if ( NULL == lumiFSRs ) {
              if ( msgLevel( MSG::ERROR ) ) error() << fileRecordAddress << " not found" << endmsg;
            } else {
              fatal() << "found " << fileRecordAddress << " size " << lumiFSRs->size() << endmsg;
            }
          }
          return StatusCode::FAILURE;
        }
        if ( n_primaryFSRs < n_timeSpanFSRs && n_primaryFSRs == 0 ) {
          warning() << " number of primary FSRs " << n_primaryFSRs << " smaller than number of timeSpanFSRs "
                    << n_timeSpanFSRs << " and not equal to one - try to correct!" << endmsg;

          // rest is for debugging
          for ( unsigned int ibx = 0; ibx < m_BXTypes.size(); ++ibx ) {
            std::string bx = m_BXTypes[ibx];
            info() << "BXType " << bx << endmsg;
            // construct the right name of the containers
            std::string fileRecordAddress( primaryFileRecordAddress );
            fileRecordAddress.replace( fileRecordAddress.find( m_PrimaryBXType ), m_PrimaryBXType.size(), bx );
            if ( msgLevel( MSG::VERBOSE ) ) verbose() << "constructed address: " << fileRecordAddress << endmsg;
            // read LumiFSR
            LHCb::LumiFSRs* lumiFSRs = getIfExists<LHCb::LumiFSRs>( &*m_fileRecordSvc, fileRecordAddress );
            if ( NULL == lumiFSRs ) {
              if ( msgLevel( MSG::FATAL ) ) fatal() << fileRecordAddress << " not found" << endmsg;
              return StatusCode::FAILURE;
            } else {
              // expect all empty
              if ( lumiFSRs->size() != 0 ) {
                fatal() << "found " << fileRecordAddress << " with different size " << lumiFSRs->size() << endmsg;
                return StatusCode::FAILURE;
              }
            }
          }
          // all are missing - can correct
          needNewLumiFSR = true;
        }
        // decide if corrective action needs to be taken
        if ( n_primaryFSRs < n_timeSpanFSRs && n_primaryFSRs == 1 ) {
          warning() << " at address: " << primaryFileRecordAddress << " number of primary FSRs " << n_primaryFSRs
                    << " smaller than number of timeSpanFSRs " << n_timeSpanFSRs
                    << " and exactly equal to one - corrective action being taken!" << endmsg;
          needCorrection = true;
        }
        if ( n_primaryFSRs > n_timeSpanFSRs && n_timeSpanFSRs == 0 ) {
          warning() << " number of primary FSRs " << n_primaryFSRs << " larger than number of timeSpanFSRs "
                    << n_timeSpanFSRs << " - corrective action taken!" << endmsg;
          needCorrection = true;
          needNewTsFSR   = true;
        }

        // look at all TimeSpanFSRs (normally only one)
        LHCb::TimeSpanFSRs::iterator tsfsr;
        if ( needCorrection ) {
          // any correction
          if ( needNewTsFSR ) {
            // correction needed - create a new TimeSpanFSR with earliest and latest times
            LHCb::TimeSpanFSR* timeSpanFSR = new LHCb::TimeSpanFSR();
            ( *timeSpanFSR ) += all_earliest;
            ( *timeSpanFSR ) += all_latest;
            n_tsFSR++;
            m_timeSpanFSRs->insert( timeSpanFSR ); // put a copy in TS container
            if ( msgLevel( MSG::INFO ) )
              info() << timeSpanRecordAddress << ": " << n_tsFSR << " successfully created - total is now "
                     << m_timeSpanFSRs->size() << endmsg;
          } else {
            // correction needed - take the sum of all timespanFSRs and create only one
            LHCb::TimeSpanFSR* timeSpanFSR = new LHCb::TimeSpanFSR();
            for ( tsfsr = timeSpanFSRs->begin(); tsfsr != timeSpanFSRs->end(); tsfsr++ ) {
              // prepare new time span FSR and put in TS
              *timeSpanFSR += *( *tsfsr );
            }
            n_tsFSR++;
            m_timeSpanFSRs->insert( timeSpanFSR ); // put a copy in TS container
            if ( msgLevel( MSG::INFO ) )
              info() << timeSpanRecordAddress << ": " << n_tsFSR << " successfully copied - total is now "
                     << m_timeSpanFSRs->size() << endmsg;
          }
        } else {
          // no correction needed
          for ( tsfsr = timeSpanFSRs->begin(); tsfsr != timeSpanFSRs->end(); tsfsr++ ) {
            // prepare new time span FSR and put in TS
            n_tsFSR++;
            LHCb::TimeSpanFSR* timeSpanFSR = new LHCb::TimeSpanFSR();
            *timeSpanFSR                   = *( *tsfsr );
            m_timeSpanFSRs->insert( timeSpanFSR ); // put a copy in TS container
          }
          if ( msgLevel( MSG::DEBUG ) )
            debug() << timeSpanRecordAddress << ": " << n_tsFSR << " successfully copied - total is now "
                    << m_timeSpanFSRs->size() << endmsg;
        }
      }

      // now handle all Lumi FSRs
      std::string fileRecordAddress( "undefined" );
      // get all FSR objects
      for ( unsigned int ibx = 0; ibx < m_BXTypes.size(); ++ibx ) {
        std::string bx = m_BXTypes[ibx];
        if ( msgLevel( MSG::DEBUG ) ) debug() << "BXType " << bx << endmsg;
        LHCb::LumiFSRs* fsrs      = m_lumiFSRsVec[ibx];
        unsigned long   n_lumiFSR = 0;
        // construct the right name of the containers
        std::string fileRecordAddress( primaryFileRecordAddress );
        fileRecordAddress.replace( fileRecordAddress.find( m_PrimaryBXType ), m_PrimaryBXType.size(), bx );
        if ( msgLevel( MSG::VERBOSE ) ) verbose() << "constructed address: " << fileRecordAddress << endmsg;
        // read LumiFSR
        LHCb::LumiFSRs* lumiFSRs = getIfExists<LHCb::LumiFSRs>( &*m_fileRecordSvc, fileRecordAddress );
        if ( NULL == lumiFSRs ) {
          if ( msgLevel( MSG::ERROR ) ) error() << fileRecordAddress << " not found" << endmsg;
        } else {
          if ( msgLevel( MSG::VERBOSE ) ) verbose() << fileRecordAddress << " found" << endmsg;
          // look at all FSRs for the BXType (normally only one)
          if ( needNewLumiFSR ) {
            // create empty one
            n_lumiFSR++;
            LHCb::LumiFSR* lumiFSR = new LHCb::LumiFSR();
            fsrs->insert( lumiFSR ); // insert empty FSR in TS
          } else {
            // just copy
            for ( auto fsr = lumiFSRs->begin(); fsr != lumiFSRs->end(); ++fsr ) {
              // create a new FSR and append to TS
              n_lumiFSR++;
              LHCb::LumiFSR* lumiFSR = new LHCb::LumiFSR();
              *lumiFSR               = *( *fsr );
              fsrs->insert( lumiFSR ); // insert in TS
            }
          }
          // count the result
          if ( n_tsFSR == n_lumiFSR ) {
            if ( msgLevel( MSG::DEBUG ) )
              debug() << fileRecordAddress << ": " << n_lumiFSR << " successfully copied - total is now "
                      << fsrs->size() << endmsg;
          } else {
            error() << fileRecordAddress << ": number of FSRs: " << n_lumiFSR
                    << " did not match expected number: " << n_tsFSR << endmsg;
          }
        }
      }
    }
  }

  // read back lumiFSR from TS
  for ( unsigned int ibx = 0; ibx < m_BXTypes.size(); ++ibx ) {
    LHCb::LumiFSRs* lumiFSRs = get<LHCb::LumiFSRs>( &*m_fileRecordSvc, m_FileRecordName + m_FSRNameVec[ibx] );
    info() << m_FileRecordName + m_FSRNameVec[ibx] << " " << lumiFSRs->size() << " FSRs" << endmsg;
    if ( msgLevel( MSG::DEBUG ) ) {
      for ( const auto& fsr : *lumiFSRs ) debug() << *fsr << endmsg;
    }
  }
  // read back timeSpanFSR from TS
  LHCb::TimeSpanFSRs* timeSpanFSRs =
      get<LHCb::TimeSpanFSRs>( &*m_fileRecordSvc, m_FileRecordName.value() + m_TimeSpanFSRName.value() );
  info() << m_FileRecordName.value() + m_TimeSpanFSRName.value() << " " << timeSpanFSRs->size() << " FSRs" << endmsg;
  if ( msgLevel( MSG::DEBUG ) ) {
    for ( const auto& tsfsr : *timeSpanFSRs ) debug() << *tsfsr << endmsg;
  }

  // clean up original FSRs
  for ( std::vector<std::string>::iterator a = addresses.begin(); a != addresses.end(); ++a ) {
    // get FSR as keyed object and cleanup the original ones - this only cleans lumiFSRs
    std::string     fileRecordAddress = ( *a );
    LHCb::LumiFSRs* lumiFSRs          = getIfExists<LHCb::LumiFSRs>( &*m_fileRecordSvc, fileRecordAddress );
    if ( NULL == lumiFSRs ) {
      if ( msgLevel( MSG::ERROR ) ) error() << fileRecordAddress << " not found" << endmsg;
    } else {
      if ( msgLevel( MSG::VERBOSE ) ) verbose() << fileRecordAddress << " found" << endmsg;
      lumiFSRs->erase( lumiFSRs->begin(), lumiFSRs->end() ); // release storage
      m_fileRecordSvc->unlinkObject( *a ).ignore();          // get it out of TS
    }
  }

  // clean up original tsFSRs
  for ( const auto& a : tsAddresses ) {
    // get FSR as keyed object and cleanup the original ones - this only cleans tsFSRs
    LHCb::TimeSpanFSRs* tsFSRs = getIfExists<LHCb::TimeSpanFSRs>( &*m_fileRecordSvc, a );
    if ( !tsFSRs ) {
      if ( msgLevel( MSG::ERROR ) ) error() << a << " not found" << endmsg;
      continue;
    }
    if ( msgLevel( MSG::VERBOSE ) ) verbose() << a << " found" << endmsg;
    tsFSRs->erase( tsFSRs->begin(), tsFSRs->end() ); // release storage
    m_fileRecordSvc->unlinkObject( a ).ignore();     // get it out of TS
  }

  // make a new inventory of the FileRecord store
  addresses = m_navigatorTool->navigate( m_FileRecordName, m_FSRName );
  if ( msgLevel( MSG::DEBUG ) )
    for ( const auto& iAddr : addresses ) { debug() << "address: " << iAddr << endmsg; }
  // get timespans
  tsAddresses = m_navigatorTool->navigate( m_FileRecordName, m_TimeSpanFSRName );
  if ( msgLevel( MSG::DEBUG ) )
    for ( const auto& iAddr : tsAddresses ) { debug() << "address: " << iAddr << endmsg; }

  // check if the original EventCount FSRs can be retrieved from the TS
  if ( msgLevel( MSG::DEBUG ) ) {
    LHCb::EventCountFSR* readFSR = get<LHCb::EventCountFSR>( &*m_fileRecordSvc, LHCb::EventCountFSRLocation::Default );
    debug() << "READ FSR: " << *readFSR << endmsg;
  }

  // clean up eventCountFSRs, except the top level
  std::vector<std::string> evAddresses = m_navigatorTool->navigate( m_FileRecordName, m_EventCountFSRName );
  for ( auto a = evAddresses.begin(); a != evAddresses.end(); ++a ) {
    // get FSR as keyed object and cleanup the original ones - this only cleans evFSRs
    std::string fileRecordAddress = ( *a );
    if ( msgLevel( MSG::DEBUG ) ) debug() << "address in list: " << ( *a ) << endmsg;
    if ( !exist<LHCb::EventCountFSR>( &*m_fileRecordSvc, fileRecordAddress ) ) {
      if ( msgLevel( MSG::ERROR ) ) error() << fileRecordAddress << " not found" << endmsg;
    } else {
      if ( msgLevel( MSG::VERBOSE ) ) verbose() << fileRecordAddress << " found" << endmsg;
      // the toplevel EventCountFSR should survive
      if ( fileRecordAddress.find( LHCb::EventCountFSRLocation::Default ) == std::string::npos ) {
        m_fileRecordSvc->unlinkObject( *a ).ignore(); // get it out of TS
      }
    }
  }
  evAddresses = m_navigatorTool->navigate( m_FileRecordName, m_EventCountFSRName );
  if ( msgLevel( MSG::DEBUG ) )
    for ( const auto& addr : evAddresses ) { debug() << "address: " << addr << endmsg; }

  return FixTESPath::finalize();
}
