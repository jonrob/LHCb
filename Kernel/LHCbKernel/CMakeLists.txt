###############################################################################
# (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#[=======================================================================[.rst:
Kernel/LHCbKernel
-----------------
#]=======================================================================]

gaudi_add_library(LHCbKernel
    SOURCES
        src/CaloCellIDParser.cpp
        src/Chrono.cpp
        src/Counters.cpp
        src/HCCellID.cpp
        src/HitPattern.cpp
        src/IAccept.cpp
        src/IOHandler.cpp
        src/ITNames.cpp
        src/Inspectors.cpp
        src/LHCbID.cpp
        src/LbAppInit.cpp
        src/MemoryPoolAllocatorReleaser.cpp
        src/OTChannelID.cpp
        src/PiecewiseTrajectory.cpp
        src/PlatformInfo.cpp
        src/PlumeChannelID.cpp
        src/RichDetectorType.cpp
        src/RichParticleIDType.cpp
        src/RichRadiatorType.cpp
        src/RichSide.cpp
        src/RichSmartID.cpp
        src/RichSmartID32.cpp
        src/RichTraceMode.cpp
        src/STChannelID.cpp
        src/SiLandauFun.cpp
        src/TTNames.cpp
        src/UTNames.cpp
        src/VeloChannelID.cpp
    LINK
        PUBLIC
            Boost::headers
            cppgsl::cppgsl
            Gaudi::GaudiAlgLib
            Gaudi::GaudiKernel
            LHCb::LHCbMathLib
            ROOT::GenVector
            ROOT::MathCore
            ROOT::Tree
            VDT::vdt
            Detector::DetectorLib
)

# Pass compile time BINARY_TAG value to PlatformInfo
set(_bin_tag ${BINARY_TAG})
if(NOT _bin_tag)
    if(NOT "$ENV{BINARY_TAG}" STREQUAL "")
        set(_bin_tag $ENV{BINARY_TAG})
    elseif(LHCB_PLATFORM)
        set(_bin_tag ${LHCB_PLATFORM})
    else()
        set(_bin_tag unknown)
    endif()
endif()
set_property(SOURCE src/PlatformInfo.cpp tests/src/test_PlatformInfo.cpp APPEND PROPERTY COMPILE_DEFINITIONS BINARY_TAG=${_bin_tag})

gaudi_add_dictionary(LHCbKernelDict
    HEADERFILES dict/dictionary.h
    SELECTION dict/selection.xml
    LINK LHCb::LHCbKernel LHCb::MagnetLib
)

gaudi_install(PYTHON)
gaudi_generate_confuserdb()

if(BUILD_TESTING)
    gaudi_add_tests(QMTest)

    gaudi_add_executable(RichSmartIDTest
        SOURCES
            src/Test/RichSmartIDTest/main.cpp
        LINK
            LHCb::LHCbKernel
    )

    foreach(test IN ITEMS
        test_allocatorReleaser
        test_arenaAllocator
        test_CaloCellID_index
        test_container
        test_count_iterator
        test_meta_enum
        test_MultiIndexContainer
        test_PartitionPosition
        test_PiecewiseTrajectory
        test_PlatformInfo
        test_PolymorphicValue
        test_STLExtensions
        test_SynchronizedValue
	test_TransformedRange
    )
        gaudi_add_executable(${test}
            SOURCES tests/src/${test}.cpp
            LINK
                Boost::unit_test_framework
                LHCb::LHCbKernel
            TEST)
    endforeach()

    # Disable tests that give false failures under sanitizers
    if(CMAKE_BUILD_TYPE MATCHES ".*San$")
        set_tests_properties(LHCbKernel.test_container PROPERTIES DISABLED TRUE)
    endif()
endif()
