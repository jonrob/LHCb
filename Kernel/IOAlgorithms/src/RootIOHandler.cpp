/*****************************************************************************\
* (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include <IOAlgorithms/RootIOHandler.h>

#include <GaudiKernel/DataIncident.h>
#include <GaudiKernel/IDataProviderSvc.h>

#include <range/v3/view.hpp>

namespace {
  /// Helper function to read string tables
  void addEntry( std::vector<std::string>& c, char* val ) { c.push_back( val ); }
  /// Helper function  to read internal file tables
  template <class C, class F>
  void readBranch( TTree* t, const char* name, C& v, F pmf ) {
    char     text[2048];
    TBranch* b = t->GetBranch( name );
    if ( !b ) { throw LHCb::IO::details::RootFileFailed( fmt::format( "Failed to read '{}' table.", name ) ); }
    TLeaf* l = b->GetLeaf( name );
    if ( !l ) { throw LHCb::IO::details::RootFileFailed( fmt::format( "Failed to GetLeaf in table '{}'.", name ) ); }
    b->SetAddress( text );
    for ( Long64_t i = 0, n = b->GetEntries(); i < n; ++i ) {
      if ( b->GetEntry( i ) <= 0 ) {
        throw LHCb::IO::details::RootFileFailed( "Root read error in RootIOHandler::readBranch" );
      }
      pmf( v, (char*)l->GetValuePointer() );
    }
  }
} // namespace

LHCb::IO::details::RootFile::RootFile( IInterface const& owner, std::string const& name, IMessageSvc& msgSvc,
                                       IIncidentSvc& incidentSvc, Gaudi::IIODataManager& ioMgr )
    : m_ioMgr( ioMgr ) {
  // Usage of IODataMgr service is the following :
  //   - first try to get a connection to your file
  //   - if you succeed, use it (you will need to dynamic_cast as you get a generic interface...)
  //   - else create the connection yourself and give it to IODataMgr
  //   - it will be owned by it in case of success and reused potentially later
  m_curConnection = dynamic_cast<Gaudi::RootDataConnection*>( ioMgr.connection( name.c_str() ) );
  if ( !m_curConnection ) {
    // create new connection and give it away to IODataMagr
    auto setup = std::make_shared<Gaudi::RootConnectionSetup>();
    setup->setMessageSvc( new MsgStream( &msgSvc, "RootIOHandler" ) );
    setup->setIncidentSvc( &incidentSvc );
    m_curConnection = new Gaudi::RootDataConnection( &owner, name.c_str(), setup );
    auto sc         = ioMgr.connectRead( false, m_curConnection );
    if ( !sc.isSuccess() ) {
      delete m_curConnection;
      throw RootFileFailed( fmt::format( "Could not open input file '{}', skipping this file", name ) );
    }
  }
  // find tree containing Root reference data
  auto refTree = file().Get<TTree>( "Refs" );
  if ( !refTree ) {
    throw RootFileFailed( fmt::format( "Could not find TTree 'Refs' in file '{}', skipping this file", name ) );
  }
  // Read links data
  readBranch( refTree, "Links", m_links, addEntry );
  // FIXME drop this once FSRSink is not relying on it anymore
  incidentSvc.fireIncident( ContextIncident<TFile*>( name, "CONNECTED_INPUT", &file() ) );
}

LHCb::IO::details::RootFile::~RootFile() {
  // explicitely close the underlying ROOT file
  // without this, he IODataManager keeps it open and depending on the order
  // of finalization of the services, you may end up with a dead lock in XRoot
  // where xroot joins its threads before it's calling close
  m_ioMgr.disconnect( m_curConnection ).ignore();
}

void LHCb::IO::details::Buffer::fillAllLinks( unsigned int evtNb ) {
  for ( unsigned int n = 0; n < m_eventData[evtNb].size(); n++ ) {
    auto* data = m_eventData[evtNb][n];
    if ( data == nullptr ) continue; // FIXME drop when m_allowMissingInput is gone
    auto* refs = m_eventRefs[evtNb][n];
    for ( const auto& i : refs->links ) { data->linkMgr()->addLink( this->getLink( i ), nullptr ); }
    auto sc = data->update();
    if ( !sc.isSuccess() ) {
      throw GaudiException( "Failed to update event data", "RootIOHandler", StatusCode::FAILURE );
    }
  }
}

void LHCb::IO::details::BufferExt::fillAllExtraLinks( unsigned int evtNb ) {
  for ( unsigned int n = 0; n < m_eventDataExt[evtNb].size(); n++ ) {
    auto* data = m_eventDataExt[evtNb][n];
    if ( data == nullptr ) continue; // FIXME drop when m_allowMissingInput is gone
    auto* refs = m_eventRefsExt[evtNb][n];
    for ( const auto& i : refs->links ) { data->linkMgr()->addLink( this->getLink( i ), nullptr ); }
    auto sc = data->update();
    if ( !sc.isSuccess() ) {
      throw GaudiException( "Failed to update event data", "RootIOHandler", StatusCode::FAILURE );
    }
  }
}

void LHCb::IO::details::InputHandler::setAllBranchAddresses() {
  for ( unsigned int n = 0; n < m_eventBranches.size(); n++ ) {
    // Event data branch
    auto branchName = m_eventBranches[n] + ".";
    std::replace( begin( branchName ), end( branchName ), '/', '_' );
    if ( m_allowMissingInput && nullptr == m_curTree->FindBranch( branchName.c_str() ) ) {
      // Silently ignore missing branch, hoping nobody will ever read that
      // This was default behavior in FetchDataFromFile and was abused for
      // handling some backward compatibility issues, basically pretending
      // that we would read both versions and never accessing he wrong one
      continue;
    }
    m_curTree->SetBranchAddress( branchName.c_str(), &m_eventPlaceHolder[n], &m_curBranch[n] );
    if ( !m_curBranch[n] ) { throw std::logic_error( branchName ); }
    // References branch. Note that name is same as event branch with _R before the last '.'
    auto refName = branchName.substr( 0, branchName.size() - 1 ) + "_R.";
    m_curTree->SetBranchAddress( refName.c_str(), &m_eventRefPlaceHolder[n], &m_curRefBranch[n] );
    if ( !m_curRefBranch[n] ) { throw std::logic_error( refName ); }
    // read that branch of the tree
  }
}

void LHCb::IO::details::InputHandlerExt::setAllBranchAddresses() {
  // if it's the first time, find out how the extra branches we have
  if ( m_eventBranchesExt.capacity() == 0 ) {
    auto nbExtbranches = m_curTree->GetListOfBranches()->GetEntriesFast();
    m_eventBranchesExt.reserve( nbExtbranches );
    m_eventPathsExt.reserve( nbExtbranches );
    // collect branch names already taken into account;
    std::set<std::string> knownBranches;
    for ( unsigned int n = 0; n < m_eventBranches.size(); n++ ) {
      auto branchName = m_eventBranches[n] + ".";
      std::replace( begin( branchName ), end( branchName ), '/', '_' );
      knownBranches.emplace( branchName );
    }
    for ( auto branch : TRangeDynCast<TBranch>( *m_curTree->GetListOfBranches() ) ) {
      // ignore "_R branches"
      if ( branch->GetFullName().EndsWith( "_R." ) ) continue;
      // check branch in not one of the already listed ones
      std::string bName{branch->GetFullName().View()};
      if ( knownBranches.find( bName ) != knownBranches.end() ) continue;
      // keep the other ones, computing TES path
      auto path = bName.substr( 0, bName.size() - 1 ); // drop last '.'
      std::replace( begin( path ), end( path ), '_', '/' );
      m_eventBranchesExt.emplace_back( bName );
      m_eventPathsExt.emplace_back( path );
    }
    // and reserve space in vectors
    m_curBranchExt.resize( m_eventBranchesExt.size(), nullptr );
    m_curRefBranchExt.resize( m_eventBranchesExt.size(), nullptr );
    m_eventPlaceHolderExt.resize( m_eventBranchesExt.size(), nullptr );
    m_eventRefPlaceHolderExt.resize( m_eventBranchesExt.size(), nullptr );
  }
  // get "regular" branches first
  if ( m_eventBranches.size() > 0 ) { InputHandler::setAllBranchAddresses(); }
  // get extra branches
  for ( unsigned int n = 0; n < m_eventBranchesExt.size(); n++ ) {
    // Event data branch
    auto branchName = m_eventBranchesExt[n];
    m_curTree->SetBranchAddress( branchName.c_str(), &m_eventPlaceHolderExt[n], &m_curBranchExt[n] );
    // References branch. Note that name is same as event branch with _R before the last '.'
    auto refName = branchName.substr( 0, branchName.size() - 1 ) + "_R.";
    m_curTree->SetBranchAddress( refName.c_str(), &m_eventRefPlaceHolderExt[n], &m_curRefBranchExt[n] );
  }
}

void LHCb::IO::details::BufferExt::fillExtraData( IDataProviderSvc& dataSvc, unsigned int evt,
                                                  DataObjectWriteHandle<std::vector<DataObject*>> const& leavesLocation,
                                                  std::vector<std::string> const&                        ignorePaths ) {
  using LeavesList = std::vector<DataObject*>;
  LeavesList all_leaves;
  // deal with precreated /Event path
  DataObject* event;
  dataSvc.retrieveObject( "/Event", event ).ignore();
  all_leaves.push_back( event );
  // first fill the TES with the different extra branches
  for ( auto&& [path, data] : ranges::views::zip( m_inputHandler.m_eventPathsExt, m_eventDataExt[evt] ) ) {
    // Do not register top level /Event, it's created by default and would fail
    if ( path == "/Event" ) continue;
    // Check the ignores paths
    if ( std::find( begin( ignorePaths ), end( ignorePaths ), path ) != end( ignorePaths ) ) continue;
    // Register input to transient store
    if ( auto sc = dataSvc.registerObject( path, data ); sc.isFailure() ) {
      throw GaudiException( "Error in put of " + path, "BufferExt::fillExtraData", sc );
    }
    all_leaves.push_back( data );
  }
  // fill the special InputFileLeavesLocation with the set of branches to save
  // this replaces FetchLeavesFromFile for the RootIOAlg case
  leavesLocation.put( std::move( all_leaves ) );
}
