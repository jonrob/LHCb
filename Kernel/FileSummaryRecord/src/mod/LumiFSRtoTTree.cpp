/*****************************************************************************\
* (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include <Gaudi/Property.h>
#include <GaudiKernel/DataIncident.h>
#include <GaudiKernel/IIncidentListener.h>
#include <GaudiKernel/IIncidentSvc.h>
#include <GaudiKernel/Service.h>
#include <Kernel/SynchronizedValue.h>
#include <LHCb/FileSummaryRecordIncident.h>
#include <TFile.h>
#include <TROOT.h>
#include <TTree.h>
#include <algorithm>
#include <iterator>
#include <map>
#include <nlohmann/json.hpp>
#include <string>
#include <string_view>
#include <utility>

namespace LHCb::FSR {
  struct LumiFSRtoTTree : extends<Service, IIncidentListener> {
    LumiFSRtoTTree( std::string name, ISvcLocator* svcloc ) : extends( std::move( name ), svcloc ) {}

    StatusCode initialize() override {
      return Service::initialize().andThen( [this] {
        // declare ourself as a incident listener (to know when files are opened/closed)
        if ( auto incSvc = service<IIncidentSvc>( "IncidentSvc" ); incSvc ) {
          incSvc->addListener( this, "CONNECTED_NTUPLE_OUTPUT" );
          incSvc->addListener( this, "CONNECTED_OUTPUT" );
          incSvc->addListener( this, FileSummaryRecordIncident::Type );
        }
        // this is to make sure we are finalized very early (in particular before RootHistSvc)
        // otherwise we would try to write to a closed file
        serviceLocator().as<ISvcManager>()->setPriority( name(), 120 ).ignore();
      } );
    }

    void handle( const Incident& inc ) override {
      if ( inc.type() == FileSummaryRecordIncident::Type ) {

        if ( msgLevel( MSG::DEBUG ) ) { debug() << "handling FileSummaryRecordIncident" << endmsg; }

        // Account for merged or unmerged sprucing outputs
        const auto&          temp_fsr     = dynamic_cast<const FileSummaryRecordIncident&>( inc ).data;
        const nlohmann::json incoming_fsr = ( temp_fsr.contains( "inputs" ) ? temp_fsr["inputs"] : temp_fsr );

        nlohmann::json jsonArray;

        // Determine if the JSON represents a single object or an array
        if ( incoming_fsr.is_array() ) {
          if ( msgLevel( MSG::DEBUG ) ) { debug() << "The FSR JSON data represents an array of objects" << endmsg; }
          jsonArray = incoming_fsr;
        } else if ( incoming_fsr.is_object() ) {
          if ( msgLevel( MSG::DEBUG ) ) { debug() << "The FSR JSON data represents a single object" << endmsg; }
          jsonArray = nlohmann::json::array( {incoming_fsr} );
        } else {
          if ( msgLevel( MSG::DEBUG ) ) { debug() << "The FSR JSON data has an unrecognized structure" << endmsg; }
        }
        if ( msgLevel( MSG::DEBUG ) ) { debug() << "check JSON array " << jsonArray << endmsg; }
        m_lumiEvtsByRun.with_lock( [&]( std::map<int, int>& map ) {
          for ( const auto& jsonFSR : jsonArray ) {
            if ( jsonFSR.contains( "LumiCounter.eventsByRun" ) &&
                 jsonFSR["LumiCounter.eventsByRun"].contains( "counts" ) ) {

              for ( const auto& [k, v] : jsonFSR["LumiCounter.eventsByRun"]["counts"].items() ) {
                int run        = std::stoi( k );
                int n_lumievts = v.get<int>();
                if ( msgLevel( MSG::DEBUG ) ) {
                  debug() << "run " << run << " has " << n_lumievts << " lumievents" << endmsg;
                }

                auto [r, inserted] = map.try_emplace( run, n_lumievts );
                if ( !inserted ) r->second += n_lumievts;
              }
            }
          }
        } );
      } else if ( inc.type() == "CONNECTED_NTUPLE_OUTPUT" || inc.type() == "CONNECTED_OUTPUT" ) {
        if ( msgLevel( MSG::DEBUG ) ) { debug() << "handling " << inc.type() << " incident" << endmsg; }
        if ( auto ci = dynamic_cast<const ContextIncident<TFile*>*>( &inc ); ci ) {
          m_outputFiles.with_lock( [&]( std::vector<TFile*>& files ) { files.push_back( ci->tag() ); } );
          if ( msgLevel( MSG::DEBUG ) ) { debug() << "found output file " << ci->tag()->GetName() << endmsg; }
        }
      }
    }

    StatusCode finalize() override {
      if ( auto incSvc = service<IIncidentSvc>( "IncidentSvc" ); incSvc ) { incSvc->removeListener( this ); }

      // Let's check if RootHistSvc created a file without telling us
      if ( auto rootHistSvc = service<IProperty>( "RootHistSvc", false ) ) {
        // the service was instantiated
        if ( auto filename = rootHistSvc->getProperty( "OutputFile" ).toString();
             filename != "UndefinedROOTOutputFileName" ) {
          // and it has a proper output file
          m_outputFiles.with_lock( [this, &filename]( std::vector<TFile*>& outputFiles ) {
            if ( std::none_of( outputFiles.begin(), outputFiles.end(),
                               [&filename]( TFile* out ) { return out->GetName() == filename; } ) ) {
              // oops, nobody told us to write to that file
              if ( auto out = gROOT->GetFile( filename.c_str() ) ) {
                // good, ROOT knows about the file, let's use it
                outputFiles.push_back( out );
                if ( msgLevel( MSG::DEBUG ) ) {
                  debug() << "added RootHistSvc output file " << filename << " to the list" << endmsg;
                }
              }
            }
          } );
        }
      }

      // Create TTree with two branches: runNumber, lumievents
      TTree lumiTree( "lumiTree", "LumiEvents Tree" );
      int   runNumber, sumLumievents;
      lumiTree.Branch( "runNumber", &runNumber, "runNumber/I" );
      lumiTree.Branch( "sumLumiEvents", &sumLumievents, "sumLumievents/I" );

      if ( msgLevel( MSG::DEBUG ) ) { debug() << "lumiTree created" << endmsg; }

      // for each observed runNumber; fill runNumber and lumievents<runNumber> to tree
      m_lumiEvtsByRun.with_lock( [&]( std::map<int, int> const& map ) {
        for ( const auto& [rn, sle] : map ) {
          runNumber     = rn;
          sumLumievents = sle;
          lumiTree.Fill();
        }
      } );

      return m_outputFiles
          .with_lock( [&]( std::vector<TFile*> const& files ) -> StatusCode {
            // Write tree
            for ( const auto& outputFile : files ) {
              if ( outputFile->IsWritable() ) {
                outputFile->WriteObject( &lumiTree, "lumiTree" );
                if ( msgLevel( MSG::DEBUG ) ) { debug() << "lumiTree written to " << outputFile->GetName() << endmsg; }
              } else {
                error() << "destination file " << outputFile->GetName() << " is not writable" << endmsg;
                return StatusCode::FAILURE;
              }
            }
            return StatusCode::SUCCESS;
          } )
          .andThen( [this] {
            if ( msgLevel( MSG::DEBUG ) ) { debug() << "lumiTree written to output file(s)" << endmsg; }
            return extends::finalize();
          } );
    }

    LHCb::cxx::SynchronizedValue<std::vector<TFile*>> m_outputFiles;

    // map of run number -> number of lumi events
    LHCb::cxx::SynchronizedValue<std::map<int, int>> m_lumiEvtsByRun;
  };
  DECLARE_COMPONENT( LumiFSRtoTTree )
} // namespace LHCb::FSR
