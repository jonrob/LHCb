/*****************************************************************************\
* (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include <GaudiKernel/Incident.h>
#include <nlohmann/json.hpp>

namespace LHCb {
  /// Simple incident to notify listeners that a new FSR has been read from an input file.
  struct FileSummaryRecordIncident final : Incident {
    static constexpr const char* Type = "FileSummaryRecordIncident";
    FileSummaryRecordIncident( const std::string& source, const nlohmann::json& data )
        : Incident{source, Type}, data{data} {}
    ~FileSummaryRecordIncident() = default;
    const nlohmann::json& data;
  };
} // namespace LHCb
