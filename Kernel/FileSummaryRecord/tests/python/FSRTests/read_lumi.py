###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import os
import json
from traceback import format_exc
from unittest import TestCase
import itertools
from pprint import pformat
from .write import GUID

FILENAMEFSR = f"{__name__}.fsr.json"
FILENAME = f"{__name__}.root"
FILENAMEJSON = f"{__name__}.json"


def checkDiff(a, b):
    try:
        TestCase().assertEqual(a, b)
    except AssertionError as err:
        return str(err)


def config():
    from PyConf.application import configure, configure_input, ApplicationOptions, root_writer, create_or_reuse_rootIOAlg
    from PyConf.control_flow import CompositeNode
    from PyConf.components import setup_component
    from PyConf.Algorithms import EventCountAlg, Gaudi__Examples__IntDataProducer, ReadTES
    from Configurables import ApplicationMgr, Gaudi__MultiFileCatalog

    Gaudi__MultiFileCatalog("FileCatalog").Catalogs = [
        "xmlcatalog_file:FSRTests.catalog.xml"
    ]

    options = ApplicationOptions(_enabled=False)
    options.input_files = ["FSRTests.write_lumi.root"]
    options.input_type = 'ROOT'
    options.output_file = FILENAME
    options.output_type = 'ROOT'
    options.data_type = 'Upgrade'
    options.dddb_tag = 'upgrade/dddb-20220705'
    options.conddb_tag = 'upgrade/sim-20220705-vc-mu100'
    options.geometry_version = 'run3/trunk'
    options.conditions_version = 'master'
    options.simulation = True
    options.evt_max = -1
    options.monitoring_file = FILENAMEJSON
    # options.output_level = 2

    config = configure_input(options)

    app = ApplicationMgr()
    app.ExtSvc.append(
        config.add(
            setup_component(
                "LHCb__FSR__Sink",
                instance_name="FileSummaryRecord",
                AcceptRegex=r"^(Evt|Other)Counter\.count$",
                OutputFile=FILENAMEFSR,
            )))
    app.ExtSvc.append(
        config.add(
            setup_component(
                "LHCb__Tests__InputFSRSpy",
                instance_name="InputFSRSpy",
            )))

    producer = Gaudi__Examples__IntDataProducer(name="IntDataProducer")
    # force IOAlg as the test expects it to be ran even if none of its output is used !
    # also pretend we need some branch (or ROOT fails reading nothing from a branch...)
    ioalg = create_or_reuse_rootIOAlg(options)
    ioalg._properties['EventBranches'].append('/Event/IntDataProducer')

    cf = CompositeNode(
        "test",
        [
            ioalg,
            ReadTES(Locations=["/Event"]),
            EventCountAlg(name="EvtCounter"),
            EventCountAlg(
                name="OtherCounter",
                ErrorMax=2,  # this is needed just because PyConf does
                # not allow two identical algs apart from the name
            ),
            producer,
            root_writer(options.output_file, [producer.OutputLocation]),
        ])
    app.OutStream.clear()
    config.update(configure(options, cf))

    # make sure the histogram file is not already there
    for name in [FILENAME, FILENAMEFSR, FILENAMEJSON]:
        if os.path.exists(name):
            os.remove(name)


def check(causes, result, stdout):
    result["root_output_file"] = FILENAME

    missing_files = [
        name for name in
        [FILENAME, FILENAMEFSR, FILENAMEJSON, "FSRTests.write_lumi.fsr.json"]
        if not os.path.exists(name)
    ]
    if missing_files:
        causes.append("missing output file(s)")
        result["missing_files"] = ", ".join(missing_files)
        return False

    expected = {
        "EvtCounter.count": {
            "empty": False,
            "nEntries": 5,
            "type": "counter:Counter:m"
        },
        "OtherCounter.count": {
            "empty": False,
            "nEntries": 5,
            "type": "counter:Counter:m"
        },
        "inputs": []
    }

    try:
        import ROOT

        fsr_dump = json.load(open(FILENAMEFSR))

        f = ROOT.TFile.Open(FILENAME)
        fsr_root = json.loads(str(f.FileSummaryRecord))

    except Exception as err:
        causes.append("failure in root file")
        result["python_exception"] = result.Quote(format_exc())
        return False

    result["FSR"] = result.Quote(pformat(fsr_dump))

    # check anything that could go wrong
    checking = "no check yet"
    try:
        guid = fsr_dump.get("guid")
        assert guid, "missing or invalid GUID in FSR dump"

        expected["guid"] = guid  # GUID is random

        # let's get the FSR of the input file from the output of the upstream test
        input_fsr = json.load(open("FSRTests.write_lumi.fsr.json"))
        expected["inputs"].append(input_fsr)

        tester = TestCase()
        tester.maxDiff = None

        # Check that the json contains what we wanted it to contain
        checking = "JSON dump"
        tester.assertEqual(expected, fsr_dump)

        # Check that it does so also after being written into the rootfile as a string
        checking = "ROOT file"
        tester.assertEqual(expected, fsr_root)

        checking = "input incident"

        # Check the correctness of the stdout
        assert "got FSR from input file" in stdout, "missing message from InputFSRSpy"
        lines = list(
            itertools.islice(
                itertools.dropwhile(
                    lambda l: "got FSR from input file" not in l,
                    stdout.splitlines()), 0, 1))[0]
        lines = lines.split('INFO got FSR from input file: ')[1]
        tester.assertEqual(input_fsr, json.loads(lines))

    # If problem found, print cause
    except AssertionError as err:
        causes.append("FSR content")
        result[f"FSR problem ({checking})"] = result.Quote(str(err))
        return False

    return True
