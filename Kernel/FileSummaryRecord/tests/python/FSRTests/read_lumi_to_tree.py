###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import os
import json
from traceback import format_exc
from unittest import TestCase
import itertools
from pprint import pformat
from .write import GUID
import ROOT

FILENAMEFSR = f"{__name__}.fsr.json"
FILENAME = f"{__name__}.root"
FILENAMEJSON = f"{__name__}.json"


def checkDiff(a, b):
    try:
        TestCase().assertEqual(a, b)
    except AssertionError as err:
        return str(err)


def config():
    from PyConf.application import configure, configure_input, ApplicationOptions, root_writer, create_or_reuse_rootIOAlg
    from PyConf.control_flow import CompositeNode
    from PyConf.components import setup_component
    from PyConf.Algorithms import EventCountAlg, Gaudi__Examples__IntDataProducer, ReadTES
    from Configurables import ApplicationMgr, Gaudi__MultiFileCatalog

    Gaudi__MultiFileCatalog("FileCatalog").Catalogs = [
        "xmlcatalog_file:FSRTests.catalog.xml"
    ]

    options = ApplicationOptions(_enabled=False)
    options.input_files = ["FSRTests.write_lumi.root"]
    options.input_type = 'ROOT'
    options.output_file = FILENAME
    options.output_type = 'ROOT'
    options.data_type = 'Upgrade'
    options.dddb_tag = 'upgrade/dddb-20220705'
    options.conddb_tag = 'upgrade/sim-20220705-vc-mu100'
    options.geometry_version = 'run3/trunk'
    options.conditions_version = 'master'
    options.simulation = True
    options.evt_max = 50
    options.monitoring_file = FILENAMEJSON
    # options.output_level = 1

    config = configure_input(options)

    app = ApplicationMgr()
    app.ExtSvc.append(
        config.add(
            setup_component(
                "LHCb__FSR__Sink",
                instance_name="FileSummaryRecord",
                AcceptRegex=r"^(Evt|Other)Counter\.count$",
                OutputFile=FILENAMEFSR,
            )))
    app.ExtSvc.append(
        config.add(
            setup_component(
                "LHCb__Tests__InputFSRSpy",
                instance_name="InputFSRSpy",
            )))
    app.ExtSvc.append(
        config.add(
            setup_component(
                "LHCb__FSR__LumiFSRtoTTree",
                instance_name="LumiFSRtoTTree",
            )))

    producer = Gaudi__Examples__IntDataProducer()
    # force IOAlg as the test expects it to be ran even if none of its output is used !
    # also pretend we need some branch (or ROOT fails reading nothing from a branch...)
    ioalg = create_or_reuse_rootIOAlg(options)
    ioalg._properties['EventBranches'].append('/Event/IntDataProducer')

    cf = CompositeNode(
        "test",
        [
            ioalg,
            ReadTES(Locations=["/Event"]),
            EventCountAlg(name="EvtCounter"),
            EventCountAlg(
                name="OtherCounter",
                ErrorMax=2,  # this is needed just because PyConf does
                # not allow two identical algs apart from the name
            ),
            producer,
            root_writer(options.output_file, [producer.OutputLocation]),
        ])
    app.OutStream.clear()
    config.update(configure(options, cf))

    # make sure the histogram file is not already there
    for name in [FILENAME, FILENAMEFSR, FILENAMEJSON]:
        if os.path.exists(name):
            os.remove(name)


def check_ttree_data(causes, json_file, root_file):
    # Load the JSON data
    json_data = json.load(open(json_file))

    # Open the ROOT file and get the TTree
    root_file = ROOT.TFile.Open(root_file)
    tree = root_file.Get("lumiTree")

    # Get the expected values from the JSON data
    expected_values = {}
    # print(json_data)

    try:
        assert len(json_data['inputs']) == 1, "too many inputs"
    except AssertionError as err:
        causes.append("Unexpected inputs in FSR")
        result[f"FSR problem (unexpected inputs)"] = result.Quote(str(err))
        return False

    input_json = json_data['inputs'][0]

    for key, value in input_json['LumiCounter.eventsByRun']['counts'].items():
        expected_values[int(key)] = value

    # Check if the TTree data matches the expected values
    for i in range(tree.GetEntries()):
        tree.GetEntry(i)
        key = tree.runNumber
        sum_value = tree.sumLumievents
        if key not in expected_values or sum_value != expected_values[key]:
            return False

    return True


def check(causes, result, stdout):
    result["root_output_file"] = FILENAME

    checking = "Content of lumiTree"
    test_content = check_ttree_data(causes, FILENAMEFSR, FILENAME)
    if not test_content:
        causes.append(
            "Problem with the TTree content: does not match FSR JSON content")
        result[f"FSR problem ({checking}"] = "check_ttree_data returned False"
        return False

    return True
