/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "GaudiKernel/IDetDataSvc.h"
#include "GaudiKernel/Time.h"
#include "LHCbAlgs/Consumer.h"

// ----------------------------------------------------------------------------
// Implementation file for class: PrintEventTime
//
// 03/10/2011: Marco Clemencic
// ----------------------------------------------------------------------------

namespace LHCbAlgsTest {

  /**
   * Simple algorithm that prints the current event time.
   *
   * @author Marco Clemencic
   * @date 03/10/2011
   */
  struct PrintEventTime : LHCb::Algorithm::Consumer<void()> {

    using Consumer::Consumer;
    void operator()() const override { info() << "Current event time: " << m_dataSvc->eventTime() << endmsg; }

    ServiceHandle<IDetDataSvc> m_dataSvc{this, "DataService", "DetectorDataSvc", "The detector data service"};
  };

  DECLARE_COMPONENT( PrintEventTime )

} // namespace LHCbAlgsTest
