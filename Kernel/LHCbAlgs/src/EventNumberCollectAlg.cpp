/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include <LHCbAlgs/Consumer.h>

#include <Event/ODIN.h>

#include <GaudiKernel/SerializeSTL.h>

#include <fstream>
#include <iostream>

namespace LHCb {

  /**
   * Super simple algo collecting all event numbers and dumping them to a JSON file
   */
  struct EventNumberCollectAlg final : Algorithm::Consumer<void( ODIN const& )> {
    EventNumberCollectAlg( const std::string& name, ISvcLocator* loc )
        : Consumer{name, loc, {KeyValue{"ODINLocation", ""}}} {}
    void operator()( ODIN const& odin ) const override {
      std::lock_guard<std::mutex> locked{m_numbers_mutex};
      m_numbers.push_back( odin.eventNumber() );
    }
    StatusCode finalize() override {
      // write data to file in json format
      if ( m_jsonFile.value().size() > 0 ) {
        std::ofstream outfile( m_jsonFile.value() );
        GaudiUtils::details::ostream_joiner( outfile << "[", m_numbers, ", " ) << "]";
      }
      return Consumer::finalize();
    }
    Gaudi::Property<std::string>       m_jsonFile{this, "JSONFileName", "",
                                            "Name of the JSON file where to write the list of event numbers at "
                                            "finalization. Nothing is written out when empty"};
    mutable std::vector<std::uint32_t> m_numbers{};
    mutable std::mutex                 m_numbers_mutex;
  };

  // Declaration of the Algorithm Factory
  DECLARE_COMPONENT_WITH_ID( EventNumberCollectAlg, "EventNumberCollectAlg" )

} // namespace LHCb
