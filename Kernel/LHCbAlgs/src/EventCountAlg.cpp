/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "LHCbAlgs/Consumer.h"

namespace LHCb {

  /**
   * Super simple algo counting events
   */
  struct EventCountAlg final : public LHCb::Algorithm::Consumer<void()> {
    using Consumer::Consumer;
    void                                   operator()() const override { ++m_count; }
    mutable Gaudi::Accumulators::Counter<> m_count{this, "count"};
  };

  // Declaration of the Algorithm Factory
  DECLARE_COMPONENT_WITH_ID( EventCountAlg, "EventCountAlg" )

} // namespace LHCb
