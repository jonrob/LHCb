/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "Gaudi/Algorithm.h"

#include "Gaudi/Accumulators.h"
#include "GaudiKernel/IDataManagerSvc.h"

/**
 *  Searches for and removes empty data nodes in the TES
 *
 *  @author Chris Jones
 *  @date   2012-01-31
 */
class EmptyEventNodeCleaner : public Gaudi::Algorithm {

public:
  using Algorithm::Algorithm;
  virtual StatusCode execute( const EventContext& ) const override;

protected:
  Gaudi::Property<std::string>    m_inputStream{this, "InputStream", "/Event", "Input stream root"};
  ServiceHandle<IDataProviderSvc> m_dataSvc{this, "DataService", "EventDataSvc", "The data service"};
};
