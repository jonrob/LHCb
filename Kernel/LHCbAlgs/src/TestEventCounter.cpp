/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Kernel/IEventCounter.h"
#include "LHCbAlgs/Consumer.h"

// ----------------------------------------------------------------------------
// Implementation file for class: TestEventCounter
//
// 11/06/2012: Marco Clemencic
// ----------------------------------------------------------------------------
namespace LHCbAlgsTests {

  /**
   * @author Marco Clemencic
   * @date 11/06/2012
   */
  struct TestEventCounter final : LHCb::Algorithm::Consumer<void()> {
    using Consumer::Consumer;
    void operator()() const override { info() << "Event count = " << m_eventCounter->getEventCounter() << endmsg; }
    PublicToolHandle<IEventCounter> m_eventCounter{this, "EvtCounter", "EvtCounter",
                                                   "Type/Name of the (public) event counter to use."};
  };

  DECLARE_COMPONENT( TestEventCounter )

  struct ForceEventCounter final : LHCb::Algorithm::Consumer<void( EventContext const& )> {
    using Consumer::Consumer;
    void operator()( EventContext const& ctx ) const override { m_cntr->setEventCounter( ctx.evt() * 10 ); }
    mutable PublicToolHandle<IEventCounter> m_cntr{this, "Counter", "EvtCounter/Forcing"};
  };

  DECLARE_COMPONENT( ForceEventCounter )

} // namespace LHCbAlgsTests
