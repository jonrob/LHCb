/*****************************************************************************\
* (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "GaudiAlg/Transformer.h"
#include "LHCbAlgs/Traits.h"

namespace LHCb::Algorithm {

  /**
   * Redefinition of Gaudi::Functional::Transformer with LHCb customization
   *
   * see Traits.h for the LHCb defaults
   */
  template <typename Signature, typename Traits_ = Traits::Default>
  using Transformer = Gaudi::Functional::Transformer<Signature, Traits::details::add_base_t<Traits_>>;

  /**
   * Redefinition of Gaudi::Functional::MultiTransformer with LHCb customization
   */
  template <typename Signature, typename Traits_ = Traits::Default>
  using MultiTransformer = Gaudi::Functional::MultiTransformer<Signature, Traits::details::add_base_t<Traits_>>;

  /**
   * Redefinition of Gaudi::Functional::MultiTransformerFilter with LHCb customization
   */
  template <typename Signature, typename Traits_ = Traits::Default>
  using MultiTransformerFilter =
      Gaudi::Functional::MultiTransformerFilter<Signature, Traits::details::add_base_t<Traits_>>;

} // namespace LHCb::Algorithm
