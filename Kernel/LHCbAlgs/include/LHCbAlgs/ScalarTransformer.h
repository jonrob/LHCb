/*****************************************************************************\
* (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "GaudiAlg/ScalarTransformer.h"
#include "LHCbAlgs/Traits.h"

namespace LHCb::Algorithm {

  /**
   * Redefinition of Gaudi::Functional::ScalarTransformer with LHCb customization
   *
   * See Traits.h for the LHCb defaults
   */
  template <typename ScalarOp, typename TransformerSignature, typename Traits_ = Traits::Default>
  using ScalarTransformer =
      Gaudi::Functional::ScalarTransformer<ScalarOp, TransformerSignature, Traits::details::add_base_t<Traits_>>;

  /**
   * Redefinition of Gaudi::Functional::MultiScalarTransformer with LHCb customization
   *
   * See Traits.h for the LHCb defaults
   */
  template <typename ScalarOp, typename TransformerSignature, typename Traits_ = Traits::Default>
  using MultiScalarTransformer =
      Gaudi::Functional::MultiScalarTransformer<ScalarOp, TransformerSignature, Traits::details::add_base_t<Traits_>>;

} // namespace LHCb::Algorithm
