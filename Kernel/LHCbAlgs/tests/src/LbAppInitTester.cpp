/*****************************************************************************\
* (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include <Kernel/LbAppInit.h>
#include <LHCbAlgs/Consumer.h>
#include <sstream>

namespace LHCbTests {
  struct LbAppInitTester : LHCb::Algorithm::Consumer<void(), LHCb::DetDesc::usesBaseAndConditions<LbAppInit>> {
    using Consumer::Consumer;

    void operator()() const override {
      auto cdbTags = condDBTags();

      std::stringstream msg;
      msg << "CondDB tags:";

      for ( const auto& [k, v] : cdbTags ) { msg << "\n  " << k << " -> " << v; }

      info() << msg.str() << endmsg;
    }
  };
  DECLARE_COMPONENT( LbAppInitTester )
} // namespace LHCbTests
