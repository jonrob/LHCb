###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from PyConf.application import ApplicationOptions, configure, configure_input, default_raw_banks
from PyConf.control_flow import CompositeNode
from PyConf.Algorithms import EventCountAlg

options = ApplicationOptions(_enabled=False)
options.set_input_and_conds_from_testfiledb("rootreadingexample")
options.input_type = "ROOT"

config = configure_input(options)

cf_node = CompositeNode(
    'Seq',
    children=[
        default_raw_banks('ODIN'),
        EventCountAlg(name="EventCounterAlg")
    ])
config.update(configure(options, cf_node))
