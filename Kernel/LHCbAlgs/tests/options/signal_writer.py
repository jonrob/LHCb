###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from PyConf.Algorithms import (
    SignalMDFWriter,
    ConfigurableDummy,
)
from PyConf.application import (
    ApplicationOptions,
    configure,
    configure_input,
    default_raw_event,
)
from PyConf.control_flow import CompositeNode, NodeLogic

options = ApplicationOptions(_enabled=False)

options.set_input_and_conds_from_testfiledb('2023_raw_hlt1_269939')
options.n_threads = 1
options.n_event_slots = 4
options.evt_max = 50
# options.use_iosvc = True
options.event_store = 'EvtStoreSvc'
options.scheduler_legacy_mode = False

configure_input(options)

ensure_event = SignalMDFWriter(
    OutputPrefix="signal", RawEvent=default_raw_event("VP"))
prescaler = ConfigurableDummy(CFD=20)
crash = ConfigurableDummy(Signal=11)
top = CompositeNode(
    "top", [ensure_event, prescaler, crash],
    combine_logic=NodeLogic.LAZY_AND,
    force_order=True)

configure(options, top)

from Configurables import LHCb__DetDesc__ReserveDetDescForEvent as reserveIOV
reserveIOV("reserveIOV").PreloadGeometry = False
