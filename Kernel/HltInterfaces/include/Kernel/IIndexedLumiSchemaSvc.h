/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

// Include files
#include "GaudiKernel/INamedInterface.h"

/** @class ITCKANNSvc ITCKANNSvc.h
 *
 * return the layout of a lumi bank given a key
 *
 *  @author Gerhard Raven
 *  @date   2014-05-29
 */

struct IIndexedLumiSchemaSvc : extend_interfaces<INamedInterface> {
public:
  /// Return the interface ID
  DeclareInterfaceID( IIndexedLumiSchemaSvc, 1, 0 );

  struct CounterDefinition {
    std::string name;
    unsigned    size;
    unsigned    offset;
    float       shift{0.f};
    float       scale{1.f};
  };

  template <typename JSON>
  static CounterDefinition from_json( const JSON& json ) {
    return CounterDefinition{json["name"].template get<std::string>(), json["size"].template get<unsigned>(),
                             json["offset"].template get<unsigned>(),
                             json.contains( "shift" ) ? json["shift"].template get<float>() : 0.f,
                             json.contains( "scale" ) ? json["scale"].template get<float>() : 1.f};
  }

  struct lumi_schema_t {
    unsigned                       size;
    std::vector<CounterDefinition> counters;
  };

  // given a bank of version `version`, and a key `key`, return which counters can be found where
  virtual lumi_schema_t const& lumiCounters( unsigned key, unsigned version ) const = 0;
};
