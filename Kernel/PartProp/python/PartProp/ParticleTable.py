#!/usr/bin/env python
###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

from ._default_table import DEFAULT_PARTICLE_PROPERTY_FILE
from io import StringIO
import pandas as pd
from GaudiKernel.SystemOfUnits import GeV, s
from typing import Union


class ParticleTable:
    """
    `ParticleTable` is a helper class to access the basic particle properties at configuration level.

    Args:
        particle_properties_file(str, optional): The name of the database file. Defaults to `DEFAULT_PARTICLE_PROPERTY_FILE`.

    Example:
        from PartProp.ParticleTable import ParticleTable
        B0_mass = ParticleTable().mass('B0')
    """

    def __init__(self,
                 particle_properties_file=DEFAULT_PARTICLE_PROPERTY_FILE):
        with open(particle_properties_file) as f:
            lines = [line.rstrip() for line in f if not line.startswith('#')]
        # Select the data lines
        start_index = lines.index('PARTICLE')
        end_index = lines.index('END PARTICLE')
        data_lines = lines[start_index + 1:end_index]
        # Read all properties
        data_str = '\n'.join([','.join(line.split()) for line in data_lines])
        data_str = 'NAME,GEANTID,PDGID,CHARGE,MASS,TLIFE,EVTGENNAME,PYTHIAID,MAXWIDTH\n' + data_str
        # Load it to pandas
        self.table = pd.read_csv(StringIO(data_str))

    def _list_column(self, column_name: str) -> list:
        return self.table[column_name].values.tolist()

    def _exist(self, col_name: str, col_value: str) -> bool:
        return (self.table[col_name] == col_value).any()

    def _get_target(self, find_name: str, find_value: Union[str, int, float],
                    target_name: str):
        if not self._exist(find_name, find_value): return None
        return self.table[self.table[find_name] ==
                          find_value][target_name].values[0]

    def list_particles(self) -> list:
        """
        Retrieve the list of all particle names

        Returns:
            list: the list of all particle names
        """
        return self._list_column('NAME')

    def particle_name(self, pdg_id: int) -> str:
        """
        Retrieve the name of the particle corresponding to a given PDG ID

        Args:
            pdg_id (int): The PDG ID

        Returns:
            str: the name of the particle corresponding to a given PDG ID
        """
        return self._get_target('PDGID', pdg_id, 'NAME')

    def cc(self, particle_name: str) -> str:
        """
        Retrieve the charge-conjugate name of a given particle

        Args:
            particle_name (str): The name of the given particle

        Returns:
            str: The charge-conjugate name of the given particle
        """
        if not self._exist('NAME', particle_name): return None
        this_pid = self._get_target('NAME', particle_name, 'PDGID')
        if this_pid is None: return None
        if self._exist('PDGID', -this_pid):
            return self._get_target('PDGID', -this_pid, 'NAME')
        else:
            # self-conjugate
            return particle_name

    def mass(self, particle_name: str) -> float:
        """
        Retrieve the mass of a given particle in GeV.

        Args:
            particle_name (str): The name of the given particle

        Returns:
            float: The mass of the given particle
        """
        return float(self._get_target('NAME', particle_name, 'MASS')) * GeV

    def pid(self, particle_name: str) -> int:
        """
        Retrieve the PDG ID of a given particle

        Args:
            particle_name (str): The name of the given particle

        Returns:
            int: the PDG ID of a given particle
        """
        return int(self._get_target('NAME', particle_name, 'PDGID'))

    def abs_pid(self, particle_name: str) -> int:
        """
        Retrieve the absolute value of the PDG ID for a given particle.

        Args:
            particle_name (str): The name of the given particle

        Returns:
            int: the absolute value of the PDG ID for a given particle
        """
        return abs(self.pid(particle_name))

    def charge(self, particle_name: str) -> float:
        """
        Retrieve the charge of a given particle

        Args:
            particle_name (str): The name of the given particle

        Returns:
            float: The charge of the given particle
        """
        return float(self._get_target('NAME', particle_name, 'CHARGE'))

    def lifetime(self, particle_name: str) -> float:
        """
        Retrieve the lifetime of a given particle in seconds.

        Args:
            particle_name (str): The name of the given particle

        Returns:
            float: The lifetime of the given particle
        """
        return float(self._get_target('NAME', particle_name, 'TLIFE')) * s

    def width(self, particle_name: str) -> float:
        """
        Retrieve the maximum width of a given particle in GeV.

        Args:
            particle_name (str): The name of the given particle

        Returns:
            float: The maximum width of the given particle
        """
        return self._get_target('NAME', particle_name, 'MAXWIDTH') * GeV
