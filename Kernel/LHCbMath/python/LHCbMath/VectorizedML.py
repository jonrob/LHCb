###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import torch
import numpy as np
from torch import nn


class Sequence(nn.Module):
    def __init__(self, model):
        super().__init__()
        mod = model.model()
        nLayers = mod.nLayers()
        # build layers
        mystack = []

        def get_torch_layer(layer):
            tlayer = None
            layer_type = layer.name()[:-2]
            if layer_type == "Linear":
                tlayer = nn.Linear(layer.nInputs(), layer.nOutputs())
            elif layer_type == "PiecewiseLinearTransform":
                return tlayer
            else:
                layer_dict = {"Sigmoid": nn.Sigmoid, "ReLU": nn.ReLU}
                tlayer = layer_dict[layer_type]()
            return tlayer

        layer_names = []
        from collections import OrderedDict
        for i in range(nLayers):
            layer = mod.get_layer(i)
            name = layer.name()
            layer_names.append(name)
            tlayer = get_torch_layer(layer)
            if tlayer: mystack.append((name, tlayer))
        self._layer_names = layer_names
        self._stack = nn.Sequential(OrderedDict(mystack))
        # option of flattening layer
        self._can_flatten = "PiecewiseLinearTransform" in layer_names[-1]
        self._nquantiles = mod.get_layer(nLayers - 1).nBins()
        self._quantiles = []
        # save feature names
        feats = model.features()
        self._features = [feats.name(i) for i in range(feats.size())]

    def forward(self, x):
        return self._stack(x)

    def features(self):
        return self._features

    def write_to_json(self, filename):
        # save weights dictionary to json
        import json
        from json import JSONEncoder
        from torch.utils.data import Dataset

        class EncodeTensor(JSONEncoder, Dataset):
            def default(self, obj):
                if isinstance(obj, torch.Tensor):
                    return obj.cpu().detach().numpy().tolist()
                if isinstance(obj, np.ndarray):
                    return obj.tolist()
                return super(EncodeTensor, self).default(obj)

        with open(filename, 'w') as json_file:
            # allow versioning of files for unforeseen changes
            # and feature names for checking proper model loading
            data = {'metadata': {'version': 1, 'features': self._features}}
            # save torch parameters
            data.update(self.state_dict())
            # save non-torch layer info if needed
            # first consider a flattening layer
            if len(self._quantiles):
                # add flatten layer, should always be last layer
                flname = self._layer_names[-1]
                # only supported with the following type of layer
                if 'PiecewiseLinearTransform' in flname:
                    data[f'{flname}.quantiles'] = self._quantiles
                else:
                    raise NameError(
                        "Flattening layer only supported with 'PiecewiseLinearTransform'"
                    )
            json.dump(data, json_file, cls=EncodeTensor)

    def read_from_json(self, filename):
        import json

        class DecodeListToTensor(json.JSONDecoder):
            def decode(self, s):
                obj = super().decode(s)
                if isinstance(obj, dict):
                    for key, value in obj.items():
                        if isinstance(value, list):
                            arr = np.array(value)
                            obj[key] = arr if 'PiecewiseLinearTransform' in key else torch.Tensor(
                                arr)
                return obj

        with open(filename, 'r') as json_file:
            data = json.load(json_file, cls=DecodeListToTensor)
            # first extract all non-torch info
            metadata = data['metadata']
            assert (self._features == metadata['features'])
            del data['metadata']
            # for now only a flattening layer (using quantiles)
            if self._can_flatten:
                qkey = "{0}.quantiles".format(self._layer_names[-1])
                assert (qkey in data)
                self._quantiles = data[qkey]
                del data[qkey]
            # now load (remaining) torch info
            self.load_state_dict(data)

    def add_flatten_layer(self, data, bounds, smoothen=False):
        from scipy import stats
        assert (self._can_flatten)
        assert (len(bounds) == 2)
        qbins = np.array(
            [i / (self._nquantiles) for i in range(1, self._nquantiles)])
        self._quantiles = np.array([bounds[0]] +
                                   list(stats.mstats.mquantiles(data, qbins)) +
                                   [bounds[1]])
        if smoothen:
            # option to smoothen the quantiles to reduce stat. fluctuations in behaviour
            from scipy.interpolate import UnivariateSpline
            diffs = [
                self._quantiles[i + 1] - self._quantiles[i]
                for i in range(len(self._quantiles) - 1)
            ]
            xvals = [i / len(diffs) for i in range(len(diffs))]
            spline = UnivariateSpline(xvals, diffs)
            new_diffs = np.array(spline(xvals))
            # ensure bounds by normalization, make sure we have non-zero positive values
            for i in range(len(new_diffs)):
                if new_diffs[i] < 0: new_diffs[i] == -new_diffs[i]
            new_diffs = new_diffs / sum(new_diffs)
            new_edges = [0]
            for diff in new_diffs:
                new_edges.append(diff + new_edges[-1])
            self._quantiles = np.array([bounds[0]] + new_edges[1:-1] +
                                       [bounds[1]])

    def forward_flattened(self, x):
        def get_index(val, array):
            if val < array[0]: return 0
            for i in range(len(array) - 1):
                if (val >= array[i]) and (val < array[i + 1]):
                    return i
            return len(array) - 2

        def get_yval(val, idx):
            xlow = self._quantiles[idx]
            xup = self._quantiles[idx + 1]
            dy = (self._quantiles[-1] - self._quantiles[0]) / self._nquantiles
            ylow = idx * dy + self._quantiles[0]
            yup = ylow + dy
            scale = dy / (xup - xlow)
            offset = ylow - scale * xlow
            return scale * val + offset

        return np.array([
            get_yval(val, get_index(val, self._quantiles))
            for val in (self.forward(x)).detach().cpu().numpy()
        ])
