/*****************************************************************************\
* (c) Copyright 2023-2024 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "InferenceBase.h"
#include <GaudiKernel/GaudiException.h>
#include <GaudiKernel/StatusCode.h>
#include <LHCbMath/SIMDWrapper.h>
#include <LHCbMath/Utils.h>
#include <algorithm>
#include <nlohmann/json.hpp>
#include <numeric>
#include <optional>
#include <stdexcept>
#include <string>
#include <tuple>
#include <utility>

/* @brief for relatively simple MLPs using SIMD, modeled after PyTorch's 'torch.nn.Sequential'
 *
 * To be trained with its pythonic version in 'LHCbMath/python/LHCbMath/VectorizedML.py'
 * using PyTorch (tested example of training/inference pipeline should be available).
 * uses SIMD linear algebra used in LHCbMath/MatVec.h, but own multiplication etc
 * because explicit unroll of loops is not very efficient (fast) in memory
 * with large matrices and vectors.
 */

namespace LHCb::VectorizedML {

  namespace Layers {

    enum class Type { Linear, Sigmoid, ReLU, PiecewiseLinearTransform };

    constexpr const char* TypeToString( Type type ) {
      switch ( type ) {
      case Type::Linear:
        return "Linear";
      case Type::Sigmoid:
        return "Sigmoid";
      case Type::ReLU:
        return "ReLU";
      case Type::PiecewiseLinearTransform:
        return "PiecewiseLinearTransform";
      default:
        throw std::invalid_argument( "Unimplemented layer type" );
      }
    }

    // base class
    class LayerBase {
    public:
      LayerBase() {}
      LayerBase( Type type, size_t nInput ) : m_type( type ), m_ninput( nInput ), m_noutput( nInput ) {}
      LayerBase( Type type, size_t nInput, size_t nOutput )
          : m_type( type ), m_ninput( nInput ), m_noutput( nOutput ) {}

      void set_index( size_t index ) { m_index = index; }
      auto name() const { return std::string( TypeToString( m_type ) ) + "_" + std::to_string( m_index ); };

      auto nInputs() const { return m_ninput; }
      auto nOutputs() const { return m_noutput; }

      auto nBins() const { return m_nbins; }

      auto type() const { return static_cast<int>( m_type ); }
      auto index() const { return m_index; }

    protected:
      Type   m_type{};
      size_t m_index{};
      int    m_ninput{};
      int    m_noutput{};
      int    m_nbins = 0;
    };

    // linear layers

    template <int nInput, int nOutput, typename FType = simd::float_v>
    class Linear : public LayerBase {
    public:
      Linear() : LayerBase( Type::Linear, nInput, nOutput ) {}

      template <typename Range>
      auto operator()( Range&& input ) const {
        Vec<FType, nOutput> out;
        auto                itr_w   = m_weights.m.begin();
        auto                last_w  = m_weights.m.end();
        auto                itr_b   = m_bias.m.begin();
        auto                itr_out = out.m.begin();
        while ( itr_w != last_w ) {
          *itr_out++ = std::inner_product( input.m.begin(), input.m.end(), itr_w, FType( 0 ) ) + *itr_b++;
          itr_w += nInput;
        }
        return out;
      }

      template <typename Range>
      void set_parameters( std::string const& key, Range const& input ) {
        if ( key.find( this->name() ) != std::string::npos ) {
          if ( key.find( "weight" ) != std::string::npos ) {
            this->set_weights( input );
          } else if ( key.find( "bias" ) != std::string::npos ) {
            this->set_bias( input );
          } else {
            throw GaudiException( "Unknown property of layer found: '" + key + "'", "", StatusCode::FAILURE );
          }
        }
      }

    private:
      template <typename Range>
      void set_weights( Range const& input ) {
        assert( input.size() == m_weights.n_rows && input.front().size() == m_weights.n_cols );
        for ( size_t i = 0; i < input.size(); i++ ) {
          for ( size_t j = 0; j < input[i].size(); j++ ) m_weights( i, j ) = FType( input[i][j] );
        }
      }

      template <typename Range>
      void set_bias( Range const& input ) {
        assert( input.size() == m_bias.size );
        for ( size_t i = 0; i < m_bias.size; i++ ) m_bias( i ) = FType( input[i] );
      }

    private:
      Mat<FType, nOutput, nInput> m_weights;
      Vec<FType, nOutput>         m_bias;
    };

    // activation layers

    template <int nInput, typename FType = simd::float_v>
    class ReLU : public LayerBase {
    public:
      ReLU() : LayerBase( Type::ReLU, nInput ) {}

      template <typename Range>
      constexpr auto operator()( Range&& input ) const {
        Vec<FType, nInput> out;
        std::transform( input.m.begin(), input.m.end(), out.m.begin(),
                        [&]( const auto& w ) { return select( w > 0, w, 0 ); } );
        return out;
      }

      template <typename Range>
      void set_parameters( std::string const&, Range const& ) {
        return;
      }
    };

    template <int nInput, typename FType = simd::float_v>
    class Sigmoid : public LayerBase {
    public:
      Sigmoid() : LayerBase( Type::Sigmoid, nInput ) {}

      template <typename Range>
      constexpr auto operator()( Range&& input ) const {
        Vec<FType, nInput> out;
        std::transform( input.m.begin(), input.m.end(), out.m.begin(),
                        [&]( const auto& w ) { return SIMDWrapper::sigmoid( w ); } );
        return out;
      }

      template <typename Range>
      void set_parameters( std::string const&, Range const& ) {
        return;
      }
    };

    // transformations
    // e.g for flattening distributions
    template <int nInput, int nBin, typename FType = simd::float_v>
    class PiecewiseLinearTransform : public LayerBase {
    public:
      PiecewiseLinearTransform() : LayerBase( Type::PiecewiseLinearTransform, nInput ) { m_nbins = nBin; }

      template <typename Range>
      constexpr auto operator()( Range&& input ) const {
        Vec<FType, nInput> out;
        std::transform( input.m.begin(), input.m.end(), out.m.begin(), [&]( const auto& x ) {
          // ensure x bounds
          auto xedge = m_xedges.m.begin();
          auto end   = m_xedges.m.end();
          // lookup bin indices (assume edges are sorted low to high)
          auto no_idx = simd::mask_v( true );
          auto it_idx = simd::int_v( 0 );
          auto up_idx = simd::int_v( 1 );
          while ( ( xedge != end ) && popcount( no_idx ) > 0 ) {
            // check bin/edge
            auto check = no_idx && ( x < *xedge );
            up_idx     = select( check, it_idx, up_idx );
            no_idx     = no_idx && !check;
            // move to next
            ++xedge;
            it_idx += 1;
          }
          // ensure bounds
          auto low_idx = select( no_idx, it_idx - 2, up_idx - 1 );
          low_idx      = select( low_idx < 0, 0, low_idx );
          // linear transformation in selected bins
          auto scale  = gather( m_scale.data(), low_idx );
          auto offset = gather( m_offset.data(), low_idx );
          return scale * x + offset;
        } );
        return out;
      }

      template <typename Range>
      void set_parameters( std::string const& key, Range const& input ) {
        if ( key.find( this->name() ) != std::string::npos ) {
          // TODO add also option if not quantiles / flattening layer, add under different key
          if ( key.find( "quantiles" ) != std::string::npos ) {
            // assert dimensionalities and load bin edges
            assert( m_xedges.size == input.size() );
            std::array<float, nBin + 1> xedges, yedges;
            float                       ystart = input[0];
            float                       yend   = input[nBin];
            auto                        width  = ( yend - ystart ) / ( (float)nBin );
            for ( int i = 0; i < nBin + 1; i++ ) {
              xedges[i] = input[i];
              yedges[i] = width * i + ystart;
            }
            for ( size_t i = 0; i < xedges.size(); i++ ) m_xedges( i ) = FType( xedges[i] );
            // buffer linear transformations
            for ( int i = 0; i < nBin; i++ ) {
              auto xup    = xedges[i + 1];
              auto xlow   = xedges[i];
              auto yup    = yedges[i + 1];
              auto ylow   = yedges[i];
              m_scale[i]  = ( yup - ylow ) / ( xup - xlow );
              m_offset[i] = ylow - m_scale[i] * xlow;
            }
          } else {
            throw GaudiException( "Unknown property of layer found: '" + key + "'", "", StatusCode::FAILURE );
          }
        }
        return;
      }

    private:
      std::array<float, nBin> m_scale;
      std::array<float, nBin> m_offset;
      Vec<FType, nBin + 1>    m_xedges;
    };

    // layer concatenation operator for evaluation, protected in namespace
    namespace Concatenation {
      template <typename Range, typename Layer>
      auto operator>>( Range&& r, Layer const& l ) {
        return l( std::forward<Range>( r ) );
      }
    } // namespace Concatenation

  } // namespace Layers

  template <int nInput, int nOutput, typename... SpecificLayers>
  class Sequence : public InferenceBase<nInput, nOutput> {
  public:
    using Base      = InferenceBase<nInput, nOutput>;
    using FType     = typename Base::FType;
    using InputVec  = typename Base::InputVec;
    using OutputVec = typename Base::OutputVec;

    Sequence() {
      Utils::unwind<0, sizeof...( SpecificLayers )>( [this]( auto i ) { this->_get_layer<i>()->set_index( i ); } );
    }

    // evaluation using concatenation
    OutputVec evaluate( InputVec const& input ) const override {
      using namespace Layers::Concatenation;
      return std::apply( [&]( auto&&... l ) { return ( input >> ... >> l ); }, m_layers );
    }

    StatusCode load( std::optional<std::string> const& weights_buffer ) override {
      if ( !weights_buffer ) throw GaudiException( "Couldn't open weights file", "", StatusCode::FAILURE );
      auto weights_json = nlohmann::json::parse( *weights_buffer );
      for ( auto const& [key, value] : weights_json.items() ) {
        Utils::unwind<0, sizeof...( SpecificLayers )>(
            [key = key, value = value, this]( auto i ) { this->_get_layer<i>()->set_parameters( key, value ); } );
      }
      return StatusCode::SUCCESS;
    }

  private:
    template <int idx_layer>
    auto _get_layer() {
      return &std::get<idx_layer>( m_layers );
    }

  public:
    constexpr size_t nInputs() const { return nInput; }
    constexpr size_t nOutputs() const { return nOutput; }
    constexpr size_t nLayers() const { return sizeof...( SpecificLayers ); }

    Layers::LayerBase const* get_layer( int idx ) const {
      Layers::LayerBase const* layer = nullptr;
      Utils::unwind<0, sizeof...( SpecificLayers )>( [&]( auto j ) {
        if ( idx == j ) layer = dynamic_cast<Layers::LayerBase const*>( &std::get<j>( m_layers ) );
      } );
      return layer;
    }

  private:
    std::tuple<SpecificLayers...> m_layers;
  };

} // namespace LHCb::VectorizedML
