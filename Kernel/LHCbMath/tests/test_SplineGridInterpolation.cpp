/*****************************************************************************\
* (c) Copyright 2000-2023 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE example
#include <boost/test/unit_test.hpp>

#include "LHCbMath/SplineGridInterpolation.h"
#include <array>
#include <cmath>

BOOST_AUTO_TEST_CASE( test_md_view ) {

  std::array<int, 60> ai{0,   1,   2,   10,  11,  12,  20,  21,  22,  30,  31,  32,  40,  41,  42,
                         100, 101, 102, 110, 111, 112, 120, 121, 122, 130, 131, 132, 140, 141, 142,
                         200, 201, 202, 210, 211, 212, 220, 221, 222, 230, 231, 232, 240, 241, 242,
                         300, 301, 302, 310, 311, 312, 320, 321, 322, 330, 331, 332, 340, 341, 342};

  auto view = LHCb::Math::make_md_view<4, 5, 3>( ai );

  for ( auto a : view ) {
    for ( auto b : a ) {
      for ( auto& c : b ) {
        c += 10000;
        std::cout << c << " ";
      }
      std::cout << '\n';
    }
    std::cout << '\n';
  }

  std::cout << ">>>\n\n";

  auto cview = LHCb::Math::make_md_view<4, 5, 3>( std::as_const( ai ) );
  for ( auto a : cview.slice<2, 1, 2>( {2, 3, 1} ) ) {
    for ( auto b : a ) {
      for ( auto c : b ) { std::cout << c << " "; }
      std::cout << "\n";
    }
    std::cout << "\n";
  }
}

template <size_t Nx, size_t Ny, typename Float = double, typename ValueType = double>
class Grid2D {
  std::array<ValueType, Nx * Ny> m_grid;
  Float                          m_xmin, m_idx, m_ymin, m_idy;

  Float xNorm( Float x ) const { return m_idx * ( x - m_xmin ); }
  Float yNorm( Float y ) const { return m_idy * ( y - m_ymin ); }

public:
  template <size_t N>
  constexpr auto xMin() const {
    static_assert( N == 4 || N == 6 || N == 8, "invalid number of gridpoints..." );
    return m_xmin + ( N - 2 ) / ( 2 * m_idx );
  }
  template <size_t N>
  constexpr auto xMax() const {
    static_assert( N == 4 || N == 6 || N == 8, "invalid number of gridpoints..." );
    return m_xmin + ( Nx - 1 - ( N - 2 ) / 2 ) / m_idx;
  }
  template <size_t N>
  constexpr auto yMin() const {
    static_assert( N == 4 || N == 6 || N == 8, "invalid number of gridpoints..." );
    return m_ymin + ( N - 2 ) / ( 2 * m_idy );
  }
  template <size_t N>
  constexpr auto yMax() const {
    static_assert( N == 4 || N == 6 || N == 8, "invalid number of gridpoints..." );
    return m_ymin + ( Ny - 1 - ( N - 2 ) / 2 ) / m_idy;
  }

  template <typename Fun>
  Grid2D( Fun&& fun, std::array<Float, 2> xr, std::array<Float, 2> yr )
      : m_xmin{xr[0]}
      , m_idx{Float{Nx - 1} / ( xr[1] - xr[0] )}
      , m_ymin{yr[0]}
      , m_idy{Float{Ny - 1} / ( yr[1] - yr[0] )} {
    auto g = m_grid.begin();
    for ( size_t ix = 0; ix != Nx; ++ix )
      for ( size_t iy = 0; iy != Ny; ++iy ) { *g++ = fun( m_xmin + ix / m_idx, m_ymin + iy / m_idy ); }
  }

  template <size_t Order, size_t NPoints>
  ValueType splineInterpolate( Float x, Float y ) const {
    // TODO: check that (x,y) within the bounds of thr grid, taking into account NPoints!
    return LHCb::Math::Spline::interpolate<Order, NPoints>( LHCb::Math::make_md_view<Nx, Ny>( m_grid ), xNorm( x ),
                                                            yNorm( y ) );
  }
};

BOOST_AUTO_TEST_CASE( test_2d_grid_interpolation ) {

  constexpr double pi  = 3.14159265358979323846;
  auto             fun = []( double x, double y ) { return sin( x ) * cos( y ); };

  Grid2D<30, 30, double, double> grid{fun, {-1, 2 * pi + 1}, {-1, 2 * pi + 1}};

  double d2 = 0;
  int    n  = 0;
  for ( double x = grid.xMin<8>(); x < grid.xMax<8>(); x += 0.01 )
    for ( double y = grid.yMin<8>(); y < grid.yMax<8>(); y += 0.01 ) {
      double f = fun( x, y );
      double a = grid.splineInterpolate<3, 8>( x, y );
      d2 += std::pow( f - a, 2 );
      ++n;
    }
  std::cout << "RMS: " << std::sqrt( d2 / n ) << '\n';
}

template <size_t Nx, size_t Ny, size_t Nz, typename Float = double, typename ValueType = double>
class Grid3D {
  std::array<ValueType, Nx * Ny * Nz> m_grid;
  Float                               m_xmin, m_idx, m_ymin, m_idy, m_zmin, m_idz;

  Float xNorm( Float x ) const { return m_idx * ( x - m_xmin ); }
  Float yNorm( Float y ) const { return m_idy * ( y - m_ymin ); }
  Float zNorm( Float z ) const { return m_idz * ( z - m_zmin ); }

public:
  template <size_t N>
  constexpr auto xMin() const {
    static_assert( N == 4 || N == 6 || N == 8, "invalid number of gridpoints..." );
    return m_xmin + ( N - 2 ) / ( 2 * m_idx );
  }
  template <size_t N>
  constexpr auto xMax() const {
    static_assert( N == 4 || N == 6 || N == 8, "invalid number of gridpoints..." );
    return m_xmin + ( Nx - 1 - ( N - 2 ) / 2 ) / m_idx;
  }
  template <size_t N>
  constexpr auto yMin() const {
    static_assert( N == 4 || N == 6 || N == 8, "invalid number of gridpoints..." );
    return m_ymin + ( N - 2 ) / ( 2 * m_idy );
  }
  template <size_t N>
  constexpr auto yMax() const {
    static_assert( N == 4 || N == 6 || N == 8, "invalid number of gridpoints..." );
    return m_ymin + ( Ny - 1 - ( N - 2 ) / 2 ) / m_idy;
  }
  template <size_t N>
  constexpr auto zMin() const {
    static_assert( N == 4 || N == 6 || N == 8, "invalid number of gridpoints..." );
    return m_zmin + ( N - 2 ) / ( 2 * m_idz );
  }
  template <size_t N>
  constexpr auto zMax() const {
    static_assert( N == 4 || N == 6 || N == 8, "invalid number of gridpoints..." );
    return m_zmin + ( Nz - 1 - ( N - 2 ) / 2 ) / m_idz;
  }

  template <typename Fun>
  Grid3D( Fun&& fun, std::array<Float, 2> xr, std::array<Float, 2> yr, std::array<Float, 2> zr )
      : m_xmin{xr[0]}
      , m_idx{Float{Nx - 1} / ( xr[1] - xr[0] )}
      , m_ymin{yr[0]}
      , m_idy{Float{Ny - 1} / ( yr[1] - yr[0] )}
      , m_zmin{zr[0]}
      , m_idz{Float{Nz - 1} / ( zr[1] - zr[0] )} {
    auto g = m_grid.begin();
    for ( size_t ix = 0; ix != Nx; ++ix )
      for ( size_t iy = 0; iy != Ny; ++iy )
        for ( size_t iz = 0; iz != Nz; ++iz ) {
          *g++ = fun( m_xmin + ix / m_idx, m_ymin + iy / m_idy, m_zmin + iz / m_idz );
        }
  }

  template <size_t Order, size_t NPoints>
  ValueType splineInterpolate( Float x, Float y, Float z ) const {
    // TODO: check that (x,y) within the bounds of thr grid, taking into account NPoints!
    return LHCb::Math::Spline::interpolate<Order, NPoints>( LHCb::Math::make_md_view<Nx, Ny, Nz>( m_grid ), xNorm( x ),
                                                            yNorm( y ), zNorm( z ) );
  }
};

// CTAD is (in C++17) 'all or nothing' -- so if we only want to deduce 'Float' and 'ValueType',
// but have explicit Nx, Ny, Nz, we still need to resort to a 'make' function...
template <size_t Nx, size_t Ny, size_t Nz, typename Fun, typename Float>
auto make_interpolated_grid( Fun&& f, std::array<Float, 2> xr, std::array<Float, 2> yr, std::array<Float, 2> zr ) {
  return Grid3D<Nx, Ny, Nz, Float, std::invoke_result_t<Fun, Float, Float, Float>>{
      std::forward<Fun>( f ), std::move( xr ), std::move( yr ), std::move( zr )};
}
template <size_t Nx, size_t Ny, typename Fun, typename Float>
auto make_interpolated_grid( Fun&& f, std::array<Float, 2> xr, std::array<Float, 2> yr ) {
  return Grid2D<Nx, Ny, Float, std::invoke_result_t<Fun, Float, Float>>{std::forward<Fun>( f ), std::move( xr ),
                                                                        std::move( yr )};
}

BOOST_AUTO_TEST_CASE( test_3d_grid_interpolation ) {

  constexpr double pi  = 3.14159265358979323846;
  auto             fun = []( double x, double y, double z ) { return sin( x ) * cos( y ) * z; };

  auto grid = make_interpolated_grid<30, 30, 20>( fun, std::array{-1., 2 * pi + 1}, std::array{-1., 2 * pi + 1},
                                                  std::array{-2., 2.} );

  double d2 = 0;
  int    n  = 0;
  for ( double x = grid.xMin<4>(); x < grid.xMax<4>(); x += 0.01 )
    for ( double y = grid.yMin<4>(); y < grid.yMax<4>(); y += 0.01 )
      for ( double z = grid.zMin<4>(); z < grid.zMax<4>(); z += 0.05 ) {
        double f = fun( x, y, z );
        double a = grid.splineInterpolate<3, 4>( x, y, z );
        // std::cout << "f( " << x << ", " << y << ", " << z << " ) = " << f << " " << a << "  " << f - a << '\n';
        d2 += std::pow( f - a, 2 );
        ++n;
      }
  std::cout << "RMS: " << std::sqrt( d2 / n ) << '\n';
}

// 'interpolatable' value types must provide:
//   1. a zero under addition, which can is also the default constructed element
//   2. addition
//   3. left-multiplication by values in the input domain
//  basically, 2 & 3 allow to take weighted averages...
class Point2D {
  std::array<double, 2> m_d = {0, 0};

public:
  Point2D() = default;
  Point2D( double x, double y ) : m_d{x, y} {}
  friend Point2D operator*( double s, Point2D p ) { return {s * p.m_d[0], s * p.m_d[1]}; }
  friend Point2D operator+( Point2D lhs, Point2D rhs ) { return {lhs.m_d[0] + rhs.m_d[0], lhs.m_d[1] + rhs.m_d[1]}; }
  friend std::ostream& operator<<( std::ostream& os, Point2D p ) {
    return os << "(" << p.m_d[0] << "," << p.m_d[1] << ")";
  }
};

// clang-8 will crash on this test...
//  clang::ExprResult clang::Sema::BuildCXXTypeConstructExpr(clang::TypeSourceInfo*, clang::SourceLocation,
//  clang::MultiExprArg, clang::SourceLocation, bool): Assertion `(!ListInitialization || (Exprs.size() == 1 &&
//  isa<InitListExpr>(Exprs[0]))) && "List initialization must have initializer list as expression."' failed.
#if !defined( __clang__ ) || ( __clang__major__ > 8 )
BOOST_AUTO_TEST_CASE( test_3d_vector_field_interpolation ) {

  constexpr double pi = 3.14159265358979323846;
  auto fun = []( double x, double y, double z ) -> Point2D { return {sin( x ) * cos( y ) * z, atan2( z, x + y )}; };

  auto grid = make_interpolated_grid<30, 30, 20>( fun, std::array{-1., 2 * pi + 1}, std::array{-1., 2 * pi + 1},
                                                  std::array{-2., 2.} );

  for ( double x = grid.xMin<4>(); x < grid.xMax<4>(); x += 2 * pi / 10 )
    for ( double y = grid.yMin<4>(); y < grid.yMax<4>(); y += 2 * pi / 10 )
      for ( double z = grid.zMin<4>(); z < grid.zMax<4>(); z += 0.8 ) {
        auto f = fun( x, y, z );
        auto a = grid.splineInterpolate<3, 4>( x, y, z );
        std::cout << "f( " << x << ", " << y << ", " << z << " ) = " << f << " " << a << '\n';
      }
}
#endif
