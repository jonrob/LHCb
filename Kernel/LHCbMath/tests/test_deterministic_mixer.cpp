/*****************************************************************************\
* (c) Copyright 2000-2023 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE example
#include "LHCbMath/DeterministicMixer.h"
#include <boost/test/unit_test.hpp>
using namespace std::literals;

// original code to check against...
/** @file LHCbMath/DeterministicPrescalerGenerator.h
 *
 *  Quasi-random generator for DeterministicPrescaler
 */

#include "Kernel/STLExtensions.h"
#include <algorithm>
#include <cstdint>
#include <string>

namespace {

  uint32_t mix( uint32_t state ) {
    // note: the constants below are _not_ arbitrary, but are picked
    //       carefully such that the bit shuffling has a large 'avalanche' effect...
    //       See https://web.archive.org/web/20130918055434/http://bretm.home.comcast.net/~bretm/hash/
    //
    // note: as a result, you might call this a quasi-random (not to be confused
    //       with psuedo-random!) number generator, in that it generates an output
    //       which satisfies a requirement on the uniformity of its output distribution.
    //       (and not on the predictability of the next number in the sequence,
    //       based on knowledge of the preceding numbers)
    //
    // note: another way to look at this is is as an (approximation of an) evaporating
    //       black hole: whatever you dump in to it, you get something uniformly
    //       distributed back ;-)
    //
    state += ( state << 16 );
    state ^= ( state >> 13 );
    state += ( state << 4 );
    state ^= ( state >> 7 );
    state += ( state << 10 );
    state ^= ( state >> 5 );
    state += ( state << 8 );
    state ^= ( state >> 16 );
    return state;
  }

  // mix some 'extra' entropy into 'state' and return result
  uint32_t mix32( uint32_t state, uint32_t extra ) { return mix( state + extra ); }

  // mix some 'extra' entropy into 'state' and return result
  uint32_t mix4( uint32_t s, LHCb::span<const char, 4> a ) {
    // FIXME: this _might_ do something different on big endian vs. small endian machines...
    return mix32( s, uint32_t( a[0] ) | uint32_t( a[1] ) << 8 | uint32_t( a[2] ) << 16 | uint32_t( a[3] ) << 24 );
  }

  // mix some 'extra' entropy into 'state' and return result
  uint32_t mix64( uint32_t state, uint64_t extra ) {
    constexpr auto mask = ( ( ~uint64_t{0} ) >> 32 );
    state               = mix32( state, uint32_t( extra & mask ) );
    return mix32( state, uint32_t( ( extra >> 32 ) & mask ) );
  }

  // mix some 'extra' entropy into 'state' and return result
  uint32_t mixString( uint32_t state, std::string_view extra ) {
    // prefix extra with ' ' until the size is a multiple of 4.
    if ( auto rem = extra.size() % 4; rem != 0 ) {
      // prefix name with ' ' until the size is a multiple of 4.
      std::array<char, 4> prefix = {' ', ' ', ' ', ' '};
      std::copy_n( extra.data(), rem, std::next( prefix.data(), ( 4 - rem ) ) );
      state = mix4( state, prefix );
      extra.remove_prefix( rem );
    }
    for ( ; !extra.empty(); extra.remove_prefix( 4 ) )
      state = mix4( state, LHCb::span<const char, 4>{extra.substr( 0, 4 )} );
    return state;
  }

} // namespace

BOOST_AUTO_TEST_CASE( test_deterministic_mixer ) {

  auto mixer = LHCb::DeterministicMixer{};
  BOOST_CHECK_EQUAL( mixer.state, 0 );

  mixer = LHCb::DeterministicMixer{"Hello"};
  BOOST_CHECK_EQUAL( mixer.state, 12607005 );
  mixer( "World" );

  BOOST_CHECK_EQUAL( mixer.state, 2573559619 );

  mixer( 1234 );
  BOOST_CHECK_EQUAL( mixer.state, 4260688599 );

  mixer( 999ull );
  BOOST_CHECK_EQUAL( mixer.state, 2948039857 );

  auto m2 = mixer;
  auto m3 = mixer;

  // longlong will do something distinct from int...
  mixer( 1001ll );
  BOOST_CHECK_EQUAL( mixer.state, 2142563391 );

  // but int and unsigned should do the same (for positive values)
  m2( 1001 );
  BOOST_CHECK_EQUAL( m2.state, 127254048 );
  m3( 1001u );
  BOOST_CHECK_EQUAL( m3.state, m2.state );

  // check against 'old code'
  std::string s = "The way to get started is to quit talking and begin doing";
  auto        o = mix32( mix64( mix32( mixString( s.size(), s ), 99 ), 101ll ), 456 );
  auto        n = LHCb::DeterministicMixer{s}( 99 )( 101ll )( 456 ).state;
  BOOST_CHECK_EQUAL( o, n );

  // check padding of strings
  auto e  = LHCb::DeterministicMixer{mixer}( "a" ).state;
  auto s4 = LHCb::DeterministicMixer{mixer}( "   a" ).state;
  BOOST_CHECK_EQUAL( e, s4 );

  auto cc = LHCb::DeterministicMixer{mixer}( "" )( "a" )( "bb" )( "ccc" )( "dddd" )( "eeeee" )( "ffffff" ).state;
  auto sv = LHCb::DeterministicMixer{mixer}( ""sv )( "a"sv )( "bb"sv )( "ccc"sv )( "dddd"sv )( "eeeee"sv )( "ffffff"sv )
                .state;
  auto st = LHCb::DeterministicMixer{mixer}( ""s )( "a"s )( "bb"s )( "ccc"s )( "dddd"s )( "eeeee"s )( "ffffff"s ).state;
  BOOST_CHECK_EQUAL( cc, sv );
  BOOST_CHECK_EQUAL( cc, st );

  // check passing of multiple arguments at once
  auto si = LHCb::DeterministicMixer{mixer}( "HelloWorld" )( 1234 )( 1ll )( 99u );
  auto mi = LHCb::DeterministicMixer{mixer}( "HelloWorld", 1234, 1ll, 99u );
  BOOST_CHECK_EQUAL( si.state, mi.state );
}
