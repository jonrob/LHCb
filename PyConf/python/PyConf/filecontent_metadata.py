###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import os
import json
import re
import time
import contextlib
import subprocess
import logging
from .tonic import configurable
from .utils import load_file
from . import ConfigurationError

log = logging.getLogger(__name__)

FILE_CONTENT_METADATA = "/cvmfs/lhcb-condb.cern.ch/git-conddb/file-content-metadata.git"


def __generate_key_zero(dct):
    _convert = lambda cfg, keys : json.dumps({ key: { v: k for k, v in cfg[key].items() } for key in keys.intersection( cfg.keys() ) })
    return {
        0:
        _convert(
            dct, {
                'SpruceSelectionID', 'Hlt2SelectionID', 'Hlt1SelectionID',
                'PackedObjectLocations'
            })
    }


def get_keys_from_jsonfile(json_file):
    # returns { int -> json }
    json_file_contents = load_file(json_file)
    if not json_file_contents: return dict()
    dct = json.loads(json_file_contents)
    keys = {
        int(k, 16): json.dumps(v)
        for k, v in dct.get("DecodingKeys", dict()).items()
    }
    old_ann_config = dct.get("HltANNSvc/HltANNSvc", None)
    if old_ann_config: keys.update(__generate_key_zero(old_ann_config))
    return keys


def update_decoding_keys_in_json_file(fname, d, **kwargs):
    try:
        with open(fname, '+x') as f:
            json.dump({'DecodingKeys': d}, f, **kwargs)
    except OSError:
        with _lockfile(fname + ".PyConf-lock"):
            with open(fname, 'rt', encoding='utf-8') as f:
                txt = f.read()
            prev = json.loads(txt) if len(txt) > 1 else dict(
            )  # json has at least '{}' as content
            prev_keys = prev.get('DecodingKeys', dict())
            if any(prev_keys[k] != d[k] for k in prev_keys.keys() & d.keys()):
                raise RuntimeError(
                    'key collision while merging dictionaries: {} vs {}'.
                    format(prev_keys, d))
            prev_keys.update(d)
            prev['DecodingKeys'] = prev_keys
            with open(fname, 'wt', encoding='utf-8') as f:
                json.dump(prev, f, **kwargs)
    # check that no keys were lost...
    with open(fname, 'rt', encoding='utf-8') as f:
        assert set(d.keys()).issubset(json.load(f)['DecodingKeys'].keys())


def _flush_keys_to_git(key_and_tables, directory):
    repos = metainfo_repos()
    path = repos[0][0]
    with _lockfile(os.path.join(path, 'PyConf.lock')):
        for key, table in key_and_tables:
            log.info('*** flush_registry_to_git: checking key: {}'.format(key))
            # check top backend after acquiring lock
            # -- only need to check first entry, assuming the other repos have not changed,
            #    and previously it wasn't there, because otherwise it wouldn't be in the registry...
            d = _retrieve_from_repos(key, {repos[0]}, directory=directory)
            if d is not None:
                assert _dict_from_json(d) == _dict_from_json(
                    table
                ), "At this point, the content associated to the key should match - but it does not... {} vs {}".format(
                    _dict_from_json(d), _dict_from_json(table))
                continue

            log.info("Adding key {} to git repository at {}".format(key, path))

            oid = _get_hash_for_text(table, write_to_odb=True, path=path)
            if oid[:8] != key:
                raise RuntimeError(
                    "inconsistent hash computation????? key = {}, oid = {}, text = {}"
                    .format(key, oid, table))
            branch_name = 'key-' + key
            _commit_oid_to_branch(path, 'master', branch_name, oid, directory)

            log.warning(
                'Created new key {0} - to publish the corresponding decoding table, please do `git -C {1} push origin {2}`'
                .format(key, path, branch_name))

            # to make sure we can continue, merge the just created branch into master,
            _merge_branch_to_master(path, branch_name)

            # check that we can read back the key _from master
            rev_and_name = 'master:{}'.format(_key_to_file(key, directory))
            result = subprocess.run(
                ['git', '--git-dir', path, 'show', rev_and_name],
                capture_output=True,
                text=True,
                check=False)
            if result.returncode != 0:
                raise RuntimeError('failed to retrieve {} from {}  {}'.format(
                    rev_and_name, path, result.stderr))
            if table != result.stdout.strip():
                raise RuntimeError('readback not identical to input???')


#TODO: make this class more like a cache backed by git.
#      - 'find' should fall-back to git, and 'add' to registry if found
#      - and 'add' should add some attribute to the key to see if is is present in git or not
#      - and there should be a way of getting all keys known here, but not backed-up in git
class __KeyRegistry(object):
    def __init__(self, directory):
        self.__d = dict()
        self.__locked = False
        self.__generated_keys = []
        self.__directory = directory

    def __del__(self):
        if not self.__locked and self.__generated_keys:
            log.error(
                "OOPS -- registry not locked, with newly generated keys {} -- someone forgot to flush"
                .format(self.__generated_keys))

    @staticmethod
    def valid_key(key):
        return '{:08x}'.format(int(key, 16)) == key

    def add(self, key, text, generated_key=False):
        assert self.valid_key(key)
        i = self.__d.get(key, None)
        if i is not None:  # already registered
            if i == text: return  # same, so done
            raise RuntimeError(
                "cannot modify registered item for key {}".format(key))
        if self.__locked:
            raise RuntimeError("trying to add new key to a locked registry")
        log.info('adding {}key {} to registry'.format(
            "newly generated " if generated_key else "", key))
        self.__d.update({key: text})
        if generated_key: self.__generated_keys += [key]

    def find(self, key):
        assert self.valid_key(key)
        return self.__d.get(key, None)

    def keys(self):
        return self.__d.keys()

    def items(self):
        return self.__d.items()

    def empty(self):
        return len(self.__d) == 0

    def lock(self):
        self.__locked = True

    def add_keys_from_jsonfile(self, fname):
        keys = get_keys_from_jsonfile(fname)  # this returns { int -> json }
        # check for collisions...
        if any(self.__d[k] != keys[k] for k in self.__d.keys() & keys.keys()):
            raise RuntimeError(
                'key collision while merging dictionaries: {} vs {}'.format(
                    self.__d, keys))
        for k, v in keys.items():
            self.add('{:08x}'.format(k), v)

    def append_to_jsonfile(self, fname, **kwargs):
        self.lock()  # prevent future updates that would not be flushed...
        update_decoding_keys_in_json_file(
            fname,
            {'0x{}'.format(k): json.loads(v)
             for k, v in self.__d.items()}, **kwargs)

    def flush_to_git(self):
        self.lock()  # prevent future updates that would not be flushed...
        if not self.empty():
            _flush_keys_to_git(
                filter(lambda x: x[0] != '00000000', self.items()),
                self.__directory)


key_registries = {}
key_registries["ann"] = __KeyRegistry("ann")
key_registries["luminosity_counters"] = __KeyRegistry("luminosity_counters")

key_registry = key_registries[
    "ann"]  #maintain compatibility with PyConf.application


@contextlib.contextmanager
def _cwd(d):
    cwd = os.getcwd()
    os.chdir(d)
    try:
        yield
    finally:
        os.chdir(cwd)


@contextlib.contextmanager
def flush_key_registry():
    try:
        yield
    finally:
        for key_registry in key_registries.values():
            key_registry.flush_to_git()


@contextlib.contextmanager
def _lockfile(name, attempts=120, sleeptime=1):
    for i in range(attempts):
        try:
            # open with  O_EXCL should be atomic for Linux > 2.6.5 and NFS v3... (see http://nfs.sourceforge.net/#faq_d10)
            log.debug('about to try and lock {}'.format(name))
            with open(name, '+x') as f:
                f.write('locked by pid = {}, host = {}'.format(
                    os.getpid(),
                    os.uname()[1]))
                f.flush()
                try:
                    yield
                finally:
                    os.remove(name)
            return
        except OSError:
            log.info('waiting in order to acquire lock {}'.format(name))
            # time.sleep(min(1.4**i,20))
            time.sleep(sleeptime)
    raise RuntimeError(
        "failed to acquire lockfile {} after {} attempts".format(
            name, attempts))


def _is_repo(path):
    return subprocess.run(
        ['git', '--git-dir', path, 'rev-parse', '--is-repository'],
        stdout=subprocess.DEVNULL,
        stderr=subprocess.DEVNULL,
        check=False).returncode == 0


def _branch_exists(path, branch_name):
    result = subprocess.run([
        'git', '--git-dir', path, 'rev-parse', '--quiet', '--verify',
        branch_name
    ],
                            check=False)
    return result.returncode == 0


def _convert_keys_from_json(d):
    # convert keys from strings (which is what JSON requires) back to integers
    for k in filter( lambda x : re.match( '((Hlt(1|2)|Spruce)Selection|Info)ID|PackedObjectLocations', x), d.keys() ):
        d[k] = {int(i): j for i, j in d[k].items()}
    return d


def _dict_from_json(s):
    return _convert_keys_from_json(json.loads(s)) if s else None


def _get_commit_for_branch(path, branch):
    result = subprocess.run(
        ['git', '--git-dir', path, 'rev-parse', '--revs-only', branch],
        capture_output=True,
        check=False,
        text=True)
    if result.returncode != 0:
        log.error('{} failed; stdout={}, stderr={} rc={}'.format(
            ' '.join(
                ['git', '--git-dir', path, 'rev-parse', '--revs-only',
                 branch]), result.stdout, result.stderr, result.returncode))

    sha1 = result.stdout.strip()
    assert len(sha1) == 40, 'expect valid SHA1 - got {}'.format(sha1)
    return sha1


def _get_hash_for_text(txt, write_to_odb=False, path=None):
    if write_to_odb and path is None:
        raise RuntimeError("write_to_odb is True, but no path given")
    cmd = ['git'] + (['--git-dir', path] if write_to_odb else []) + [
        'hash-object'
    ] + (['-w'] if write_to_odb else []) + ['--stdin']
    oid = subprocess.run(
        cmd, text=True, capture_output=True, input=txt,
        check=True).stdout.strip()
    assert len(oid) == 40, "invalid oid: {}".format(oid)
    return oid


def _commit_oid_to_branch(path, parent, branch_name, oid, directory):
    key = oid[:8]
    commit = _get_commit_for_branch(path, parent)
    with _cwd(path.removesuffix('/.git')):
        # check that the target branch does _not_ exist
        if _branch_exists(path, branch_name):
            log.warning(
                "requested branch {} already exists -- should check whether key is valid, but for now just returning"
                .format(branch_name))
            return

        # make sure we are in a detached head by checking out an explicit commit
        result = subprocess.run(
            ['git', '--git-dir', path, 'checkout', '-b', branch_name, commit],
            capture_output=True,
            text=True,
            check=False)
        if result.returncode != 0:
            log.error(
                "Failed to checkout new branch {} on top of commit {}: {}".
                format(branch_name, commit, result.stderr))
            raise RuntimeError(
                "could not create branch {} ".format(branch_name))
        # grab the content of the specified oid and create the corresponding file
        result = subprocess.run(
            ['git', '--git-dir', path, 'cat-file', '-p', oid],
            capture_output=True,
            text=True,
            check=False)
        if result.returncode != 0:
            raise RuntimeError("could not get content for oid {} ".format(oid))
        fname = _key_to_file(key, directory)
        os.makedirs(os.path.dirname(fname), exist_ok=True)
        with open(fname, 'w') as f:
            f.write(result.stdout)
        subprocess.run(['git', '--git-dir', path, 'add', fname],
                       capture_output=True,
                       check=True)
        result = subprocess.run([
            'git', '--git-dir', path, 'commit', '-m',
            'automated commit to define key {}'.format(key), fname
        ],
                                capture_output=True,
                                check=False)
        if result.returncode != 0:
            # we got this far, so probably the commit failed because of 'lack of identity'
            # -- this happens eg. during ci-tests
            # so try again, with an explicitly specified dummy identity
            log.warning(
                'git commit failed -- retrying with explicit unknown identity')
            subprocess.run([
                'git', '-c', 'user.name=unknown', '-c',
                'user.email=noreply@cern.ch', '--git-dir', path, 'commit',
                '-m', 'automated commit to define key {}'.format(key), fname
            ],
                           capture_output=True,
                           check=True)


def _default_metainfo_repo():
    repo = os.environ.get('LHCbFileContentMetaDataRepo', None)
    if repo:
        if not _is_repo(repo):
            raise RuntimeError(
                '$LHCbFileContentMetaDataRepo is set to {}, but there is no valid git repository there'
                .format(repo))
        return repo

    cwd = os.getcwd()
    repo = "{}/lhcb-metainfo/.git".format(cwd)
    if _is_repo(repo): return repo

    with _lockfile('PyConf-create-lhcb-metadata-git-repo.lock'):
        if _is_repo(repo): return repo
        log.warning('*** cloning metadata git repo into {}'.format(repo))
        result = subprocess.run([
            'git', 'clone', '-q', FILE_CONTENT_METADATA,
            repo.removesuffix('/.git')
        ],
                                capture_output=True,
                                text=True,
                                check=False)
        if result.returncode == 0: return repo
        log.warning('*** failed to clone metadata git repo into {}: {}'.format(
            repo, result.stderr))
        return None


def _key_to_file(key, directory):
    if not isinstance(key, str): key = '{:08x}'.format(key)
    return os.path.join(directory, "json/{0:.2}/{0}.json".format(key))


def _make_key(cfg, directory):
    if directory not in key_registries:
        log.info("*** creating KeyRegistry for directory {}".format(directory))
        key_registries[directory] = __KeyRegistry(directory)
    table = json.dumps(cfg)
    while True:
        key = _get_hash_for_text(table)[:8]

        # check whether the key is somewhere in the _stack_ of repos
        d = retrieve_encoding_dictionary(key, directory=directory)
        if d is None:
            key_registries[directory].add(
                key, table, generated_key=True
            )  # queue for writing -- note: currently, we check, but do not resolve collision inside the queue... if that happens (VERY unlikely), we abort...
            return key
        if d == cfg:
            # to make sure that, when writing keys to the manifest, _all_ keys encountered are written
            # also pre-exisiting keys are registered at this point.
            #
            # This eg. avoids the scenario that in a first run, a key is (still) new and would be
            # added, but then in any subsequent run the key is already in the (local) git repository,
            # thus it is no longer new, and is thus subsequently _not_ added to the manifest...
            #  -- which implies that the key has to be published to the global reco if someone else
            #     wants to process the data, or the local repo must be made configured for use by the
            #     job processing the data.
            key_registries[directory].add(key, table)
            return key

        # we've got a hash collision - add a space to get a different key, and try again
        #  NOTE: as generating a hash collision 'on purpose' is a bit difficult, this code
        #        has not been tested - it should work from first principles, provided I've
        #        not overlooked anything... but that we will probably only find out the first
        #        time this happens in practice...
        log.warning(
            "****  hash collision for {} -- tweaking string rep of table".
            format(key))
        # whitespace characters are not significant in json outside of quoted string
        # so we can always append a whitespace without changing the payload contained in the
        # JSON representation
        table += ' '


def _merge_branch_to_master(path, branch_name):
    with _cwd(path.removesuffix('/.git')):
        subprocess.run(['git', '--git-dir', path, 'checkout', 'master'],
                       capture_output=True,
                       check=True)
        subprocess.run(['git', '--git-dir', path, 'reset', '--hard'],
                       capture_output=True,
                       check=True)
        result = subprocess.run(
            ['git', '--git-dir', path, 'merge', '--no-edit', branch_name],
            capture_output=True,
            check=False)
        if result.returncode != 0:
            log.warning(
                'git merge failed -- retrying with explicit unknown identity')
            subprocess.run([
                'git', '-c', 'user.name=unknown', '-c',
                'user.email=noreply@cern.ch', '--git-dir', path, 'merge',
                '--no-edit', branch_name
            ],
                           capture_output=True,
                           check=True)


@configurable
def metainfo_repos(repos=None, extra_central_tags=[]):
    # make sure the _default_metainfo_repos is the first entry (if it exists)
    if repos is None:
        def_repo = _default_metainfo_repo()
        repos = [(def_repo, 'master')] if def_repo else []
    # make sure the /cvmfs repo and its requested tags are present (and last)
    for tag in extra_central_tags + ['master']:
        repos.append((FILE_CONTENT_METADATA, tag))
    #  remove potential duplicates while preserving order (python3.7 and later!)
    repos = list(dict.fromkeys(repos))
    assert (all(r.endswith('.git')
                for r, _ in repos)), ' no .git at end: {}'.format(repos)
    return repos


def _cvmfs_metainfo_repos():
    repos = list(
        filter(lambda x: x[0] == FILE_CONTENT_METADATA, metainfo_repos()))
    return repos


def _retrieve_from_repos(key, repos, directory):
    for path, tag in repos:
        result = subprocess.run([
            'git', '--git-dir', path, 'show', '{0}:{1}'.format(
                tag, _key_to_file(key, directory))
        ],
                                text=True,
                                capture_output=True)
        if result.returncode == 0:
            return result.stdout
    return None


@configurable
def retrieve_encoding_dictionary(key,
                                 require_key_present=False,
                                 directory="ann"):
    if directory not in key_registries:
        log.info("*** creating KeyRegistry for directory {}".format(directory))
        key_registries[directory] = __KeyRegistry(directory)
    d = _retrieve_from_repos(key, metainfo_repos(), directory=directory)
    if d is None:
        if (require_key_present):
            message = f' key {key} should have been present in the following repositories and branches \n'
            for repo in metainfo_repos():
                message = message + f"{repo[0]} in branch {repo[1]} \n"
            raise ConfigurationError(message)
        d = key_registries[directory].find(
            key)  # check the key_registry as last resort
    return _dict_from_json(d)


def generate_encoding_dictionary(label, cfg):
    # cfg must be 'just a bunch of strings'
    assert isinstance(cfg, list) and all(isinstance(i, str)
                                         for i in cfg), '{}'.format(cfg)

    ### WARNING: if Hlt1, do NOT change the order...
    ### Allen produces a list, and will use the offset in the list (starting at 1)
    ### as the encoded value...
    if label == 'Hlt1SelectionID':
        assert len(set(cfg)) == len(cfg)
        return {
            label: {i: l
                    for i, l in enumerate(cfg, start=1)},
            "version": "0"
        }
    else:
        return {
            label: {i: loc
                    for i, loc in enumerate(sorted(set(cfg)), start=1)},
            "version": "0"
        }


def register_encoding_dictionary(label, cfg, directory="ann"):
    """Create and register encoding dictionary, returning the key of dictionary
    """
    log.debug("****  register_encoding_dictionary ****")
    # either cfg is already a dict in which `label` is a valid key,
    # or it is 'just a bunch of strings' and we have to make the dict...
    if not isinstance(cfg, dict):
        cfg = generate_encoding_dictionary(label, cfg)

    assert isinstance(cfg, dict)
    assert label in cfg.keys()
    assert "version" in cfg.keys()

    return _make_key(cfg, directory)
