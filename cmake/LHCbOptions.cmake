###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#[========================================================================[.rst:
LHCbOptions.cmake
-----------------

Definition of compile time options for LHCb.
#]========================================================================]

option(USE_DD4HEP "Controls whether DD4hep is used for the subdetectors supporting it" ON)
option(USE_TORCH "Use pyTorch libraries" ON)

# List of options to record for downstream projects
list(APPEND LHCb_PERSISTENT_OPTIONS
    USE_DD4HEP
    USE_TORCH
)
