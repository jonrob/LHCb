/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef CALOFUTUREDAQ_CALOFUTURE2DVIEW_H
#define CALOFUTUREDAQ_CALOFUTURE2DVIEW_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiHistoAlg.h"
// from LHCb
#include "CaloDet/DeCalorimeter.h"
#include "Detector/Calo/CaloCellID.h"
// from Event
#include "Event/CaloAdc.h"
#include "Event/CaloCluster.h"
#include "Event/CaloDigit.h"
#include "Event/MCCaloDigit.h"
#include "Event/MCCaloHit.h"
// from ROOT
#include <TH1.h>
#include <TH2.h>
#include <TProfile2D.h>

//==============================================================================

/** @class CaloFuture2Dview CaloFuture2Dview.h
 *
 *
 *  @author Olivier Deschamps
 *  @date   2008-01-18
 */
class CaloFuture2Dview : public GaudiHistoAlg {
public:
  /// Standard constructor
  CaloFuture2Dview( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode initialize() override; ///< Algorithm initialization

  void bookCaloFuture2D( const HistoID& unit, const std::string title, std::string name, int area = -1 ) const;
  void bookCaloFuture2D( const HistoID& unit, const std::string title, LHCb::Detector::Calo::CellCode::Index calo,
                         int area = -1 ) const;
  void fillCaloFuture2D( const HistoID& unit, const LHCb::Detector::Calo::CellID& id, double value,
                         const DeCalorimeter& calo, const std::string title = "" ) const;

  bool          split() const { return m_split; }
  const HistoID getUnit( const HistoID& unit, int calo, int area ) const;

  // Container of the subdetector info
  struct CaloFutureParams {
    int                                       nChan;
    int                                       centre;
    int                                       reg;
    int                                       fCard;
    int                                       lCard;
    std::vector<LHCb::Detector::Calo::CellID> refCell;
    std::vector<float>                        cellSizes;
  };

protected:
  Gaudi::Property<bool>  m_1d{this, "OneDimension", false}; // 1D histo (value versus CaloCellID) default is 2D view
  Gaudi::Property<bool>  m_profile{this, "Profile", false}; // 1D histo profile type ?
  Gaudi::Property<int>   m_bin1d{this, "Bin1D", 16384};     // 1D histo binning (default : full 14 bits dynamics)
  Gaudi::Property<bool>  m_geo{this, "GeometricalView",
                              true}; // 2D :  true : geometrical (x,y) | false : readout (FEB,channel)
  Gaudi::Property<float> m_threshold{this, "Threshold", -256.}; // threshold on the input value (bin weight)
  Gaudi::Property<float> m_offset{this, "Offset", 0.0};         // bin weight =  (value + offset)
  Gaudi::Property<bool>  m_dim{this, "ActualSize", true};    // true: (x,y) | false : (col,row) (geometrical view only)
  Gaudi::Property<bool>  m_l0{this, "L0ClusterView", false}; // bin weight on 2x2 cluster (geometrical view only)
  Gaudi::Property<bool>  m_energyWeighted{this, "EnergyWeighted", true}; // true : bin weight = energy | false :  1
  Gaudi::Property<bool>  m_flux{this, "Flux", false}; // bin weight is normalized to the cell area (for both views)
  Gaudi::Property<bool>  m_split{this, "SplitAreas", false}; // produce one view per area (for geometrical view only)
  Gaudi::Property<std::string> m_lab{this, "xLabelOptions", "v"}; // Crate-FEB text label on Xaxis (readout view only)
  Gaudi::Property<std::string> m_prof{this, "ProfileError", ""};
  mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_id_error{this, "Cell ID and Calo ID mismatch, skipping cell"};

private:
  std::string getTitle( std::string title, int calo, int area ) const;
  // Container of 4 subdectector params
  std::array<CaloFutureParams, 4>                                  m_caloParams;
  mutable std::map<HistoID, LHCb::Detector::Calo::CellCode::Index> m_caloViewMap;
};
#endif // CALOFUTUREDAQ_CALOFUTURE2DVIEW_H
