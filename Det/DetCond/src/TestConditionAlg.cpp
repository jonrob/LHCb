/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files
#include "DetDesc/Condition.h"

#include "GaudiAlg/FixTESPath.h"
#include "GaudiKernel/Algorithm.h"
#include "GaudiKernel/IDetDataSvc.h"
#include "GaudiKernel/IUpdateManagerSvc.h"
#include "GaudiKernel/Map.h"

#include <vector>

namespace DetCondTest {

  /** @class TestConditionAlg TestConditionAlg.h component/TestConditionAlg.h
   *
   *  Simple algorithm that prints the requested conditions at every event.
   *
   *  @author Marco CLEMENCIC
   *  @date   2008-06-27
   */
  class TestConditionAlg : public Gaudi::Algorithm {
  public:
    /// Standard constructor
    using Algorithm::Algorithm;

    StatusCode initialize() override;                         ///< Algorithm initialization
    StatusCode execute( const EventContext& ) const override; ///< Algorithm execution
    StatusCode finalize() override;                           ///< Algorithm finalization

  protected:
    void i_dump() const;

    ServiceHandle<IUpdateManagerSvc> m_updMgrSvc{this, "UpdateManagerSvc", "UpdateManagerSvc"};

    Gaudi::Property<std::vector<std::string>> m_condPaths{
        this, "Conditions", {}, "list of paths to conditions in the detector transient store"};
    Gaudi::Property<bool> m_loadAtInit{this, "LoadDuringInitialize", false,
                                       "load the requested conditions already during the initialization"};

    /// Container of the conditions to print
    GaudiUtils::Map<std::string, Condition*> m_conditions;

    struct CondUpdateMonitor {
      virtual ~CondUpdateMonitor() = default;
      CondUpdateMonitor( MsgStream& _log, std::string _path ) : log{_log}, path{std::move( _path )} {}
      virtual StatusCode handler() {
        log << MSG::INFO << path << " was updated" << endmsg;
        return StatusCode::SUCCESS;
      }
      MsgStream&  log;
      std::string path;
    };
    std::vector<CondUpdateMonitor> m_updateMonitors;
  };

  StatusCode TestConditionAlg::initialize() {
    return Algorithm::initialize().andThen( [&]() -> StatusCode {
      m_updateMonitors.reserve( m_condPaths.size() );
      for ( const auto& path : m_condPaths ) {
        // C++ 17 version
        auto mon = &m_updateMonitors.emplace_back( msgStream(), path );
        m_updMgrSvc->registerCondition( mon, path, &CondUpdateMonitor::handler, m_conditions[path] );
        m_updMgrSvc->registerCondition<TestConditionAlg>( this, mon );
      }

      if ( m_loadAtInit ) {
        return m_updMgrSvc->update( this ).andThen( [&]() {
          info() << "Conditions loaded at initialize" << endmsg;
          i_dump();
          return StatusCode::SUCCESS;
        } );
      }
      return StatusCode::SUCCESS;
    } );
  }

  StatusCode TestConditionAlg::execute( const EventContext& ) const {
    i_dump();
    return StatusCode::SUCCESS;
  }

  StatusCode TestConditionAlg::finalize() {
    m_conditions.clear();
    m_condPaths.clear();
    return Algorithm::finalize(); // must be called after all other actions
  }

  void TestConditionAlg::i_dump() const {
    info() << "Requested Conditions:\n";
    for ( const auto& item : m_conditions ) { info() << "--- " << item.first << "\n" << *( item.second ) << "\n"; }
    info() << endmsg;
  }

  //=============================================================================

  /** Small algorithm that runs a fake event loop during finalize to scan the
   *  values of the conditions (see bug #74255).
   *
   *  @author Marco CLEMENCIC
   *  @date   2010-10-25
   */
  class FinalizationEvtLoop : public Gaudi::Algorithm {
  public:
    using Algorithm::Algorithm;

    StatusCode initialize() override {
      return Algorithm::initialize().andThen( [&]() {
        for ( const auto& path : m_condPaths ) {
          m_updMgrSvc->registerCondition( this, path, NULL, m_conditions[path] );
        }
        return StatusCode::SUCCESS;
      } );
    }

    StatusCode execute( const EventContext& ) const override { return StatusCode::SUCCESS; }

    StatusCode finalize() override {
      Gaudi::Time           t( m_initTime );
      const Gaudi::Time     fin( m_finalTime );
      const Gaudi::TimeSpan step( m_step );

      SmartIF<IDetDataSvc> dds( detSvc() );
      for ( ; t < fin; t += step ) {
        dds->setEventTime( t );
        info() << "Update for event time " << t << endmsg;
        if ( m_updMgrSvc->newEvent().isSuccess() ) {
          info() << "Requested Conditions:\n";
          GaudiUtils::Map<std::string, Condition*>::iterator it;
          for ( it = m_conditions.begin(); it != m_conditions.end(); ++it ) {
            info() << "--- " << it->first << "\n" << *( it->second ) << "\n";
          }
          info() << endmsg;
        } else {
          error() << "Failure updating" << endmsg;
          return StatusCode::FAILURE;
        }
      }
      return StatusCode::SUCCESS;
    }

  private:
    ServiceHandle<IUpdateManagerSvc> m_updMgrSvc{this, "UpdateManagerSvc", "UpdateManagerSvc"};
    // default value is 1970-01-01 00:00:00UTC
    Gaudi::Property<Gaudi::Time::ValueType> m_initTime{this, "InitialTime", 0,
                                                       "First event time of the fake event loop"};
    // init + 10s by default
    Gaudi::Property<Gaudi::Time::ValueType> m_finalTime{this,
                                                        "FinalTime",
                                                        10000000000LL,
                                                        [this]( auto& ) { m_finalTime += m_initTime; },
                                                        Gaudi::Details::Property::ImmediatelyInvokeHandler{true},
                                                        "Final time of the loop"};
    // 1s step by default
    Gaudi::Property<Gaudi::Time::ValueType> m_step{this, "Step", 1000000000LL, "Step of the loop"};

    /// Names of the conditions to print
    Gaudi::Property<std::vector<std::string>> m_condPaths{
        this, "Conditions", {}, "list of paths to conditions in the detector transient store"};

    /// Container of the conditions to print
    GaudiUtils::Map<std::string, Condition*> m_conditions;
  };

  /** Test algorithm that triggers the bug #80076
   *  https://savannah.cern.ch/bugs/?80076
   */
  class bug_80076 : public FixTESPath<TestConditionAlg> {
  public:
    using FixTESPath::FixTESPath;

    /// Override the initialize to ensure that the conditions are already loaded
    /// during the initialize.
    StatusCode initialize() override {
      return TestConditionAlg::initialize().andThen( [&]() {
        std::vector<std::string>::const_iterator path;
        for ( path = m_condPaths.begin(); path != m_condPaths.end(); ++path ) {
          // this ensures that the objects are loaded in the transient store
          DataObject* obj = nullptr;
          detSvc()->retrieveObject( this->fullTESLocation( *path, true ), obj ).ignore();
        }
        return StatusCode::SUCCESS;
      } );
    }
  };

} // namespace DetCondTest

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( DetCondTest::TestConditionAlg )
DECLARE_COMPONENT( DetCondTest::FinalizationEvtLoop )
DECLARE_COMPONENT( DetCondTest::bug_80076 )
