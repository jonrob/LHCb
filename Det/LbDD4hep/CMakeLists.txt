###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
################################################################################
# Package: LbDD4hep
################################################################################

if(USE_DD4HEP)
    gaudi_add_header_only_library(LbDD4hepLib
        LINK
            Boost::headers
            DD4hep::DDCond
            DD4hep::DDCore
            Detector::DetectorLib
            Gaudi::GaudiAlgLib
            Gaudi::GaudiKernel
            LHCb::LHCbKernel
            nlohmann_json::nlohmann_json
            yaml-cpp
    )

    if(DD4hep_ROOT)
        lhcb_env(SET DD4hepINSTALL "${DD4hep_ROOT}")
    endif()

    gaudi_add_module(LbDD4hep
        SOURCES
            src/DD4hepSvc.cpp
            src/IOVProducer.cpp
            src/TGeoTransportSvc.cpp
            src/DetectorTests.cpp
            src/AlignmentTestAlgs.cpp
        LINK
            DD4hep::DDCore
            Detector::DetectorLib
            Gaudi::GaudiKernel
            LHCb::LHCbAlgsLib
            LHCb::DAQEventLib
            LHCb::LbDD4hepLib
            ROOT::Core
            ROOT::Geom
            yaml-cpp
    )

    gaudi_add_tests(QMTest)
    gaudi_add_pytest(tests/pytest)

    set_property(
        TEST
            LbDD4hep.dd4hep.load
        APPEND PROPERTY
            ENVIRONMENT DETECTOR_PROJECT_ROOT=${DETECTOR_PROJECT_ROOT}
    )
endif()
