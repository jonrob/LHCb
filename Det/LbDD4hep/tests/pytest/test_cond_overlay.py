###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import os
import re
from pathlib import Path
from subprocess import PIPE

import yaml

from GaudiTests import run_gaudi


def config():
    # FIXME: this should be an argument to the function, but it's not yet possible
    #        to invoke `gaudirun.py "module:function('some_value')"`
    overlay_root = os.environ["TEST_OVERLAY_ROOT"]

    from Configurables import (
        ApplicationMgr,
        DDDBConf,
        EventSelector,
        LHCbApp,
        LHCb__Det__LbDD4hep__DD4hepSvc as DD4hepSvc,
        LHCb__Det__LbDD4hep__IOVProducer as IOVProducer,
        LHCb__Tests__FakeRunNumberProducer as FRNP,
        LHCb__Det__LbDD4hep__Tests__Alignment__UpdateTest as AlignTest,
    )

    # Event numbers
    nEvents = 3
    EventSelector().PrintFreq = 1

    # Just to initialise
    DDDBConf()
    LHCbApp()

    # Only load the detector we want
    DD4hepSvc(
        DetectorList=["/world", "VP"],
        ConditionsOverlayInitPath=overlay_root,
    )

    # Finally set up the application
    app = ApplicationMgr(
        EvtSel="NONE",
        EvtMax=nEvents,
        ExtSvc=["ToolSvc", "AuditorSvc"],
        AuditAlgorithms=True,
    )

    odin_path = "/Event/DummyODIN"
    app.TopAlg = [
        FRNP("FakeRunNumber", ODIN=odin_path, Start=1000, Step=20),
        IOVProducer("ReserveIOVDD4hep", ODIN=odin_path),
        AlignTest(
            "AlignTest",
            OverlayRoot=overlay_root,
        ),
    ]


GLOBAL_ALIGN_TPL = """---
VPSystem: !alignment
  position: [0.0 * mm, 5.0 * mm, 0.0 * mm]
VPLeft: !alignment
  position: [1.0 * mm, -2.0 * mm, 0.0 * mm]
VPRight: !alignment
  position: [-1.0 * mm, -2.0 * mm, 0.0 * mm]
"""


def test(tmp_path: Path):
    """
    Test loading of DD4Hep detector elements.
    """
    # create the initial alignment values
    global_align = tmp_path / "Conditions" / "VP" / "Alignment" / "Global.yml"
    global_align.parent.mkdir(parents=True)
    global_align.write_text(GLOBAL_ALIGN_TPL)

    env = os.environ.copy()
    env["TEST_OVERLAY_ROOT"] = str(tmp_path.resolve())
    job = run_gaudi(
        f"{__file__}:config",
        check=True,
        env=env,
        stdout=PIPE,
        errors='replace',
    )

    # check that the alignments printed follow the expected pattern
    pattern = re.compile(
        r"^.*current alignment -> \[\((-?\d+),(-?\d+),(-?\d+)\)")
    alignments = [[int(i) for i in m.groups()]
                  for m in map(pattern.match, job.stdout.splitlines()) if m]
    assert alignments, "missing AlignTest 'current alignment' messages"

    assert alignments == [
        [0, 5, 0],
        [3, 6, 0],
        [-3, 7, 0],
    ], "wrong sequence of alignments"

    # check that we do not have spurious entries in the dump directory
    assert len(
        os.listdir(tmp_path / "Conditions" / "VP" /
                   "Alignment")) == 1, "wrong number of files in overlay dump"

    # check the content of the YAML dump
    class Alignment(yaml.YAMLObject):
        yaml_tag = u'!alignment'

        def __init__(self, position=None, rotation=None, pivot=None):
            self.position = position
            self.rotation = rotation
            self.pivot = pivot

    data = yaml.load(global_align.open(), Loader=yaml.Loader)
    # convert ["<n> * mm", ...] into numbers (rounding to make the comparison stable)
    pos = [
        round(eval(v, {}, {"mm": 1.}), 5) for v in data["VPSystem"].position
    ]
    assert pos == [9., 8., 0.]
