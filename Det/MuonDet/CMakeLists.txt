###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#[=======================================================================[.rst:
Det/MuonDet
-----------
#]=======================================================================]

if (USE_DD4HEP)
  set(BUILD_TESTING false) # prevent headers build test

  gaudi_add_library(MuonDetLib
    SOURCES
        src/Lib/MuonTilePosition.cpp
        src/Lib/MuonTilePositionUpgrade.cpp
        src/Lib/MuonTell40LinksMap.cpp 
    LINK
        PUBLIC
            Detector::DetectorLib
            LHCb::LHCbKernel
  )

else()

  gaudi_add_library(MuonDetLib
    SOURCES
        src/Lib/legacy/DeMuonChamber.cpp
        src/Lib/legacy/DeMuonDetector.cpp
        src/Lib/legacy/DeMuonRegion.cpp
        src/Lib/legacy/MuonChamberGrid.cpp
        src/Lib/legacy/MuonChamberLayout.cpp
        src/Lib/legacy/MuonDAQHelper.cpp
        src/Lib/legacy/MuonL1Board.cpp
        src/Lib/legacy/MuonNODEBoard.cpp
        src/Lib/legacy/MuonODEBoard.cpp
        src/Lib/legacy/MuonReadoutCond.cpp
        src/Lib/legacy/MuonStationCabling.cpp
        src/Lib/legacy/MuonTell40Board.cpp
        src/Lib/legacy/MuonTell40PCI.cpp
        src/Lib/legacy/MuonTSMap.cpp
        src/Lib/legacy/MuonUpgradeDAQHelper.cpp
        src/Lib/legacy/MuonUpgradeStationCabling.cpp
        src/Lib/legacy/MuonUpgradeTSMap.cpp
        src/Lib/MuonTell40LinksMap.cpp
        src/Lib/MuonTilePosition.cpp
        src/Lib/MuonTilePositionUpgrade.cpp
    LINK
        PUBLIC
            Boost::headers
            Gaudi::GaudiKernel
            LHCb::DetDescLib
            LHCb::LHCbKernel
  )

  gaudi_add_module(MuonDet
    SOURCES
        src/component/MuonDetFactories.cpp
        src/component/XmlMuonCablingCnv.cpp
        src/component/XmlMuonL1BoardCnv.cpp
        src/component/XmlMuonNODEBoardCnv.cpp
        src/component/XmlMuonODECnv.cpp
        src/component/XmlMuonReadoutCondCnv.cpp
        src/component/XmlMuonTSMapCnv.cpp
        src/component/XmlMuonTell40BoardCnv.cpp
        src/component/XmlMuonTell40PCICnv.cpp
        src/component/XmlMuonUpgradeStationCablingCnv.cpp
        src/component/XmlMuonUpgradeTSMapCnv.cpp
    LINK
        Gaudi::GaudiKernel
        LHCb::DetDescCnvLib
        LHCb::MuonDetLib
        XercesC::XercesC
  )

  gaudi_add_dictionary(MuonDetDict
    HEADERFILES dict/MuonDetDict.h
    SELECTION dict/MuonDetDict.xml
    LINK LHCb::MuonDetLib
  )

endif()
