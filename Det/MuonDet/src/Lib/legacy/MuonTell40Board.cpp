/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "MuonDet/MuonTell40Board.h"

#include <iostream>

//-----------------------------------------------------------------------------
// Implementation file for class : MuonTell40Board
//
// 2004-01-05 : Alessia Satta
//-----------------------------------------------------------------------------
StatusCode MuonTell40Board::initialize() {
  return Condition::initialize().andThen( [&] {
    const auto tellStation = this->param<int>( "StationNumber" );
    setTell40Station( tellStation );
    const auto TNumb = this->param<int>( "Tell40Number" );
    setTell40Number( TNumb );
    const auto PNumb = this->param<int>( "PCINumber" );
    setNumberOfPCI( PNumb );

    const auto tellPCIName = this->paramVect<std::string>( "TellPCIName" );
    m_PCIName.clear();

    m_numberOfPCI = 0;

    for ( auto tname = tellPCIName.begin(); tname < tellPCIName.end(); tname++ ) {
      addPCI( *tname );
      m_numberOfPCI++;
    }
    return StatusCode::SUCCESS;
  } );
}

void MuonTell40Board::addPCI( std::string name ) { m_PCIName.push_back( name ); }
