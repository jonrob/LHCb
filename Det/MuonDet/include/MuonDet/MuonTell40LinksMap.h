/*****************************************************************************\
* (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "Kernel/STLExtensions.h"
#include "MuonDet/DeMuonDetector.h"

#include "GaudiKernel/Point3DTypes.h"

#ifdef USE_DD4HEP

#  include "Detector/LHCb/DeLHCb.h"

#else

#  include "DetDesc/DetectorElement.h"

#endif

#include "boost/container/static_vector.hpp"

#include "MuonDet/MuonDAQConstants.h"

#include <array>
#include <iostream>
#include <vector>

#pragma once

#ifdef USE_DD4HEP

#  include "Detector/Muon/Tell40PCIBoard.h"
using MuonTell40PCIBoard = LHCb::Detector::Muon::detail::Tell40PCIBoard;

#else
#  include "MuonDet/MuonTell40PCI.h"
using MuonTell40PCIBoard = MuonTell40PCI;
#endif

namespace LHCb::MuonUpgrade {

  struct connectionMap {
    unsigned int                                              connectedLinks = 0;
    std::array<unsigned int, MuonUpgradeDAQHelper_linkNumber> map            = {0};
    connectionMap(){};
    connectionMap( unsigned int q, std::array<unsigned int, MuonUpgradeDAQHelper_linkNumber> m )
        : connectedLinks{q}, map{m} {};
  };

  class Tell40LinksMap {
  public:
    struct LinkPair {
      unsigned int corresponding = 0;
      bool         connected     = false;
      LinkPair() {}
      LinkPair( unsigned int corr, bool conn ) : corresponding{corr}, connected{conn} {}
    };

  public:
    enum InitStatus { UNINITIALIZED, OK, FAKE };

    Tell40LinksMap() = default; // needed by DD4hep even if unused !
    Tell40LinksMap( const DeMuonDetector& det, const LHCb::Detector::DeLHCb& lhcbdet );

    InitStatus getInitStatus( unsigned int TNumber, unsigned int PCINumber ) const {

      // std::cout<<" entra "<<std::endl;
      return m_initializedCondition[( TNumber - 1 ) * MuonUpgradeDAQHelper_maxTell40PCINumber + PCINumber];
    };

    connectionMap getAllConnection( unsigned int TNumber, unsigned int PCINumber ) const;

    unsigned int getEnabled( unsigned int, unsigned int ) const;

    std::optional<unsigned int> linkNumberWithNoHole( unsigned int TNumber, unsigned int PCINumber,
                                                      unsigned int link ) const;

  private:
    void fillDefaultMaps( const MuonTell40PCIBoard& pci );
    std::array<unsigned int, MuonUpgradeDAQHelper_maxTell40Number* MuonUpgradeDAQHelper_maxTell40PCINumber>
        m_mapEnabledWithHole = {};
    std::array<std::array<LinkPair, MuonUpgradeDAQHelper_linkNumber>,
               MuonUpgradeDAQHelper_maxTell40Number * MuonUpgradeDAQHelper_maxTell40PCINumber>
        m_internalMapNoHole;
    std::array<InitStatus, MuonUpgradeDAQHelper_maxTell40Number* MuonUpgradeDAQHelper_maxTell40PCINumber>
        m_initializedCondition = {InitStatus::UNINITIALIZED};
  };

} // namespace LHCb::MuonUpgrade
