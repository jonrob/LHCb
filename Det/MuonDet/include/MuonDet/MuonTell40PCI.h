/*****************************************************************************\
* (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "DetDesc/Condition.h"
#include "GaudiKernel/DataObject.h"
#include "MuonDet/CLID_MuonTell40PCI.h"
#include "MuonDet/MuonDAQConstants.h"

#include <vector>

/**
 *  @author Alessia Satta
 *  @date   2004-01-05
 */
class MuonTell40PCI : public Condition {
public:
  StatusCode initialize() override;
  /// Class ID of this class
  static const CLID& classID() { return CLID_MuonTell40PCI; };
  long               Tell40Number() const { return m_Tell40Number; };
  void               setTell40Number( long number ) { m_Tell40Number = number; };
  long               Tell40PCINumber() const { return m_PCINumber; };

  // long               getmydata(){return 1;);

  void        setTell40PCINumber( long number ) { m_PCINumber = number; };
  void        setStation( long number ) { m_station = number; };
  int         getStation() const { return m_station; };
  void        setActiveLinkNumber( long number ) { m_numberOfActiveLink = number; };
  int         activeLinkNumber() const { return m_numberOfActiveLink; };
  void        setSourceID( long number ) { m_sourceID = number; };
  long        getSourceID() const { return m_sourceID; };
  std::string getODEName( int i ) const { return m_ODEName[i]; };
  long        getODENumber( int i ) const { return m_ODENumberList[i]; };
  long        getLinkNumber( int i ) const { return m_linkConnection[i]; };

  void addLayout( long region, long X, long Y );
  long getTSLayoutX( long i ) { return m_regionLayout[i][0]; };
  long getTSLayoutY( long i ) { return m_regionLayout[i][1]; };
  // long        getODEPosition( long ode, long link, bool hole = true );
  long         getODEAndLinkPosition( long ode, long link, bool hole = true );
  void         addLinkAndODE( long index, long ode, long link );
  unsigned int connectedLinks() const { return ( m_declaredLink & 0xFFFFFF ); };

private:
  long                               m_station            = 0;
  long                               m_PCINumber          = 0;
  long                               m_Tell40Number       = 0;
  long                               m_sourceID           = 0;
  std::string                        m_ODEName[24]        = {};
  std::array<std::array<long, 2>, 4> m_regionLayout       = {{}};
  std::array<long, 24>               m_ODENumberList      = {};
  std::array<long, 24>               m_linkConnection     = {};
  long                               m_numberOfActiveLink = 0;
  unsigned int                       m_declaredLink       = 0;

  // long        alessia=1;
};
