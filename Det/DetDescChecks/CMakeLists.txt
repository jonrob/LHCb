###############################################################################
# (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#[=======================================================================[.rst:
Det/DetDescChecks
-----------------
#]=======================================================================]
gaudi_add_module(DetDescChecks
    SOURCES
        src/CheckGeometryOverlaps.cpp
        src/MaterialBudgetAlg.cpp
        src/VolumeCheckAlg.cpp
    LINK
        Boost::headers
        Gaudi::GaudiKernel
        LHCb::LHCbAlgsLib
        LHCb::DetDescLib
        LHCb::LHCbAlgsLib
)

gaudi_install(PYTHON)

if(NOT USE_DD4HEP)
    # these tests make sense only when using DetDesc
    gaudi_add_tests(QMTest)
endif()
