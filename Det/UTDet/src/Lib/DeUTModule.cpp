/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "UTDet/DeUTModule.h"
#include "DetDesc/IGeometryInfo.h"
#include "Kernel/UTNames.h"
#include "UTDet/DeUTFace.h"
#include "UTDet/DeUTSector.h"
#include "UTDet/DeUTStave.h"
#include <algorithm>
#include <numeric>

using namespace LHCb;

/** @file DeUTModule.cpp
 *
 *  Implementation of class :  DeUTModule
 *
 *  @author Xuhao Yuan
 *  @date   2021-03-29
 *
 */

const CLID& DeUTModule::clID() const { return DeUTModule::classID(); }

StatusCode DeUTModule::initialize() {
  // initialize method
  StatusCode sc = DeUTBaseElement::initialize();

  if ( sc.isFailure() ) {
    MsgStream msg( msgSvc(), name() );
    msg << MSG::ERROR << "Failed to initialize detector element" << endmsg;
  } else {
    if ( !exists( "moduleID" ) ) throw std::runtime_error{"No moduleID found. Should check the DDDB."};
    m_module                               = param<int>( "moduleID" );
    m_version                              = GeoVersion::v1;
    m_parent                               = getParent<DeUTFace>();
    m_type                                 = m_parent->stavetype();
    const Detector::UT::ChannelID parentID = m_parent->elementID();
    m_staveRotZ                            = m_parent->staveRotZ();
    Detector::UT::ChannelID chan( Detector::UT::ChannelID::detType::typeUT, parentID.side(), parentID.layer(),
                                  parentID.stave(), parentID.face(), m_module, 0, 0 );
    setElementID( chan );
    m_sectors = getChildren<DeUTSector>();
  }

  return sc;
}

std::ostream& DeUTModule::printOut( std::ostream& os ) const {
  return os << " Stave : " /* << name() << " type " << m_type << " Det region " << m_detRegion << " Column " << m_column
                              << " side " << m_sideID << " staveID " << m_staveID*/
            << std::endl;
}

MsgStream& DeUTModule::printOut( MsgStream& os ) const {
  return os << " Stave : " /* << name() << " type " << m_type << " Det region " << m_detRegion << " Column " << m_column
                              << " side " << m_sideID << " staveID " << m_staveID*/
            << std::endl;
}

const DeUTSector* DeUTModule::findSector( const Detector::UT::ChannelID aChannel ) const {
  auto iter = std::find_if( m_sectors.begin(), m_sectors.end(),
                            [&]( const DeUTSector* s ) { return s->contains( aChannel ); } );

  return iter != m_sectors.end() ? *iter : nullptr;
}

const DeUTSector* DeUTModule::findSector( const Gaudi::XYZPoint& point ) const {
  auto iter =
      std::find_if( m_sectors.begin(), m_sectors.end(), [&]( const DeUTSector* s ) { return s->isInside( point ); } );
  return iter != m_sectors.end() ? *iter : nullptr;
}

double DeUTModule::fractionActive() const {
  return std::accumulate( m_sectors.begin(), m_sectors.end(), 0.0,
                          []( double f, const DeUTSector* s ) { return f + s->fractionActive(); } ) /
         double( m_sectors.size() );
}
