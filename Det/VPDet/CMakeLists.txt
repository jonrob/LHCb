###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#[=======================================================================[.rst:
Det/VPDet
---------
#]=======================================================================]

if(USE_DD4HEP)
  set(BUILD_TESTING false) # prevent headers build test

  gaudi_add_header_only_library(VPDetLib
    LINK
        Gaudi::GaudiKernel
        Detector::DetectorLib
        LHCb::DetDescLib
      )
else()
  gaudi_add_library(VPDetLib
      SOURCES
          src/Lib/legacy/DeVP.cpp
          src/Lib/legacy/DeVPSensor.cpp
          src/Lib/VeloAlignCond.cpp
      LINK
          PUBLIC
              Gaudi::GaudiKernel
              LHCb::DetDescLib
              LHCb::LHCbKernel
            )

gaudi_add_module(VPDet
    SOURCES 
        src/component/legacy/VeloMotionSystemFromYaml.cpp
        src/component/legacy/XmlDeVPCnv.cpp
        src/component/legacy/XmlDeVPSensorCnv.cpp
        src/component/XmlVeloAlignCondCnv.cpp
    LINK
        LHCb::DetDescCnvLib
        LHCb::VPDetLib
)

target_link_libraries(VPDet
  PRIVATE
      fmt::fmt
      LHCb::DAQEventLib
      LHCb::LHCbAlgsLib
      LHCb::XmlToolsLib
      yaml-cpp
    )

gaudi_add_dictionary(VPDetDict
    HEADERFILES dict/VPDetDict.h
    SELECTION dict/VPDetDict.xml
    LINK LHCb::VPDetLib
)

if(BUILD_TESTING)
    # Prepare test Git CondDB overlay
    file(REMOVE_RECURSE ${CMAKE_CURRENT_BINARY_DIR}/test/DB)
    file(COPY tests/data/DB/simple/ DESTINATION test/DB/simple/)
    file(COPY tests/data/DB/simple/ DESTINATION test/DB/updates/)
    file(REMOVE
    ${CMAKE_CURRENT_BINARY_DIR}/test/DB/updates/Conditions/Velo/Alignment/Global.xml
    ${CMAKE_CURRENT_BINARY_DIR}/test/DB/updates/Conditions/online.xml)
    file(COPY tests/data/DB/updates/ DESTINATION test/DB/updates/)
    execute_process(COMMAND git init -q ${CMAKE_CURRENT_BINARY_DIR}/test/DB/simple)
    execute_process(COMMAND git init -q ${CMAKE_CURRENT_BINARY_DIR}/test/DB/updates)

    gaudi_add_tests(QMTest)

    set_property(
        TEST
            VPDet.veloaligncond.basic
            VPDet.veloaligncond.override1
            VPDet.veloaligncond.override2
            VPDet.veloaligncond.override3
            VPDet.veloaligncond.updates
            VPDet.veloaligncond.updates_yaml
        APPEND PROPERTY 
            ENVIRONMENT TEST_OVERLAY_ROOT=${CMAKE_CURRENT_BINARY_DIR}/test/DB
    )
    set_property(
        TEST
            VPDet.veloaligncond.updates_yaml
        APPEND PROPERTY
            ENVIRONMENT TEST_YAML_ROOT=${CMAKE_CURRENT_SOURCE_DIR}/tests/data/DB/updates/yaml
    )
endif()
endif()
