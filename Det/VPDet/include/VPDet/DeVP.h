/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "GaudiKernel/GaudiException.h"
#include "VPDet/VPDetPaths.h"

#include <string>

namespace DeVPLocation {
  inline const std::string Default = LHCb::Det::VP::det_path;
}

#ifdef USE_DD4HEP

#  include "Detector/VP/DeVP.h"
using DeVP       = LHCb::Detector::DeVP;
using DeVPSensor = LHCb::Detector::DeVPSensor;

#else

#  include "DetDesc/Condition.h"
#  include "GaudiKernel/SmartDataPtr.h"

// Local
#  include "VPDet/DeVPSensor.h"

/** @class DeVP DeVP.h VPDet/DeVP.h
 *
 *  Detector element class for the VP as a whole.
 *
 *  @author Victor Coco Victor.Coco@cern.ch
 *  @date 20/5/2009
 */

constexpr CLID CLID_DeVP = 8200;

class DeVP : public DetDesc::DetectorElementPlus {

public:
  /// Constructor
  using DetectorElementPlus::DetectorElementPlus;

  /// Object identification
  static const CLID&  classID() { return CLID_DeVP; }
  virtual const CLID& clID() const override;

  /// Initialization method
  StatusCode initialize() override;

  /// Return sensitive volume identifier for a given point in the global frame.
  int sensitiveVolumeID( const Gaudi::XYZPoint& point ) const override;

  /// Return the number of sensors.
  unsigned int numberSensors() const { return m_sensors.size(); }

  template <class Operation>
  /// runs the given callable on every sensor
  void runOnAllSensors( Operation op ) const {
    for ( auto& sensor : m_sensors ) op( *sensor );
  }

  /// Return reference to sensor for a given point in the global frame.
  const DeVPSensor& sensor( const Gaudi::XYZPoint& point ) const {
    const int sensorNumber = sensitiveVolumeID( point );
    if ( sensorNumber >= 0 ) return *m_sensors[sensorNumber];
    throw GaudiException( "Invalid xyz coordinate asked for sensor of in DeVP", "DeVP", StatusCode::FAILURE );
  }
  /// Return reference to sensor for a given channel ID.
  const DeVPSensor& sensor( LHCb::Detector::VPChannelID channel ) const { return sensor( channel.sensor() ); }
  /// Return reference to sensor for a given sensor number.
  const DeVPSensor& sensor( LHCb::Detector::VPChannelID::SensorID sensorNumber ) const {
    return *m_sensors[to_unsigned( sensorNumber )];
  }
  const DetectorElementPlus* module( LHCb::Detector::VPChannelID::SensorID sensorNumber ) const {
    return dynamic_cast<DetectorElementPlus*>(
        sensor( sensorNumber ).parentIDetectorElementPlus()->parentIDetectorElementPlus() );
  }

  /// returns the position of the beamSpot
  auto beamSpot() const { return m_beamSpot; }

  /// says whether the velo is closed
  auto veloClosed() const { return m_isVeloClosed; }

private:
  /// local to global matrix cache
  std::array<std::array<float, 12>, VP::NSensors> m_ltg{{}};

  /// cache of x coordinates to sensors
  std::array<float, VP::NSensorColumns> m_local_x{};

  /// cache of the x_pitch of sensors
  std::array<float, VP::NSensorColumns> m_x_pitch{};

  /// Cosine squared of rotation angle for each sensor.
  std::array<double, VP::NSensors> m_c2;

  /// Sine squared of rotation angle for each sensor.
  std::array<double, VP::NSensors> m_s2;

  /// cache of the pixel size of sensors
  float m_pixel_size = 0;

  // beamspot
  Condition const* m_beamSpotCond{nullptr};
  Gaudi::XYZPoint  m_beamSpot{0., 0., 0.};
  // whether the velo is closed
  bool m_isVeloClosed{false};

public:
  /// Return local x for a given sensor.
  float local_x( unsigned int n ) const { return m_local_x[n]; }
  /// Return pitch for a given sensor.
  float x_pitch( unsigned int n ) const { return m_x_pitch[n]; }
  /// Return the pixel size.
  float pixel_size() const { return m_pixel_size; }
  /// Return local to global matrix for a sensor
  const std::array<float, 12>& ltg( LHCb::Detector::VPChannelID::SensorID n ) const { return m_ltg[to_unsigned( n )]; }
  /// Return Cosine squared of rotation angle for a sensor
  double cos2OfSensor( LHCb::Detector::VPChannelID::SensorID n ) const { return m_c2[to_unsigned( n )]; }
  /// Return Sine squared of rotation angle for a sensor
  double sin2OfSensor( LHCb::Detector::VPChannelID::SensorID n ) const { return m_s2[to_unsigned( n )]; }

private:
  /// Find sensors inside detector element tree.
  void findSensors( IDetectorElementPlus* det, std::vector<DeVPSensor*>& sensors );

  /// List of pointers to all sensors.
  std::vector<DeVPSensor*> m_sensors;

  /// Custom operator for sorting sensors by sensor number.
  struct less_SensorNumber {
    bool operator()( DeVPSensor* const& x, DeVPSensor* const& y ) { return ( x->sensorNumber() < y->sensorNumber() ); }
  };

  /// Output level flag
  bool m_debug = false;

  /// Message stream
  mutable std::unique_ptr<MsgStream> m_msg;
  /// On-demand access to message stream.
  MsgStream& msg() const {
    if ( !m_msg ) m_msg.reset( new MsgStream( msgSvc(), "DeVP" ) );
    return *m_msg;
  }

  /// code updateing the cached data when geometry changes
  StatusCode updateCache();

  // update beamspot location
  StatusCode updateBeamSpot();
};

#endif
