###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from PyConf.application import ApplicationOptions
from DDDB.CheckDD4Hep import UseDD4Hep

options = ApplicationOptions(_enabled=False)
if UseDD4Hep:
    options.geometry_version = "run3/trunk"
    # A commit on master from before the addition of the
    # InteractionRegion condition
    options.conditions_version = "74dfb641cf4116acb2b7b327b3445215506d4c4a"

    # In the tag the condition is valid for run numbers 200-400
    from Configurables import LHCb__Tests__FakeRunNumberProducer as DummyRunNumber
    DummyRunNumber("DummyRunNumber").Start = 400
else:
    options.dddb_tag = "upgrade/dddb-20230313"
    options.conddb_tag = "upgrade/sim-20230626-vc-md100"
