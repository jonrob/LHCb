/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "CaloDet/DeCalorimeter.h"
#include "DetDesc/GenericConditionAccessorHolder.h"

#include "LHCbAlgs/Consumer.h"

#include "GaudiAlg/GaudiTupleAlg.h"
#include "GaudiKernel/IRndmGenSvc.h"
#include "GaudiKernel/RndmGenerators.h"

#include <atomic>
#include <map>
#include <string>
#include <vector>

namespace LHCb {

  /**
   *
   * Simple algorithm to overwrite the condDB calibration constant
   * with arbitrary value (user-defined or gaussian or flat randomly distributed)
   * at the DeCalorimeter initialisation level.
   *
   * Useful to produce mis-calibrated energy and check/develop calibration procedure
   * or to check calibration constant without feeding the condDB
   *
   * 2007-08-22 : Olivier DESCHAMPS
   */
  class DeCaloCalib : public Algorithm::Consumer<void( const DeCalorimeter& ),
                                                 DetDesc::usesBaseAndConditions<GaudiTupleAlg, DeCalorimeter>> {
  public:
    DeCaloCalib( const std::string& name, ISvcLocator* pSvcLocator )
        : Consumer( name, pSvcLocator, {KeyValue{"DetectorName", ""}} ) {}
    StatusCode initialize() override;
    void       operator()( const DeCalorimeter& ) const override;

  private:
    double delta( long id ) const {
      auto delta = m_deltas.find( std::to_string( id ) );
      return delta != m_deltas.end() ? delta->second : m_deltas.value().at( "Default" );
    }
    void update( const DeCalorimeter& ) const;
    bool isDead( int channel ) const;

    // Note that thread safety depends on the thread safety of the underlying random generator
    mutable Rndm::Numbers m_shoot;

    ServiceHandle<IRndmGenSvc> m_rndmSvc{this, "RndmGenSvc", "RndmGenSvc"};

    Gaudi::Property<std::string>                   m_method{this, "Method", "Flat", "Flat/Gauss/User"};
    Gaudi::Property<std::vector<double>>           m_params{this, "Params", {1.0, 1.0}, "gauss/flat  parameters"};
    Gaudi::Property<std::map<std::string, double>> m_deltas{
        this, "deltaGain", {{"Default", 1.0}}, "User defined params mapping  <key : value>"};
    Gaudi::Property<std::string>      m_key{this, "Key", "CellID"};
    Gaudi::Property<bool>             m_update{this, "EventUpdate", false, "default is update in initialize only"};
    Gaudi::Property<bool>             m_ntup{this, "Ntupling", true};
    Gaudi::Property<std::vector<int>> m_dead{this, "DeadChannelList", {}};

    // used to run only one first event
    mutable std::once_flag m_onlyFirstEvent;
  };

} // namespace LHCb

DECLARE_COMPONENT_WITH_ID( LHCb::DeCaloCalib, "PDeCaloCalib" )

StatusCode LHCb::DeCaloCalib::initialize() {
  return GaudiTupleAlg::initialize().andThen( [&]() -> StatusCode {
    info() << " ======= SIMULATING THE (MIS)Calibration of " << inputLocation<DeCalorimeter>()
           << " gains  ======= " << endmsg;
    double a, b;
    if ( m_method == "Gauss" ) {
      // Gaussian random (mean, rms)
      info() << "---- Method : gaussian random timing values " << endmsg;
      if ( m_params.size() != 2 ) error() << "wrong parameters size" << endmsg;
      a = *( m_params.begin() );
      b = *( m_params.begin() + 1 );
      info() << " mean/sigma = " << a << "/" << b << endmsg;
      auto sc = m_shoot.initialize( m_rndmSvc.get(), Rndm::Gauss( a, b ) );
      if ( !sc.isSuccess() ) return sc;
    } else if ( m_method == "Flat" ) {
      // Flat random (min, max)
      info() << "---- Method : flat random timing values " << endmsg;
      if ( m_params.size() != 2 ) error() << "wrong parameters size" << endmsg;
      a = *( m_params.begin() );
      b = *( m_params.begin() + 1 );
      info() << " min/max = " << a << "/" << b << endmsg;
      auto sc = m_shoot.initialize( m_rndmSvc.get(), Rndm::Flat( a, b ) );
      if ( !sc.isSuccess() ) return sc;
    } else if ( m_method == "User" ) {
      info() << "---- Method : user-defined timing values " << endmsg;
      info() << "Timing value have been defined for " << m_deltas.size() << " cells " << endmsg;
      info() << "Default value [" << m_deltas["Default"] << "] will be applied to other cells." << endmsg;
      if ( m_key == "CellID" ) {
        info() << "The calib values are mapped with KEY = CellID " << endmsg;
      } else if ( m_key == "Index" ) {
        info() << "The calib values are are mapped with KEY = Index" << endmsg;
      } else {
        error() << "undefined deltaKey : must be either 'CellID' or 'Index' " << endmsg;
        return StatusCode::FAILURE;
      }
    } else {
      error() << "Method " << m_method << " unknown - should be 'Flat', 'Gauss' or 'User'" << endmsg;
      return StatusCode::FAILURE;
    }
    return StatusCode::SUCCESS;
  } );
}

void LHCb::DeCaloCalib::operator()( const DeCalorimeter& deCalo ) const {
  // update at each event ?
  if ( m_update ) {
    std::call_once( m_onlyFirstEvent, [&]() { update( deCalo ); } );
  }
}

void LHCb::DeCaloCalib::update( const DeCalorimeter& deCalo ) const {
  // update cellParams
  auto&               cells = const_cast<CaloVector<CellParam>&>( deCalo.cellParams() ); // no-const conversion
  std::vector<int>    cellids, cellind;
  std::vector<double> gains, dgains;
  for ( auto& cell : cells ) {
    LHCb::Detector::Calo::CellID id = cell.cellID();
    if ( !deCalo.valid( id ) ) continue;
    if ( deCalo.isPinId( id ) ) continue;

    long   num = deCalo.cellIndex( id );
    double dt;
    long   index = id.index();
    if ( m_key == "Index" ) index = num;

    if ( isDead( index ) ) {
      dt = 0.;
      cell.addQualityFlag( CaloCellQuality::Dead );
    } else if ( m_method == "User" )
      dt = delta( index );
    else
      dt = m_shoot();

    if ( msgLevel( MSG::DEBUG ) ) debug() << num << " Calibration constant for cellID " << id << " : " << dt << endmsg;
    cell.setCalibration( dt ); //
    cellids.push_back( id.index() );
    cellind.push_back( num );
    gains.push_back( cell.gain() );
    dgains.push_back( cell.calibration() );
  }

  if ( !m_ntup ) return;
  // Ntupling
  StatusCode sc;
  Tuple      ntp = nTuple( 500 + LHCb::Detector::Calo::CellCode::CaloNumFromName( inputLocation<DeCalorimeter>() ),
                      inputLocation<DeCalorimeter>() + "DeCalib", CLID_ColumnWiseTuple );
  int        max = deCalo.numberOfCells();
  sc             = ntp->farray( "cellID", cellids, "nchannels", max );
  sc             = ntp->farray( "index", cellind, "nchannels", max );
  sc             = ntp->farray( "gain", gains, "nchannels", max );
  sc             = ntp->farray( "calib", dgains, "nchannels", max );
  sc             = ntp->write();
  if ( sc.isFailure() ) Warning( "cannot write NTP" ).ignore();
}

bool LHCb::DeCaloCalib::isDead( int channel ) const {
  return ( m_key == "Index" )
             ? std::any_of( m_dead.begin(), m_dead.end(), [&]( int i ) { return channel == i; } )
             : ( m_key == "CellID" )
                   ? std::any_of( m_dead.begin(), m_dead.end(), [&]( int i ) { return channel == ( i & 0x3FFF ); } )
                   : false;
}
